package dk.aarhus2017.aarhusevents.util;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;

/**
 * Created by Dell on 12/20/2016.
 */

public class HttpHelper {

    public static AsyncHttpClient GetHttpClient()
    {
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Accept", "application/json");
        client.addHeader("Content-Type", "application/json;charset=utf-8");
        return client;
    }

    public static StringEntity GetHttpParams(Date fromDate, Date toDate, String language, String eventType)
    {
        StringEntity entity = null;
        SimpleDateFormat simpleDateFormat =new SimpleDateFormat("yyyy-MM-dd");
        JSONArray priceJson = new JSONArray();
        priceJson.put("DKK 0");
        priceJson.put("DKK 99999");
        JSONObject data = new JSONObject();
        try {
            data.put("Lan", language);
            data.put("FromDate", fromDate == null ? null : simpleDateFormat.format(fromDate));
            data.put("ToDate", toDate == null ? null : simpleDateFormat.format(toDate));
            data.put("Price", priceJson);
            data.put("SortOrder", "date");
            data.put("EventParentNodename", "events");;
            data.put("EventDisplayType",eventType);

            entity = new StringEntity(data.toString());
            entity.setContentType(new BasicHeader("Content-Type","application/json;charset=utf-8"));

        } catch (Exception ex) {
            Log.e("GetHttpParams Error", ex.getMessage());
        }

       return entity;
    }
    public static StringEntity GetFullDataHttpParams(Date fromDate, Date toDate, String language, boolean isUpdated)
    {
        StringEntity entity = null;
        SimpleDateFormat simpleDateFormat =new SimpleDateFormat("yyyy-MM-dd");
        JSONArray priceJson = new JSONArray();
        priceJson.put("DKK 0");
        priceJson.put("DKK 99999");
        JSONObject data = new JSONObject();
        try {
            data.put("Lan", language);
            data.put("FromDate", fromDate == null ? simpleDateFormat.format(new Date()) : simpleDateFormat.format(fromDate));
            data.put("ToDate", toDate == null ? "2017-12-31" : simpleDateFormat.format(toDate));
            data.put("Price", priceJson);
            data.put("SortOrder", "date");
            data.put("EventParentNodename", "events");
            data.put("EventDisplayType","All");
            if(isUpdated)
            {
                Date lastUpdated =new Date();
                //lastUpdated.setTime(lastUpdated.getTime() - TimeZone.getDefault().getRawOffset());
                data.put("LastUpdated",GlobalConfig.DateFullFormat.format(lastUpdated));
                //data.put("LastUpdated","2017-01-01T00:00:00");
            }

            entity = new StringEntity(data.toString());
            entity.setContentType(new BasicHeader("Content-Type","application/json;charset=utf-8"));

        } catch (Exception ex) {
            Log.e("GetHttpParams Error", ex.getMessage());
        }

        return entity;
    }



}
