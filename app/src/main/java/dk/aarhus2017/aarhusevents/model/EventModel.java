package dk.aarhus2017.aarhusevents.model;

import android.graphics.Region;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;

import dk.aarhus2017.aarhusevents.util.ConvertHelper;
import dk.aarhus2017.aarhusevents.util.GeneralHelper;
import dk.aarhus2017.aarhusevents.util.GlobalConfig;

/**
 * Created by Dell on 11/12/2016.
 */

public class EventModel {
    private int eventId;
    private String eventHeadline;
    private String dates;
    private String startDate;
    private String endDate;
    private String coordinates;
    private String themesList;
    private ArrayList<String> artFormList;
    private String locationList;
    private ArrayList<String> audienceList;
    private String price;
    private String imageUrl;
    private String eventUrl;
    private Boolean languageDisabled;
    private String descriptionTitle;
    private String description;
    private String lastUpdated;
    private Boolean externallyCreated;

    private ArrayList<ScheduledEventModel> scheduledEvents;
    public PracticalInfoModel practicalInfo;

    private String ticketUrl;
    private String languageCulture;
    private String languageTwoLetterCode;
    private String videoUrl;

    private String descriptionHtml;
    private String eventVenueUrl;

    private ArrayList<OpeningHourModel> openingHourModels;
    private String openingHoursAlternativeText;


    public EventModel(int _eventId, String _eventHeadline, String _dates, String _coordinates, String _themesList, ArrayList<String> _artFormList, String _locationList, ArrayList<String> _audienceList, String _price,
                      String _imageUrl, String _eventUrl, boolean _languageDisabled, String _descriptionTitle, String _description, String _ticketUrl, String _languageCulture, String _languageTwoLetterCode,
                      String _videoUrl, ArrayList<ScheduledEventModel> _scheduledEventModel, PracticalInfoModel _practicalInfo) {
        this.eventId = _eventId;
        this.eventHeadline = _eventHeadline;
        this.dates = _dates;
        this.coordinates = _coordinates;
        this.themesList = _themesList;
        this.artFormList = _artFormList;
        this.locationList = _locationList;
        this.audienceList = _audienceList;
        this.price = _price;
        this.imageUrl = _imageUrl;
        this.eventUrl = _eventUrl;
        this.languageDisabled = _languageDisabled;
        this.descriptionTitle = _descriptionTitle;
        this.description = _description;

        this.ticketUrl = _ticketUrl;
        this.languageCulture = _languageCulture;
        this.languageTwoLetterCode = _languageTwoLetterCode;
        this.videoUrl = _videoUrl;

        this.scheduledEvents = _scheduledEventModel;
        this.practicalInfo = _practicalInfo;

    }

    public EventModel(int _eventId, String _eventHeadline, ArrayList<String> _artFormList, String _locationList, ArrayList<String> _audienceList,
                      String _imageUrl, String _eventUrl, boolean _languageDisabled, String _descriptionTitle, String _description, String _descriptionHtml, String _ticketUrl, String _languageCulture, String _languageTwoLetterCode,
                      String _videoUrl, String _lastUpdated, Boolean _externallyCreated, ArrayList<ScheduledEventModel> _scheduledEventModel, PracticalInfoModel _practicalInfo) {
        this.eventId = _eventId;
        this.eventHeadline = _eventHeadline;
        this.artFormList = _artFormList;
        this.locationList = _locationList;
        this.audienceList = _audienceList;

        this.imageUrl = _imageUrl;
        this.eventUrl = _eventUrl;
        this.languageDisabled = _languageDisabled;
        this.descriptionTitle = _descriptionTitle;
        this.description = _description;
        this.descriptionHtml = _descriptionHtml;
        this.ticketUrl = _ticketUrl;
        this.languageCulture = _languageCulture;
        this.languageTwoLetterCode = _languageTwoLetterCode;
        this.videoUrl = _videoUrl;
        this.lastUpdated = _lastUpdated;
        this.externallyCreated = _externallyCreated;
        this.scheduledEvents = _scheduledEventModel;
        this.practicalInfo = _practicalInfo;
    }


    public void setDescriptionHtml(String _descriptionHtml) {
        this.descriptionHtml = _descriptionHtml;
    }

    public void setLastUpdated(String _lastUpdated) {
        this.lastUpdated = _lastUpdated;
    }

    public void setExternallyCreated(Boolean _externallyCreated) {
        this.externallyCreated = _externallyCreated;
    }

    public void setScheduledEvents(ArrayList<ScheduledEventModel> _scheduledEventModes) {
        this.scheduledEvents = _scheduledEventModes;
    }

    public void setStartDate(String _startDate) {
        this.startDate = _startDate;
    }

    public void setEndDate(String _endDate) {
        this.endDate = _endDate;
    }

    public void setEventVenueUrl(String _eventVenueUrl) {
        this.eventVenueUrl = _eventVenueUrl;
    }

    public void setOpeningHourModels(ArrayList<OpeningHourModel> _openingHourModels) {
        this.openingHourModels = _openingHourModels;
    }


    public int getEventId() {
        return eventId;
    }

    public String getEventHeadline() {
        return eventHeadline;
    }

    public String getDates() {
        return dates;
    }

    public long getStartDate() {
        long time = 0;
        if (!this.startDate.isEmpty()) {
            time = GeneralHelper.GetDateTimeSpan(this.startDate);
        }

        return time;
    }

    public long getEndDate() {
        long time = 0;
        if (!this.endDate.isEmpty()) {
            time = GeneralHelper.GetDateTimeSpan(this.endDate);
        }
        return time;
    }


    public String getCoordinates() {
        return coordinates;
    }

    public String getThemesList() {
        return themesList;
    }

    public ArrayList<String> getArtFormList() {
        return artFormList;
    }

    public String getLocationList() {
        return locationList;
    }

    public ArrayList<String> getAudienceList() {
        return audienceList;
    }

    public String getPrice() {
        return price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getEventUrl() {
        if (!this.eventUrl.isEmpty()) {
            if (!this.eventUrl.contains("http")) {
                this.eventUrl = "http://" + this.eventUrl;
            }
        }
        return this.eventUrl;
    }

    public boolean getLanguageDisabled() {
        return languageDisabled;
    }

    public String getDescriptionHeadline() {
        return descriptionTitle;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionHtml() {
        return this.descriptionHtml;
    }

    public String getTicketUrl() {
        return ticketUrl;
    }

    public String getLanguageCulture() {
        return languageCulture;
    }

    public String getLanguageTwoLetterCode() {
        return languageTwoLetterCode;
    }

    public String getVideoUrl() {
        if (!this.videoUrl.isEmpty() && !this.videoUrl.equals("null")) {
            if (!this.videoUrl.contains("http")) {
                return "http://" + this.videoUrl;
            }
            return this.videoUrl;
        } else {
            return "";
        }
    }


    public ArrayList<ScheduledEventModel> getScheduledEvents() {
        return this.scheduledEvents;
    }

    public PracticalInfoModel getPracticalInfo() {
        return this.practicalInfo;
    }

    public String getLastUpdated() {
        return this.lastUpdated;
    }

    public Boolean getExternallyCreated() {
        return this.externallyCreated;
    }

    public String getEventVenueUrl() {
        return this.eventVenueUrl;
    }


    public ArrayList<OpeningHourModel> getOpeningHourModels() {
        return this.openingHourModels;
    }

    public String getOpeningHoursAlternativeText() {
        return this.openingHoursAlternativeText;
    }

    public void setOpeningHoursAlternativeText(String openingHoursAlternativeText) {
        this.openingHoursAlternativeText = openingHoursAlternativeText;
    }


}


