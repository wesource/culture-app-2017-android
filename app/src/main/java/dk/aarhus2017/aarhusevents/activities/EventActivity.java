package dk.aarhus2017.aarhusevents.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;

import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.adapter.EventDataAdapter;
import dk.aarhus2017.aarhusevents.adapter.MenuAdapter;
import dk.aarhus2017.aarhusevents.model.CustomLinearLayoutManager;
import dk.aarhus2017.aarhusevents.model.EndlessRecyclerOnScrollListener;
import dk.aarhus2017.aarhusevents.model.EventData;
import dk.aarhus2017.aarhusevents.model.EventDataBinding;
import dk.aarhus2017.aarhusevents.model.EventModel;
import dk.aarhus2017.aarhusevents.model.GeoLocationModel;
import dk.aarhus2017.aarhusevents.model.MenuModel;
import dk.aarhus2017.aarhusevents.model.My2017EventType;
import dk.aarhus2017.aarhusevents.model.PermissionResult;
import dk.aarhus2017.aarhusevents.model.PromptPermissionResult;
import dk.aarhus2017.aarhusevents.notification.ConnectivityReceiver;
import dk.aarhus2017.aarhusevents.notification.DeletedEventBroadcastReceiver;
import dk.aarhus2017.aarhusevents.util.ConvertHelper;
import dk.aarhus2017.aarhusevents.util.EventHelper;
import dk.aarhus2017.aarhusevents.util.FontHelper;
import dk.aarhus2017.aarhusevents.util.GATrackerHelper;
import dk.aarhus2017.aarhusevents.util.GeneralHelper;
import dk.aarhus2017.aarhusevents.util.GlobalConfig;
import dk.aarhus2017.aarhusevents.util.GlobalConstant;
import dk.aarhus2017.aarhusevents.util.HttpHelper;
import dk.aarhus2017.aarhusevents.util.InternetHelper;
import dk.aarhus2017.aarhusevents.util.LocalDataHelper;
import dk.aarhus2017.aarhusevents.util.LocationHelper;
import dk.aarhus2017.aarhusevents.util.ParserHelper;
import dk.aarhus2017.aarhusevents.util.PermissionHelper;
import dk.aarhus2017.aarhusevents.util.ServicesHelper;
import dk.aarhus2017.aarhusevents.util.SharedPreferenceHelper;

public class EventActivity extends AppCompatActivity implements View.OnClickListener {


    EventActivity self = this;

    // Welcome
    TextView eventWelcomeHeadlineTextView;
    TextView eventWelcomeSubheadlineTextView;
    ImageView eventWelcomeTextBackgroundImageView;
    LinearLayout eventWelcomeTextHolderLinearLayout;
    ImageView eventWelcomeScreenImageView;

    // Header
    TextView eventHeaderTotalLabelTextView;
    TextView eventHeaderTotalTextView;
    TextView eventHeaderSelectedMenuItemTextView;
    Button eventHeaderFilterButton;
    ImageButton eventHeaderMenuIconImageButton;


    LinearLayout headerBarLinearLayout;
    LinearLayout eventInternetHolderLinearLayout;
    TextView eventNoInternetTextView;

    FrameLayout welcomeHolderFrameLayout;
    LinearLayout eventHolderLinearLayout;

    RelativeLayout loadingHolderRelativeLayout;

    // My event 2017
    LinearLayout my2017SynchronizeHolderLinearLayout;
    TextView my2017SynchronizeTextView;
    RelativeLayout my2017HolderRelativeLayout;
    LinearLayout myEventEmptyTextHolderLinearLayout;
    Button eventShowMeEventButton;
    ImageView myEventEmpty2017ImageView;
    TextView my2017EmptyHeadlineTextView;
    TextView my2017EmptyDescriptionTextView;

    // No Result Event
    RelativeLayout eventNoResultHolderRelativeLayout;
    ImageView eventNoResultImageView;
    LinearLayout eventNoResultTextHolderLinearLayout;
    Button eventUseFilterButton;
    TextView eventNoResultHeadlineTextView;
    TextView eventNoResultDescriptionTextView;

    RecyclerView eventEventListRecyclerView;
    Paint paint = new Paint();


    //Menu footer
    LinearLayout eventIconHolderLinearLayout;
    ImageButton eventIconImageButton;
    TextView eventIconTextTextView;

    LinearLayout my2017IconHolderLinearLayout;
    ImageButton my2017IconImageButton;
    TextView my2017IconTextTextView;
    ImageView logoImageView;


    Location myLocation;
    ArrayList<EventModel> eventModels = new ArrayList<EventModel>();
    ArrayList<EventModel> my2017Models = new ArrayList<>();
    Map<String, ArrayList<EventData>> sharedData = new TreeMap<>();
    //ArrayList<EventDataBinding> sharedEventDataBindings=new ArrayList<>();

    EventDataAdapter eventDataAdapter;
    EventDataAdapter.EventListener eventListener;
    //Refresh
    SwipeRefreshLayout eventSwipeRefreshLayout;
    LinearLayoutManager linearLayoutManager;
    EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;
    Date loadMoreDate;
    Boolean isLoadingMoreData=false;

    ArrayList<EventDataBinding> eventDataBindings = new ArrayList<>();
    ArrayList<EventDataBinding> my2017Events = new ArrayList<>();
    // Filter
    Date fromDate = new Date();
    Date toDate = new Date();
    Set<String> artformsFilter;
    Set<String> audienceFilter;
    int distanceFilter;
    String cityFilter;
    GeoLocationModel locationFilter;
    String language = "da";
    String eventType = "All";
    Boolean isCurrentMy2017 = false;
    EventDataBinding currentEventDataRow;
    ArrayList<MenuModel> menuItems = new ArrayList<>();
    private ListPopupWindow listPopupWindow;
    private ConnectivityReceiver connectivityReceiver;
    private int selectedEventType = 0;

    private DeletedEventBroadcastReceiver deletedEventBroadcastReceiver = new DeletedEventBroadcastReceiver();

    private GlobalConfig.MainActivityStatus actionStatus = GlobalConfig.MainActivityStatus.Welcome;


    private int index = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);


        eventWelcomeHeadlineTextView = (TextView) findViewById(R.id.eventWelcomeHeadlineTextView);
        eventWelcomeSubheadlineTextView = (TextView) findViewById(R.id.eventWelcomeSubheadlineTextView);
        eventWelcomeTextHolderLinearLayout = (LinearLayout) findViewById(R.id.eventWelcomeTextHolderLinearLayout);
        eventWelcomeTextBackgroundImageView = (ImageView) findViewById(R.id.eventWelcomeTextBackgroundImageView);
        eventWelcomeScreenImageView = (ImageView) findViewById(R.id.eventWelcomeScreenImageView);


        // Header
        eventHeaderTotalLabelTextView = (TextView) findViewById(R.id.eventHeaderTotalLabelTextView);
        eventHeaderTotalTextView = (TextView) findViewById(R.id.eventHeaderTotalTextView);
        eventHeaderSelectedMenuItemTextView = (TextView) findViewById(R.id.eventHeaderSelectedMenuItemTextView);
        eventHeaderFilterButton = (Button) findViewById(R.id.eventHeaderFilterButton);
        eventHeaderMenuIconImageButton = (ImageButton) findViewById(R.id.eventHeaderMenuIconImageButton);
        eventNoInternetTextView = (TextView) findViewById(R.id.eventNoInternetTextView);


        eventSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.eventSwipeRefreshLayout);
        linearLayoutManager=new LinearLayoutManager(this);


        // Header bar
        headerBarLinearLayout = (LinearLayout) findViewById(R.id.headerBarLinearLayout);
        eventInternetHolderLinearLayout = (LinearLayout) findViewById(R.id.eventInternetHolderLinearLayout);

        welcomeHolderFrameLayout = (FrameLayout) findViewById(R.id.welcomeHolderFrameLayout);
        eventHolderLinearLayout = (LinearLayout) findViewById(R.id.eventHolderLinearLayout);
        eventEventListRecyclerView = (RecyclerView) findViewById(R.id.eventEventListRecyclerView);


        loadingHolderRelativeLayout = (RelativeLayout) findViewById(R.id.loadingHolderRelativeLayout);
        // My event 2017
        my2017SynchronizeHolderLinearLayout = (LinearLayout) findViewById(R.id.my2017SynchronizeHolderLinearLayout);
        my2017SynchronizeTextView = (TextView) findViewById(R.id.my2017SynchronizeTextView);
        my2017HolderRelativeLayout = (RelativeLayout) findViewById(R.id.my2017HolderRelativeLayout);
        eventShowMeEventButton = (Button) findViewById(R.id.eventShowMeEventButton);
        myEventEmptyTextHolderLinearLayout = (LinearLayout) findViewById(R.id.myEventEmptyTextHolderLinearLayout);
        myEventEmpty2017ImageView = (ImageView) findViewById(R.id.myEventEmpty2017ImageView);
        my2017EmptyHeadlineTextView = (TextView) findViewById(R.id.my2017EmptyHeadlineTextView);
        my2017EmptyDescriptionTextView = (TextView) findViewById(R.id.my2017EmptyDescriptionTextView);


        // No Result Event
        eventNoResultHolderRelativeLayout = (RelativeLayout) findViewById(R.id.eventNoResultHolderRelativeLayout);
        eventNoResultImageView = (ImageView) findViewById(R.id.eventNoResultImageView);
        eventNoResultTextHolderLinearLayout = (LinearLayout) findViewById(R.id.eventNoResultTextHolderLinearLayout);
        eventUseFilterButton = (Button) findViewById(R.id.eventUseFilterButton);
        eventNoResultHeadlineTextView = (TextView) findViewById(R.id.eventNoResultHeadlineTextView);
        eventNoResultDescriptionTextView = (TextView) findViewById(R.id.eventNoResultDescriptionTextView);


        // Menu footer
        eventIconHolderLinearLayout = (LinearLayout) findViewById(R.id.eventIconHolderLinearLayout);
        eventIconImageButton = (ImageButton) findViewById(R.id.eventIconImageButton);
        eventIconTextTextView = (TextView) findViewById(R.id.eventIconTextTextView);

        my2017IconHolderLinearLayout = (LinearLayout) findViewById(R.id.my2017IconHolderLinearLayout);
        my2017IconImageButton = (ImageButton) findViewById(R.id.my2017IconImageButton);
        my2017IconTextTextView = (TextView) findViewById(R.id.my2017IconTextTextView);
        logoImageView = (ImageView) findViewById(R.id.logoImageView);

        // Register event


        // Header
        eventHeaderSelectedMenuItemTextView.setOnClickListener(this);
        eventHeaderMenuIconImageButton.setOnClickListener(this);
        eventHeaderFilterButton.setOnClickListener(this);

        eventIconHolderLinearLayout.setOnClickListener(this);
        eventIconImageButton.setOnClickListener(this);
        eventIconTextTextView.setOnClickListener(this);

        my2017SynchronizeHolderLinearLayout.setOnClickListener(this);
        my2017IconHolderLinearLayout.setOnClickListener(this);
        my2017IconImageButton.setOnClickListener(this);
        my2017IconTextTextView.setOnClickListener(this);
        logoImageView.setOnClickListener(this);

        eventShowMeEventButton.setOnClickListener(this);
        eventUseFilterButton.setOnClickListener(this);

        // Register Internet Status
        RegisterInternetStatus();


        // Set fonts
        setFonts();

        // Set animation
        setAnimation();

        language = GeneralHelper.getDefaultLanguage();
        menuItems = GeneralHelper.GetMenuItems(self);

        setDefaultLanguage();


        // Get default or customer filter config
        long fDate = SharedPreferenceHelper.GetFromDateFilter(self);
        final long tDate = SharedPreferenceHelper.GetToDateFilter(self);
        if (fDate > 0 & tDate > 0) {
            fromDate.setTime(fDate);
            toDate.setTime(tDate);
        } else {
            fromDate = new Date();
            toDate.setTime(new Date().getTime() + (14 * 24 * 60 * 60 * 1000));
        }
        artformsFilter = SharedPreferenceHelper.GetArtformFilter(self);
        audienceFilter = SharedPreferenceHelper.GetAudienceFilter(self);
        cityFilter = SharedPreferenceHelper.GetCityFilter(self);
        locationFilter = SharedPreferenceHelper.GetLocationFilter(self);
        distanceFilter = SharedPreferenceHelper.GetLocationDistanceFilter(self);

        loadMoreDate = toDate;

        InitEventList();


        listPopupWindow = new ListPopupWindow(self);
        MenuAdapter.MenuListener menuListener = new MenuAdapter.MenuListener() {
            @Override
            public void onMenuItemChanged(MenuModel rowData) {
                listPopupWindow.dismiss();
                eventHeaderSelectedMenuItemTextView.setText(rowData.getName());

                switch (rowData.getId()) {
                    case "All": {
                        eventType = "All";
                        Event(fromDate, toDate, language, eventType, false);

                        selectedEventType = 0;
                        GATrackerHelper.SelectEventType(self, selectedEventType);
                        break;
                    }
                    case "Office": {
                        eventType = "OnlyBackendCreated";
                        Event(fromDate, toDate, language, eventType, false);
                        selectedEventType = 1;
                        GATrackerHelper.SelectEventType(self, selectedEventType);
                        break;
                    }
                    case "UserSubmitted": {
                        eventType = "OnlyExternallyCreated";
                        Event(fromDate, toDate, language, eventType, false);
                        selectedEventType = 2;
                        GATrackerHelper.SelectEventType(self, selectedEventType);
                        break;
                    }
                    default:
                        break;

                }
            }
        };
        final MenuAdapter menuAdapter = new MenuAdapter(self, menuItems);
        menuAdapter.setTaskListener(menuListener);
        listPopupWindow.setAdapter(menuAdapter);


        eventSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(self, R.color.active));
        eventSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        Event(fromDate, toDate, language, eventType, false);
                    }
                }, 100);

            }
        });

        //  GATrackerHelper.SendGAScreenView("Events List", self);

        //PreloadEvent(GeneralHelper.GetShortDate("2017-01-01"), GeneralHelper.GetShortDate("2017-12-31"), language, eventType);

        // Support old version
        if (!SharedPreferenceHelper.CheckIsExistedEventEventVenueURLColumn(self)) {
            SharedPreferenceHelper.ResetDatabase(self);
            SharedPreferenceHelper.SaveIsSynchronizeEventData(self, false);
        }


        PrepareData(GeneralHelper.GetShortDate("2017-01-01"), GeneralHelper.GetShortDate("2017-12-31"), language, eventType);

        SynchronizeEventData(language);

        // Register call back when the deleted events has synchronized to local storage
        RegisterDeletedEventSynchronizationStatus();

        // Start the deleted event synchronization process
        ServicesHelper.DeletedEventsSynchronization(self);

        // Register event for load more data
        eventEventListRecyclerView.setLayoutManager(linearLayoutManager);
        RegisterLoadMoreDataOnScroll();


    }

private void RegisterLoadMoreDataOnScroll(){

    isLoadingMoreData=false;
     endlessRecyclerOnScrollListener=new EndlessRecyclerOnScrollListener(linearLayoutManager,loadMoreDate){
        @Override
        public void onLoadMore(Date endDate) {
            isLoadingMoreData=true;
            loadMoreDate = endDate;
            Log.e("onLoadMore",String.valueOf(endDate));
            eventEventListRecyclerView.removeOnScrollListener(endlessRecyclerOnScrollListener);
            //eventSwipeRefreshLayout.setEnabled(true);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;
            eventSwipeRefreshLayout.setProgressViewEndTarget(false, height-500);
            eventSwipeRefreshLayout.setRefreshing(true);

            new EventAsyncTask().execute(My2017EventType.LoadMoreData, endDate);
        };


     };

    eventEventListRecyclerView.addOnScrollListener(endlessRecyclerOnScrollListener);
}


    private void setDefaultLanguage() {

        String defaultLanguage = Locale.getDefault().getLanguage();
        if (defaultLanguage.equals("en") || defaultLanguage.equals("da") || defaultLanguage.equals("de")) {
        } else {
            Locale locale = new Locale("en");
            Locale.setDefault(locale);
            Configuration config = getBaseContext().getResources().getConfiguration();
            config.setLocale(locale);

            Resources resources = self.getResources();


            resources.updateConfiguration(config, resources.getDisplayMetrics());
        }
    }


    private void setAnimation() {
        Animation animationWelcomeText = AnimationUtils.loadAnimation(self, R.anim.translate_welcome_text);
        Animation animationWelcomeTextBackground = AnimationUtils.loadAnimation(self, R.anim.translate_welcome_text_streger);
        Animation animationWelcomeScreen = AnimationUtils.loadAnimation(self, R.anim.translate_welcome_screen_background);

        eventWelcomeTextHolderLinearLayout.startAnimation(animationWelcomeText);
        eventWelcomeTextBackgroundImageView.startAnimation(animationWelcomeTextBackground);
        eventWelcomeScreenImageView.startAnimation(animationWelcomeScreen);
    }

    private void setMyEmpty2017Animation() {
        myEventEmpty2017ImageView.setImageResource(R.drawable.ic_empty_my2017_0);
        Animation animation = AnimationUtils.loadAnimation(self, R.anim.empty2017_fade_in);
        eventShowMeEventButton.startAnimation(animation);

        Animation animationText = AnimationUtils.loadAnimation(self, R.anim.empty2017_fade_in);
        animationText.setStartOffset(300);
        myEventEmptyTextHolderLinearLayout.startAnimation(animationText);

        final Animation animationImage = AnimationUtils.loadAnimation(self, R.anim.empty2017_fade_in);
        animationImage.setStartOffset(100);


        final Animation animationLoop = AnimationUtils.loadAnimation(self, R.anim.empty2017_fade);
        animationLoop.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (index < 8) {
                    myEventEmpty2017ImageView.clearAnimation();
                    String uri = "@drawable/ic_empty_my2017_" + index;
                    int drawableID = getResources().getIdentifier(uri, null, getPackageName());
                    myEventEmpty2017ImageView.setImageResource(drawableID);// .setImageDrawable(ContextCompat.getDrawable(self,drawableID));
                    myEventEmpty2017ImageView.startAnimation(animationLoop);
                    index++;
                } else {
                    myEventEmpty2017ImageView.clearAnimation();
                    index = 0;
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationImage.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                myEventEmpty2017ImageView.startAnimation(animationLoop);
                index = 1;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        myEventEmpty2017ImageView.startAnimation(animationImage);


    }

    private void setNoResultEventAnimation() {
        eventNoResultImageView.setImageResource(R.drawable.ic_empty_my2017_0);
        Animation animation = AnimationUtils.loadAnimation(self, R.anim.empty2017_fade_in);
        eventUseFilterButton.startAnimation(animation);

        Animation animationText = AnimationUtils.loadAnimation(self, R.anim.empty2017_fade_in);
        animationText.setStartOffset(300);
        eventNoResultTextHolderLinearLayout.startAnimation(animationText);

        final Animation animationImage = AnimationUtils.loadAnimation(self, R.anim.empty2017_fade_in);
        animationImage.setStartOffset(100);


        final Animation animationLoop = AnimationUtils.loadAnimation(self, R.anim.empty2017_fade);
        animationLoop.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (index < 8) {
                    eventNoResultImageView.clearAnimation();
                    String uri = "@drawable/ic_empty_my2017_" + index;
                    int drawableID = getResources().getIdentifier(uri, null, getPackageName());
                    eventNoResultImageView.setImageResource(drawableID);// .setImageDrawable(ContextCompat.getDrawable(self,drawableID));
                    eventNoResultImageView.startAnimation(animationLoop);
                    index++;
                } else {
                    eventNoResultImageView.clearAnimation();
                    index = 0;
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationImage.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                eventNoResultImageView.startAnimation(animationLoop);
                index = 1;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        eventNoResultImageView.startAnimation(animationImage);


    }


    private void InitEventList() {

        paint.setColor(Color.parseColor("#FF1E5A"));
        eventDataAdapter = new EventDataAdapter(self, eventDataBindings,false);
        eventEventListRecyclerView.setAdapter(eventDataAdapter);
        eventListener = new EventDataAdapter.EventListener() {
            @Override
            public void onRowClick(EventDataBinding rowData) {
                Intent eventDetail = new Intent(self, EventDetailActivity.class);

                eventDetail.putExtra("EventID", rowData.getEventData().getEventId());
                eventDetail.putExtra("IsFavoriteEvent", rowData.getEventData().getFavoriteEvent());
                eventDetail.putExtra("TicketUrl", rowData.getEventData().getTicketUrl());

                eventDetail.putExtra("ImageUrl", rowData.getEventData().getImageFullSize());
                eventDetail.putExtra("EventHeadline", rowData.getEventData().getEventHeadline());
                eventDetail.putExtra("DescriptionHeadline", rowData.getEventData().getDescriptionHeadline());
                eventDetail.putExtra("Description", rowData.getEventData().getDescription());
                eventDetail.putExtra("DescriptionHtml", rowData.getEventData().getDescriptionHtml());

                String time = rowData.getEventData().getTimeSpan() == 0 ? self.getString(R.string.event_allday) : rowData.getEventData().getTime();
                eventDetail.putExtra("Time", GeneralHelper.TimeFormat(rowData.getEventData().getTimeSpan(), rowData.getEventData().getEndTimeSpan()));
                eventDetail.putExtra("TimeSpan", rowData.getEventData().getTimeSpan());
                eventDetail.putExtra("EndTimeSpan", rowData.getEventData().getEndTimeSpan());

                eventDetail.putExtra("City", rowData.getEventData().getLocationModel().getCity());
                eventDetail.putExtra("Distance", rowData.getEventData().getLocationModel().getDistance());
                eventDetail.putExtra("LocationName", rowData.getEventData().getLocationModel().getName());
                eventDetail.putExtra("Address", rowData.getEventData().getLocationModel().getAddress() + ", " + rowData.getEventData().getLocationModel().getZipCity());
                eventDetail.putExtra("VideoUrl", rowData.getEventData().getVideoUrl());
                eventDetail.putStringArrayListExtra("VideoUrls", rowData.getEventData().getVideoUrls());
                eventDetail.putExtra("EventVenueUrl", rowData.getEventData().getEventVenueUrl());
                eventDetail.putExtra("CurrentGroupDate", rowData.getDate());
                eventDetail.putExtra("FromDate", rowData.getEventData().getStartDateSimple());
                eventDetail.putExtra("ToDate", rowData.getEventData().getEndDateSimple());

                eventDetail.putExtra("ArtFormList", rowData.getEventData().getArtFormList());
                eventDetail.putExtra("AudienceList", rowData.getEventData().getAudienceList());
                eventDetail.putExtra("Disability", rowData.getEventData().getDisabilityAccess());
                eventDetail.putExtra("EventUrl", rowData.getEventData().getEventUrl());
                eventDetail.putExtra("ContactName", rowData.getEventData().getPracticalInfo().getContactName());
                eventDetail.putExtra("ContactPhone", rowData.getEventData().getPracticalInfo().getContactPhone());
                eventDetail.putExtra("ContactEmail", rowData.getEventData().getPracticalInfo().getContactEmail());
                eventDetail.putExtra("SoldOut", rowData.getEventData().getSoldOut());
                eventDetail.putExtra("PriceFrom", rowData.getEventData().getPriceFrom());
                eventDetail.putExtra("PriceTo", rowData.getEventData().getPriceTo());

                eventDetail.putExtra("Latitude", rowData.getEventData().getLocationModel().getLatitude());
                eventDetail.putExtra("Longitude", rowData.getEventData().getLocationModel().getLongitude());
                eventDetail.putExtra("Zoom", rowData.getEventData().getLocationModel().getZoomDistance());
                eventDetail.putExtra("ScheduledEventID", rowData.getEventData().getScheduledEventID());
                eventDetail.putExtra("IsMy2017Event", isCurrentMy2017);

                startActivityForResult(eventDetail, GlobalConstant.StartForEventDetailResult);

                GATrackerHelper.ShowEventDetail(self, rowData.getEventData().getEventId(), rowData.getEventData().getEventHeadline(), rowData.getDate(), selectedEventType);
            }


        };
        eventDataAdapter.setTaskListener(eventListener);

        LinearLayoutManager layoutManager = new LinearLayoutManager(self.getApplicationContext());

        eventEventListRecyclerView.setLayoutManager(layoutManager);
        eventEventListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        eventEventListRecyclerView.setDrawingCacheEnabled(true);
        eventEventListRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        eventEventListRecyclerView.setHasFixedSize(false);


        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {


            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                EventDataBinding rowData;
                if (isCurrentMy2017) {
                    rowData = self.my2017Events.get(position);
                } else {
                    rowData = self.eventDataBindings.get(position);
                }

                if (rowData.isGroupHeader()) {
                    return;
                }

                switch (direction) {
                    case ItemTouchHelper.LEFT: {
                        if (rowData.getEventData().getFavoriteEvent()) {
                            rowData.getEventData().setFavoriteEvent(false);

                            ContentResolver contentResolver = getContentResolver();
                            SharedPreferenceHelper.RemoveFavoriteEvent(self, contentResolver, rowData.getEventData().getScheduledEventID(), rowData.getDate());
                            if (isCurrentMy2017) {
                                self.my2017Events.remove(rowData);
                                new UpdateFavoriteEventAsyncTask().execute();
                            } else {
                                eventDataAdapter.updateData(self.eventDataBindings);
                            }
                        }
                        break;
                    }
                    case ItemTouchHelper.RIGHT: {
                        if (!rowData.isGroupHeader()) {
                            if (!rowData.getEventData().getFavoriteEvent()) {
                                rowData.getEventData().setFavoriteEvent(true);
                                AddFavorite(rowData);
                                GATrackerHelper.SaveFavorite(self, rowData.getEventData().getEventId(), rowData.getEventData().getEventHeadline(), rowData.getDate(), true);
                            }
                            eventDataAdapter.updateData(self.eventDataBindings);
                        }
                        break;
                    }
                    default:
                        break;
                }


            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

                EventDataBinding rowData;
                if (isCurrentMy2017) {
                    rowData = self.my2017Events.get(viewHolder.getAdapterPosition());
                } else {
                    rowData = self.eventDataBindings.get(viewHolder.getAdapterPosition());
                }
                if (!rowData.isGroupHeader()) {
                    if (rowData.getEventData().getFavoriteEvent()) {
                        return ItemTouchHelper.LEFT;
                    } else {
                        return ItemTouchHelper.RIGHT;
                    }
                } else {
                    return super.getSwipeDirs(recyclerView, viewHolder);
                }
            }

            @Override
            public void onChildDrawOver(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                if (!isCurrentlyActive) {
                    LinearLayout eventItemContentHolderLinearLayout = (LinearLayout) viewHolder.itemView.findViewById(R.id.eventItemContentHolderLinearLayout);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                    int margin = ConvertHelper.ConvertDpToPx(self, 10);
                    layoutParams.setMarginStart(margin);
                    layoutParams.setMarginEnd(margin);
                    eventItemContentHolderLinearLayout.setLayoutParams(layoutParams);

                    super.onChildDrawOver(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }
            }


            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                int position = viewHolder.getAdapterPosition();
                EventDataBinding rowData;
                if (isCurrentMy2017) {
                    rowData = self.my2017Events.get(position);
                } else {
                    rowData = self.eventDataBindings.get(position);
                }

                if (rowData.isGroupHeader()) {
                    return;
                }

                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;
                    LinearLayout seperateLine = (LinearLayout) viewHolder.itemView.findViewById(R.id.eventItemSeperationLinearLayout);
                    int seperateLineHeight = 0;
                    if (seperateLine.getVisibility() == View.VISIBLE) {
                        seperateLineHeight = seperateLine.getHeight();
                    } else {
                        seperateLineHeight = 0;
                    }

                    LinearLayout eventItemContentHolderLinearLayout = (LinearLayout) viewHolder.itemView.findViewById(R.id.eventItemContentHolderLinearLayout);

                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    if (rowData.getEventData().getFavoriteEvent()) {

                        if (dX < 0) {
                            layoutParams.setMarginEnd(0);
                            eventItemContentHolderLinearLayout.setLayoutParams(layoutParams);

                            RectF background = new RectF(itemView.getWidth() - Math.abs(dX), (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom() - seperateLineHeight);
                            c.drawRect(background, paint);

                            icon = BitmapFactory.decodeResource(self.getResources(), R.drawable.remove_saved_favorite);
                            int top = (itemView.getBottom() - itemView.getTop() - seperateLineHeight - icon.getHeight()) / 2;
                            int padding = ConvertHelper.ConvertDpToPx(self, 30);
                            RectF icon_dest = new RectF(itemView.getWidth() - Math.abs(dX) + padding, (float) itemView.getTop() + top, itemView.getWidth() - Math.abs(dX) + padding + icon.getWidth(), (float) itemView.getBottom() - seperateLineHeight - top);
                            c.drawBitmap(icon, null, icon_dest, paint);


                            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

                        } else {
                            c = new Canvas();
                            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        }

                    } else {
                        if (dX > 0) {
                            layoutParams.setMarginStart(0);
                            eventItemContentHolderLinearLayout.setLayoutParams(layoutParams);
                            RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom() - seperateLineHeight);
                            c.drawRect(background, paint);
                            if (dX > 50) {
                                icon = BitmapFactory.decodeResource(self.getResources(), R.drawable.save_favorite);

                                int top = (itemView.getBottom() - itemView.getTop() - seperateLineHeight - icon.getHeight()) / 2;
                                int padding = ConvertHelper.ConvertDpToPx(self, 30);

                                RectF icon_dest = new RectF(dX - padding - icon.getWidth(), (float) itemView.getTop() + top, dX - padding, (float) itemView.getBottom() - seperateLineHeight - top);
                                c.drawBitmap(icon, null, icon_dest, paint);
                            }

                            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

                        } else {

                            c = new Canvas();
                            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        }
                    }
                }
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(eventEventListRecyclerView);


    }

    private void Event(final Date fromDate, final Date toDate, final String language, final String eventType, final Boolean isRefreshing) {
        headerBarLinearLayout.setVisibility(View.VISIBLE);
        welcomeHolderFrameLayout.setVisibility(View.GONE);
        eventHolderLinearLayout.setVisibility(View.GONE);
        loadingHolderRelativeLayout.setVisibility(View.VISIBLE);
        my2017HolderRelativeLayout.setVisibility(View.GONE);
        eventNoResultHolderRelativeLayout.setVisibility(View.GONE);
        my2017SynchronizeHolderLinearLayout.setVisibility(View.GONE);


        eventIconImageButton.setColorFilter(ContextCompat.getColor(self, R.color.active));
        eventIconTextTextView.setTextColor(ContextCompat.getColor(self, R.color.active));

        my2017IconImageButton.setColorFilter(ContextCompat.getColor(self, R.color.bottomBarIcon));
        my2017IconTextTextView.setTextColor(ContextCompat.getColor(self, R.color.bottomBarIcon));

        if (!InternetHelper.HasInternetConnection(self)) {
            eventInternetHolderLinearLayout.setVisibility(View.VISIBLE);
        } else {
            eventInternetHolderLinearLayout.setVisibility(View.GONE);
        }

        if (self.sharedData.size() > 0 && isRefreshing == false) {
            headerBarLinearLayout.setVisibility(View.VISIBLE);
            eventHolderLinearLayout.setVisibility(View.VISIBLE);
            loadingHolderRelativeLayout.setVisibility(View.GONE);

            new EventAsyncTask().execute(My2017EventType.SharedData, self.sharedData);
        } else {
            if (SharedPreferenceHelper.GetSynchronizeEventData(self)) {
                new EventAsyncTask().execute(My2017EventType.RawLocal);
            } else {
                eventHeaderTotalTextView.setText(" 0");
                JSONArray jsonArray = LocalDataHelper.ReadDataJson(self, language);
                new EventAsyncTask().execute(My2017EventType.RawApi, jsonArray);
            }

        }

        GATrackerHelper.SendGAScreenView(GATrackerHelper.EventListScreen, self);

    }


    private void My2017Event(Date fromDate, Date toDate, String language, String eventType) {
        headerBarLinearLayout.setVisibility(View.GONE);
        welcomeHolderFrameLayout.setVisibility(View.GONE);
        eventHolderLinearLayout.setVisibility(View.GONE);
        loadingHolderRelativeLayout.setVisibility(View.VISIBLE);
        my2017HolderRelativeLayout.setVisibility(View.GONE);
        eventNoResultHolderRelativeLayout.setVisibility(View.GONE);
        eventNoResultHolderRelativeLayout.setVisibility(View.GONE);

        eventIconImageButton.setColorFilter(ContextCompat.getColor(self, R.color.bottomBarIcon));
        eventIconTextTextView.setTextColor(ContextCompat.getColor(self, R.color.bottomBarIcon));

        my2017IconImageButton.setColorFilter(ContextCompat.getColor(self, R.color.active));
        my2017IconTextTextView.setTextColor(ContextCompat.getColor(self, R.color.active));

        if (!InternetHelper.HasInternetConnection(self)) {
            eventInternetHolderLinearLayout.setVisibility(View.VISIBLE);
        } else {
            eventInternetHolderLinearLayout.setVisibility(View.GONE);
        }

        if (eventSwipeRefreshLayout.isRefreshing()) {
            eventSwipeRefreshLayout.setRefreshing(false);
        }
        eventSwipeRefreshLayout.setEnabled(false);


        if (self.sharedData.size() > 0) {
            headerBarLinearLayout.setVisibility(View.GONE);
            welcomeHolderFrameLayout.setVisibility(View.GONE);
            eventHolderLinearLayout.setVisibility(View.GONE);
            loadingHolderRelativeLayout.setVisibility(View.VISIBLE);
            my2017HolderRelativeLayout.setVisibility(View.GONE);

            new My2017EventAsyncTask().execute(My2017EventType.SharedData, self.sharedData);
        } else {
            if (SharedPreferenceHelper.GetSynchronizeEventData(self)) {
                new My2017EventAsyncTask().execute(My2017EventType.RawLocal);
            } else {
                JSONArray dataLocal = LocalDataHelper.ReadDataJson(self, language);
                new My2017EventAsyncTask().execute(My2017EventType.RawApi, dataLocal);

//                if (InternetHelper.HasInternetConnection(self)) {
//
//                    AsyncHttpClient client = HttpHelper.GetHttpClient();
//                    StringEntity params = HttpHelper.GetFullDataHttpParams(null, null, language,false);
//                    try {
//                        client.post(self.getApplicationContext(), GlobalConfig.EventListUrl, params, "application/json", new JsonHttpResponseHandler() {
//                            @Override
//                            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
//                                new My2017EventAsyncTask().execute(My2017EventType.RawApi, response);
//                            }
//                        });
//                    } catch (Exception ex) {
//                        Log.e("Api Error", ex.getMessage());
//                    }
//                }
            }
        }
        GATrackerHelper.SendGAScreenView(GATrackerHelper.EventListScreen, self);

    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case GlobalConstant.RequestLocationPermissionResultCode: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    myLocation = LocationHelper.getLocation(self);
                    if (myLocation != null && eventDataAdapter != null) {
                        eventDataAdapter.updateLocation(myLocation);
                    }
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    // Toast.makeText(MainActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case GlobalConstant.RequestCalendarPermissionResultCode: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (currentEventDataRow != null) {
                        ContentResolver contentResolver = getContentResolver();
                        long calendarEventID = GeneralHelper.AddEventToCalendar(contentResolver, currentEventDataRow.getDate(), currentEventDataRow.getEventData().getTimeSpan(), currentEventDataRow.getEventData().getEndTimeSpan()
                                , currentEventDataRow.getEventData().getEventHeadline(), currentEventDataRow.getEventData().getDescription(), currentEventDataRow.getEventData().getEventUrl()
                                , currentEventDataRow.getEventData().getLocationModel().getName() + ", " + currentEventDataRow.getEventData().getLocationModel().getAddress());
                        SharedPreferenceHelper.UpdateFavoriteEvent(self, currentEventDataRow.getEventData().getScheduledEventID(), calendarEventID, currentEventDataRow.getDate());
                    }
                }
                break;
            }
            case GlobalConstant.RequestCalendarPermissionSynchronizationResultCode: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (self.my2017Events.size() > 0) {
                        new SynchronizeFavoriteToCalendarAsyncTask().execute(self.my2017Events);
                    }
                }
                break;
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case GlobalConstant.StartForEventDetailResult: {
                if (resultCode == RESULT_OK) {
                    String type = data.getStringExtra("Type");
                    switch (type) {
                        case "Back": {
                            if (isCurrentMy2017) {
                                isCurrentMy2017 = true;
                                eventSwipeRefreshLayout.setEnabled(false);
                                My2017Event(fromDate, toDate, language, eventType);
                            } else {
                                isCurrentMy2017 = false;
                                eventSwipeRefreshLayout.setEnabled(true);
                                Event(fromDate, toDate, language, eventType, false);
                            }
                            break;
                        }
                        case "Event": {
                            isCurrentMy2017 = false;
                            eventSwipeRefreshLayout.setEnabled(true);
                            Event(fromDate, toDate, language, eventType, false);
                            break;
                        }
                        case "My2017": {
                            isCurrentMy2017 = true;
                            eventSwipeRefreshLayout.setEnabled(false);
                            My2017Event(fromDate, toDate, eventType, language);
                            break;
                        }
                        default:
                            break;
                    }
                }
                break;
            }
            case GlobalConstant.StartActivityForFilterResult: {
                if (resultCode == RESULT_OK) {
                    long fDate = SharedPreferenceHelper.GetFromDateFilter(self);
                    long tDate = SharedPreferenceHelper.GetToDateFilter(self);

                    if (fDate > 0 && tDate > 0) {
                        fromDate = new Date(fDate);
                        toDate = new Date(tDate);
                    } else {
                        fromDate = new Date();
                        toDate.setTime(new Date().getTime() + (14 * 24 * 60 * 60 * 1000));
                    }

                    artformsFilter = SharedPreferenceHelper.GetArtformFilter(self);
                    audienceFilter = SharedPreferenceHelper.GetAudienceFilter(self);
                    cityFilter = SharedPreferenceHelper.GetCityFilter(self);
                    locationFilter = SharedPreferenceHelper.GetLocationFilter(self);
                    distanceFilter = SharedPreferenceHelper.GetLocationDistanceFilter(self);

                    Event(fromDate, toDate, language, eventType, false);
                }

                break;
            }
            default:
                break;

        }


        //  super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.eventHeaderFilterButton: {
                GATrackerHelper.SendGAAction("Event Filter", self);
                Intent filterIntent = new Intent(self, FilterActivity.class);
                startActivityForResult(filterIntent, GlobalConstant.StartActivityForFilterResult);
                break;
            }
            case R.id.eventUseFilterButton: {
                Intent filterIntent = new Intent(self, FilterActivity.class);
                startActivityForResult(filterIntent, GlobalConstant.StartActivityForFilterResult);
                headerBarLinearLayout.setVisibility(View.INVISIBLE);
                eventNoResultHolderRelativeLayout.setVisibility(View.INVISIBLE);
                break;
            }

            case R.id.eventHeaderSelectedMenuItemTextView:
            case R.id.eventHeaderMenuIconImageButton: {
                listPopupWindow.setAnchorView(eventHeaderMenuIconImageButton);
                listPopupWindow.setWidth(800);
                listPopupWindow.show();
                break;
            }
            case R.id.eventIconHolderLinearLayout:
            case R.id.eventIconImageButton:
            case R.id.eventIconTextTextView: {
                isCurrentMy2017 = false;
                if (eventSwipeRefreshLayout.isRefreshing()) {
                    eventSwipeRefreshLayout.setRefreshing(false);
                }
                eventSwipeRefreshLayout.setEnabled(true);
                Event(fromDate, toDate, language, eventType, false);


                break;
            }
            case R.id.my2017IconHolderLinearLayout:
            case R.id.my2017IconImageButton:
            case R.id.my2017IconTextTextView: {
                isCurrentMy2017 = true;

                My2017Event(fromDate, toDate, language, eventType);
                break;
            }
            case R.id.eventShowMeEventButton: // Show me events button in my 2017
            {
                isCurrentMy2017 = false;
                eventSwipeRefreshLayout.setEnabled(true);
                Event(fromDate, toDate, language, eventType, false);
                break;
            }
            case R.id.logoImageView: {
                String websiteUrl = GeneralHelper.GetWebSiteUrl();
                Intent websiteIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(websiteUrl));
                startActivity(websiteIntent);
                break;
            }
            case R.id.my2017SynchronizeHolderLinearLayout: {
                SynchronizeFavoriteToCalendar(my2017Events);
                break;
            }
            default:
                break;
        }
    }

    private void setFonts() {
        Typeface flavinBold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Flavin Bold.otf");
        Typeface sourceSansProRegular = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/SourceSansPro-Regular.otf");
        Typeface sourceSansProBold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/SourceSansPro-Bold.otf");
        Typeface flavinRegular = FontHelper.FlavinRegular(self);

        // Welcome
        eventWelcomeHeadlineTextView.setTypeface(flavinBold);
        eventWelcomeSubheadlineTextView.setTypeface(flavinBold);

        // Header
        eventHeaderTotalLabelTextView.setTypeface(sourceSansProRegular);
        eventHeaderTotalTextView.setTypeface(sourceSansProRegular);
        eventHeaderSelectedMenuItemTextView.setTypeface(sourceSansProBold);
        eventHeaderFilterButton.setTypeface(sourceSansProBold);

        eventNoInternetTextView.setTypeface(sourceSansProRegular);

        // Empty 2017
        my2017SynchronizeTextView.setTypeface(sourceSansProBold);
        my2017EmptyHeadlineTextView.setTypeface(flavinBold);
        my2017EmptyDescriptionTextView.setTypeface(flavinRegular);
        eventShowMeEventButton.setTypeface(sourceSansProBold);

        // No Result
        eventNoResultHeadlineTextView.setTypeface(flavinBold);
        eventNoResultDescriptionTextView.setTypeface(flavinRegular);
        eventUseFilterButton.setTypeface(sourceSansProBold);


        // Empty 2017


        // Footer
        eventIconTextTextView.setTypeface(flavinBold);
        my2017IconTextTextView.setTypeface(flavinBold);


        // Header

    }

    private void AddFavorite(EventDataBinding rowData) {
        currentEventDataRow = rowData;
        SharedPreferenceHelper.SaveFavoriteEvent(self, rowData.getEventData().getScheduledEventID(), rowData.getDate());

        PromptPermissionResult setting = SharedPreferenceHelper.GetCalendarPromptSetting(self);

        switch (setting) {
            case Yes: {
                PermissionResult permissionResult = PermissionHelper.CheckCalendarPermission(self);
                switch (permissionResult) {
                    case Granted: {
                        ContentResolver contentResolver = getContentResolver();
                        long calendarEventID = GeneralHelper.AddEventToCalendar(contentResolver, rowData.getDate(), rowData.getEventData().getTimeSpan(), rowData.getEventData().getEndTimeSpan(), rowData.getEventData().getEventHeadline(),
                                rowData.getEventData().getDescription(), rowData.getEventData().getEventUrl(), rowData.getEventData().getLocationModel().getName() + ", " + rowData.getEventData().getLocationModel().getAddress());
                        SharedPreferenceHelper.UpdateFavoriteEvent(self, rowData.getEventData().getScheduledEventID(), calendarEventID, rowData.getDate());


                        break;
                    }
                    case Requested: {
                        ActivityCompat.requestPermissions(self, new String[]{Manifest.permission.WRITE_CALENDAR}, GlobalConstant.RequestCalendarPermissionResultCode);
                        break;
                    }
                    case Denied:
                        break;
                    default:
                        break;
                }
                break;
            }
            case No: {
                break;
            }
            case Ignore: {
                final Dialog acceptDialog = new Dialog(self);
                acceptDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                acceptDialog.setContentView(R.layout.prompt_calendar_dialog);
                acceptDialog.show();
                Typeface flavinBold = FontHelper.FlavinBold(self);
                Typeface flavinRegular = FontHelper.FlavinRegular(self);
                Typeface sourceSansProBold = FontHelper.SourceSansProBold(self);
                TextView titleTextView = (TextView) acceptDialog.findViewById(R.id.promtTitleTextView);
                TextView descriptionTextView = (TextView) acceptDialog.findViewById(R.id.promtDescriptionTextView);
                Button acceptButton = (Button) acceptDialog.findViewById(R.id.promptAcceptButton);
                Button noButton = (Button) acceptDialog.findViewById(R.id.promptNoButton);

                titleTextView.setTypeface(flavinBold);
                descriptionTextView.setTypeface(flavinRegular);
                acceptButton.setTypeface(sourceSansProBold);
                noButton.setTypeface(sourceSansProBold);
                acceptButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        acceptDialog.dismiss();
                        SharedPreferenceHelper.SaveCalendarPromptSetting(self, PromptPermissionResult.Yes);
                        ActivityCompat.requestPermissions(self, new String[]{Manifest.permission.WRITE_CALENDAR}, GlobalConstant.RequestCalendarPermissionResultCode);
                        GATrackerHelper.SynCalendar(self);

                    }
                });
                ImageButton closeImageButton = (ImageButton) acceptDialog.findViewById(R.id.promtCloseImageButton);
                closeImageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        acceptDialog.dismiss();
                        SharedPreferenceHelper.SaveCalendarPromptSetting(self, PromptPermissionResult.Ignore);
                    }
                });
                noButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        acceptDialog.dismiss();
                        SharedPreferenceHelper.SaveCalendarPromptSetting(self, PromptPermissionResult.No);
                    }
                });
                break;
            }
            default: {
                break;
            }
        }

    }

    private void RegisterInternetStatus() {
        // Register internet status

        ConnectivityReceiver.ConnectivityReceiverListener listener = new ConnectivityReceiver.ConnectivityReceiverListener() {
            @Override
            public void onNetworkConnectionChanged(boolean isConnected) {
                if (welcomeHolderFrameLayout.getVisibility() == View.GONE) {
                    if (isConnected) {
                        eventInternetHolderLinearLayout.setVisibility(View.GONE);
                    } else {
                        eventInternetHolderLinearLayout.setVisibility(View.VISIBLE);
                    }
                }
            }
        };
        connectivityReceiver = new ConnectivityReceiver();
        connectivityReceiver.setConnectivityReceiverListener(listener);
        this.registerReceiver(connectivityReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    // Register to reload event list when the deleted events has synchronized
    private void RegisterDeletedEventSynchronizationStatus() {
        // Register internet status

        DeletedEventBroadcastReceiver.DeletedEventBroadcastReceiverListener listener = new DeletedEventBroadcastReceiver.DeletedEventBroadcastReceiverListener() {
            @Override
            public void SynchronizeDeletedEvent(ArrayList<Integer> deletedEvents) {
                Log.e("Delete", "Ok");
                //deletedEvents = new ArrayList<>();

                if (deletedEvents.size() > 0) {
                    switch (actionStatus) {
                        case EventList: {
                            new UpdateDeletedEventSynchronizationAsyncTask().execute(deletedEvents);
                            new UpdateDeletedEventSynchronizationForSharingDataAsyncTask().execute(deletedEvents);
                            break;
                        }
                        case My2017: {
                            new UpdateDeletedEventSynchronizationForFavoriteAsyncTask().execute(deletedEvents);
                            new UpdateDeletedEventSynchronizationForSharingDataAsyncTask().execute(deletedEvents);
                            break;
                        }
                        default: {
                            new UpdateDeletedEventSynchronizationForSharingDataAsyncTask().execute(deletedEvents);
                            break;
                        }
                    }
                }
            }
        };
        deletedEventBroadcastReceiver = new DeletedEventBroadcastReceiver();
        deletedEventBroadcastReceiver.setDeletedEventBroadcastReceiverListener(listener);
        LocalBroadcastManager.getInstance(this).registerReceiver(deletedEventBroadcastReceiver, new IntentFilter(GlobalConfig.ServiceType.DeletedEventSynchronization));
    }


    private void PrepareData(Date fromDate, Date toDate, String language, String eventType) {

        if (SharedPreferenceHelper.GetSynchronizeEventData(self)) {
            new PreloadEventDataAsyncTask().execute(false);
        } else {
//            if (InternetHelper.HasInternetConnection(self)) {
//                AsyncHttpClient client = HttpHelper.GetHttpClient();
//                StringEntity params =  HttpHelper.GetFullDataHttpParams(fromDate, toDate, language,false);
//                try {
//                    client.post(self.getApplicationContext(), GlobalConfig.EventListUrl, params, "application/json", new JsonHttpResponseHandler() {
//                        @Override
//                        public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
//                            new PreloadEventDataAsyncTask().execute(true, response);
//                        }
//
//                        @Override
//                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                            Log.e("SyncData Error API", throwable.getMessage());
//
//                        }
//
//                        @Override
//                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
//                            super.onFailure(statusCode, headers, throwable, errorResponse);
//                        }
//                    });
//                } catch (Exception ex) {
//                    Log.e("Api Error", ex.getMessage());
//                }
//            }
            JSONArray dataLocal = LocalDataHelper.ReadDataJson(self, language);
            new PreloadEventDataAsyncTask().execute(true, dataLocal);
        }
    }


    private void SynchronizeEventData(final String language) {

        if (InternetHelper.HasInternetConnection(self)) {
            AsyncHttpClient client = HttpHelper.GetHttpClient();
            Boolean isSynchronized = SharedPreferenceHelper.GetSynchronizeEventData(self);
            StringEntity params = HttpHelper.GetFullDataHttpParams(null, null, language, isSynchronized);
            try {
                client.post(self.getApplicationContext(), GlobalConfig.EventListUrl, params, "application/json", new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        Log.e("SynCount", String.valueOf(response.length()));
                        new SaveEventAsyncTask().execute(response);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Log.e("SyncData Error API", throwable.getMessage());

                    }
                });

            } catch (Exception ex) {
                Log.e("Api Error", ex.getMessage());
            }

        }

    }

    private void SynchronizeFavoriteToCalendar(ArrayList<EventDataBinding> favoriteItems) {
        PromptPermissionResult setting = SharedPreferenceHelper.GetCalendarPromptSetting(self);

        switch (setting) {
            case Yes: {
                PermissionResult permissionResult = PermissionHelper.CheckCalendarPermission(self);
                switch (permissionResult) {
                    case Granted: {
                        new SynchronizeFavoriteToCalendarAsyncTask().execute(favoriteItems);
                        break;
                    }
                    case Requested: {
                        ActivityCompat.requestPermissions(self, new String[]{Manifest.permission.WRITE_CALENDAR}, GlobalConstant.RequestCalendarPermissionSynchronizationResultCode);
                        break;
                    }
                    case Denied:
                        break;
                    default:
                        break;
                }
                break;
            }
            case No:
            case Ignore: {
                final Dialog acceptDialog = new Dialog(self);
                acceptDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                acceptDialog.setContentView(R.layout.prompt_calendar_dialog);
                acceptDialog.show();
                Typeface flavinBold = FontHelper.FlavinBold(self);
                Typeface flavinRegular = FontHelper.FlavinRegular(self);
                Typeface sourceSansProBold = FontHelper.SourceSansProBold(self);
                TextView titleTextView = (TextView) acceptDialog.findViewById(R.id.promtTitleTextView);
                TextView descriptionTextView = (TextView) acceptDialog.findViewById(R.id.promtDescriptionTextView);
                Button acceptButton = (Button) acceptDialog.findViewById(R.id.promptAcceptButton);
                Button noButton = (Button) acceptDialog.findViewById(R.id.promptNoButton);

                titleTextView.setTypeface(flavinBold);
                descriptionTextView.setTypeface(flavinRegular);
                acceptButton.setTypeface(sourceSansProBold);
                noButton.setTypeface(sourceSansProBold);
                acceptButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        acceptDialog.dismiss();
                        SharedPreferenceHelper.SaveCalendarPromptSetting(self, PromptPermissionResult.Yes);
                        ActivityCompat.requestPermissions(self, new String[]{Manifest.permission.WRITE_CALENDAR}, GlobalConstant.RequestCalendarPermissionSynchronizationResultCode);
                        GATrackerHelper.SynCalendar(self);

                    }
                });
                ImageButton closeImageButton = (ImageButton) acceptDialog.findViewById(R.id.promtCloseImageButton);
                closeImageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        acceptDialog.dismiss();
                        SharedPreferenceHelper.SaveCalendarPromptSetting(self, PromptPermissionResult.Ignore);
                    }
                });
                noButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        acceptDialog.dismiss();
                        SharedPreferenceHelper.SaveCalendarPromptSetting(self, PromptPermissionResult.No);
                    }
                });
                break;
            }
            default: {
                break;
            }
        }

    }

    public class EventAsyncTask extends AsyncTask<Object, Boolean, ArrayList<EventDataBinding>> {


        @Override
        protected void onProgressUpdate(Boolean... values) {
            super.onProgressUpdate(values);
        }


        @Override
        protected void onPostExecute(ArrayList<EventDataBinding> bindings) {
            self.eventDataBindings = bindings;
            actionStatus = GlobalConfig.MainActivityStatus.EventList;
            int totalEvent = GeneralHelper.GetTotalEvent(self.eventDataBindings);
            eventHeaderTotalTextView.setText(" " + String.valueOf(totalEvent));

            headerBarLinearLayout.setVisibility(View.GONE);
            welcomeHolderFrameLayout.setVisibility(View.GONE);
            eventHolderLinearLayout.setVisibility(View.GONE);
            loadingHolderRelativeLayout.setVisibility(View.GONE);
            my2017HolderRelativeLayout.setVisibility(View.GONE);
            eventNoResultHolderRelativeLayout.setVisibility(View.GONE);
            if (eventSwipeRefreshLayout.isRefreshing()) {
                eventSwipeRefreshLayout.setRefreshing(false);
                eventSwipeRefreshLayout.setProgressViewEndTarget(false,400);
            }
            if(isLoadingMoreData){
                RegisterLoadMoreDataOnScroll();
            }

            if (self.eventDataBindings.size() > 0) {
                headerBarLinearLayout.setVisibility(View.VISIBLE);
                eventHolderLinearLayout.setVisibility(View.VISIBLE);


                eventDataAdapter.updateData(self.eventDataBindings);

                myLocation = LocationHelper.getLocation(self);
                if (myLocation != null) {
                    eventDataAdapter.updateLocation(myLocation);
                }
            } else {
                eventNoResultHolderRelativeLayout.setVisibility(View.VISIBLE);
                setNoResultEventAnimation();
            }
        }


        @Override
        protected ArrayList<EventDataBinding> doInBackground(Object... arrayLists) {
            ArrayList<EventDataBinding> results = new ArrayList<>();
            My2017EventType type = (My2017EventType) arrayLists[0];
            switch (type) {
                case SharedData: {
                    Map<String, ArrayList<EventData>> events = (Map<String, ArrayList<EventData>>) arrayLists[1];
                    //   results = ConvertHelper.ConvertToFilterEventData(self,events,eventType,fromDate,toDate,artformsFilter,audienceFilter,locationFilter,distanceFilter);
                    results = EventHelper.ConvertToFilterEventData(self, events, eventType, fromDate, toDate, artformsFilter, audienceFilter, locationFilter, distanceFilter);
                    break;
                }
                case RawLocal: {
                    ArrayList<EventModel> models = SharedPreferenceHelper.GetEvents(self, eventType, language, GeneralHelper.GetShortDate("2017-01-01"), GeneralHelper.GetShortDate("2017-12-31"));
                    self.sharedData = EventHelper.PrepareEvents(self, models, GeneralHelper.GetShortDate("2017-01-01"), GeneralHelper.GetShortDate("2017-12-31"));
                    results = EventHelper.ConvertToFilterEventData(self, self.sharedData, eventType, fromDate, toDate, artformsFilter, audienceFilter, locationFilter, distanceFilter);
                    break;
                }
                case RawApi: {
                    JSONArray jsonArray = (JSONArray) arrayLists[1];
                    ArrayList<EventModel> models = ParserHelper.ParseToEventObject(jsonArray);
                    self.sharedData = EventHelper.PrepareEvents(self, models, GeneralHelper.GetShortDate("2017-01-01"), GeneralHelper.GetShortDate("2017-12-31"));
                    results = EventHelper.ConvertToFilterEventData(self, self.sharedData, eventType, fromDate, toDate, artformsFilter, audienceFilter, locationFilter, distanceFilter);
                    break;
                }
                case LoadMoreData:{
                    Date extendDate = (Date) arrayLists[1];
                    results = EventHelper.ConvertToFilterEventData(self, self.sharedData, eventType, fromDate, extendDate, artformsFilter, audienceFilter, locationFilter, distanceFilter);
                    break;
                }
            }
            return results;
        }
    }

    public class UpdateFavoriteEventAsyncTask extends AsyncTask<Object, Void, ArrayList<EventDataBinding>> {

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(ArrayList<EventDataBinding> results) {
            self.my2017Events = results;
            if (my2017Events.size() > 0) {
                eventDataAdapter.updateData(my2017Events);
            } else {
                eventHolderLinearLayout.setVisibility(View.GONE);
                my2017HolderRelativeLayout.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected ArrayList<EventDataBinding> doInBackground(Object... arrayLists) {
            return EventHelper.UpdateFavoriteEvents(self.my2017Events);
        }
    }

    public class My2017EventAsyncTask extends AsyncTask<Object, Void, ArrayList<EventDataBinding>> {

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(ArrayList<EventDataBinding> results) {
            self.my2017Events = results;
            actionStatus = GlobalConfig.MainActivityStatus.My2017;

            headerBarLinearLayout.setVisibility(View.GONE);
            welcomeHolderFrameLayout.setVisibility(View.GONE);
            eventHolderLinearLayout.setVisibility(View.GONE);
            loadingHolderRelativeLayout.setVisibility(View.GONE);
            my2017HolderRelativeLayout.setVisibility(View.GONE);
            eventNoResultHolderRelativeLayout.setVisibility(View.GONE);

            if (self.my2017Events.size() > 0) {
                eventDataAdapter.updateData(self.my2017Events);
                eventHolderLinearLayout.setVisibility(View.VISIBLE);
                new CheckSynchronizeToCalendarAsyncTask().execute(self.my2017Events);
            } else {
                my2017HolderRelativeLayout.setVisibility(View.VISIBLE);
                setMyEmpty2017Animation();
            }

        }

        @Override
        protected ArrayList<EventDataBinding> doInBackground(Object... arrayLists) {

            ArrayList<EventDataBinding> results = new ArrayList<>();
            My2017EventType type = (My2017EventType) arrayLists[0];
            switch (type) {
                case SharedData: {
                    Map<String, ArrayList<EventData>> sharedItems = (Map<String, ArrayList<EventData>>) arrayLists[1];
                    results = EventHelper.ConvertToEventDataFilterByFavorite(self, sharedItems);
                    break;
                }
                case RawLocal: {
                    ArrayList<EventModel> models = SharedPreferenceHelper.GetEvents(self, "All", language, GeneralHelper.GetShortDate("2017-01-01"), GeneralHelper.GetShortDate("2017-12-31"));
                    self.sharedData = EventHelper.PrepareEvents(self, models, GeneralHelper.GetShortDate("2017-01-01"), GeneralHelper.GetShortDate("2017-12-31"));
                    results = EventHelper.ConvertToEventDataFilterByFavorite(self, self.sharedData);
                    break;
                }
                case RawApi: {
                    JSONArray jsonArray = (JSONArray) arrayLists[1];
                    ArrayList<EventModel> models = ParserHelper.ParseToEventObject(jsonArray);
                    self.sharedData = EventHelper.PrepareEvents(self, models, GeneralHelper.GetShortDate("2017-01-01"), GeneralHelper.GetShortDate("2017-12-31"));
                    results = EventHelper.ConvertToEventDataFilterByFavorite(self, self.sharedData);
                    break;
                }
            }
            return results;

        }
    }


    public class PreloadEventDataAsyncTask extends AsyncTask<Object, Boolean, Map<String, ArrayList<EventData>>> {

        @Override
        protected void onProgressUpdate(Boolean... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Map<String, ArrayList<EventData>> results) {
            // self.sharedEventDataBindings = bindings;
            self.sharedData = results;

            // Google Analytics
            if (!SharedPreferenceHelper.GetGALanguageStatus(self)) {
                GATrackerHelper.SelectedLanguage(self, language);
                SharedPreferenceHelper.SaveGALanguageStatus(self);
            }

        }

        @Override
        protected Map<String, ArrayList<EventData>> doInBackground(Object... arrayLists) {
            ArrayList<EventModel> models = new ArrayList<>();
            boolean isFormatJson = (Boolean) arrayLists[0];
            if (isFormatJson) {
                JSONArray jsonArray = (JSONArray) arrayLists[1];
                models = ParserHelper.ParseToEventObject(jsonArray);
            } else {
                models = SharedPreferenceHelper.GetEvents(self, "All", language, GeneralHelper.GetShortDate("2017-01-01"), GeneralHelper.GetShortDate("2017-12-31"));
            }
            return EventHelper.PrepareEvents(self, models, GeneralHelper.GetShortDate("2017-01-01"), GeneralHelper.GetShortDate("2017-12-31"));// .ConvertToEventData(models,GeneralHelper.GetShortDate("2017-01-01"), GeneralHelper.GetShortDate("2017-12-31") );
        }
    }


    public class SaveEventAsyncTask extends AsyncTask<JSONArray, Void, Void> {
        @Override
        protected Void doInBackground(JSONArray... jsonArrays) {
            JSONArray response = (JSONArray) jsonArrays[0];
            ArrayList<EventModel> items = ParserHelper.ParseToEventObject(response);
            if (items.size() > 0) {
                Boolean result = SharedPreferenceHelper.SaveEvents(self, items);
                if (result) {
                    SharedPreferenceHelper.SaveIsSynchronizeEventData(self, true);
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

    }

    public class CheckSynchronizeToCalendarAsyncTask extends AsyncTask<ArrayList<EventDataBinding>, Void, Boolean> {
        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                my2017SynchronizeHolderLinearLayout.setVisibility(View.VISIBLE);
            } else {
                my2017SynchronizeHolderLinearLayout.setVisibility(View.GONE);
            }
        }

        @Override
        protected Boolean doInBackground(ArrayList<EventDataBinding>... arrayLists) {
            ArrayList<EventDataBinding> favoritedItems = (ArrayList<EventDataBinding>) arrayLists[0];
            Boolean result = SharedPreferenceHelper.CheckSynchronizeToCalendar(self, favoritedItems);
            return result;
        }
    }


    public class SynchronizeFavoriteToCalendarAsyncTask extends AsyncTask<ArrayList<EventDataBinding>, Void, Boolean> {
        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                my2017SynchronizeHolderLinearLayout.setVisibility(View.GONE);
            } else {
                my2017SynchronizeHolderLinearLayout.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected Boolean doInBackground(ArrayList<EventDataBinding>... arrayLists) {
            ArrayList<EventDataBinding> favoritedItems = (ArrayList<EventDataBinding>) arrayLists[0];
            ContentResolver contentResolver = self.getContentResolver();
            Boolean result = SharedPreferenceHelper.SynchronizeToCalendar(self, contentResolver, favoritedItems);
            return result;
        }
    }

    public class UpdateDeletedEventSynchronizationAsyncTask extends AsyncTask<ArrayList<Integer>, Void, ArrayList<EventDataBinding>> {
        @Override
        protected void onPostExecute(ArrayList<EventDataBinding> result) {
            self.eventDataBindings = result;
            if (actionStatus.equals(GlobalConfig.MainActivityStatus.EventList)) {
                if (self.eventDataBindings.size() > 0) {
                    headerBarLinearLayout.setVisibility(View.VISIBLE);
                    eventHolderLinearLayout.setVisibility(View.VISIBLE);
                    if (eventSwipeRefreshLayout.isRefreshing()) {
                        eventSwipeRefreshLayout.setRefreshing(false);
                    }

                    eventDataAdapter.updateData(self.eventDataBindings);

                    myLocation = LocationHelper.getLocation(self);
                    if (myLocation != null) {
                        eventDataAdapter.updateLocation(myLocation);
                    }
                } else {
                    eventNoResultHolderRelativeLayout.setVisibility(View.VISIBLE);
                    setNoResultEventAnimation();
                }
            }
        }

        @Override
        protected ArrayList<EventDataBinding> doInBackground(ArrayList<Integer>... arrayLists) {
            ArrayList<Integer> deletedEvents = (ArrayList<Integer>) arrayLists[0];

            ArrayList<EventDataBinding> eventDataBindings = EventHelper.UpdateDeletedEvents(self.eventDataBindings, deletedEvents);

            return eventDataBindings;

        }
    }

    public class UpdateDeletedEventSynchronizationForFavoriteAsyncTask extends AsyncTask<ArrayList<Integer>, Void, ArrayList<EventDataBinding>> {
        @Override
        protected void onPostExecute(ArrayList<EventDataBinding> result) {
            self.my2017Events = result;
            if (actionStatus.equals(GlobalConfig.MainActivityStatus.My2017)) {
                if (self.my2017Events.size() > 0) {
                    eventDataAdapter.updateData(self.my2017Events);
                    eventHolderLinearLayout.setVisibility(View.VISIBLE);
                } else {
                    my2017HolderRelativeLayout.setVisibility(View.VISIBLE);
                    setMyEmpty2017Animation();
                }
            }
        }

        @Override
        protected ArrayList<EventDataBinding> doInBackground(ArrayList<Integer>... arrayLists) {
            ArrayList<Integer> deletedEvents = (ArrayList<Integer>) arrayLists[0];

            ArrayList<EventDataBinding> eventDataBindings = EventHelper.UpdateDeletedEvents(self.my2017Events, deletedEvents);

            return eventDataBindings;
        }
    }

    public class UpdateDeletedEventSynchronizationForSharingDataAsyncTask extends AsyncTask<ArrayList<Integer>, Void, Map<String, ArrayList<EventData>>> {
        @Override
        protected void onPostExecute(Map<String, ArrayList<EventData>> result) {
            self.sharedData = result;
        }

        @Override
        protected Map<String, ArrayList<EventData>> doInBackground(ArrayList<Integer>... arrayLists) {
            ArrayList<Integer> deletedEvents = (ArrayList<Integer>) arrayLists[0];

            Map<String, ArrayList<EventData>> sharingData = EventHelper.UpdateDeletedEvents(self.sharedData, deletedEvents);

            return sharingData;
        }
    }





}
