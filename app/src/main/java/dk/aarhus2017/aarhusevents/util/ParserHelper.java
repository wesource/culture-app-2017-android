package dk.aarhus2017.aarhusevents.util;

import android.text.format.DateUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import dk.aarhus2017.aarhusevents.model.EventModel;
import dk.aarhus2017.aarhusevents.model.GeoLocationModel;
import dk.aarhus2017.aarhusevents.model.LocationModel;
import dk.aarhus2017.aarhusevents.model.OpeningHourModel;
import dk.aarhus2017.aarhusevents.model.PracticalInfoModel;
import dk.aarhus2017.aarhusevents.model.ScheduledEventModel;

/**
 * Created by Dell on 11/28/2016.
 */

public class ParserHelper {

    public static ArrayList<EventModel> ParseToEventObject(JSONArray items)
    {
        ArrayList<EventModel> events = new ArrayList<>();
        int eventId;
        String eventHeadline;
        String dates;
        String startDate;
        String endDate;
        String coordinates;
        String themesList;
        ArrayList<String> artFormList;
        String locationList;
        ArrayList<String> audienceList;
        String price;
        String imageUrl;
        String eventUrl;
        Boolean languageDisabled;
        String descriptionTitle;
        String description;
        String descriptionHtml;
        String lastUpdated;
        Boolean externallyCreated;


        String ticketUrl;
        String languageCulture;
        String languageTwoLetterCode;
        String videoUrl;
        String eventVenueUrl;


        ArrayList<ScheduledEventModel> scheduledEvents;

        PracticalInfoModel practicalInfoModel;
        String contactName;
        String contactPhone;
        String contactEmail;

        ArrayList<OpeningHourModel> openingHourModels;
        String openingHourAlternativeText;

        try {

            for (int i = 0; i < items.length(); i++) {
                JSONObject item = items.getJSONObject(i);
                eventId = item.getInt("EventId");
                eventHeadline = item.getString("EventHeadline");
                dates = item.getString("Dates");
                startDate = item.getString("StartDate");
                endDate = item.getString("EndDate");
                coordinates = item.getString("Coordinates");
                themesList = item.getString("ThemesList");
                artFormList = new ArrayList<>();
                JSONArray artForms=item.getJSONArray("ArtFormList");
                if(artForms.length()>0){
                    for (int iArt=0;iArt < artForms.length();iArt++)
                    {
                        artFormList.add(artForms.getString(iArt));
                    }
                }
                locationList = item.getString("LocationList");
                JSONArray audiences = item.getJSONArray("AudienceList");
                audienceList =new ArrayList<>();
                if(audiences.length()>0) {
                    for (int iAu=0;iAu < audiences.length();iAu++)
                    {
                        audienceList.add(audiences.getString(iAu));
                    }
                }

                price = item.getString("Price");
                imageUrl = item.getString("ImageUrl");
                eventUrl = item.getString("EventUrl");
                languageDisabled = item.getBoolean("LanguageDisabled");
                descriptionTitle = item.getString("DescriptionTitle");
                description = item.getString("Description");
                descriptionHtml = item.getString("DescriptionHTML");
                lastUpdated = item.getString("LastUpdated");
                externallyCreated = item.getBoolean("ExternallyCreated");
                eventVenueUrl = item.getString("EventVenueUrl");


                ticketUrl = item.getString("TicketUrl");
                languageCulture = item.getString("LanguageCulture");
                languageTwoLetterCode = item.getString("LanguageTwoLetterCode");
                videoUrl = item.getString("VideoUrl");

                JSONArray scheduleArrayJson= item.getJSONArray("ScheduledEvents");
                scheduledEvents = new ArrayList<>();
                for (int x = 0; x <scheduleArrayJson.length();x++)
                {
                    JSONObject scheduleJsonObj = scheduleArrayJson.getJSONObject(x);
                    int scheduledEventID = scheduleJsonObj.getInt("Id");
                    String startDateTime = scheduleJsonObj.getString("StartDateTime");
                    String endDateTime =scheduleJsonObj.getString("EndDateTime");
                    String time = scheduleJsonObj.getString("Time");
                    String venue =scheduleJsonObj.getString("Venue");
                    boolean disabilityAccess = scheduleJsonObj.getBoolean("DisabilityAccess");
                    int priceFrom = scheduleJsonObj.getInt("PriceFrom");
                    int priceTo = scheduleJsonObj.getInt("PriceTo");

                    JSONObject locationJson = scheduleJsonObj.getJSONObject("Location");
                    String name = locationJson.getString("Name");
                    String address = locationJson.getString("Address");
                    String zipCity = locationJson.getString("ZipCity");
                    String locationVenue = locationJson.getString("Venue");

                    double latitude =0;
                    double longitude=0;
                    int zoomDistance =0;
                    if(locationJson.isNull("GeoLocation"))
                    {
                        GeoLocationModel geoLocationModel = ConvertHelper.ConvertToGeoLocation(coordinates);
                        latitude = geoLocationModel.getLatitude();
                        longitude = geoLocationModel.getLongitude();
                        zoomDistance = geoLocationModel.getZoom();
                    }
                    else {
                        JSONObject geoLocationJson = locationJson.getJSONObject("GeoLocation");

                        latitude = geoLocationJson.getDouble("Latitude");
                        longitude = geoLocationJson.getDouble("Longitude");
                        zoomDistance = geoLocationJson.getInt("ZoomDistance");
                    }
                    LocationModel locationModel = new LocationModel(name,address,zipCity,venue,latitude,longitude,zoomDistance);

                    boolean soldOUt= scheduleJsonObj.getBoolean("SoldOut");
                    String locationTicketUrl = scheduleJsonObj.getString("TicketUrl");

                    ScheduledEventModel scheduleModel = new ScheduledEventModel(scheduledEventID, startDateTime,endDateTime,time,venue,disabilityAccess,priceFrom,priceTo,locationModel,soldOUt,ticketUrl);

                    try{
                       // SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        scheduleModel.setStartDateTimeFormat(GlobalConfig.FormatDate.parse(startDateTime));
                        scheduleModel.setEndDateTimeFormat(GlobalConfig.FormatDate.parse(endDateTime));
                        scheduleModel.setFullStartDate(GlobalConfig.DateFullFormat.parse(startDateTime));
                        scheduleModel.setFullEndDate(GlobalConfig.DateFullFormat.parse(endDateTime));

                    }catch (Exception ex){
                        Log.e("Parse Time",ex.getMessage());
                    }

                    scheduledEvents.add(scheduleModel);

                }


                JSONObject practicalInfo =item.getJSONObject("PracticalInfo");
                contactName = practicalInfo.getString("ContactName");
                contactPhone=practicalInfo.getString("ContactPhone");
                contactEmail=practicalInfo.getString("ContactEmail");
                practicalInfoModel = new PracticalInfoModel(contactName,contactPhone,contactEmail);

                EventModel model = new EventModel(eventId,eventHeadline,dates,coordinates,themesList,artFormList,locationList,audienceList,price,imageUrl,eventUrl,languageDisabled,
                        descriptionTitle,description,ticketUrl,languageCulture,languageTwoLetterCode,videoUrl, scheduledEvents ,practicalInfoModel);
                model.setDescriptionHtml(descriptionHtml);
                model.setLastUpdated(lastUpdated);
                model.setExternallyCreated(externallyCreated);
                model.setStartDate(startDate);
                model.setEndDate(endDate);
                model.setEventVenueUrl(eventVenueUrl);

                // Opening hours
                openingHourModels =new ArrayList<>();
                JSONArray openingHoursArrayJson= item.getJSONArray("OpeningHours");
                if(openingHoursArrayJson.length()>0){
                    for(int o = 0;o <openingHoursArrayJson.length();o++){
                        JSONObject openingHourObject = openingHoursArrayJson.getJSONObject(o);
                        int weekdayId = openingHourObject.getInt("WeekdayId");
                        String weekdayLabel = openingHourObject.getString("WeekdayLabel");

                        JSONArray openingTimes = openingHourObject.getJSONArray("OpeningTimes");
                        if(openingTimes.length()>0){
                            JSONObject openingTimeObject = openingTimes.getJSONObject(0);

                            String opens=openingTimeObject.getString("Opens");
                            String closes=openingTimeObject.getString("Closes");
                            boolean isClosed = openingTimeObject.getBoolean("IsClosed");
                            String closeString =openingTimeObject.getString("ClosedString");

                            OpeningHourModel openingHourModel = new OpeningHourModel(weekdayId,weekdayLabel,opens,closes,isClosed,closeString);
                            openingHourModels.add(openingHourModel);
                        }
                    }
                }
                model.setOpeningHourModels(openingHourModels);

                //
                openingHourAlternativeText = item.getString("OpeningHoursAlternativeText");
                model.setOpeningHoursAlternativeText(openingHourAlternativeText);

                events.add(model);
            }
        }catch (Exception ex){
            Log.e("Parse Exception",ex.getMessage());
        }
        return events;
    }

    public static ArrayList<Integer> ParseToDeletedEvents(JSONArray items){
        ArrayList<Integer> events=new ArrayList<>();
        try {
            for (int i=0;i<items.length();i++){
                JSONObject item = items.getJSONObject(i);
                events.add(item.getInt("NodeId"));
            }
        }catch (Exception ex){

        }

        return events;
    }
}
