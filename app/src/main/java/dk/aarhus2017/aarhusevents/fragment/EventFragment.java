package dk.aarhus2017.aarhusevents.fragment;


import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.activities.EventActivity;
import dk.aarhus2017.aarhusevents.activities.FilterActivity;
import dk.aarhus2017.aarhusevents.adapter.EventDataAdapter;
import dk.aarhus2017.aarhusevents.adapter.MenuAdapter;
import dk.aarhus2017.aarhusevents.model.EndlessRecyclerOnScrollListener;
import dk.aarhus2017.aarhusevents.model.EventDataBinding;
import dk.aarhus2017.aarhusevents.model.EventModel;
import dk.aarhus2017.aarhusevents.model.GeoLocationModel;
import dk.aarhus2017.aarhusevents.model.LocationModel;
import dk.aarhus2017.aarhusevents.model.MenuModel;
import dk.aarhus2017.aarhusevents.model.My2017EventType;
import dk.aarhus2017.aarhusevents.model.PermissionResult;
import dk.aarhus2017.aarhusevents.model.PromptPermissionResult;
import dk.aarhus2017.aarhusevents.notification.InitDataBroadcastReceiver;
import dk.aarhus2017.aarhusevents.services.DataSynchronizationService;
import dk.aarhus2017.aarhusevents.util.ConvertHelper;
import dk.aarhus2017.aarhusevents.util.EventHelper;
import dk.aarhus2017.aarhusevents.util.FBTrackerHelper;
import dk.aarhus2017.aarhusevents.util.FontHelper;
import dk.aarhus2017.aarhusevents.util.GATrackerHelper;
import dk.aarhus2017.aarhusevents.util.GeneralHelper;
import dk.aarhus2017.aarhusevents.util.GlobalConfig;
import dk.aarhus2017.aarhusevents.util.GlobalConstant;
import dk.aarhus2017.aarhusevents.util.InternetHelper;
import dk.aarhus2017.aarhusevents.util.KeyboardHelper;
import dk.aarhus2017.aarhusevents.util.LocationHelper;
import dk.aarhus2017.aarhusevents.util.PermissionHelper;
import dk.aarhus2017.aarhusevents.util.SharedPreferenceHelper;

import static android.app.Activity.RESULT_OK;


public class EventFragment extends Fragment implements View.OnClickListener {

    // Header
    TextView eventHeaderTotalLabelTextView;
    TextView eventHeaderTotalTextView;
    TextView eventHeaderSelectedMenuItemTextView;
    Button eventHeaderFilterButton;
    ImageButton eventHeaderMenuIconImageButton;

    // Internet
    LinearLayout internetHolderLinearLayout;
    TextView internetTextView;


    // Loading
    RelativeLayout loadingHolderRelativeLayout;

    // Content
    // SwipeRefreshLayout eventFragmentSwipeRefreshLayout;
    RecyclerView eventListFragmentRecyclerView;
    LinearLayoutManager linearLayoutManager;
    EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;
    Date loadMoreDate;
    Boolean isLoadingMoreData = false;

    // No Result Event
    RelativeLayout noResultHolderRelativeLayout;
    ImageView noResultImageView;
    LinearLayout noResultTextHolderLinearLayout;
    Button noResultActionButton;
    TextView noResultHeadlineTextView;
    TextView noResultDescriptionTextView;

    // Loading more
    LinearLayout loadingMoreHolderRelativeLayout;
    TextView loadingMoreTextView;

    // Pull down
//    LinearLayout pullDownHolderLinearLayout;
//    TextView pullDownTextView;


    Context context = getContext();
    EventFragment self = this;

    ArrayList<MenuModel> menuItems = new ArrayList<>();
    ListPopupWindow listPopupWindow;


    EventDataAdapter eventDataAdapter;
    EventDataAdapter.EventListener eventListener;

    ArrayList<EventDataBinding> eventList = new ArrayList<>();
    Paint paint = new Paint();


    static Date fromDate = new Date();
    static Date toDate = new Date();
    static Set<String> artformsFilter;
    static Set<String> audienceFilter;
    static int distanceFilter;
    static String cityFilter;
    static GeoLocationModel locationFilter;
    static String eventType = "All";
    int selectedEventType = 0;
    int animationIndex = 1;


    private OnEventFragmentInteractionListener mListener;

    public EventFragment() {

    }


    // TODO: Rename and change types and number of parameters
    public static EventFragment newInstance(Context context) {
        EventFragment fragment = new EventFragment();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //   mParam1 = getArguments().getString(ARG_PARAM1);
            //  mParam2 = getArguments().getString(ARG_PARAM2);
        }
        context = getActivity().getApplicationContext();
        menuItems = GeneralHelper.GetMenuItems(context);
        self = this;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        paint.setColor(Color.parseColor("#FF1E5A"));
        context = getActivity().getApplicationContext();


        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_event, container, false);

        // Header
        eventHeaderTotalLabelTextView = (TextView) v.findViewById(R.id.eventHeaderTotalLabelTextView);
        eventHeaderTotalTextView = (TextView) v.findViewById(R.id.eventHeaderTotalTextView);
        eventHeaderSelectedMenuItemTextView = (TextView) v.findViewById(R.id.eventHeaderSelectedMenuItemTextView);
        eventHeaderFilterButton = (Button) v.findViewById(R.id.eventHeaderFilterButton);
        eventHeaderMenuIconImageButton = (ImageButton) v.findViewById(R.id.eventHeaderMenuIconImageButton);

        // Internet Status
        internetHolderLinearLayout = (LinearLayout) v.findViewById(R.id.internetHolderLinearLayout);
        internetTextView = (TextView) v.findViewById(R.id.internetTextView);

        // Loading
        loadingHolderRelativeLayout = (RelativeLayout) v.findViewById(R.id.loadingHolderRelativeLayout);

        // Content
        //   eventFragmentSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.eventFragmentSwipeRefreshLayout);
        eventListFragmentRecyclerView = (RecyclerView) v.findViewById(R.id.eventListFragmentRecyclerView);


        // No Result Event
        noResultHolderRelativeLayout = (RelativeLayout) v.findViewById(R.id.noResultHolderRelativeLayout);
        noResultImageView = (ImageView) v.findViewById(R.id.noResultImageView);
        noResultTextHolderLinearLayout = (LinearLayout) v.findViewById(R.id.noResultTextHolderLinearLayout);
        noResultActionButton = (Button) v.findViewById(R.id.noresultActionButton);
        noResultHeadlineTextView = (TextView) v.findViewById(R.id.noResultHeadlineTextView);
        noResultDescriptionTextView = (TextView) v.findViewById(R.id.noResultDescriptionTextView);


        // Loading more
        loadingMoreHolderRelativeLayout = (LinearLayout) v.findViewById(R.id.loadingMoreHolderRelativeLayout);
        loadingMoreTextView = (TextView) v.findViewById(R.id.loadingMoreTextView);


        // Hide
        loadingHolderRelativeLayout.setVisibility(View.VISIBLE);
        eventListFragmentRecyclerView.setVisibility(View.GONE);
        noResultHolderRelativeLayout.setVisibility(View.GONE);
        loadingMoreHolderRelativeLayout.setVisibility(View.GONE);


        // Register Event
        eventHeaderSelectedMenuItemTextView.setOnClickListener(this);
        eventHeaderMenuIconImageButton.setOnClickListener(this);
        eventHeaderFilterButton.setOnClickListener(this);
        noResultActionButton.setOnClickListener(this);

        // Set text
        noResultHeadlineTextView.setText(getString(R.string.event_noresult_headline));
        noResultDescriptionTextView.setText(getString(R.string.event_noresult_description));
        noResultActionButton.setText(getString(R.string.event_use_filter_button));

        if (InternetHelper.HasInternetConnection(context)) {
            internetHolderLinearLayout.setVisibility(View.GONE);
        } else {
            internetHolderLinearLayout.setVisibility(View.VISIBLE);
        }

        eventDataAdapter = new EventDataAdapter(getContext(), eventList,true);
        eventListener = new EventDataAdapter.EventListener() {
            @Override
            public void onRowClick(EventDataBinding rowData) {
                if (mListener != null) {
                    mListener.onClickEventFragment(rowData, 0, selectedEventType);
                }
            }
        };
        eventDataAdapter.setTaskListener(eventListener);


        eventListFragmentRecyclerView.setAdapter(eventDataAdapter);
        linearLayoutManager = new LinearLayoutManager(this.getContext());

        eventListFragmentRecyclerView.setLayoutManager(linearLayoutManager);
        eventListFragmentRecyclerView.setItemAnimator(new DefaultItemAnimator());
        eventListFragmentRecyclerView.setDrawingCacheEnabled(true);
        eventListFragmentRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        eventListFragmentRecyclerView.setHasFixedSize(false);

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                EventDataBinding rowData = self.eventList.get(position);

                if (rowData.isGroupHeader()) {
                    return;
                }

                switch (direction) {
                    case ItemTouchHelper.LEFT: {
                        if (rowData.getEventData().getFavoriteEvent()) {
                            rowData.getEventData().setFavoriteEvent(false);

                            eventDataAdapter.updateData(self.eventList);
                            if (mListener != null) {
                                mListener.onRemoveFavoriteEventFragment(rowData, 0);
                            }
                        }
                        break;
                    }
                    case ItemTouchHelper.RIGHT: {
                        if (!rowData.isGroupHeader()) {
                            if (!rowData.getEventData().getFavoriteEvent()) {
                                rowData.getEventData().setFavoriteEvent(true);
                                eventDataAdapter.updateData(self.eventList);
                                if (mListener != null) {
                                    mListener.onAddFavoriteEventFragment(rowData, 0);
                                }
                            }
                        }
                        break;
                    }
                    default:
                        break;
                }
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                EventDataBinding rowData = self.eventList.get(viewHolder.getAdapterPosition());
                if (!rowData.isGroupHeader()) {
                    if (rowData.getEventData().getFavoriteEvent()) {
                        return ItemTouchHelper.LEFT;
                    } else {
                        return ItemTouchHelper.RIGHT;
                    }
                } else {
                    return super.getSwipeDirs(recyclerView, viewHolder);
                }
            }

            @Override
            public void onChildDrawOver(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                if (!isCurrentlyActive) {
                    LinearLayout eventItemContentHolderLinearLayout = (LinearLayout) viewHolder.itemView.findViewById(R.id.eventItemContentHolderLinearLayout);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                    int margin = ConvertHelper.ConvertDpToPx(self.getContext(), 10);
                    layoutParams.setMarginStart(margin);
                    layoutParams.setMarginEnd(margin);
                    eventItemContentHolderLinearLayout.setLayoutParams(layoutParams);

                    super.onChildDrawOver(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }
            }


            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                int position = viewHolder.getAdapterPosition();
                EventDataBinding rowData = self.eventList.get(position);

                if (rowData.isGroupHeader()) {
                    return;
                }

                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;
                    LinearLayout seperateLine = (LinearLayout) viewHolder.itemView.findViewById(R.id.eventItemSeperationLinearLayout);
                    int seperateLineHeight = 0;
                    if (seperateLine.getVisibility() == View.VISIBLE) {
                        seperateLineHeight = seperateLine.getHeight();
                    } else {
                        seperateLineHeight = 0;
                    }

                    LinearLayout eventItemContentHolderLinearLayout = (LinearLayout) viewHolder.itemView.findViewById(R.id.eventItemContentHolderLinearLayout);

                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    if (rowData.getEventData().getFavoriteEvent()) {

                        if (dX < 0) {
                            layoutParams.setMarginEnd(0);
                            eventItemContentHolderLinearLayout.setLayoutParams(layoutParams);

                            RectF background = new RectF(itemView.getWidth() - Math.abs(dX), (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom() - seperateLineHeight);
                            c.drawRect(background, paint);

                            icon = BitmapFactory.decodeResource(self.getResources(), R.drawable.remove_saved_favorite);
                            int top = (itemView.getBottom() - itemView.getTop() - seperateLineHeight - icon.getHeight()) / 2;
                            int padding = ConvertHelper.ConvertDpToPx(self.getContext(), 30);
                            RectF icon_dest = new RectF(itemView.getWidth() - Math.abs(dX) + padding, (float) itemView.getTop() + top, itemView.getWidth() - Math.abs(dX) + padding + icon.getWidth(), (float) itemView.getBottom() - seperateLineHeight - top);
                            c.drawBitmap(icon, null, icon_dest, paint);


                            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

                        } else {
                            c = new Canvas();
                            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        }

                    } else {
                        if (dX > 0) {
                            layoutParams.setMarginStart(0);
                            eventItemContentHolderLinearLayout.setLayoutParams(layoutParams);
                            RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom() - seperateLineHeight);
                            c.drawRect(background, paint);
                            if (dX > 50) {
                                icon = BitmapFactory.decodeResource(self.getResources(), R.drawable.save_favorite);

                                int top = (itemView.getBottom() - itemView.getTop() - seperateLineHeight - icon.getHeight()) / 2;
                                int padding = ConvertHelper.ConvertDpToPx(self.getContext(), 30);

                                RectF icon_dest = new RectF(dX - padding - icon.getWidth(), (float) itemView.getTop() + top, dX - padding, (float) itemView.getBottom() - seperateLineHeight - top);
                                c.drawBitmap(icon, null, icon_dest, paint);
                            }

                            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

                        } else {

                            c = new Canvas();
                            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        }
                    }
                }
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(eventListFragmentRecyclerView);


        SetFonts();

        // Show menuitem
        ShowMenuItem();


        long fDate = SharedPreferenceHelper.GetFromDateFilter(context);
        final long tDate = SharedPreferenceHelper.GetToDateFilter(context);
        if (fDate > 0 & tDate > 0) {
            fromDate.setTime(fDate);
            toDate.setTime(tDate);
        } else {
            fromDate = new Date();
            toDate.setTime(new Date().getTime() + (14 * 24 * 60 * 60 * 1000));
        }
        artformsFilter = SharedPreferenceHelper.GetArtformFilter(context);
        audienceFilter = SharedPreferenceHelper.GetAudienceFilter(context);
        cityFilter = SharedPreferenceHelper.GetCityFilter(context);
        locationFilter = SharedPreferenceHelper.GetLocationFilter(context);
        distanceFilter = SharedPreferenceHelper.GetLocationDistanceFilter(context);

        if (self.loadMoreDate == null) {
            self.loadMoreDate = new Date();
            self.loadMoreDate.setTime(toDate.getTime());
        }


        if (GlobalConfig.SharedEvents != null) {
            new LoadEventAsyncTask().execute(My2017EventType.SharedData, toDate);
        }


        RegisterLoadMoreDataOnScroll();


        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        GATrackerHelper.SendGAScreenView(GATrackerHelper.EventListScreen, self.getActivity());

        FBTrackerHelper.LogScreenView(self.getContext(), GATrackerHelper.EventListScreen);
    }

    private void LoadEvent() {


        new LoadEventAsyncTask().execute(My2017EventType.SharedData, self.loadMoreDate);
    }

    public void FilterEvent() {
        long fDate = SharedPreferenceHelper.GetFromDateFilter(context);
        final long tDate = SharedPreferenceHelper.GetToDateFilter(context);
        if (fDate > 0 & tDate > 0) {
            fromDate.setTime(fDate);
            toDate.setTime(tDate);
        } else {
            fromDate = new Date();
            toDate.setTime(new Date().getTime() + (14 * 24 * 60 * 60 * 1000));
        }
        artformsFilter = SharedPreferenceHelper.GetArtformFilter(context);
        audienceFilter = SharedPreferenceHelper.GetAudienceFilter(context);
        cityFilter = SharedPreferenceHelper.GetCityFilter(context);
        locationFilter = SharedPreferenceHelper.GetLocationFilter(context);
        distanceFilter = SharedPreferenceHelper.GetLocationDistanceFilter(context);

        if (self.loadMoreDate != null) {
            self.loadMoreDate.setTime(toDate.getTime());
        }

        new LoadEventAsyncTask().execute(My2017EventType.SharedData, toDate);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.noresultActionButton:
            case R.id.eventHeaderFilterButton: {
                GATrackerHelper.SendGAAction("Event Filter", self.getActivity());
                Intent filterIntent = new Intent(getActivity(), FilterActivity.class);
                getActivity().startActivityForResult(filterIntent, GlobalConstant.StartActivityForFilterResult);
                break;
            }
            case R.id.eventHeaderSelectedMenuItemTextView:
            case R.id.eventHeaderMenuIconImageButton: {
                listPopupWindow.setAnchorView(eventHeaderMenuIconImageButton);
                listPopupWindow.setWidth(800);
                listPopupWindow.show();
                break;
            }
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnEventFragmentInteractionListener) {
            mListener = (OnEventFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private class LoadEventAsyncTask extends AsyncTask<Object, Void, ArrayList<EventDataBinding>> {
        @Override
        protected ArrayList<EventDataBinding> doInBackground(Object... objects) {

            ArrayList<EventDataBinding> bindings = new ArrayList<>();
            My2017EventType type = (My2017EventType) objects[0];
            switch (type) {
                case SharedData: {
                    Date toDate = (Date) objects[1];
                    bindings = EventHelper.ConvertToFilterEventData(context, GlobalConfig.SharedEvents, eventType, fromDate, toDate, artformsFilter, audienceFilter, locationFilter, distanceFilter);
                    break;
                }
                case LoadMoreData: {
                    try {
                        Thread.sleep(400);
                    } catch (Exception ex) {
                    }
                    Date extendDate = (Date) objects[1];
                    bindings = EventHelper.ConvertToFilterEventData(context, GlobalConfig.SharedEvents, eventType, fromDate, extendDate, artformsFilter, audienceFilter, locationFilter, distanceFilter);
                    break;
                }
            }

            return bindings;
        }

        @Override
        protected void onPostExecute(ArrayList<EventDataBinding> bindings) {
            super.onPostExecute(bindings);
            self.eventList = bindings;
            int totalEvent = GeneralHelper.GetTotalEvent(self.eventList);
            eventHeaderTotalTextView.setText(" " + String.valueOf(totalEvent));

            loadingHolderRelativeLayout.setVisibility(View.GONE);
            loadingMoreHolderRelativeLayout.setVisibility(View.GONE);

            if (self.eventList.size() > 0) {
                eventListFragmentRecyclerView.setVisibility(View.VISIBLE);
                noResultHolderRelativeLayout.setVisibility(View.GONE);
                eventDataAdapter.updateData(self.eventList);
                Location myLocation = LocationHelper.getLocation(context);
                if (myLocation != null) {
                    eventDataAdapter.updateLocation(myLocation);
                }
            } else {
                eventListFragmentRecyclerView.setVisibility(View.GONE);
                noResultHolderRelativeLayout.setVisibility(View.VISIBLE);
                setNoResultEventAnimation();
            }

            if (isLoadingMoreData) {
                RegisterLoadMoreDataOnScroll();
            }
        }
    }

    private class UpdateEventAsyncTask extends AsyncTask<Object, Void, ArrayList<EventDataBinding>> {
        @Override
        protected ArrayList<EventDataBinding> doInBackground(Object... objects) {
            return EventHelper.ConvertToFilterEventData(context, GlobalConfig.SharedEvents, eventType, fromDate, self.loadMoreDate, artformsFilter, audienceFilter, locationFilter, distanceFilter);
        }

        @Override
        protected void onPostExecute(ArrayList<EventDataBinding> bindings) {
            super.onPostExecute(bindings);
            self.eventList = bindings;
            int totalEvent = GeneralHelper.GetTotalEvent(self.eventList);
            eventHeaderTotalTextView.setText(" " + String.valueOf(totalEvent));

            loadingHolderRelativeLayout.setVisibility(View.GONE);
            loadingMoreHolderRelativeLayout.setVisibility(View.GONE);

            if (self.eventList.size() > 0) {
                eventListFragmentRecyclerView.setVisibility(View.VISIBLE);
                noResultHolderRelativeLayout.setVisibility(View.GONE);
                eventDataAdapter.updateData(self.eventList);
                Location myLocation = LocationHelper.getLocation(context);
                if (myLocation != null) {
                    eventDataAdapter.updateLocation(myLocation);
                }
            } else {
                eventListFragmentRecyclerView.setVisibility(View.GONE);
                noResultHolderRelativeLayout.setVisibility(View.VISIBLE);
                setNoResultEventAnimation();
            }

            if (isLoadingMoreData) {
                RegisterLoadMoreDataOnScroll();
            }
        }
    }

    private void SetFonts() {

        Typeface sourceSansProRegular = FontHelper.SourceSansProRegular(context);
        Typeface sourceSansProBold = FontHelper.SourceSansProBold(context);
        Typeface flavinBold = FontHelper.FlavinBold(context);
        Typeface flavinRegular = FontHelper.FlavinRegular(context);
        // Header
        eventHeaderTotalLabelTextView.setTypeface(sourceSansProRegular);
        eventHeaderTotalTextView.setTypeface(sourceSansProRegular);
        eventHeaderSelectedMenuItemTextView.setTypeface(sourceSansProBold);
        eventHeaderFilterButton.setTypeface(sourceSansProBold);

        // Internet
        internetTextView.setTypeface(sourceSansProRegular);

        // No Result
        noResultHeadlineTextView.setTypeface(flavinBold);
        noResultDescriptionTextView.setTypeface(flavinRegular);
        noResultActionButton.setTypeface(sourceSansProBold);

        // Loading
        loadingMoreTextView.setTypeface(sourceSansProRegular);
    }

    private void setNoResultEventAnimation() {

        noResultImageView.setImageResource(R.drawable.ic_empty_my2017_0);
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.empty2017_fade_in);
        noResultActionButton.startAnimation(animation);

        Animation animationText = AnimationUtils.loadAnimation(context, R.anim.empty2017_fade_in);
        animationText.setStartOffset(300);
        noResultTextHolderLinearLayout.startAnimation(animationText);

        final Animation animationImage = AnimationUtils.loadAnimation(context, R.anim.empty2017_fade_in);
        animationImage.setStartOffset(100);


        final Animation animationLoop = AnimationUtils.loadAnimation(context, R.anim.empty2017_fade);
        animationLoop.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (animationIndex < 8) {
                    noResultImageView.clearAnimation();
                    String uri = "@drawable/ic_empty_my2017_" + animationIndex;
                    int drawableID = getResources().getIdentifier(uri, null, context.getPackageName());
                    noResultImageView.setImageResource(drawableID);
                    noResultImageView.startAnimation(animationLoop);
                    animationIndex++;
                } else {
                    noResultImageView.clearAnimation();
                    animationIndex = 0;
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationImage.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                noResultImageView.startAnimation(animationLoop);
                animationIndex = 1;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        noResultImageView.startAnimation(animationImage);


    }

    private void RegisterLoadMoreDataOnScroll() {
        isLoadingMoreData = false;
        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager, self.loadMoreDate) {
            @Override
            public void onLoadMore(Date endDate) {
                isLoadingMoreData = true;
                loadingMoreHolderRelativeLayout.setVisibility(View.VISIBLE);
                if (self.eventList.size() > 1) {
                    eventListFragmentRecyclerView.scrollToPosition(self.eventList.size() - 1);
                }
                self.loadMoreDate.setTime(endDate.getTime());
                eventListFragmentRecyclerView.removeOnScrollListener(endlessRecyclerOnScrollListener);
                new LoadEventAsyncTask().execute(My2017EventType.LoadMoreData, self.loadMoreDate);
            }
        };

        eventListFragmentRecyclerView.addOnScrollListener(endlessRecyclerOnScrollListener);
    }


    // Exchange data with Main Activity
    public void ChangedInternetStatus(boolean isConnected) {
        if (internetHolderLinearLayout != null) {
            if (isConnected) {
                internetHolderLinearLayout.setVisibility(View.GONE);
            } else {
                internetHolderLinearLayout.setVisibility(View.VISIBLE);
            }
        }
    }


    // Communicate with Main Activity
    public void Reload() {

        if (context != null) {
            // Hide keyboard
            if (self.getView() != null) {
                KeyboardHelper.HideKeyboard(self.getView(), context);
            }
            // Show internet status
            if (InternetHelper.HasInternetConnection(context)) {
                internetHolderLinearLayout.setVisibility(View.GONE);
            } else {
                internetHolderLinearLayout.setVisibility(View.VISIBLE);
            }
            // Load Events
            LoadEvent();
        }
    }


    public void UpdateDataChanged() {
        if (context != null && self.getView() != null) {
            KeyboardHelper.HideKeyboard(self.getView(), context);
        }
        if (GlobalConfig.SharedEvents != null) {
            new UpdateEventAsyncTask().execute();
        }
    }

    private void ShowMenuItem() {

        listPopupWindow = new ListPopupWindow(context);
        MenuAdapter.MenuListener menuListener = new MenuAdapter.MenuListener() {
            @Override
            public void onMenuItemChanged(MenuModel rowData) {

                listPopupWindow.dismiss();
                eventHeaderSelectedMenuItemTextView.setText(rowData.getName());
                rowData.setSelected(true);

                switch (rowData.getId()) {
                    case "All": {
                        eventType = "All";
                        selectedEventType = 0;
                        break;
                    }
                    case "Office": {
                        eventType = "OnlyBackendCreated";
                        selectedEventType = 1;
                        break;
                    }
                    case "UserSubmitted": {
                        eventType = "OnlyExternallyCreated";
                        selectedEventType = 2;
                        break;
                    }
                    default:
                        break;

                }


                loadingHolderRelativeLayout.setVisibility(View.VISIBLE);
                eventListFragmentRecyclerView.setVisibility(View.GONE);
                noResultHolderRelativeLayout.setVisibility(View.GONE);
                loadingMoreHolderRelativeLayout.setVisibility(View.GONE);

                new LoadEventAsyncTask().execute(My2017EventType.SharedData, self.loadMoreDate);
                GATrackerHelper.SelectEventType(self.getActivity(), selectedEventType);

            }
        };
        final MenuAdapter menuAdapter = new MenuAdapter(context, menuItems);
        menuAdapter.setTaskListener(menuListener);
        listPopupWindow.setAdapter(menuAdapter);

        eventHeaderSelectedMenuItemTextView.setText(GeneralHelper.GetSelectedMenuItem(self.getActivity(), menuItems));
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnEventFragmentInteractionListener {
        // TODO: Update argument type and name
        void onAddFavoriteEventFragment(EventDataBinding value, int tabPosition);

        void onRemoveFavoriteEventFragment(EventDataBinding value, int tabPosition);

        void onClickEventFragment(EventDataBinding value, int tabPosition, int selectedEventType);
    }
}
