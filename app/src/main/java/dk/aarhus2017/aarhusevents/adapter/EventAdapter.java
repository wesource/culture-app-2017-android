package dk.aarhus2017.aarhusevents.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.hardware.TriggerEvent;
import android.location.Location;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;



import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.model.EventData;
import dk.aarhus2017.aarhusevents.model.EventDataBinding;
import dk.aarhus2017.aarhusevents.model.EventModel;
import dk.aarhus2017.aarhusevents.model.OnSwipeTouchListener;
import dk.aarhus2017.aarhusevents.util.FontHelper;

/**
 * Created by Dell on 11/12/2016.
 */

public class EventAdapter extends ArrayAdapter<EventDataBinding> {
    Context context;
    ArrayList<EventDataBinding> events = new ArrayList<>();

    protected EventListener eventAdapter;
    int index = 0;
    Location myLocation;

    private float initialX = 0;
    private float initialY = 0;

    private float finalX = 0;
    private float finalY = 0;

    public EventAdapter(Context context, ArrayList<EventDataBinding> data) {
        super(context, -1, data);
        this.context = context;
        this.events = data;


    }


    public void updateData(ArrayList<EventDataBinding> data) {
        this.events = data;
        notifyDataSetChanged();
    }

    public void updateLocation(Location _myLocation) {
        this.myLocation = _myLocation;
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return events.size();
    }

    @Override
    public void add(EventDataBinding object) {
        events.add(object);
        super.add(object);
    }

    @Nullable
    @Override
    public EventDataBinding getItem(int position) {
        return events.get(position);
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row;
        final EventDataBinding dataRow = getItem(position);

        final Typeface sourceSansProRegular = FontHelper.SourceSansProRegular(context);
        final Typeface sourceSansProBold = FontHelper.SourceSansProBold(context);
        final Typeface flavinRegular = FontHelper.FlavinRegular(context);
        final Typeface flavinBold = FontHelper.FlavinBold(context);

        if (!dataRow.isGroupHeader()) {
            row = inflater.inflate(R.layout.event_item, parent, false);
            final LinearLayout eventItemHolderLinearLayout = (LinearLayout) row.findViewById(R.id.eventItemHolderLinearLayout);

            TextView eventItemEventNameTextView = (TextView) row.findViewById(R.id.eventItemEventNameTextView);
            TextView eventItemDescriptionTextView = (TextView) row.findViewById(R.id.eventItemDescriptionTextView);
            TextView eventItemTimeTextView = (TextView) row.findViewById(R.id.eventItemTimeTextView);
            TextView eventItemDistanceTextView = (TextView) row.findViewById(R.id.eventItemDistanceTextView);
            LinearLayout eventItemSeperationLinearLayout = (LinearLayout) row.findViewById(R.id.eventItemSeperationLinearLayout);
            final ImageView eventItemEventImageImageView = (ImageView) row.findViewById(R.id.eventItemEventImageImageView);

            // Set fonts
            eventItemEventNameTextView.setTypeface(flavinBold);
            eventItemDescriptionTextView.setTypeface(sourceSansProRegular);
            eventItemTimeTextView.setTypeface(sourceSansProBold);
            eventItemDistanceTextView.setTypeface(sourceSansProBold);

            // Set values
            eventItemEventNameTextView.setText(dataRow.getEventData().getEventHeadline());
            eventItemDescriptionTextView.setText(dataRow.getEventData().getDescription());

            if(dataRow.getEventData().getTime().equals("00:00"))
            {
                eventItemTimeTextView.setText(context.getString(R.string.event_allday));
            }
            else
            {
                eventItemTimeTextView.setText(dataRow.getEventData().getTime());
            }


            try {
                //Picasso.with(context).load(dataRow.getEventData().getImageUrl()).resize(100, 100).into(eventItemEventImageImageView);
            } catch (Exception ex) {
                Log.e("Load Image Exception", ex.getMessage());
            }

            // Make a seperation for next item
            if ((position + 1) < getCount()) {
                EventDataBinding nextItem = getItem(position + 1);
                if (!nextItem.isGroupHeader()) {
                    eventItemSeperationLinearLayout.setVisibility(View.VISIBLE);
                }
            }

            final ImageView eventItemFavoriteImageView = (ImageView) row.findViewById(R.id.eventItemFavoriteImageView);


            if (dataRow.getEventData().getFavoriteEvent()) {
                eventItemFavoriteImageView.setVisibility(View.VISIBLE);
            }

            if (myLocation != null) {
                LinearLayout eventLocationGroupPlaceHolder = (LinearLayout) row.findViewById(R.id.eventLocationGroupPlaceHolder);
                eventLocationGroupPlaceHolder.setVisibility(View.VISIBLE);
                final Location toLocation = new Location("");
                toLocation.setLatitude(dataRow.getEventData().getLocationModel().getLatitude());
                toLocation.setLatitude(dataRow.getEventData().getLocationModel().getLatitude());
                dataRow.getEventData().getLocationModel().setLocation(myLocation);
                eventItemDistanceTextView.setText(String.format("%.1f KM", dataRow.getEventData().getLocationModel().getDistance()));
            }


//            eventItemHolderLinearLayout.setOnTouchListener(new OnSwipeTouchListener(row.getContext()) {
//                @Override
//                public void onSwipeLeft() {
//                    if (favoriteEventPlaceHolder.getVisibility() == View.VISIBLE) {
//                        favoriteEventPlaceHolder.setVisibility(View.GONE);
//                    } else {
//                        if (dataRow.getEventData().getFavoriteEvent()) {
//                            eventItemRemoveFavoriteHolderLinearLayout.setVisibility(View.VISIBLE);
//                        }
//                    }
//                    Log.e("Swipe", "Left");
//                }
//
//                @Override
//                public void onSwipeRight() {
//                    if (eventItemRemoveFavoriteHolderLinearLayout.getVisibility() == View.VISIBLE) {
//                        eventItemRemoveFavoriteHolderLinearLayout.setVisibility(View.GONE);
//                    } else {
//                        if (!dataRow.getEventData().getFavoriteEvent()) {
//                            favoriteEventPlaceHolder.setVisibility(View.VISIBLE);
//                        }
//                    }
//                    //  imageView.animate().translationX(80).setDuration(500).start();
//                  //  eventItemHolderLinearLayout.setMa .setPadding(10,0,0,0);
//
//
//                    Log.e("Swipe", "Right");
//                }
//
//                @Override
//                public void onClick() {
//                    Log.e("Swipe", "Click");
//                    if (eventAdapter != null) {
//                        eventAdapter.onRowClick(dataRow);
//                    }
//                }
//            });
//
//            eventItemRemoveFavoriteHolderLinearLayout.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    dataRow.getEventData().setFavoriteEvent(false);
//                    if (eventAdapter != null) {
//                        eventAdapter.onRemoveFavorite(dataRow);
//                    }
//                    eventItemRemoveFavoriteHolderLinearLayout.setVisibility(View.GONE);
//                    eventItemFavoriteImageView.setVisibility(View.GONE);
//                    // notifyDataSetChanged();
//                }
//            });
//
//            favoriteEventPlaceHolder.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    dataRow.getEventData().setFavoriteEvent(true);
//                    if (eventAdapter != null) {
//                        eventAdapter.onSaveFavorite(dataRow);
//                    }
//                    favoriteEventPlaceHolder.setVisibility(View.GONE);
//                    eventItemFavoriteImageView.setVisibility(View.VISIBLE);
//                    //notifyDataSetChanged();
//                }
//            });

        } else {
            row = inflater.inflate(R.layout.event_item, parent, false);
            TextView eventItemSectionNameTextView = (TextView) row.findViewById(R.id.eventItemSectionNameTextView);
            eventItemSectionNameTextView.setText(new SimpleDateFormat("EEEE dd. MMMM yyyy").format(dataRow.getDate()));
            eventItemSectionNameTextView.setTypeface(sourceSansProBold);
        }
        return row;
    }

    public void setTaskListener(EventListener listener) {
        eventAdapter = listener;
    }

    public interface EventListener {
        void onRowClick(EventDataBinding rowData);

        void onSaveFavorite(EventDataBinding rowData);

        void onRemoveFavorite(EventDataBinding rowData);
    }
}
