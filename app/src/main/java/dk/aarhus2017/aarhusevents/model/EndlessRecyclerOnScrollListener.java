package dk.aarhus2017.aarhusevents.model;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.Date;

import dk.aarhus2017.aarhusevents.util.GlobalConfig;

import static android.support.v7.widget.RecyclerView.SCROLL_STATE_DRAGGING;
import static android.support.v7.widget.RecyclerView.SCROLL_STATE_IDLE;
import static android.support.v7.widget.RecyclerView.SCROLL_STATE_SETTLING;
import static android.support.v7.widget.RecyclerView.TOUCH_SLOP_DEFAULT;
import static android.support.v7.widget.RecyclerView.TOUCH_SLOP_PAGING;

/**
 * Created by Dell on 3/4/2017.
 */

public abstract class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {

    int firstVisibleItem, totalItemCount;

    private Date endDate;
    private int y;
    private int time;

    private LinearLayoutManager mLinearLayoutManager;

    public EndlessRecyclerOnScrollListener(LinearLayoutManager _linearLayoutManager, Date _endDate) {
        this.mLinearLayoutManager = _linearLayoutManager;
        this.endDate = _endDate;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        y = dy;
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        totalItemCount = mLinearLayoutManager.getItemCount();
        firstVisibleItem = mLinearLayoutManager.findLastVisibleItemPosition();
        if(newState==TOUCH_SLOP_DEFAULT && y>-1 && firstVisibleItem == totalItemCount-1){;
            if(time==1) {
                if (endDate.getTime() < GlobalConfig.GetDefaultMaxDate().getTime()) {
                    long days14 = 24 * 60 * 60 * 1000 * 14;
                    endDate.setTime(endDate.getTime() + days14);
                    onLoadMore(endDate);
                }
                time=0;
            }
            time++;
        }
    }

    public abstract void onLoadMore(Date endDate);

}
