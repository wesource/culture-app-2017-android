package dk.aarhus2017.aarhusevents.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;

/**
 * Created by Dell on 3/1/2017.
 */

public class DeletedEventBroadcastReceiver extends  BroadcastReceiver {


   protected DeletedEventBroadcastReceiverListener listener;

    public DeletedEventBroadcastReceiver(){
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(listener!=null){
            ArrayList<Integer> deletedResult = intent.getIntegerArrayListExtra("DeletedEvent");
            listener.SynchronizeDeletedEvent(deletedResult);
        }
    }
    public void setDeletedEventBroadcastReceiverListener(DeletedEventBroadcastReceiver.DeletedEventBroadcastReceiverListener _listener){
        listener = _listener;
    }

    public interface DeletedEventBroadcastReceiverListener{
        void SynchronizeDeletedEvent(ArrayList<Integer> deletedEvents);
    }

}
