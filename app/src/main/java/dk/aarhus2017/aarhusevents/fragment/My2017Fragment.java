package dk.aarhus2017.aarhusevents.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;

import java.util.ArrayList;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.activities.EventActivity;
import dk.aarhus2017.aarhusevents.adapter.EventDataAdapter;
import dk.aarhus2017.aarhusevents.model.EventDataBinding;
import dk.aarhus2017.aarhusevents.model.PermissionResult;
import dk.aarhus2017.aarhusevents.model.PromptPermissionResult;
import dk.aarhus2017.aarhusevents.util.ConvertHelper;
import dk.aarhus2017.aarhusevents.util.EventHelper;
import dk.aarhus2017.aarhusevents.util.FBTrackerHelper;
import dk.aarhus2017.aarhusevents.util.FontHelper;
import dk.aarhus2017.aarhusevents.util.GATrackerHelper;
import dk.aarhus2017.aarhusevents.util.GeneralHelper;
import dk.aarhus2017.aarhusevents.util.GlobalConfig;
import dk.aarhus2017.aarhusevents.util.GlobalConstant;
import dk.aarhus2017.aarhusevents.util.InternetHelper;
import dk.aarhus2017.aarhusevents.util.KeyboardHelper;
import dk.aarhus2017.aarhusevents.util.LocationHelper;
import dk.aarhus2017.aarhusevents.util.PermissionHelper;
import dk.aarhus2017.aarhusevents.util.SharedPreferenceHelper;


public class My2017Fragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    Context context;
    My2017Fragment self = this;
    RecyclerView eventListFragmentRecyclerView;

    // Synchronize
    LinearLayout calendarSynchronizationHolderLinearLayout;
    TextView synchronizeTextView;

    // Internet
    LinearLayout internetHolderLinearLayout;
    TextView internetTextView;

    // Loading
    RelativeLayout loadingHolderRelativeLayout;

    // No Result
    RelativeLayout noResultHolderRelativeLayout;
    ImageView noResultImageView;
    LinearLayout noResultTextHolderLinearLayout;
    TextView noResultHeadlineTextView;
    TextView noResultDescriptionTextView;
    Button noResultActionButton;


    EventDataAdapter eventDataAdapter;
    EventDataAdapter.EventListener eventListener;

    ArrayList<EventDataBinding> event2017s = new ArrayList<>();
    Paint paint = new Paint();
    int animationIndex = 1;


    private OnMy2017FragmentInteractionListener mListener;

    public My2017Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment My2017Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static My2017Fragment newInstance(String param1, String param2) {
        My2017Fragment fragment = new My2017Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        context = getActivity().getApplicationContext();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        paint.setColor(Color.parseColor("#FF1E5A"));

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_my2017, container, false);
        eventListFragmentRecyclerView = (RecyclerView) v.findViewById(R.id.eventListFragmentRecyclerView);

        // Syncrhonize
        calendarSynchronizationHolderLinearLayout =(LinearLayout)v.findViewById(R.id.calendarSynchronizationHolderLinearLayout);
        synchronizeTextView = (TextView)v.findViewById(R.id.synchronizeTextView);

        // Internet Status
        internetHolderLinearLayout = (LinearLayout)v.findViewById(R.id.internetHolderLinearLayout);
        internetTextView = (TextView)v.findViewById(R.id.internetTextView);

        // Loading
        loadingHolderRelativeLayout = (RelativeLayout) v.findViewById(R.id.loadingHolderRelativeLayout);

        // Register Event
        calendarSynchronizationHolderLinearLayout.setOnClickListener(this);

        // No result section
        InitNoResult(v);

        // Set fonts
        SetFonts();

        InitMy2017();

        if (InternetHelper.HasInternetConnection(context)) {
            internetHolderLinearLayout.setVisibility(View.GONE);
        } else {
            internetHolderLinearLayout.setVisibility(View.VISIBLE);
        }

        eventListFragmentRecyclerView.setVisibility(View.GONE);
        noResultHolderRelativeLayout.setVisibility(View.GONE);
        loadingHolderRelativeLayout.setVisibility(View.VISIBLE);
        if (GlobalConfig.SharedEvents != null) {
            new LoadEvent2017AsyncTask().execute();
        }


        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        GATrackerHelper.SendGAScreenView(GATrackerHelper.My2017Screen, self.getActivity());
        FBTrackerHelper.LogScreenView(self.getContext(),GATrackerHelper.My2017Screen);
    }

    private void InitNoResult(View v) {
        // No result
        noResultHolderRelativeLayout = (RelativeLayout) v.findViewById(R.id.noResultHolderRelativeLayout);
        noResultImageView = (ImageView) v.findViewById(R.id.noResultImageView);
        noResultTextHolderLinearLayout = (LinearLayout) v.findViewById(R.id.noResultTextHolderLinearLayout);
        noResultHeadlineTextView = (TextView) v.findViewById(R.id.noResultHeadlineTextView);
        noResultDescriptionTextView = (TextView) v.findViewById(R.id.noResultDescriptionTextView);
        noResultActionButton = (Button) v.findViewById(R.id.noresultActionButton);

        noResultHolderRelativeLayout.setVisibility(View.GONE);

        noResultActionButton.setOnClickListener(this);

        // Set text
        noResultHeadlineTextView.setText(getString(R.string.my2017_noresult_headline));
        noResultDescriptionTextView.setText(getString(R.string.my2017_noresult_description));
        noResultActionButton.setText(getString(R.string.my2017_noresult_button));


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.noresultActionButton: {
                if(mListener!=null){
                    mListener.onShowEventListTab();
                }
                break;
            }
            case R.id.calendarSynchronizationHolderLinearLayout:
            {
                SynchronizeFavoriteToCalendar(self.event2017s);
                break;
            }
        }
    }

    private void SetFonts() {
        Typeface sourceSansProRegular = FontHelper.SourceSansProRegular(context);
        Typeface sourceSansProBold = FontHelper.SourceSansProBold(context);
        Typeface flavinBold = FontHelper.FlavinBold(context);
        Typeface flavinRegular = FontHelper.FlavinRegular(context);


        // Internet
        internetTextView.setTypeface(sourceSansProRegular);

        noResultHeadlineTextView.setTypeface(flavinBold);
        noResultDescriptionTextView.setTypeface(flavinRegular);
        noResultActionButton.setTypeface(sourceSansProBold);

    }

    private void setNoResultEventAnimation() {

        noResultImageView.setImageResource(R.drawable.ic_empty_my2017_0);
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.empty2017_fade_in);
        noResultActionButton.startAnimation(animation);

        Animation animationText = AnimationUtils.loadAnimation(context, R.anim.empty2017_fade_in);
        animationText.setStartOffset(300);
        noResultTextHolderLinearLayout.startAnimation(animationText);

        final Animation animationImage = AnimationUtils.loadAnimation(context, R.anim.empty2017_fade_in);
        animationImage.setStartOffset(100);


        final Animation animationLoop = AnimationUtils.loadAnimation(context, R.anim.empty2017_fade);
        animationLoop.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (animationIndex < 8) {
                    noResultImageView.clearAnimation();
                    String uri = "@drawable/ic_empty_my2017_" + animationIndex;
                    int drawableID = getResources().getIdentifier(uri, null, context.getPackageName());
                    noResultImageView.setImageResource(drawableID);
                    noResultImageView.startAnimation(animationLoop);
                    animationIndex++;
                } else {
                    noResultImageView.clearAnimation();
                    animationIndex = 0;
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationImage.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                noResultImageView.startAnimation(animationLoop);
                animationIndex = 1;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        noResultImageView.startAnimation(animationImage);


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMy2017FragmentInteractionListener) {
            mListener = (OnMy2017FragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    public void ReloadMy2017() {

        // Hide keyboard
        if(context!=null) {

            KeyboardHelper.HideKeyboard(self.getView(), context);
            // Check internet status
            if (InternetHelper.HasInternetConnection(context)) {
                internetHolderLinearLayout.setVisibility(View.GONE);
            } else {
                internetHolderLinearLayout.setVisibility(View.VISIBLE);
            }

            // Reload my 2017 events
            //if (GlobalConfig.SharedEvents != null) {
            new LoadEvent2017AsyncTask().execute();
            //}
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    private void InitMy2017() {
        eventDataAdapter = new EventDataAdapter(getContext(), event2017s,false);

        eventListFragmentRecyclerView.setAdapter(eventDataAdapter);
        eventListener = new EventDataAdapter.EventListener() {
            @Override
            public void onRowClick(EventDataBinding rowData) {
                if(mListener!=null){
                    mListener.onClickMy2017Fragment(rowData,2,0);
                }
            }
        };
        eventDataAdapter.setTaskListener(eventListener);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());

        eventListFragmentRecyclerView.setLayoutManager(layoutManager);
        eventListFragmentRecyclerView.setItemAnimator(new DefaultItemAnimator());
        eventListFragmentRecyclerView.setDrawingCacheEnabled(true);
        eventListFragmentRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        eventListFragmentRecyclerView.setHasFixedSize(false);

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {


            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                EventDataBinding rowData = event2017s.get(position);


                if (rowData.isGroupHeader()) {
                    return;
                }

                switch (direction) {
                    case ItemTouchHelper.LEFT: {
                        if (rowData.getEventData().getFavoriteEvent()) {
                            rowData.getEventData().setFavoriteEvent(false);
                            self.event2017s.remove(rowData);
                            new UpdateFavoriteEventAsyncTask().execute();
                            if(mListener!=null){
                                mListener.onRemoveFavoriteMy2017Fragment(rowData,2);
                            }

                        }
                        break;
                    }

                    default:
                        break;
                }


            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

                EventDataBinding rowData = self.event2017s.get(viewHolder.getAdapterPosition());
//                if (isCurrentMy2017) {
//                    rowData = self.my2017Events.get(viewHolder.getAdapterPosition());
//                } else {
//                    rowData = self.eventDataBindings.get(viewHolder.getAdapterPosition());
//                }
                if (!rowData.isGroupHeader()) {
                    if (rowData.getEventData().getFavoriteEvent()) {
                        return ItemTouchHelper.LEFT;
                    } else {
                        return ItemTouchHelper.RIGHT;
                    }
                } else {
                    return super.getSwipeDirs(recyclerView, viewHolder);
                }
            }

            @Override
            public void onChildDrawOver(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                if (!isCurrentlyActive) {
                    LinearLayout eventItemContentHolderLinearLayout = (LinearLayout) viewHolder.itemView.findViewById(R.id.eventItemContentHolderLinearLayout);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                    int margin = ConvertHelper.ConvertDpToPx(self.getContext(), 10);
                    layoutParams.setMarginStart(margin);
                    layoutParams.setMarginEnd(margin);
                    eventItemContentHolderLinearLayout.setLayoutParams(layoutParams);

                    super.onChildDrawOver(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }
            }


            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                int position = viewHolder.getAdapterPosition();
                EventDataBinding rowData = self.event2017s.get(position);
//                if (isCurrentMy2017) {
//                    rowData = self.my2017Events.get(position);
//                } else {
//                    rowData = self.eventDataBindings.get(position);
//                }

                if (rowData.isGroupHeader()) {
                    return;
                }

                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;
                    LinearLayout seperateLine = (LinearLayout) viewHolder.itemView.findViewById(R.id.eventItemSeperationLinearLayout);
                    int seperateLineHeight = 0;
                    if (seperateLine.getVisibility() == View.VISIBLE) {
                        seperateLineHeight = seperateLine.getHeight();
                    } else {
                        seperateLineHeight = 0;
                    }

                    LinearLayout eventItemContentHolderLinearLayout = (LinearLayout) viewHolder.itemView.findViewById(R.id.eventItemContentHolderLinearLayout);

                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    if (rowData.getEventData().getFavoriteEvent()) {

                        if (dX < 0) {
                            layoutParams.setMarginEnd(0);
                            eventItemContentHolderLinearLayout.setLayoutParams(layoutParams);

                            RectF background = new RectF(itemView.getWidth() - Math.abs(dX), (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom() - seperateLineHeight);
                            c.drawRect(background, paint);

                            icon = BitmapFactory.decodeResource(self.getResources(), R.drawable.remove_saved_favorite);
                            int top = (itemView.getBottom() - itemView.getTop() - seperateLineHeight - icon.getHeight()) / 2;
                            int padding = ConvertHelper.ConvertDpToPx(self.getContext(), 30);
                            RectF icon_dest = new RectF(itemView.getWidth() - Math.abs(dX) + padding, (float) itemView.getTop() + top, itemView.getWidth() - Math.abs(dX) + padding + icon.getWidth(), (float) itemView.getBottom() - seperateLineHeight - top);
                            c.drawBitmap(icon, null, icon_dest, paint);


                            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

                        } else {
                            c = new Canvas();
                            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        }

                    } else {
                        if (dX > 0) {
                            layoutParams.setMarginStart(0);
                            eventItemContentHolderLinearLayout.setLayoutParams(layoutParams);
                            RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom() - seperateLineHeight);
                            c.drawRect(background, paint);
                            if (dX > 50) {
                                icon = BitmapFactory.decodeResource(self.getResources(), R.drawable.save_favorite);

                                int top = (itemView.getBottom() - itemView.getTop() - seperateLineHeight - icon.getHeight()) / 2;
                                int padding = ConvertHelper.ConvertDpToPx(self.getContext(), 30);

                                RectF icon_dest = new RectF(dX - padding - icon.getWidth(), (float) itemView.getTop() + top, dX - padding, (float) itemView.getBottom() - seperateLineHeight - top);
                                c.drawBitmap(icon, null, icon_dest, paint);
                            }

                            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

                        } else {

                            c = new Canvas();
                            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        }
                    }
                }
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(eventListFragmentRecyclerView);

    }

    private void SynchronizeFavoriteToCalendar(ArrayList<EventDataBinding> favoriteItems) {
        final PromptPermissionResult setting = SharedPreferenceHelper.GetCalendarPromptSetting(context);

        switch (setting) {
            case Yes: {
                PermissionResult permissionResult = PermissionHelper.CheckCalendarPermission(self.getActivity());
                switch (permissionResult) {
                    case Granted: {
                        new SynchronizeFavoriteToCalendarAsyncTask().execute(favoriteItems);
                        break;
                    }
                    case Requested: {
                        ActivityCompat.requestPermissions(self.getActivity(), new String[]{Manifest.permission.WRITE_CALENDAR}, GlobalConstant.RequestCalendarPermissionSynchronizationResultCode);
                        break;
                    }
                    case Denied:
                        break;
                    default:
                        break;
                }
                break;
            }
            case No:
            case Ignore: {
                final Dialog acceptDialog = new Dialog(self.getContext());
                acceptDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                acceptDialog.setContentView(R.layout.prompt_calendar_dialog);
                acceptDialog.show();
                Typeface flavinBold = FontHelper.FlavinBold(context);
                Typeface flavinRegular = FontHelper.FlavinRegular(context);
                Typeface sourceSansProBold = FontHelper.SourceSansProBold(context);
                TextView titleTextView = (TextView) acceptDialog.findViewById(R.id.promtTitleTextView);
                TextView descriptionTextView = (TextView) acceptDialog.findViewById(R.id.promtDescriptionTextView);
                Button acceptButton = (Button) acceptDialog.findViewById(R.id.promptAcceptButton);
                Button noButton = (Button) acceptDialog.findViewById(R.id.promptNoButton);

                titleTextView.setTypeface(flavinBold);
                descriptionTextView.setTypeface(flavinRegular);
                acceptButton.setTypeface(sourceSansProBold);
                noButton.setTypeface(sourceSansProBold);
                acceptButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        acceptDialog.dismiss();
                        if(mListener!=null){
                            mListener.onSynchronizationFavoriteToCalendarMy2017Fragment(self.event2017s);
                        }

                        SharedPreferenceHelper.SaveCalendarPromptSetting(context, PromptPermissionResult.Yes);
                        ActivityCompat.requestPermissions(self.getActivity(), new String[]{Manifest.permission.WRITE_CALENDAR}, GlobalConstant.RequestCalendarPermissionSynchronizationResultCode);
                        GATrackerHelper.SynCalendar(context);
                    }
                });
                ImageButton closeImageButton = (ImageButton) acceptDialog.findViewById(R.id.promtCloseImageButton);
                closeImageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        acceptDialog.dismiss();
                        SharedPreferenceHelper.SaveCalendarPromptSetting(context, PromptPermissionResult.Ignore);
                    }
                });
                noButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        acceptDialog.dismiss();
                        SharedPreferenceHelper.SaveCalendarPromptSetting(context, PromptPermissionResult.No);
                    }
                });
                break;
            }
            default: {
                break;
            }
        }

    }


    public class SynchronizeFavoriteToCalendarAsyncTask extends AsyncTask<ArrayList<EventDataBinding>, Void, Boolean> {
        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                calendarSynchronizationHolderLinearLayout.setVisibility(View.GONE);
            } else {
                calendarSynchronizationHolderLinearLayout.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected Boolean doInBackground(ArrayList<EventDataBinding>... arrayLists) {
            ArrayList<EventDataBinding> favoritedItems = (ArrayList<EventDataBinding>) arrayLists[0];
            ContentResolver contentResolver = context.getContentResolver();
            Boolean result = SharedPreferenceHelper.SynchronizeToCalendar(context, contentResolver, favoritedItems);
            return result;
        }
    }

    public class CheckSynchronizeToCalendarAsyncTask extends AsyncTask<ArrayList<EventDataBinding>, Void, Boolean> {
        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                calendarSynchronizationHolderLinearLayout.setVisibility(View.VISIBLE);
            } else {
                calendarSynchronizationHolderLinearLayout.setVisibility(View.GONE);
            }
        }

        @Override
        protected Boolean doInBackground(ArrayList<EventDataBinding>... arrayLists) {
            ArrayList<EventDataBinding> favoritedItems = (ArrayList<EventDataBinding>) arrayLists[0];
            Boolean result = SharedPreferenceHelper.CheckSynchronizeToCalendar(context, favoritedItems);
            return result;
        }
    }


    public class LoadEvent2017AsyncTask extends AsyncTask<Object, Void, ArrayList<EventDataBinding>> {
        @Override
        protected ArrayList<EventDataBinding> doInBackground(Object... objects) {

            ArrayList<EventDataBinding> bindings = EventHelper.ConvertToEventDataFilterByFavorite(context, GlobalConfig.SharedEvents);

            return bindings;
        }

        @Override
        protected void onPostExecute(ArrayList<EventDataBinding> bindings) {
            super.onPostExecute(bindings);
            self.event2017s = bindings;

             loadingHolderRelativeLayout.setVisibility(View.GONE);

            if (self.event2017s.size() > 0) {
                eventListFragmentRecyclerView.setVisibility(View.VISIBLE);
                noResultHolderRelativeLayout.setVisibility(View.GONE);
                eventDataAdapter.updateData(self.event2017s);
                new CheckSynchronizeToCalendarAsyncTask().execute(self.event2017s);
                Location myLocation  = LocationHelper.getLocation(context);
                if (myLocation != null) {
                    eventDataAdapter.updateLocation(myLocation);
                }
            } else {
                eventListFragmentRecyclerView.setVisibility(View.GONE);
                noResultHolderRelativeLayout.setVisibility(View.VISIBLE);
                setNoResultEventAnimation();
            }
        }
    }

    public class UpdateFavoriteEventAsyncTask extends AsyncTask<Object, Void, ArrayList<EventDataBinding>> {

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(ArrayList<EventDataBinding> results) {
            self.event2017s = results;
            if (self.event2017s.size() > 0) {
                eventListFragmentRecyclerView.setVisibility(View.VISIBLE);
                noResultHolderRelativeLayout.setVisibility(View.GONE);
                eventDataAdapter.updateData(self.event2017s);
                Location myLocation  = LocationHelper.getLocation(context);
                if (myLocation != null) {
                    eventDataAdapter.updateLocation(myLocation);
                }
            } else {
                calendarSynchronizationHolderLinearLayout.setVisibility(View.GONE);
                eventListFragmentRecyclerView.setVisibility(View.GONE);
                noResultHolderRelativeLayout.setVisibility(View.VISIBLE);
                setNoResultEventAnimation();
            }
        }

        @Override
        protected ArrayList<EventDataBinding> doInBackground(Object... arrayLists) {
            return EventHelper.UpdateFavoriteEvents(self.event2017s);
        }
    }

    // Exchange data with Main Activity
    public void ChangedInternetStatus(boolean isConnected){
        if(internetHolderLinearLayout !=null) {
            if (isConnected) {
                internetHolderLinearLayout.setVisibility(View.GONE);
            } else {
                internetHolderLinearLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    public void SynchronizedStatus(boolean result){
        if (result) {
            calendarSynchronizationHolderLinearLayout.setVisibility(View.GONE);
        } else {
            calendarSynchronizationHolderLinearLayout.setVisibility(View.VISIBLE);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnMy2017FragmentInteractionListener {
        void onShowEventListTab();

        void onRemoveFavoriteMy2017Fragment(EventDataBinding value, int tabPosition);

        void onClickMy2017Fragment(EventDataBinding value, int tabPosition, int selectedEventType);

        void onSynchronizationFavoriteToCalendarMy2017Fragment(ArrayList<EventDataBinding> favoriteEvents);
    }
}
