package dk.aarhus2017.aarhusevents.model;

import android.location.Location;

import java.io.Serializable;

/**
 * Created by Dell on 11/29/2016.
 */

public class LocationModel{
    private String name;
    private String address;
    private String zipCity;
    private String venue;
    private double latitude;
    private double longitude;
    private int zoomDistance;

    private String distance="";
    private Location currentLocation;

    public LocationModel(String _name, String _address, String _zipCity, String _venue, double _latitude, double _longitude, int _zoomDistance)
    {
        this.name =_name;
        this.address =_address;
        this.zipCity = _zipCity;
        this.venue = _venue;
        this.latitude =_latitude;
        this.longitude =_longitude;
        this.zoomDistance =_zoomDistance;
    }

    public  void setLocation(Location _currentLocation){
        this.currentLocation = _currentLocation;
    }


    public String getName(){return this.name;}

    public String getAddress(){return  this.address;}

    public  String getZipCity(){return  this.zipCity;}

    public String getVenue(){return this.venue;}

    public double getLatitude(){return  this.latitude;}

    public double getLongitude(){return this.longitude;}

    public  int getZoomDistance(){return this.zoomDistance;}

    public double getDistance(){
        if(currentLocation != null && this.latitude !=0 & this.longitude !=0 )
        {
            Location toLocation = new Location(this.name);
            toLocation.setLatitude(latitude);
            toLocation.setLongitude(longitude);
            double distance= currentLocation.distanceTo(toLocation);
            if(distance>0)
            {
                distance = distance/1000;
            }
            return  distance;
        }
        else
        {
            return 0;
        }
    }

    public String getCity(){
        String city="";
        if(this.zipCity !=null){
            if(!this.zipCity.isEmpty()){
                int index = this.zipCity.indexOf(" ");
                if(index > 0) {
                    city = this.zipCity.substring(index, this.zipCity.length());
                }
            }
        }
        return city;
    }
}
