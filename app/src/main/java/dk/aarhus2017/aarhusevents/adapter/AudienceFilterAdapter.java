package dk.aarhus2017.aarhusevents.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.model.ArtformFilterModel;
import dk.aarhus2017.aarhusevents.model.AudienceFilterModel;
import dk.aarhus2017.aarhusevents.util.FontHelper;

/**
 * Created by Dell on 12/16/2016.
 */

public class AudienceFilterAdapter extends ArrayAdapter<AudienceFilterModel> {

    Context contextAdapter;
    ArrayList<AudienceFilterModel> audienceItems=new ArrayList<>();
    protected AudienceFilterListener artformFilterListener;

    public AudienceFilterAdapter(Context context, ArrayList<AudienceFilterModel> audienceFilterModels)
    {
        super(context,-1,audienceFilterModels);
        this.audienceItems = audienceFilterModels;
        this.contextAdapter=context;
    }

    @Override
    public int getCount() {
        return audienceItems.size();
    }

    @Override
    public void add(AudienceFilterModel object) {
        audienceItems.add(object);
        super.add(object);
    }

    public void updateData(ArrayList<AudienceFilterModel> values)
    {
        this.audienceItems = values;
        notifyDataSetChanged();
    }

    @Nullable
    @Override
    public AudienceFilterModel getItem(int position) {
        return audienceItems.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.filter_audience_item, parent, false);

        final Typeface sourceSansProRegular = FontHelper.SourceSansProRegular(contextAdapter);
        final Typeface sourceSansProBold = FontHelper.SourceSansProBold(contextAdapter);

        final AudienceFilterModel dataRow = getItem(position);
        final TextView name =(TextView)row.findViewById(R.id.audienceFilterNameTextView);
        final ImageView selected = (ImageView)row.findViewById(R.id.audienceFilterSelectedCheckedImageView);

        name.setText(dataRow.getName());
        if(dataRow.getSelected())
        {
            name.setTypeface(sourceSansProBold);
            selected.setVisibility(View.VISIBLE);
        }
        else
        {
            name.setTypeface(sourceSansProRegular);
            selected.setVisibility(View.INVISIBLE);
        }

        row.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(dataRow.getSelected())
                {
                    name.setTypeface(sourceSansProRegular);
                    selected.setColorFilter(R.color.filter_deactive);
                    selected.setVisibility(View.INVISIBLE);
                    dataRow.setSelected(false);
                }
                else
                {
                    name.setTypeface(sourceSansProBold);
                    selected.setColorFilter(R.color.filter_active);
                    selected.setVisibility(View.VISIBLE);
                    dataRow.setSelected(true);
                }
                notifyDataSetChanged();

                if(artformFilterListener!=null){
                    artformFilterListener.onStatusChanged(dataRow);
                }

            }
        });





        return row;
    }

   public void setListenerTask(AudienceFilterListener _listener){
       this.artformFilterListener = _listener;
   }

    public interface AudienceFilterListener {
        void onStatusChanged(AudienceFilterModel rowData);

    }
}
