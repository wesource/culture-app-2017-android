package dk.aarhus2017.aarhusevents.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.FacebookSdk;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.util.FBTrackerHelper;
import dk.aarhus2017.aarhusevents.util.FontHelper;
import dk.aarhus2017.aarhusevents.util.ServicesHelper;

public class WelcomeActivity extends AppCompatActivity {

    // Welcome
    TextView eventWelcomeHeadlineTextView;
    TextView eventWelcomeSubheadlineTextView;
    ImageView eventWelcomeTextBackgroundImageView;
    LinearLayout eventWelcomeTextHolderLinearLayout;
    ImageView eventWelcomeScreenImageView;

    TabLayout footerTabLayout;

    WelcomeActivity self = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        // Start loading data in background services
        ServicesHelper.InitGlobalDataService(self);

        eventWelcomeHeadlineTextView = (TextView) findViewById(R.id.eventWelcomeHeadlineTextView);
        eventWelcomeSubheadlineTextView = (TextView) findViewById(R.id.eventWelcomeSubheadlineTextView);
        eventWelcomeTextHolderLinearLayout = (LinearLayout) findViewById(R.id.eventWelcomeTextHolderLinearLayout);
        eventWelcomeTextBackgroundImageView = (ImageView) findViewById(R.id.eventWelcomeTextBackgroundImageView);
        eventWelcomeScreenImageView = (ImageView) findViewById(R.id.eventWelcomeScreenImageView);


        footerTabLayout = (TabLayout) findViewById(R.id.footerTabLayout);
        footerTabLayout.setTabTextColors(ContextCompat.getColor(self, R.color.bottomBarIcon), ContextCompat.getColor(self, R.color.bottomBarIcon));

        for (int i = 0; i < footerTabLayout.getTabCount(); i++) {
            switch (i) {
                case 0: {
                    footerTabLayout.getTabAt(i).setIcon(R.drawable.ic_event);
                    break;
                }
                case 1: {
                    footerTabLayout.getTabAt(i).setIcon(R.drawable.ic_search);
                    break;
                }
                case 2: {
                    footerTabLayout.getTabAt(i).setIcon(R.drawable.ic_user);
                    break;
                }
                case 3: {
                    footerTabLayout.getTabAt(i).setIcon(R.drawable.ic_info);
                    break;
                }
            }
        }


        // Set font
        // Welcome
        SetFonts();

        // Animation
        SetAnimation();

        footerTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Intent mainIntent = new Intent(self, MainActivity.class);
                mainIntent.putExtra("TabPosition", tab.getPosition());
                startActivity(mainIntent);
                self.finish();

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Intent mainIntent = new Intent(self, MainActivity.class);
                mainIntent.putExtra("TabPosition", tab.getPosition());
                startActivity(mainIntent);
                self.finish();
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        // Facebook analytics
        FBTrackerHelper.LogScreenView(this,"Welcome");
    }

    private void SetFonts() {
        Typeface flavinBold = FontHelper.FlavinBold(self);
        eventWelcomeHeadlineTextView.setTypeface(flavinBold);
        eventWelcomeSubheadlineTextView.setTypeface(flavinBold);
    }

    private void SetAnimation() {
        Animation animationWelcomeText = AnimationUtils.loadAnimation(self, R.anim.translate_welcome_text);
        Animation animationWelcomeTextBackground = AnimationUtils.loadAnimation(self, R.anim.translate_welcome_text_streger);
        Animation animationWelcomeScreen = AnimationUtils.loadAnimation(self, R.anim.translate_welcome_screen_background);

        eventWelcomeTextHolderLinearLayout.startAnimation(animationWelcomeText);
        eventWelcomeTextBackgroundImageView.startAnimation(animationWelcomeTextBackground);
        eventWelcomeScreenImageView.startAnimation(animationWelcomeScreen);
    }

}
