package dk.aarhus2017.aarhusevents.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.util.FBTrackerHelper;
import dk.aarhus2017.aarhusevents.util.FontHelper;
import dk.aarhus2017.aarhusevents.util.GATrackerHelper;
import dk.aarhus2017.aarhusevents.util.GeneralHelper;
import dk.aarhus2017.aarhusevents.util.KeyboardHelper;



public class InfoFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    Context context;
    InfoFragment self = this;

    protected My2017Fragment.OnMy2017FragmentInteractionListener mListener;

    // No Result
    RelativeLayout noResultHolderRelativeLayout;
    ImageView noResultImageView;
    LinearLayout noResultTextHolderLinearLayout;
    TextView noResultHeadlineTextView;
    TextView noResultDescriptionTextView;
    Button noResultActionButton;

    int animationIndex = 0;


    public InfoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment InfoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InfoFragment newInstance(String param1, String param2) {
        InfoFragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        context = this.getContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_info, container, false);

        //
        InitNoResult(v);

        // Set fonts
        SetFonts();

        KeyboardHelper.HideKeyboard(v,context);

        // Start animation
        setNoResultEventAnimation();



        return v;
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        GATrackerHelper.SendGAScreenView(GATrackerHelper.InfoScreen, self.getActivity());

        FBTrackerHelper.LogScreenView(self.getContext(),GATrackerHelper.InfoScreen);
    }

    private void InitNoResult(View v) {
        // No result
        noResultHolderRelativeLayout = (RelativeLayout) v.findViewById(R.id.noResultHolderRelativeLayout);
        noResultImageView = (ImageView) v.findViewById(R.id.noResultImageView);
        noResultTextHolderLinearLayout = (LinearLayout) v.findViewById(R.id.noResultTextHolderLinearLayout);
        noResultHeadlineTextView = (TextView) v.findViewById(R.id.noResultHeadlineTextView);
        noResultDescriptionTextView = (TextView) v.findViewById(R.id.noResultDescriptionTextView);
        noResultActionButton = (Button) v.findViewById(R.id.noresultActionButton);

        noResultHolderRelativeLayout.setVisibility(View.VISIBLE);

        noResultActionButton.setOnClickListener(this);

        // Set text
        noResultHeadlineTextView.setText(getString(R.string.info_noresult_headline));
        noResultDescriptionTextView.setText(getString(R.string.info_noresult_description));
        noResultActionButton.setText(getString(R.string.info_noresult_button));


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.noresultActionButton: {
                String websiteUrl = GeneralHelper.GetWebSiteUrl();
                Intent websiteIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(websiteUrl));
                startActivity(websiteIntent);
                break;
            }
        }
    }

    private void SetFonts() {
        Typeface sourceSansProBold = FontHelper.SourceSansProBold(context);
        Typeface flavinBold = FontHelper.FlavinBold(context);
        Typeface flavinRegular = FontHelper.FlavinRegular(context);


        noResultHeadlineTextView.setTypeface(flavinBold);
        noResultDescriptionTextView.setTypeface(flavinRegular);
        noResultActionButton.setTypeface(sourceSansProBold);

    }


    private void setNoResultEventAnimation() {

        noResultImageView.setImageResource(R.drawable.ic_noresult_infor_0);
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.empty2017_fade_in);
        noResultActionButton.startAnimation(animation);

        Animation animationText = AnimationUtils.loadAnimation(context, R.anim.empty2017_fade_in);
        animationText.setStartOffset(300);
        noResultTextHolderLinearLayout.startAnimation(animationText);

        final Animation animationImage = AnimationUtils.loadAnimation(context, R.anim.empty2017_fade_in);
        animationImage.setStartOffset(100);


        final Animation animationLoop = AnimationUtils.loadAnimation(context, R.anim.empty2017_fade);
        animationLoop.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (animationIndex < 8) {
                    noResultImageView.clearAnimation();
                    String uri = "@drawable/ic_noresult_infor_" + animationIndex;
                    int drawableID = getResources().getIdentifier(uri, null, context.getPackageName());
                    noResultImageView.setImageResource(drawableID);
                    noResultImageView.startAnimation(animationLoop);
                    animationIndex++;
                } else {
                    noResultImageView.clearAnimation();
                    animationIndex = 0;
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationImage.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                noResultImageView.startAnimation(animationLoop);
                animationIndex = 1;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        noResultImageView.startAnimation(animationImage);


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof My2017Fragment.OnMy2017FragmentInteractionListener) {
            mListener = (My2017Fragment.OnMy2017FragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    // Communicate with Main Activity

    public void Reload() {
        KeyboardHelper.HideKeyboard(self.getView(),context);
    }


}
