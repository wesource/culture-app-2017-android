package dk.aarhus2017.aarhusevents.activities;


import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.adapter.MainViewPagerAdapter;
import dk.aarhus2017.aarhusevents.fragment.EventFragment;
import dk.aarhus2017.aarhusevents.fragment.InfoFragment;
import dk.aarhus2017.aarhusevents.fragment.My2017Fragment;
import dk.aarhus2017.aarhusevents.fragment.SearchFragment;
import dk.aarhus2017.aarhusevents.model.EventDataBinding;
import dk.aarhus2017.aarhusevents.model.GeoLocationModel;
import dk.aarhus2017.aarhusevents.model.NonSwipeableViewPager;
import dk.aarhus2017.aarhusevents.model.PermissionResult;
import dk.aarhus2017.aarhusevents.model.PromptPermissionResult;
import dk.aarhus2017.aarhusevents.notification.ConnectivityReceiver;
import dk.aarhus2017.aarhusevents.notification.DeletedEventBroadcastReceiver;
import dk.aarhus2017.aarhusevents.notification.InitDataBroadcastReceiver;
import dk.aarhus2017.aarhusevents.notification.LocalBroadcastReceiver;
import dk.aarhus2017.aarhusevents.util.FontHelper;
import dk.aarhus2017.aarhusevents.util.GATrackerHelper;
import dk.aarhus2017.aarhusevents.util.GeneralHelper;
import dk.aarhus2017.aarhusevents.util.GlobalConfig;
import dk.aarhus2017.aarhusevents.util.GlobalConstant;
import dk.aarhus2017.aarhusevents.util.LocationHelper;
import dk.aarhus2017.aarhusevents.util.PermissionHelper;
import dk.aarhus2017.aarhusevents.util.SharedPreferenceHelper;

public class MainActivity extends AppCompatActivity implements SearchFragment.OnSearchFragmentInteractionListener,
        EventFragment.OnEventFragmentInteractionListener,
        My2017Fragment.OnMy2017FragmentInteractionListener {


    NonSwipeableViewPager mainViewPager;
    //NonSwipeableViewPager mainViewPager;
    TabLayout footerTabLayout;

    MainActivity self = this;

    ArrayList<Fragment> fragmentList = new ArrayList<>();
    EventDataBinding currentEventDataRow;

    ArrayList<EventDataBinding> my2017Events = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainViewPager = (NonSwipeableViewPager) findViewById(R.id.mainViewPager);
        footerTabLayout = (TabLayout) findViewById(R.id.footerTabLayout);
        Intent intent = getIntent();
        final int tabPosition = intent.getIntExtra("TabPosition", 0);


        fragmentList.add(new EventFragment());
        fragmentList.add(new SearchFragment());
        fragmentList.add(new My2017Fragment());
        fragmentList.add(new InfoFragment());

        final MainViewPagerAdapter mainAdapter = new MainViewPagerAdapter(getSupportFragmentManager(), fragmentList);

        mainViewPager.setAdapter(mainAdapter);

        if (tabPosition < fragmentList.size()) {
            mainViewPager.setCurrentItem(tabPosition);
            footerTabLayout.getTabAt(tabPosition).select();
        }

        // mainTabLayout.setupWithViewPager(mainViewPager);
        mainViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(footerTabLayout));
        footerTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int tabPosition = tab.getPosition();
                SelectedTab(tabPosition);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                boolean selected = tab.isSelected();
//                int tabPosition = tab.getPosition();
//
//                SelectedTab(tabPosition);
            }
        });


        // Set Fonts
        SetFonts();

        // Internet Status
        RegisterInternetStatus();


        // Initialize data
        RegisterReceivingInitGlobalData();

        // Updated or Deleted events
        RegisterUpdatedOrDeletedEventBroadcast();

    }

    private void SelectedTab(int position) {
        mainViewPager.setCurrentItem(position);
        Fragment fragment = fragmentList.get(position);
        switch (position) {
            case 0: {
                EventFragment infoFragment = (EventFragment) fragment;
                infoFragment.UpdateDataChanged();
                break;
            }
            case 1: {
                SearchFragment infoFragment = (SearchFragment) fragment;
                infoFragment.UpdateData();
                break;
            }
            case 2: {
                My2017Fragment my2017Fragment = (My2017Fragment) fragment;
                my2017Fragment.ReloadMy2017();
                break;
            }
            case 3: {
                InfoFragment infoFragment = (InfoFragment) fragment;
                infoFragment.Reload();
                break;
            }
        }
    }

    private void UpdateData() {
        int position = mainViewPager.getCurrentItem();
        Fragment fragment = fragmentList.get(position);

        switch (position) {
            case 0: {
                EventFragment infoFragment = (EventFragment) fragment;
                infoFragment.UpdateDataChanged();
                break;
            }
            case 1: {
                SearchFragment infoFragment = (SearchFragment) fragment;
                infoFragment.UpdateData();
                break;
            }
            case 2: {
                My2017Fragment my2017Fragment = (My2017Fragment) fragment;
                my2017Fragment.ReloadMy2017();
                break;
            }
            case 3: {
                InfoFragment infoFragment = (InfoFragment) fragment;
                infoFragment.Reload();
            }
        }
    }


    private void SetFonts() {

        // Typeface sourceSansProRegular = FontHelper.SourceSansProRegular(self);


    }

    private void RegisterInternetStatus() {
        ConnectivityReceiver.ConnectivityReceiverListener listener = new ConnectivityReceiver.ConnectivityReceiverListener() {
            @Override
            public void onNetworkConnectionChanged(boolean isConnected) {

                int postion = mainViewPager.getCurrentItem();

                Fragment fragment = fragmentList.get(postion);

                switch (postion) {
                    case 0: {
                        if (fragment instanceof EventFragment) {
                            EventFragment event = (EventFragment) fragment;
                            event.ChangedInternetStatus(isConnected);
                        }
                        break;
                    }
                    case 1: {
                        if (fragment instanceof SearchFragment) {
                            SearchFragment event = (SearchFragment) fragment;
                            event.ChangedInternetStatus(isConnected);
                        }
                        break;
                    }
                    case 2: {
                        if (fragment instanceof My2017Fragment) {
                            My2017Fragment event = (My2017Fragment) fragment;
                            event.ChangedInternetStatus(isConnected);
                        }
                        break;
                    }

                }
            }
        };
        ConnectivityReceiver connectivityReceiver = new ConnectivityReceiver();
        connectivityReceiver.setConnectivityReceiverListener(listener);
        this.registerReceiver(connectivityReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }


    private void BroadCastFavoriteEventSynchronization(Boolean result) {
        int postion = mainViewPager.getCurrentItem();

        if (postion == 2) {
            Fragment fragment = fragmentList.get(postion);
            My2017Fragment event = (My2017Fragment) fragment;
            event.SynchronizedStatus(result);
        }

    }

    private void BroadCastInitializedData() {
        int position = mainViewPager.getCurrentItem();
        Fragment fragment = fragmentList.get(position);

        switch (position) {
            case 0: {
                EventFragment infoFragment = (EventFragment) fragment;
                infoFragment.Reload();
                break;
            }
            case 1: {
                SearchFragment infoFragment = (SearchFragment) fragment;
                //infoFragment.UpdateData();
                break;
            }
            case 2: {
                My2017Fragment my2017Fragment = (My2017Fragment) fragment;
                my2017Fragment.ReloadMy2017();
                break;
            }
//            case 3:{
//                InfoFragment infoFragment = (InfoFragment)fragment;
//                infoFragment.Reload();
//            }
        }
    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode,permissions,grantResults);
        switch (requestCode) {
//            case GlobalConstant.RequestLocationPermissionResultCode: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    // permission was granted, yay! Do the
//                    // contacts-related task you need to do.
//                    myLocation = LocationHelper.getLocation(self);
//                    if (myLocation != null && eventDataAdapter != null) {
//                        eventDataAdapter.updateLocation(myLocation);
//                    }
//                } else {
//
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                    // Toast.makeText(MainActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
//                }
//                break;
//            }
            case GlobalConstant.RequestCalendarPermissionResultCode: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (currentEventDataRow != null) {
                        ContentResolver contentResolver = getContentResolver();
                        long calendarEventID = GeneralHelper.AddEventToCalendar(contentResolver, currentEventDataRow.getDate(), currentEventDataRow.getEventData().getTimeSpan(), currentEventDataRow.getEventData().getEndTimeSpan()
                                , currentEventDataRow.getEventData().getEventHeadline(), currentEventDataRow.getEventData().getDescription(), currentEventDataRow.getEventData().getEventUrl()
                                , currentEventDataRow.getEventData().getLocationModel().getName() + ", " + currentEventDataRow.getEventData().getLocationModel().getAddress());
                        SharedPreferenceHelper.UpdateFavoriteEvent(self, currentEventDataRow.getEventData().getScheduledEventID(), calendarEventID, currentEventDataRow.getDate());
                    }
                }
                break;
            }
            case GlobalConstant.RequestCalendarPermissionSynchronizationResultCode: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (self.my2017Events.size() > 0) {
                        new SynchronizeFavoriteToCalendarAsyncTask().execute(self.my2017Events);
                    }
                }
                break;
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case GlobalConstant.StartForEventDetailResult: {
                if (resultCode == RESULT_OK) {
                    int position = data.getIntExtra("TabPosition", 0);
                    SelectedTab(position);
                }
                break;
            }
            case GlobalConstant.StartActivityForFilterResult: {
                if (resultCode == RESULT_OK) {
                    EventFragment eventFragment = (EventFragment) fragmentList.get(0);
                    eventFragment.FilterEvent();
                    break;
                }
            }
        }
    }

    public class SynchronizeFavoriteToCalendarAsyncTask extends AsyncTask<ArrayList<EventDataBinding>, Void, Boolean> {
        @Override
        protected void onPostExecute(Boolean result) {
            BroadCastFavoriteEventSynchronization(result);
        }

        @Override
        protected Boolean doInBackground(ArrayList<EventDataBinding>... arrayLists) {
            ArrayList<EventDataBinding> favoritedItems = (ArrayList<EventDataBinding>) arrayLists[0];
            ContentResolver contentResolver = self.getContentResolver();
            Boolean result = SharedPreferenceHelper.SynchronizeToCalendar(self, contentResolver, favoritedItems);
            return result;
        }
    }




/*
    * Listener callback from background service
    * */

    // Fires when the data is available
    private void RegisterReceivingInitGlobalData() {
        // Register internet status

        InitDataBroadcastReceiver.InitDataBroadcastReceiverListener initDatalistener = new InitDataBroadcastReceiver.InitDataBroadcastReceiverListener() {
            @Override
            public void InitDataEvent() {
                BroadCastInitializedData();
            }
        };
        InitDataBroadcastReceiver initData = new InitDataBroadcastReceiver();
        initData.setDeletedEventBroadcastReceiverListener(initDatalistener);
        LocalBroadcastManager.getInstance(this).registerReceiver(initData, new IntentFilter(GlobalConfig.ServiceType.InitGlobalData));
    }

    // Fires when the data is available
    private void RegisterUpdatedOrDeletedEventBroadcast() {
        // Register internet status

        LocalBroadcastReceiver.LocalBroadcastReceiverListener localListener = new LocalBroadcastReceiver.LocalBroadcastReceiverListener() {
            @Override
            public void onReceiveListener() {
                UpdateData();
            }
        };
        LocalBroadcastReceiver localReceiver = new LocalBroadcastReceiver();
        localReceiver.setLocalBroadcastReceiverListener(localListener);
        LocalBroadcastManager.getInstance(this).registerReceiver(localReceiver, new IntentFilter(GlobalConfig.ServiceType.UpdatedOrDeletedEventSynchronization));
    }


    /*
    Listener callback from background service
    * */


    public void ShowEventDetail(EventDataBinding rowData, int tabPosition, int selectedEventType) {
        Intent eventDetail = new Intent(self, EventDetailActivity.class);

        eventDetail.putExtra("EventID", rowData.getEventData().getEventId());
        eventDetail.putExtra("IsFavoriteEvent", rowData.getEventData().getFavoriteEvent());
        eventDetail.putExtra("TicketUrl", rowData.getEventData().getTicketUrl());

        eventDetail.putExtra("ImageUrl", rowData.getEventData().getImageFullSize());
        eventDetail.putExtra("EventHeadline", rowData.getEventData().getEventHeadline());
        eventDetail.putExtra("DescriptionHeadline", rowData.getEventData().getDescriptionHeadline());
        eventDetail.putExtra("Description", rowData.getEventData().getDescription());
        eventDetail.putExtra("DescriptionHtml", rowData.getEventData().getDescriptionHtml());

        //String time = rowData.getEventData().getTimeSpan() == 0 ? self.getString(R.string.event_allday) : rowData.getEventData().getTime();
        eventDetail.putExtra("Time", rowData.getEventData().getTime());
        eventDetail.putExtra("TimeSpan", rowData.getEventData().getTimeSpan());
        eventDetail.putExtra("EndTimeSpan", rowData.getEventData().getEndTimeSpan());

        eventDetail.putExtra("City", rowData.getEventData().getLocationModel().getCity());
        eventDetail.putExtra("Distance", rowData.getEventData().getLocationModel().getDistance());
        eventDetail.putExtra("LocationName", rowData.getEventData().getLocationModel().getName());
        eventDetail.putExtra("Address", rowData.getEventData().getLocationModel().getAddress() + ", " + rowData.getEventData().getLocationModel().getZipCity());
        eventDetail.putExtra("VideoUrl", rowData.getEventData().getVideoUrl());
        eventDetail.putStringArrayListExtra("VideoUrls", rowData.getEventData().getVideoUrls());
        eventDetail.putExtra("EventVenueUrl", rowData.getEventData().getEventVenueUrl());
        eventDetail.putExtra("CurrentGroupDate", rowData.getDate());
        eventDetail.putExtra("FromDate", rowData.getEventData().getStartDateSimple());
        eventDetail.putExtra("ToDate", rowData.getEventData().getEndDateSimple());

        eventDetail.putExtra("ArtFormList", rowData.getEventData().getArtFormList());
        eventDetail.putExtra("AudienceList", rowData.getEventData().getAudienceList());
        eventDetail.putExtra("Disability", rowData.getEventData().getDisabilityAccess());
        eventDetail.putExtra("EventUrl", rowData.getEventData().getEventUrl());
        eventDetail.putExtra("ContactName", rowData.getEventData().getPracticalInfo().getContactName());
        eventDetail.putExtra("ContactPhone", rowData.getEventData().getPracticalInfo().getContactPhone());
        eventDetail.putExtra("ContactEmail", rowData.getEventData().getPracticalInfo().getContactEmail());
        eventDetail.putExtra("SoldOut", rowData.getEventData().getSoldOut());
        eventDetail.putExtra("PriceFrom", rowData.getEventData().getPriceFrom());
        eventDetail.putExtra("PriceTo", rowData.getEventData().getPriceTo());

        eventDetail.putExtra("Latitude", rowData.getEventData().getLocationModel().getLatitude());
        eventDetail.putExtra("Longitude", rowData.getEventData().getLocationModel().getLongitude());
        eventDetail.putExtra("Zoom", rowData.getEventData().getLocationModel().getZoomDistance());
        eventDetail.putExtra("ScheduledEventID", rowData.getEventData().getScheduledEventID());
        //eventDetail.putExtra("IsMy2017Event", isCurrentMy2017);
        eventDetail.putExtra("TabPosition", tabPosition);
        eventDetail.putExtra("IsHoliday",rowData.getIsHoliday());

        startActivityForResult(eventDetail, GlobalConstant.StartForEventDetailResult);

        GATrackerHelper.ShowEventDetail(self, rowData.getEventData().getEventId(), rowData.getEventData().getEventHeadline(), rowData.getDate(), selectedEventType);
    }

    private void AddFavorite(EventDataBinding rowData) {
        currentEventDataRow = rowData;
        SharedPreferenceHelper.SaveFavoriteEvent(self, rowData.getEventData().getScheduledEventID(), rowData.getDate());

        PromptPermissionResult setting = SharedPreferenceHelper.GetCalendarPromptSetting(self);

        switch (setting) {
            case Yes: {
                PermissionResult permissionResult = PermissionHelper.CheckCalendarPermission(self);
                switch (permissionResult) {
                    case Granted: {
                        ContentResolver contentResolver = self.getContentResolver();
                        long calendarEventID = GeneralHelper.AddEventToCalendar(contentResolver, rowData.getDate(), rowData.getEventData().getTimeSpan(), rowData.getEventData().getEndTimeSpan(), rowData.getEventData().getEventHeadline(),
                                rowData.getEventData().getDescription(), rowData.getEventData().getEventUrl(), rowData.getEventData().getLocationModel().getName() + ", " + rowData.getEventData().getLocationModel().getAddress());
                        SharedPreferenceHelper.UpdateFavoriteEvent(self, rowData.getEventData().getScheduledEventID(), calendarEventID, rowData.getDate());


                        break;
                    }
                    case Requested: {
                        ActivityCompat.requestPermissions(self, new String[]{Manifest.permission.WRITE_CALENDAR}, GlobalConstant.RequestCalendarPermissionResultCode);
                        break;
                    }
                    case Denied:
                        break;
                    default:
                        break;
                }
                break;
            }
            case No: {
                break;
            }
            case Ignore: {
                final Dialog acceptDialog = new Dialog(self);
                acceptDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                acceptDialog.setContentView(R.layout.prompt_calendar_dialog);
                acceptDialog.show();
                Typeface flavinBold = FontHelper.FlavinBold(self);
                Typeface flavinRegular = FontHelper.FlavinRegular(self);
                Typeface sourceSansProBold = FontHelper.SourceSansProBold(self);
                TextView titleTextView = (TextView) acceptDialog.findViewById(R.id.promtTitleTextView);
                TextView descriptionTextView = (TextView) acceptDialog.findViewById(R.id.promtDescriptionTextView);
                Button acceptButton = (Button) acceptDialog.findViewById(R.id.promptAcceptButton);
                Button noButton = (Button) acceptDialog.findViewById(R.id.promptNoButton);

                titleTextView.setTypeface(flavinBold);
                descriptionTextView.setTypeface(flavinRegular);
                acceptButton.setTypeface(sourceSansProBold);
                noButton.setTypeface(sourceSansProBold);
                acceptButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        acceptDialog.dismiss();
                        SharedPreferenceHelper.SaveCalendarPromptSetting(self, PromptPermissionResult.Yes);
                        ActivityCompat.requestPermissions(self, new String[]{Manifest.permission.WRITE_CALENDAR}, GlobalConstant.RequestCalendarPermissionResultCode);
                        GATrackerHelper.SynCalendar(self);

                    }
                });
                ImageButton closeImageButton = (ImageButton) acceptDialog.findViewById(R.id.promtCloseImageButton);
                closeImageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        acceptDialog.dismiss();
                        SharedPreferenceHelper.SaveCalendarPromptSetting(self, PromptPermissionResult.Ignore);
                    }
                });
                noButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        acceptDialog.dismiss();
                        SharedPreferenceHelper.SaveCalendarPromptSetting(self, PromptPermissionResult.No);
                    }
                });
                break;
            }
            default: {
                break;
            }
        }


        // Google Analytics
        GATrackerHelper.SaveFavorite(self, rowData.getEventData().getEventId(), rowData.getEventData().getEventHeadline(), rowData.getDate(), true);

    }

    private void RemoveFavorite(EventDataBinding rowData) {
        ContentResolver contentResolver = self.getContentResolver();
        SharedPreferenceHelper.RemoveFavoriteEvent(self, contentResolver, rowData.getEventData().getScheduledEventID(), rowData.getDate());
    }



    /*
    * Event Fragment Interaction
    * */

    @Override
    public void onAddFavoriteEventFragment(EventDataBinding value, int tabPosition) {
        AddFavorite(value);
    }

    @Override
    public void onRemoveFavoriteEventFragment(EventDataBinding value, int tabPosition) {
        RemoveFavorite(value);
    }

    @Override
    public void onClickEventFragment(EventDataBinding value, int tabPosition, int selectedEventType) {
        ShowEventDetail(value, tabPosition, selectedEventType);
    }

    /*
    * My2017 Fragment Interaction
    * */

    @Override
    public void onRemoveFavoriteMy2017Fragment(EventDataBinding value, int tabPosition) {
        RemoveFavorite(value);
    }

    @Override
    public void onShowEventListTab() {
        if (mainViewPager != null) {
            // Show event list tab
            mainViewPager.setCurrentItem(0);
        }
    }

    @Override
    public void onClickMy2017Fragment(EventDataBinding value, int tabPosition, int selectedEventType) {
        ShowEventDetail(value, tabPosition, selectedEventType);
    }

    @Override
    public void onSynchronizationFavoriteToCalendarMy2017Fragment(ArrayList<EventDataBinding> favoriteEvents) {
        self.my2017Events = favoriteEvents;
    }


   /*
    * Search Fragment
    * */

    @Override
    public void onAddFavoriteSearchFragment(EventDataBinding value, int tabPosition) {
        AddFavorite(value);
    }

    @Override
    public void onRemoveFavoriteSearchFragment(EventDataBinding value, int tabPosition) {
        RemoveFavorite(value);
    }

    @Override
    public void onClickSearchFragment(EventDataBinding value, int tabPosition, int selectedEventType) {
        ShowEventDetail(value, tabPosition, selectedEventType);
    }

}
