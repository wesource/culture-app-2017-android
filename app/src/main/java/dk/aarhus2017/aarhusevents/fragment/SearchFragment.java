package dk.aarhus2017.aarhusevents.fragment;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.adapter.EventDataAdapter;
import dk.aarhus2017.aarhusevents.model.EventData;
import dk.aarhus2017.aarhusevents.model.EventDataBinding;
import dk.aarhus2017.aarhusevents.model.My2017EventType;
import dk.aarhus2017.aarhusevents.model.SearchEndlessRecyclerOnScrollListener;
import dk.aarhus2017.aarhusevents.util.ConvertHelper;
import dk.aarhus2017.aarhusevents.util.EventHelper;
import dk.aarhus2017.aarhusevents.util.FBTrackerHelper;
import dk.aarhus2017.aarhusevents.util.FontHelper;
import dk.aarhus2017.aarhusevents.util.GATrackerHelper;
import dk.aarhus2017.aarhusevents.util.GlobalConfig;
import dk.aarhus2017.aarhusevents.util.InternetHelper;
import dk.aarhus2017.aarhusevents.util.KeyboardHelper;
import dk.aarhus2017.aarhusevents.util.LocationHelper;
import dk.aarhus2017.aarhusevents.util.SharedPreferenceHelper;


public class SearchFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    Context context;
    SearchFragment self = this;

    LinearLayout headerSearchHolderLinearLayout;
    EditText headerSearchEditText;
    Button headerSearchButton;
    SearchView headerSearchView;
    //EditText headerSearchViewEditText;


    RecyclerView eventListFragmentRecyclerView;
    RelativeLayout loadingHolderRelativeLayout;
    TextView loadingTextView;

    // Internet
    LinearLayout internetHolderLinearLayout;
    TextView internetTextView;

    // No Result
    RelativeLayout noResultHolderRelativeLayout;
    ImageView noResultImageView;
    LinearLayout noResultTextHolderLinearLayout;
    TextView noResultHeadlineTextView;
    TextView noResultDescriptionTextView;
    Button noResultActionButton;


    ArrayList<EventDataBinding> searchEventList = new ArrayList<>();
    EventDataAdapter eventDataAdapter;
    EventDataAdapter.EventListener eventListener;
    LinearLayoutManager layoutManager;
    SearchEndlessRecyclerOnScrollListener searchEndlessRecyclerOnScrollListener;

    SearchEventImmediateAsyncTask searchAsyncTask = new SearchEventImmediateAsyncTask();

    Paint paint = new Paint();

    String seachValue = "";
    int animationIndex = 1;


    private OnSearchFragmentInteractionListener mListener;

    public SearchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFragment newInstance(String param1, String param2) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        context = this.getContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        paint.setColor(Color.parseColor("#FF1E5A"));
        View v = inflater.inflate(R.layout.fragment_search, container, false);
        headerSearchHolderLinearLayout = (LinearLayout) v.findViewById(R.id.headerSearchHolderLinearLayout);
        //headerSearchEditText = (EditText) v.findViewById(R.id.headerSearchEditText);
        headerSearchButton = (Button) v.findViewById(R.id.headerSearchButton);

        headerSearchView = (SearchView) v.findViewById(R.id.headerSearchView);
        int editId = headerSearchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        headerSearchEditText = (EditText) headerSearchView.findViewById(editId);
        headerSearchEditText.setSingleLine(true);
        headerSearchEditText.setTextColor(ContextCompat.getColor(self.getContext(), R.color.text));
        headerSearchEditText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        headerSearchEditText.setGravity(Gravity.CENTER_VERTICAL);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, (float) 1.0);
        headerSearchEditText.setLayoutParams(layoutParams);

        // Internet Status
        internetHolderLinearLayout = (LinearLayout) v.findViewById(R.id.internetHolderLinearLayout);
        internetTextView = (TextView) v.findViewById(R.id.internetTextView);


        // Loading
        loadingHolderRelativeLayout = (RelativeLayout) v.findViewById(R.id.loadingHolderRelativeLayout);
        loadingTextView = (TextView) v.findViewById(R.id.loadingTextView);

        eventListFragmentRecyclerView = (RecyclerView) v.findViewById(R.id.eventListFragmentRecyclerView);

        // Init No result section
        InitNoResult(v);

        // Register event
        headerSearchButton.setOnClickListener(this);
        headerSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                seachValue = s.trim();
                if (!seachValue.isEmpty()) {
                    loadingHolderRelativeLayout.setVisibility(View.VISIBLE);
                    noResultHolderRelativeLayout.setVisibility(View.GONE);
                    eventListFragmentRecyclerView.setVisibility(View.GONE);

                    if (searchAsyncTask.getStatus() != AsyncTask.Status.FINISHED) {
                        try {
                            searchAsyncTask.cancel(true);
                        } catch (Exception ex) {
                        }
                    }
                    searchAsyncTask = new SearchEventImmediateAsyncTask();
                    searchAsyncTask.execute(seachValue.toLowerCase());

                } else {
                    if (searchAsyncTask.getStatus() != AsyncTask.Status.FINISHED) {
                        try {
                            searchAsyncTask.cancel(true);
                        } catch (Exception ex) {
                        }
                    }

                    self.searchEventList = new ArrayList<EventDataBinding>();
                    loadingHolderRelativeLayout.setVisibility(View.GONE);
                    noResultHolderRelativeLayout.setVisibility(View.GONE);
                    eventListFragmentRecyclerView.setVisibility(View.VISIBLE);
                    eventDataAdapter.updateData(self.searchEventList);
                }
                return false;
            }
        });


        // Set text
        loadingHolderRelativeLayout.setVisibility(View.GONE);
        loadingTextView.setText(getString(R.string.searching_event));


        InitSearchEventList();

        registerHideKeyboardOnScroll();

        // Set fonts
        SetFonts();


        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        headerSearchView.requestFocus();
        //KeyboardHelper.ShowKeyboard(headerSearchView,context);

        headerSearchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (keyEvent != null && (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) || i == EditorInfo.IME_ACTION_DONE) {
                    seachValue = headerSearchEditText.getText().toString().trim();
                    if (!seachValue.isEmpty()) {
                        loadingHolderRelativeLayout.setVisibility(View.VISIBLE);
                        eventListFragmentRecyclerView.setVisibility(View.GONE);
                        noResultHolderRelativeLayout.setVisibility(View.GONE);
                        new SearchEventAsyncTask().execute(seachValue.toLowerCase());
                    }
                }
                return false;
            }
        });

        GATrackerHelper.SendGAScreenView(GATrackerHelper.SearchScreen, self.getActivity());

        FBTrackerHelper.LogScreenView(self.getContext(), GATrackerHelper.SearchScreen);
    }

    private void InitNoResult(View v) {
        // No result
        noResultHolderRelativeLayout = (RelativeLayout) v.findViewById(R.id.noResultHolderRelativeLayout);
        noResultImageView = (ImageView) v.findViewById(R.id.noResultImageView);
        noResultTextHolderLinearLayout = (LinearLayout) v.findViewById(R.id.noResultTextHolderLinearLayout);
        noResultHeadlineTextView = (TextView) v.findViewById(R.id.noResultHeadlineTextView);
        noResultDescriptionTextView = (TextView) v.findViewById(R.id.noResultDescriptionTextView);
        noResultActionButton = (Button) v.findViewById(R.id.noresultActionButton);

        noResultHolderRelativeLayout.setVisibility(View.GONE);

        noResultActionButton.setOnClickListener(this);

        // Set text
        noResultHeadlineTextView.setText(getString(R.string.search_noresult_headline));
        noResultDescriptionTextView.setText(getString(R.string.search_noresult_description));
        noResultActionButton.setText(getString(R.string.search_noresult_button));


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.headerSearchButton: {
                seachValue = headerSearchEditText.getText().toString().trim();
                if (!seachValue.isEmpty()) {
                    if (searchAsyncTask.getStatus() != AsyncTask.Status.FINISHED) {
                        try {
                            searchAsyncTask.cancel(true);
                        } catch (Exception ex) {
                        }
                    }

                    loadingHolderRelativeLayout.setVisibility(View.VISIBLE);
                    eventListFragmentRecyclerView.setVisibility(View.GONE);
                    noResultHolderRelativeLayout.setVisibility(View.GONE);
                    new SearchEventAsyncTask().execute(seachValue.toLowerCase());
                }
                break;
            }
            case R.id.noresultActionButton: {
                noResultHolderRelativeLayout.setVisibility(View.GONE);
                headerSearchHolderLinearLayout.setVisibility(View.VISIBLE);
                headerSearchEditText.setText("");
                headerSearchEditText.requestFocus();
                ShowKeyboard();
                break;
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSearchFragmentInteractionListener) {
            mListener = (OnSearchFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void SetFonts() {
        Typeface sourceSansProRegular = FontHelper.SourceSansProRegular(context);
        Typeface sourceSansProBold = FontHelper.SourceSansProBold(context);
        Typeface flavinBold = FontHelper.FlavinBold(context);
        Typeface flavinRegular = FontHelper.FlavinRegular(context);

        headerSearchEditText.setTypeface(sourceSansProRegular);
        //headerSearchViewEditText.setTypeface(sourceSansProRegular);
        headerSearchButton.setTypeface(sourceSansProBold);

        // Internet
        internetTextView.setTypeface(sourceSansProRegular);

        noResultHeadlineTextView.setTypeface(flavinBold);
        noResultDescriptionTextView.setTypeface(flavinRegular);
        noResultActionButton.setTypeface(sourceSansProBold);

    }

    private void InitSearchEventList() {
        eventDataAdapter = new EventDataAdapter(getContext(), searchEventList,false);

        eventListFragmentRecyclerView.setAdapter(eventDataAdapter);
        eventListener = new EventDataAdapter.EventListener() {
            @Override
            public void onRowClick(EventDataBinding rowData) {
                if (mListener != null) {
                    mListener.onClickSearchFragment(rowData, 1, 0);
                }
            }
        };
        eventDataAdapter.setTaskListener(eventListener);

        layoutManager = new LinearLayoutManager(getContext());

        eventListFragmentRecyclerView.setLayoutManager(layoutManager);
        eventListFragmentRecyclerView.setItemAnimator(new DefaultItemAnimator());
        eventListFragmentRecyclerView.setDrawingCacheEnabled(true);
        eventListFragmentRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        eventListFragmentRecyclerView.setHasFixedSize(false);

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {


            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                EventDataBinding rowData = searchEventList.get(position);


                if (rowData.isGroupHeader()) {
                    return;
                }

                switch (direction) {
                    case ItemTouchHelper.LEFT: {
                        if (rowData.getEventData().getFavoriteEvent()) {
                            rowData.getEventData().setFavoriteEvent(false);
                            if (mListener != null) {
                                mListener.onRemoveFavoriteSearchFragment(rowData, 1);
                            }
                            eventDataAdapter.updateData(self.searchEventList);
                        }
                        break;
                    }
                    case ItemTouchHelper.RIGHT: {
                        if (!rowData.isGroupHeader()) {
                            if (!rowData.getEventData().getFavoriteEvent()) {
                                rowData.getEventData().setFavoriteEvent(true);

                                if (mListener != null) {
                                    mListener.onAddFavoriteSearchFragment(rowData, position);
                                }
                                eventDataAdapter.updateData(self.searchEventList);
                            }

                        }
                        break;
                    }
                    default:
                        break;
                }


            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

                EventDataBinding rowData = self.searchEventList.get(viewHolder.getAdapterPosition());
//                if (isCurrentMy2017) {
//                    rowData = self.my2017Events.get(viewHolder.getAdapterPosition());
//                } else {
//                    rowData = self.eventDataBindings.get(viewHolder.getAdapterPosition());
//                }
                if (!rowData.isGroupHeader()) {
                    if (rowData.getEventData().getFavoriteEvent()) {
                        return ItemTouchHelper.LEFT;
                    } else {
                        return ItemTouchHelper.RIGHT;
                    }
                } else {
                    return super.getSwipeDirs(recyclerView, viewHolder);
                }
            }

            @Override
            public void onChildDrawOver(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                if (!isCurrentlyActive) {
                    LinearLayout eventItemContentHolderLinearLayout = (LinearLayout) viewHolder.itemView.findViewById(R.id.eventItemContentHolderLinearLayout);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                    int margin = ConvertHelper.ConvertDpToPx(self.getContext(), 10);
                    layoutParams.setMarginStart(margin);
                    layoutParams.setMarginEnd(margin);
                    eventItemContentHolderLinearLayout.setLayoutParams(layoutParams);

                    super.onChildDrawOver(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }
            }


            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                int position = viewHolder.getAdapterPosition();
                EventDataBinding rowData = self.searchEventList.get(position);
//                if (isCurrentMy2017) {
//                    rowData = self.my2017Events.get(position);
//                } else {
//                    rowData = self.eventDataBindings.get(position);
//                }

                if (rowData.isGroupHeader()) {
                    return;
                }

                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;
                    LinearLayout seperateLine = (LinearLayout) viewHolder.itemView.findViewById(R.id.eventItemSeperationLinearLayout);
                    int seperateLineHeight = 0;
                    if (seperateLine.getVisibility() == View.VISIBLE) {
                        seperateLineHeight = seperateLine.getHeight();
                    } else {
                        seperateLineHeight = 0;
                    }

                    LinearLayout eventItemContentHolderLinearLayout = (LinearLayout) viewHolder.itemView.findViewById(R.id.eventItemContentHolderLinearLayout);

                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    if (rowData.getEventData().getFavoriteEvent()) {

                        if (dX < 0) {
                            layoutParams.setMarginEnd(0);
                            eventItemContentHolderLinearLayout.setLayoutParams(layoutParams);

                            RectF background = new RectF(itemView.getWidth() - Math.abs(dX), (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom() - seperateLineHeight);
                            c.drawRect(background, paint);

                            icon = BitmapFactory.decodeResource(self.getResources(), R.drawable.remove_saved_favorite);
                            int top = (itemView.getBottom() - itemView.getTop() - seperateLineHeight - icon.getHeight()) / 2;
                            int padding = ConvertHelper.ConvertDpToPx(self.getContext(), 30);
                            RectF icon_dest = new RectF(itemView.getWidth() - Math.abs(dX) + padding, (float) itemView.getTop() + top, itemView.getWidth() - Math.abs(dX) + padding + icon.getWidth(), (float) itemView.getBottom() - seperateLineHeight - top);
                            c.drawBitmap(icon, null, icon_dest, paint);


                            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

                        } else {
                            c = new Canvas();
                            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        }

                    } else {
                        if (dX > 0) {
                            layoutParams.setMarginStart(0);
                            eventItemContentHolderLinearLayout.setLayoutParams(layoutParams);
                            RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom() - seperateLineHeight);
                            c.drawRect(background, paint);
                            if (dX > 50) {
                                icon = BitmapFactory.decodeResource(self.getResources(), R.drawable.save_favorite);

                                int top = (itemView.getBottom() - itemView.getTop() - seperateLineHeight - icon.getHeight()) / 2;
                                int padding = ConvertHelper.ConvertDpToPx(self.getContext(), 30);

                                RectF icon_dest = new RectF(dX - padding - icon.getWidth(), (float) itemView.getTop() + top, dX - padding, (float) itemView.getBottom() - seperateLineHeight - top);
                                c.drawBitmap(icon, null, icon_dest, paint);
                            }

                            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

                        } else {

                            c = new Canvas();
                            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        }
                    }
                }
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(eventListFragmentRecyclerView);

    }

    public class SearchEventAsyncTask extends AsyncTask<String, Void, ArrayList<EventDataBinding>> {
        @Override
        protected ArrayList<EventDataBinding> doInBackground(String... objects) {
            ArrayList<EventDataBinding> bindings = null;
            try {
                String searchValue = objects[0];
                bindings = EventHelper.SearchEvents(context, GlobalConfig.SharedEvents, searchValue, new Date(), GlobalConfig.GetDefaultMaxDate());
                return bindings;
            } catch (Exception ex) {
            }
            return bindings;
        }

        @Override
        protected void onPostExecute(ArrayList<EventDataBinding> bindings) {
            super.onPostExecute(bindings);
            if (bindings == null) {
                self.searchEventList = new ArrayList<>();
            } else {
                self.searchEventList = bindings;
            }
            loadingHolderRelativeLayout.setVisibility(View.GONE);

            //HideKeyboard();
            KeyboardHelper.HideKeyboard(self.getView(), context);
            if (self.searchEventList.size() > 0) {
                eventListFragmentRecyclerView.setVisibility(View.VISIBLE);
                noResultHolderRelativeLayout.setVisibility(View.GONE);
                eventDataAdapter.updateData(self.searchEventList);
                Location myLocation = LocationHelper.getLocation(context);
                if (myLocation != null) {
                    eventDataAdapter.updateLocation(myLocation);
                }
            } else {
                headerSearchHolderLinearLayout.setVisibility(View.GONE);
                eventListFragmentRecyclerView.setVisibility(View.GONE);
                noResultHolderRelativeLayout.setVisibility(View.VISIBLE);

                // Start animation
                setNoResultEventAnimation();
            }

            // FB tracking search events
            FBTrackerHelper.LogSearchedEvent(self.getContext(), seachValue, self.searchEventList.size() > 0 ? true : false, self.searchEventList.size());

        }
    }

    public class UpdateEventAsyncTask extends AsyncTask<String, Void, ArrayList<EventDataBinding>> {
        @Override
        protected ArrayList<EventDataBinding> doInBackground(String... objects) {

            String searchValue = objects[0];

            ArrayList<EventDataBinding> bindings = EventHelper.SearchEvents(context, GlobalConfig.SharedEvents, searchValue, new Date(), GlobalConfig.GetDefaultMaxDate());

            return bindings;
        }

        @Override
        protected void onPostExecute(ArrayList<EventDataBinding> bindings) {
            super.onPostExecute(bindings);
            if (bindings == null) {
                self.searchEventList = new ArrayList<>();
            } else {
                self.searchEventList = bindings;
            }

            KeyboardHelper.HideKeyboard(self.getView(), context);
            if (self.searchEventList.size() > 0) {
                eventListFragmentRecyclerView.setVisibility(View.VISIBLE);
                noResultHolderRelativeLayout.setVisibility(View.GONE);
                eventDataAdapter.updateData(self.searchEventList);
                Location myLocation = LocationHelper.getLocation(context);
                if (myLocation != null) {
                    eventDataAdapter.updateLocation(myLocation);
                }
            } else {
                headerSearchHolderLinearLayout.setVisibility(View.GONE);
                eventListFragmentRecyclerView.setVisibility(View.GONE);
                noResultHolderRelativeLayout.setVisibility(View.VISIBLE);

                // Start animation
                setNoResultEventAnimation();
            }
        }
    }

    public class SearchEventImmediateAsyncTask extends AsyncTask<String, Void, ArrayList<EventDataBinding>> {
        @Override
        protected ArrayList<EventDataBinding> doInBackground(String... objects) {

            ArrayList<EventDataBinding> bindings = null;
            try {
                String searchValue = objects[0];

                bindings = EventHelper.SearchEvents(context, GlobalConfig.SharedEvents, searchValue, new Date(), GlobalConfig.GetDefaultMaxDate());

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return bindings;
        }

        @Override
        protected void onPostExecute(ArrayList<EventDataBinding> bindings) {
            super.onPostExecute(bindings);
            if (bindings == null) {
                self.searchEventList = new ArrayList<>();
            } else {
                self.searchEventList = bindings;
            }
            loadingHolderRelativeLayout.setVisibility(View.GONE);
            if (self.searchEventList.size() > 0) {
                eventListFragmentRecyclerView.setVisibility(View.VISIBLE);
                noResultHolderRelativeLayout.setVisibility(View.GONE);
                eventDataAdapter.updateData(self.searchEventList);
                Location myLocation = LocationHelper.getLocation(context);
                if (myLocation != null) {
                    eventDataAdapter.updateLocation(myLocation);
                }
            } else {
                eventListFragmentRecyclerView.setVisibility(View.GONE);
                noResultHolderRelativeLayout.setVisibility(View.VISIBLE);
                // Start animation
                setNoResultEventAnimation();
            }
        }
    }


    private void setNoResultEventAnimation() {

        noResultImageView.setImageResource(R.drawable.ic_empty_my2017_0);
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.empty2017_fade_in);
        noResultActionButton.startAnimation(animation);

        Animation animationText = AnimationUtils.loadAnimation(context, R.anim.empty2017_fade_in);
        animationText.setStartOffset(300);
        noResultTextHolderLinearLayout.startAnimation(animationText);

        final Animation animationImage = AnimationUtils.loadAnimation(context, R.anim.empty2017_fade_in);
        animationImage.setStartOffset(100);


        final Animation animationLoop = AnimationUtils.loadAnimation(context, R.anim.empty2017_fade);
        animationLoop.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (animationIndex < 8) {
                    noResultImageView.clearAnimation();
                    String uri = "@drawable/ic_empty_my2017_" + animationIndex;
                    int drawableID = getResources().getIdentifier(uri, null, context.getPackageName());
                    noResultImageView.setImageResource(drawableID);
                    noResultImageView.startAnimation(animationLoop);
                    animationIndex++;
                } else {
                    noResultImageView.clearAnimation();
                    animationIndex = 0;
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationImage.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                noResultImageView.startAnimation(animationLoop);
                animationIndex = 1;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        noResultImageView.startAnimation(animationImage);


    }

    public void UpdateData() {
        if (self.getContext() != null) {
            // Check internet status
            if (InternetHelper.HasInternetConnection(self.getContext() )) {
                internetHolderLinearLayout.setVisibility(View.GONE);
            } else {
                internetHolderLinearLayout.setVisibility(View.VISIBLE);
            }

            if (self.searchEventList.size() > 0) {
                new UpdateEventAsyncTask().execute(seachValue);
            } else {
                headerSearchView.requestFocus();
                KeyboardHelper.ShowKeyboard(headerSearchView, self.getContext());
                eventDataAdapter.updateData(self.searchEventList);

            }
        }
    }


    // Exchange data with Main Activity
    public void ChangedInternetStatus(boolean isConnected) {
        if (internetHolderLinearLayout != null) {
            if (isConnected) {
                internetHolderLinearLayout.setVisibility(View.GONE);
            } else {
                internetHolderLinearLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    private void ShowKeyboard() {
        // Hide keyboard
        headerSearchEditText.requestFocus();
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

    }

    private void registerHideKeyboardOnScroll() {
        searchEndlessRecyclerOnScrollListener = new SearchEndlessRecyclerOnScrollListener(layoutManager ) {
            @Override
            public void onHideKeyboard() {
                KeyboardHelper.HideKeyboard(headerSearchView,self.getContext());
            }
        };

        eventListFragmentRecyclerView.addOnScrollListener(searchEndlessRecyclerOnScrollListener);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnSearchFragmentInteractionListener {
        // TODO: Update argument type and name
        void onAddFavoriteSearchFragment(EventDataBinding value, int tabPosition);

        void onRemoveFavoriteSearchFragment(EventDataBinding value, int tabPosition);

        void onClickSearchFragment(EventDataBinding value, int tabPosition, int selectedEventType);
    }
}
