package dk.aarhus2017.aarhusevents.util;

import android.app.Activity;
import android.content.Intent;

import dk.aarhus2017.aarhusevents.services.DataSynchronizationService;

/**
 * Created by Dell on 2/28/2017.
 */

public class ServicesHelper {
    public static void DeletedEventsSynchronization(Activity activity){
        Intent syncServices=new Intent(activity, DataSynchronizationService.class);
        syncServices.putExtra(GlobalConfig.ServiceAction, GlobalConfig.ServiceType.DeletedEventSynchronization);
        activity.startService(syncServices);
    }

    public static void InitGlobalDataService(Activity activity){
        Intent syncServices=new Intent(activity, DataSynchronizationService.class);
        syncServices.putExtra(GlobalConfig.ServiceAction, GlobalConfig.ServiceType.InitGlobalData);
        activity.startService(syncServices);
    }
}
