package dk.aarhus2017.aarhusevents.model;

/**
 * Created by Dell on 12/16/2016.
 */

public class ArtformFilterModel {
    private String name;
    private int icon;
    private Boolean selected;
    private int metricIndex;

    public ArtformFilterModel(String _name, int _icon, boolean _selected)
    {
        this.name=_name;
        this.icon=_icon;
        this.selected = _selected;
    }
    public ArtformFilterModel(String _name, int _icon, boolean _selected, int _metricIndex)
    {
        this.name=_name;
        this.icon=_icon;
        this.selected = _selected;
        this.metricIndex = _metricIndex;
    }
    public void setSelected(boolean _selected)
    {
        this.selected=_selected;
    }

    public String getName(){return this.name;}

    public int getIcon(){return this.icon;}

    public Boolean getSelected(){return this.selected;}

    public String getMetricIndex(){return "metric"+ this.metricIndex;}
}
