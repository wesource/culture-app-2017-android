package dk.aarhus2017.aarhusevents.model;

/**
 * Created by Dell on 12/17/2016.
 */

public class AudienceFilterModel {
    private String name;
    private boolean selected;
    private int metricIndex;

    public AudienceFilterModel(String _name, Boolean _selected)
    {
        this.name = _name;
        this.selected =_selected;
    }
    public AudienceFilterModel(String _name, Boolean _selected, int _metricIndex)
    {
        this.name = _name;
        this.selected =_selected;
        this.metricIndex = _metricIndex;
    }

    public void setSelected(boolean _selected)
    {
        this.selected =_selected;
    }
    public String getName(){return this.name;}

    public Boolean getSelected(){return this.selected;}

    public String getMetricIndex(){return "metric"+this.metricIndex;}
}
