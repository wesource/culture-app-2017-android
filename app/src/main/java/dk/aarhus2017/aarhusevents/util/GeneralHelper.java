package dk.aarhus2017.aarhusevents.util;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.MenuItem;

import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.model.ArtformFilterModel;
import dk.aarhus2017.aarhusevents.model.AudienceFilterModel;
import dk.aarhus2017.aarhusevents.model.EventData;
import dk.aarhus2017.aarhusevents.model.EventDataBinding;
import dk.aarhus2017.aarhusevents.model.EventModel;
import dk.aarhus2017.aarhusevents.model.LocationCityFilterModel;
import dk.aarhus2017.aarhusevents.model.MenuModel;
import dk.aarhus2017.aarhusevents.model.PromptPermissionResult;
import dk.aarhus2017.aarhusevents.model.ScheduleDateModel;
import dk.aarhus2017.aarhusevents.model.ScheduledEventModel;

/**
 * Created by Dell on 12/3/2016.
 */

public class GeneralHelper {
    public static int GetTotalEvent(ArrayList<EventDataBinding> events) {
        int total = 0;
        for (EventDataBinding event : events) {
            if (!event.isGroupHeader()) {
                total++;
            }
        }
        return total;
    }

    public static void CheckPermissionLocation(Activity activity) {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        GlobalConstant.RequestLocationPermissionResultCode);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }


    public static Date GetMaxDate(ArrayList<EventModel> events) {
        Date date = new Date(System.currentTimeMillis());

        Date currentDate = new Date(System.currentTimeMillis());
        Calendar fromDate = ConvertHelper.getDate(currentDate);
        int index = 0;
        try {
            ArrayList<EventData> eventDataItem = new ArrayList<>();
            for (int x = 0; x < events.size(); x++) {
                EventModel eventModel = events.get(x);
                for (int y = 0; y < eventModel.getScheduledEvents().size(); y++) {
                    ScheduledEventModel scheduledEventModel = eventModel.getScheduledEvents().get(y);
                    if (date.before(scheduledEventModel.getEndDateTimeFormat())) {
                        date = scheduledEventModel.getEndDateTimeFormat();
                    }
                }
            }

        } catch (Exception ex) {
            Log.e("Convert Error", ex.getMessage());
        }

        return date;
    }

    public static Date GetMinDate(ArrayList<EventModel> events) {
        Date date = new Date(System.currentTimeMillis());

        Date currentDate = new Date(System.currentTimeMillis());
        Calendar fromDate = ConvertHelper.getDate(currentDate);
        int index = 0;
        try {
            ArrayList<EventData> eventDataItem = new ArrayList<>();
            for (int x = 0; x < events.size(); x++) {
                EventModel eventModel = events.get(x);
                for (int y = 0; y < eventModel.getScheduledEvents().size(); y++) {
                    ScheduledEventModel scheduledEventModel = eventModel.getScheduledEvents().get(y);
                    if (date.after(scheduledEventModel.getStartDateTimeFormat())) {
                        date = scheduledEventModel.getStartDateTimeFormat();
                    }
                }
            }

        } catch (Exception ex) {
            Log.e("Convert Error", ex.getMessage());
        }

        return date;
    }

    public static long AddEventToCalendar(ContentResolver contentResolver, Date currentDate, long timeSpan, long endTimeSpan, String title, String description, String eventUrl, String loctionName) {
        ArrayList<Long> eventIDs = new ArrayList<>();
        long eventIDResult = 0;
        try {
            ArrayList<Integer> calendarIDs = getCalendarId(contentResolver);
            for (Integer calendarID : calendarIDs) {
                ContentValues values = new ContentValues();
                values.put(CalendarContract.Events.CALENDAR_ID, calendarID);

                if (timeSpan == 0) {
                    Calendar calendar = GregorianCalendar.getInstance(getAppLocale());
                    calendar.setTimeInMillis(currentDate.getTime());
                    long fTime = calendar.getTimeInMillis() + 1000; //  86400000 is a day

                    long localTime = ConvertHelper.ConvertDenmarkToLocalTime(currentDate.getTime());
                    // values.put(CalendarContract.Events.DTSTART, GeneralHelper.GetDateStartOfDay( currentDate).getTime()+86400000);
                    //values.put( CalendarContract.Events.DTEND,  GeneralHelper.GetDateEndOfDay(currentDate).getTime());

                    values.put(CalendarContract.Events.DTSTART, localTime);
                    values.put(CalendarContract.Events.DTEND, localTime + 86400000);
                    // values.put(CalendarContract.Events.ALL_DAY,1);
                } else {

//                Calendar calendar = GregorianCalendar.getInstance();
//                calendar.setTimeInMillis(currentDate.getTime());
//                long fTime =   GeneralHelper.GetDateStartOfDay(currentDate).getTime() + timeSpan;
//
//                long tTime = GeneralHelper.GetDateStartOfDay(currentDate).getTime() + endTimeSpan;// currentDate.getTime() + 86400000-1000;

                    long localTime = ConvertHelper.ConvertDenmarkToLocalTime(currentDate.getTime());
                    long fTime = localTime + timeSpan;
                    long tTime = localTime + endTimeSpan;// currentDate.getTime() + 86400000-1000;


                    values.put(CalendarContract.Events.DTSTART, fTime);
                    values.put(CalendarContract.Events.DTEND, tTime);
                }
                //values.put(CalendarContract.Events.DURATION, time);
                values.put(CalendarContract.Events.TITLE, title);
                values.put(CalendarContract.Events.DESCRIPTION, description + " " + eventUrl);
                values.put(CalendarContract.Events.EVENT_LOCATION, loctionName);
                values.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());
                values.put(CalendarContract.Events.ACCESS_LEVEL, CalendarContract.Events.ACCESS_PUBLIC);
                values.put(CalendarContract.Events.HAS_ALARM, 1);

                Uri EVENTS_URI = Uri.parse(getCalendarUriBase(true) + "events");
                Uri uri = contentResolver.insert(EVENTS_URI, values);

                if (uri != null) {
                    eventIDResult = Long.parseLong(uri.getLastPathSegment());

                    eventIDs.add(eventIDResult);
                    Uri REMINDERS_URI = Uri.parse(getCalendarUriBase(true) + "reminders");
                    values = new ContentValues();
                    values.put(CalendarContract.Reminders.EVENT_ID, eventIDResult);
                    values.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
                    values.put(CalendarContract.Reminders.MINUTES, 30); // 720
                    Uri uriReminder = contentResolver.insert(REMINDERS_URI, values);
                }
            }
        } catch (Exception ex) {
            Log.e("Add Favorite Error", ex.getMessage());
        }

        return eventIDResult;
    }

    public static boolean DeleteCalendarEvent(ContentResolver contentResolver, long calendarEventID) {
        try {
            Uri EVENTS_URI = Uri.parse(getCalendarUriBase(true) + "events");
            int result = contentResolver.delete(EVENTS_URI, "_id=" + String.valueOf(calendarEventID), null);
            return result > 0;
        } catch (Exception ex) {
            return false;
        }
    }

    private static ArrayList<Integer> getCalendarId(ContentResolver contentResolver) {
        String projection[] = {"_id", "calendar_displayName"};
        Cursor cursor = contentResolver.query(Uri.parse("content://com.android.calendar/calendars"), projection, CalendarContract.CalendarEntity.IS_PRIMARY + "=1", null, null);
        if (cursor.getCount() < 1) {
            cursor = contentResolver.query(Uri.parse("content://com.android.calendar/calendars"), projection, null, null, null);
        }

        ArrayList<Integer> calendarIDs = new ArrayList<>();

        while (cursor.moveToNext()) {
            final String _id = cursor.getString(0);
            final String displayName = cursor.getString(1);
            //  final Boolean selected = !cursor.getString(2).equals("0");
            calendarIDs.add(Integer.parseInt(_id));
            break;
        }
        return calendarIDs;
    }

    private static String getCalendarUriBase(boolean eventUri) {
        Uri calendarURI = null;
        try {
            if (android.os.Build.VERSION.SDK_INT <= 7) {
                calendarURI = (eventUri) ? Uri.parse("content://calendar/") : Uri.parse("content://calendar/calendars");
            } else {
                calendarURI = (eventUri) ? Uri.parse("content://com.android.calendar/") : Uri
                        .parse("content://com.android.calendar/calendars");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return calendarURI.toString();
    }

    public static Boolean CheckStatusArtformFilter(ArrayList<ArtformFilterModel> values) {
        boolean result = false;
        for (ArtformFilterModel item : values) {
            if (item.getSelected()) {
                result = true;
                break;
            }
        }
        return result;
    }

    public static Boolean CheckStatusAudienceFilter(ArrayList<AudienceFilterModel> values) {
        boolean result = false;
        for (AudienceFilterModel item : values) {
            if (item.getSelected()) {
                result = true;
                break;
            }
        }
        return result;
    }

    public static String getDefaultLanguage() {
        String language = "en";
        String defaultLanguage = Locale.getDefault().getLanguage();
        if (defaultLanguage.equals("en") || defaultLanguage.equals("da") || defaultLanguage.equals("de")) {
            language = defaultLanguage;
        }

        return language;
    }

    public static ArrayList<MenuModel> GetMenuItems(Context context) {
        ArrayList<MenuModel> items = new ArrayList<>();
        items.add(new MenuModel("All", context.getString(R.string.event_menu_all_event), true));
        items.add(new MenuModel("Office", context.getString(R.string.event_menu_office_event), false));
        items.add(new MenuModel("UserSubmitted", context.getString(R.string.event_menu_user_submitted_event), false));
        return items;
    }

    public static String GetSelectedMenuItem(Context context, ArrayList<MenuModel> items) {
        String name = "";
        for (MenuModel item : items) {
            if (item.getSelected()) {
                name = item.getName();
                break;
            }
        }

        return name;
    }

    public static String GetWebSiteUrl() {
        String language = getDefaultLanguage();
        String url;
        switch (language) {
            case "en": {
                url = GlobalConfig.EnWebsiteUrl;
                break;
            }
            case "da": {
                url = GlobalConfig.DaWebsiteUrl;
                break;
            }
            case "de": {
                url = GlobalConfig.DeWebsiteUrl;
                break;
            }
            default: {
                url = GlobalConfig.EnWebsiteUrl;
                break;
            }
        }
        return url;
    }

    public static Locale getAppLocale() {
        Locale locale = Locale.ENGLISH;
        String language = Locale.getDefault().getLanguage();


        switch (language) {
            case "da": {
                locale = new Locale("da");
                break;
            }
            case "de": {
                locale = Locale.GERMAN;
                break;
            }
            default: {
                locale = Locale.ENGLISH;
                break;
            }
        }
        return locale;
    }

    public static long GetDateTimeSpan(String _utcDate) {
        long time = 0;
        try {
            Date date = GlobalConfig.DateFullFormat.parse(_utcDate);
            time = date.getTime();
        } catch (Exception ex) {
        }
        return time;
    }

    public static Date GetFullDate(String _date) {
        Date date = new Date();
        try {
            date = GlobalConfig.DateFullFormat.parse(_date);
        } catch (Exception ex) {
        }
        return date;
    }

    public static Date GetShortDate(String _date) {
        Date date = new Date();
        try {
            date = GlobalConfig.FormatDate.parse(_date);
        } catch (Exception ex) {
        }
        return date;
    }


    public static Date GetDateStartOfDay(Date _date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(_date.getTime());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Date GetDateStartOfDay(long _date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(_date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Date GetDateEndOfDay(Date _date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(_date.getTime());
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    public static Date GetDateEndOfDay(long _date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(_date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }


    public static Calendar GetCalendarStartOfDay(Date _date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(_date.getTime());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    public static Calendar GetCalendarEndOfDay(Date _date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(_date.getTime());
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar;
    }

    public static Calendar GetFromDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight
        calendar.set(Calendar.MINUTE, 0);                 // set minute in hour
        calendar.set(Calendar.SECOND, 0);                 // set second in minute
        calendar.set(Calendar.MILLISECOND, 0);            // set millis in second
        return calendar;
    }

    public static Calendar GetToDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);            // set hour to midnight
        calendar.set(Calendar.MINUTE, 59);                 // set minute in hour
        calendar.set(Calendar.SECOND, 59);                 // set second in minute
        calendar.set(Calendar.MILLISECOND, 999);            // set millis in second
        return calendar;
    }

    public static ArrayList<ScheduleDateModel> GetRangeDates(String _fromDate, String _toDate) {
        ArrayList<ScheduleDateModel> results = new ArrayList<>();
        try {
            Date startDate = GlobalConfig.DateFullFormat.parse(_fromDate);
            Date endDate = GlobalConfig.DateFullFormat.parse(_toDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startDate);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);

            Calendar endCalendar = Calendar.getInstance();
            endCalendar.setTime(endDate);
            endCalendar.set(Calendar.HOUR_OF_DAY, 23);
            endCalendar.set(Calendar.MINUTE, 59);
            endCalendar.set(Calendar.SECOND, 59);

            while (calendar.before(endCalendar)) {
                results.add(new ScheduleDateModel(calendar.getTime(), 0, 0, calendar.get(Calendar.DAY_OF_WEEK) - 1));
                calendar.add(Calendar.DAY_OF_MONTH, 1);
            }
            if (results.size() > 0) {
                calendar.setTime(startDate);
                int hours = calendar.get(Calendar.HOUR_OF_DAY);
                int min = calendar.get(Calendar.MINUTE);
                int second = calendar.get(Calendar.SECOND);
                long timeSpan = (hours * 60 * 60 * 1000) + (min * 60 * 1000) + (second * 1000);

                ScheduleDateModel model = results.get(0);
                model.setTimeSpan(timeSpan);
                if (results.size() == 1) {
                    calendar.setTime(endDate);
                    int h = calendar.get(Calendar.HOUR_OF_DAY);
                    int m = calendar.get(Calendar.MINUTE);
                    int s = calendar.get(Calendar.SECOND);
                    long t = (h * 60 * 60 * 1000) + (m * 60 * 1000) + (s * 1000);
                    model.setEndTimeSpan(t);
                }
            }

            if (results.size() > 1) {
                calendar.setTime(endDate);
                int hours = calendar.get(Calendar.HOUR_OF_DAY);
                int min = calendar.get(Calendar.MINUTE);
                int second = calendar.get(Calendar.SECOND);
                long timeSpan = (hours * 60 * 60 * 1000) + (min * 60 * 1000) + (second * 1000);

                ScheduleDateModel model = results.get(results.size() - 1);
                model.setTimeSpan(timeSpan);
                model.setEndTimeSpan(86399000);// End of day
            }
        } catch (Exception ex) {
        }
        return results;
    }

    public static ArrayList<String> GetRangeDates(Date _fromDate, Date _toDate) {
        ArrayList<String> results = new ArrayList<>();
        try {
            Calendar fromCalendar = GeneralHelper.GetCalendarStartOfDay(_fromDate);
            Calendar endCalendar = GeneralHelper.GetCalendarEndOfDay(_toDate);
            while (fromCalendar.before(endCalendar)) {
                results.add(GlobalConfig.FormatDate.format(fromCalendar.getTime()));
                fromCalendar.add(Calendar.DAY_OF_MONTH, 1);
            }
        } catch (Exception ex) {
        }
        return results;
    }

    public static Boolean IsValidateEventType(String eventType, Boolean externallyCreated) {
        boolean result = false;
        switch (eventType) {
            case "All": {
                result = true;
                break;
            }
            case "OnlyBackendCreated": {
                if (externallyCreated == false) {
                    result = true;
                } else {
                    result = false;
                }
                break;
            }
            case "OnlyExternallyCreated": {
                if (externallyCreated) {
                    result = true;
                } else {
                    result = false;
                }
                break;
            }
        }
        return result;
    }

    public static String TimeFormat(long fromTimeSpan, long toTimeSpan) {
        String time = "";
        if (fromTimeSpan < toTimeSpan) {
            time = GetTime(fromTimeSpan) + " - " + GetTime(toTimeSpan);
        } else {
            time = GetTime(fromTimeSpan);
        }
        return time;
    }

    public static String GetTime(long timeSpan) {
        String time = "";
        long hours = (timeSpan / 1000 / 60 / 60);
        long min = (timeSpan - (hours * 60 * 60 * 1000)) / (1000 * 60);

        time = (hours > 9 ? String.valueOf(hours) : "0" + hours) + ":" + (min > 9 ? String.valueOf(min) : "0" + min);
        return time;
    }


    public static long GetTimeSpanOfDay(String timeSpan) {
        long result = 0;
        long hour = 0;
        long minute = 0;
        long second = 0;
        if (timeSpan.contains(":")) {
            String[] items = timeSpan.split(":");
            switch (items.length) {
                case 2: {
                    hour = Integer.parseInt(items[0]);
                    minute = Integer.parseInt(items[1]);
                    break;
                }
                case 3: {
                    hour = Integer.parseInt(items[0]);
                    minute = Integer.parseInt(items[1]);
                    second = Integer.parseInt(items[2]);
                    break;
                }
            }
        } else {
            hour = Integer.parseInt(timeSpan);
        }

        result = ((hour * 60 * 60) + (minute * 60) + second) * 1000;

        return result;

    }


    public static String ShortDateFormat(Date date) {
        String format = "";
        try {
            format = GlobalConfig.FormatDate.format(date);
        } catch (Exception ex) {
        }
        return format;
    }

    public static String ShortDateFormat(long fDate) {
        String format = "";
        try {
            format = GlobalConfig.FormatDate.format(fDate);
        } catch (Exception ex) {
        }
        return format;
    }


    public static int GetNumberOfDate(long _fDate, long _tDate) {
        int numberOfDay = 0;
        try {
            Date fromDate = new Date(_fDate);
            Date toDate = new Date(_tDate);
            Calendar fromCalendar = GeneralHelper.GetCalendarStartOfDay(fromDate);
            Calendar endCalendar = GeneralHelper.GetCalendarEndOfDay(toDate);
            while (fromCalendar.before(endCalendar)) {
                fromCalendar.add(Calendar.DAY_OF_MONTH, 1);
                numberOfDay++;
            }
        } catch (Exception ex) {
        }
        return numberOfDay;
    }

    public static ArrayList<ArtformFilterModel> GetArtformModels(Activity activity) {
        ArrayList<ArtformFilterModel> artformItems = new ArrayList<>();
        artformItems.add(new ArtformFilterModel(activity.getString(R.string.artform_hightlight), (R.drawable.ic_highlight), false, 2));
        artformItems.add(new ArtformFilterModel(activity.getString(R.string.artform_music), (R.drawable.ic_musicandsound), false, 3));
        artformItems.add(new ArtformFilterModel(activity.getString(R.string.artform_performingart), (R.drawable.ic_performingarts), false, 4));
        artformItems.add(new ArtformFilterModel(activity.getString(R.string.artform_design), (R.drawable.ic_design), false, 5));
        artformItems.add(new ArtformFilterModel(activity.getString(R.string.artform_visualart), (R.drawable.ic_visualarts), false, 6));
        artformItems.add(new ArtformFilterModel(activity.getString(R.string.artform_festival), (R.drawable.ic_festivalandhappenings), false, 7));
        artformItems.add(new ArtformFilterModel(activity.getString(R.string.artform_film), (R.drawable.ic_filmandanimation), false, 8));
        artformItems.add(new ArtformFilterModel(activity.getString(R.string.artform_litterature), (R.drawable.ic_litterature), false, 9));
        artformItems.add(new ArtformFilterModel(activity.getString(R.string.artform_architecture), (R.drawable.ic_architecture), false, 10));
        return artformItems;
    }

    public static ArrayList<AudienceFilterModel> GetAudienceModels(Activity activity) {
        ArrayList<AudienceFilterModel> audienceItems = new ArrayList<>();
        audienceItems.add(new AudienceFilterModel(activity.getString(R.string.audience_children), false, 11));
        audienceItems.add(new AudienceFilterModel(activity.getString(R.string.audience_everyone), false, 12));
        audienceItems.add(new AudienceFilterModel(activity.getString(R.string.audience_family), false, 13));
        audienceItems.add(new AudienceFilterModel(activity.getString(R.string.audience_adult), false, 14));
        audienceItems.add(new AudienceFilterModel(activity.getString(R.string.audience_youngpeople), false, 15));
        audienceItems.add(new AudienceFilterModel(activity.getString(R.string.audience_senior), false, 16));
        return audienceItems;
    }

    public static ArrayList<LocationCityFilterModel> GetCity(Activity activity, String currentCity, Boolean isCurrentLocation) {

        final PromptPermissionResult setting = SharedPreferenceHelper.GetGeoLocationPromptSetting(activity);

        ArrayList<LocationCityFilterModel> cityFilterModels = new ArrayList<>();
//        if (setting != PromptPermissionResult.No) {
        cityFilterModels.add(new LocationCityFilterModel(true, activity.getString(R.string.city_current), 0.0, 0.0, 8, false));
        //}
        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_aarhus), 56.162939, 10.203921000000037, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_favrskov), 56.3012924, 9.892063199999939, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_hedensted), 55.77171800000001, 9.702279999999973, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_herning), 56.138557, 8.967321999999967, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_holstebro), 56.36153400000001, 8.621726999999964, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_horsens), 55.8581302, 9.847588099999939, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_ikast), 56.0145296, 9.233738700000004, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_lemvig), 56.5443443, 8.302487100000008, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_norddjurs), 56.4858413, 10.662047400000006, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_odder), 55.97571799999999, 10.14995799999997, 8, false));

        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_randers), 56.460584, 10.036538999999948, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_region_midtjylland), 56.302139, 9.302776900000026, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_ringkobing), 56.04468869999999, 8.505890000000022, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_samso), 55.81466649999999, 10.588629399999945, 8, false));

        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_silkeborg), 56.176362, 9.554921599999943, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_skanderborg), 56.03724700000001, 9.929798900000037, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_skive), 56.5651232, 9.030908299999965, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_struer), 56.48493, 8.589932999999974, 8, false));

        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_syddjurs), 56.31636779999999, 0.526505799999995, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_viborg), 56.45202699999999, 9.396346999999992, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(activity.getString(R.string.city_pafos), 34.9164594, 32.49200880000001, 8, false));

        for (LocationCityFilterModel city : cityFilterModels) {
            if (isCurrentLocation) {
                if (city.getIsCurrentLocation()) {
                    city.setSelected(true);
                }
            } else {
                if (city.getCity().equals(currentCity)) {
                    city.setSelected(true);
                }
            }
        }
        return cityFilterModels;
    }

    public static ArrayList<String> GetHolidays(Context context) {
        ArrayList<String> holidays = new ArrayList<>();
        String[] arrayHolidays = context.getResources().getStringArray(R.array.holiday_list);
        for (String holiday : arrayHolidays) {
            holidays.add(holiday);
        }
        return holidays;
    }


}
