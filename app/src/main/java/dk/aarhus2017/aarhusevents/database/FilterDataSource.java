package dk.aarhus2017.aarhusevents.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import dk.aarhus2017.aarhusevents.model.DBFavoriteEventModel;
import dk.aarhus2017.aarhusevents.util.GeneralHelper;
import dk.aarhus2017.aarhusevents.util.GlobalConfig;

/**
 * Created by Dell on 12/21/2016.
 */

public class FilterDataSource extends BaseDataSource {
    public FilterDataSource(Context context) {
        super(context);
    }

    public Boolean InsertFavoriteEvent(int scheduledEventID, Date scheduledDate) {
        long result = 0;
        String date =  "";

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            date = simpleDateFormat.format(scheduledDate);
        } catch (Exception ex) {
        }

        try {

            ContentValues values = new ContentValues();
            values.put(DatabaseHelper.Table_Favorite_ScheduledEventID, scheduledEventID);
            values.put(DatabaseHelper.Table_Favorite_ScheduledDate, date);
            values.put(DatabaseHelper.Table_Favorite_CalendarEventID, 0);
            result = database.insert(DatabaseHelper.Table_Favorite, null, values);
        } catch (Exception ex) {
            Log.e("InsertFavorite", ex.getMessage());
        }
        return result > 0;
    }

    public Boolean UpdateFavoriteEvent(int scheduledEventID, long calendarEventID, Date scheduledDate) {
        long result = 0;
        String date = "";

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            date = simpleDateFormat.format(scheduledDate);
        } catch (Exception ex) {
        }
        try {
            String whereClause = DatabaseHelper.Table_Favorite_ScheduledEventID + " = ? AND " + DatabaseHelper.Table_Favorite_ScheduledDate + " = ?";
            String whereArg[] = {String.valueOf(scheduledEventID), date};

            ContentValues values = new ContentValues();
            values.put(DatabaseHelper.Table_Favorite_CalendarEventID, calendarEventID);
            result = database.update(DatabaseHelper.Table_Favorite, values, whereClause, whereArg);
        } catch (Exception ex) {
            Log.e("InsertFavorite", ex.getMessage());
        }
        return result > 0;
    }

    public ArrayList<DBFavoriteEventModel> GetFavoriteEvents() {
        ArrayList<DBFavoriteEventModel> items = new ArrayList<>();

        String selectQuery = "SELECT  * FROM " + DatabaseHelper.Table_Favorite;
        Cursor cursor = database.rawQuery(selectQuery, null);

        Integer scheduledEventID = 0;
        String scheduledDate;

        if (cursor.moveToFirst()) {
            do {
                scheduledEventID = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Table_Favorite_ScheduledEventID));
                scheduledDate = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Favorite_ScheduledDate));
                items.add(new DBFavoriteEventModel(scheduledEventID,scheduledDate));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return items;
    }



    public Boolean RemoveFavoriteEvent(int scheduledEventID, Date scheduledDate) {
        long result = 0;
        String date = "";

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            date = simpleDateFormat.format(scheduledDate);
        } catch (Exception ex) {
        }
        try {
            String whereClause = DatabaseHelper.Table_Favorite_ScheduledEventID + " = ? AND " + DatabaseHelper.Table_Favorite_ScheduledDate + " = ?";
            String whereArg[] = {String.valueOf(scheduledEventID), date};

            result = database.delete(DatabaseHelper.Table_Favorite, whereClause, whereArg);
        } catch (Exception ex) {
            Log.e("InsertFavorite", ex.getMessage());
        }
        return result > 0;
    }

    public Integer GetCalendarID(int scheduledEventID, Date scheduledDate) {

        String date = "";
        try {
            date = GlobalConfig.FormatDate.format(scheduledDate);
        } catch (Exception ex) {
        }
        String selectQuery = "SELECT * FROM " + DatabaseHelper.Table_Favorite + " WHERE " + DatabaseHelper.Table_Favorite_ScheduledEventID + " = ? "
                + " AND " + DatabaseHelper.Table_Favorite_ScheduledDate + " = ?" ;

        String[] selectionArg ={String.valueOf(scheduledEventID),date};

        Cursor cursor = database.rawQuery(selectQuery, selectionArg);

        Integer calendarEventID = 0;

        if (cursor.moveToFirst()) {
            calendarEventID = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Table_Favorite_CalendarEventID));
//            do {
//                calendarEventID = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Table_Favorite_CalendarEventID));
//                break;
//            } while (cursor.moveToNext());
        }
        cursor.close();

        return calendarEventID;
    }

    public ArrayList<DBFavoriteEventModel> GetNotSyncScheduledEvents() {

        ArrayList<DBFavoriteEventModel> items = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + DatabaseHelper.Table_Favorite + " WHERE " + DatabaseHelper.Table_Favorite_CalendarEventID + " < 1 ";
        Cursor cursor = database.rawQuery(selectQuery, null);

        Integer scheduledEventID = 0;
        String scheduledDate="";

        while (cursor.moveToNext()) {

            scheduledEventID = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Table_Favorite_ScheduledEventID));
            scheduledDate = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Favorite_ScheduledDate));
            items.add(new DBFavoriteEventModel(scheduledEventID,scheduledDate));
        }
        cursor.close();
        return items;
    }


}
