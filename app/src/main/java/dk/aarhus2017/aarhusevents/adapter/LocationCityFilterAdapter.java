package dk.aarhus2017.aarhusevents.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.model.LocationCityFilterModel;
import dk.aarhus2017.aarhusevents.util.FontHelper;


/**
 * Created by Dell on 12/23/2016.
 */

public class LocationCityFilterAdapter extends ArrayAdapter<LocationCityFilterModel> {
    Context context;
    ArrayList<LocationCityFilterModel> cityItems=new ArrayList<>();
    protected LocationCityFilterAdapter.LocationCityFilterListener locationCityFilterListener;

    public LocationCityFilterAdapter(Context _context, ArrayList<LocationCityFilterModel> values)
    {
        super(_context,-1,values);
        this.cityItems = values;
        this.context=_context;
    }



    @Override
    public void add(LocationCityFilterModel object) {
        cityItems.add(object);
        super.add(object);
    }


    public void UpdateData(ArrayList<LocationCityFilterModel> values){
        this.cityItems =values;
        notifyDataSetChanged();
    }

    @Override
    public LocationCityFilterModel getItem(int position) {
        return cityItems.get(position);
    }

    @Override
    public int getCount() {
        return  this.cityItems.size();
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return customView(position, convertView, parent);

    }



      @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
          return customView(position, convertView, parent);
    }


    private View customView(int position, final View convertView, final ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.filter_city_item, parent, false);
        final Typeface sourceSansProRegular = FontHelper.SourceSansProRegular(context);
        final Typeface sourceSansProBold = FontHelper.SourceSansProBold(context);

        final LocationCityFilterModel dataRow = getItem(position);

        final TextView cityName =(TextView)row.findViewById(R.id.locationFilterCityNameTextView);
        final ImageView selected = (ImageView) row.findViewById(R.id.locationFilterCitySelectedImageView);

        cityName.setText(dataRow.getCity());

        if(dataRow.getSelected())
        {
            cityName.setTypeface(sourceSansProBold);
            selected.setVisibility(View.VISIBLE);
        }
        else
        {
            cityName.setTypeface(sourceSansProRegular);
            selected.setVisibility(View.INVISIBLE);
        }
        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(locationCityFilterListener!=null){
                    locationCityFilterListener.onCityChanged(dataRow);
                    dataRow.setSelected(true);
                }
            }
        });


        return row;
    }



    public void setTaskListener(LocationCityFilterAdapter.LocationCityFilterListener _listener){
        this.locationCityFilterListener = _listener;
    }

    public interface LocationCityFilterListener {
        void onCityChanged(LocationCityFilterModel rowData);

    }
}
