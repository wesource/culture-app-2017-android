package dk.aarhus2017.aarhusevents.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;

/**
 * Created by Dell on 3/1/2017.
 */

public class InitDataBroadcastReceiver extends  BroadcastReceiver {


   protected InitDataBroadcastReceiverListener listener;

    public InitDataBroadcastReceiver(){
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(listener!=null){
            listener.InitDataEvent();
        }
    }
    public void setDeletedEventBroadcastReceiverListener(InitDataBroadcastReceiver.InitDataBroadcastReceiverListener _listener){
        listener = _listener;
    }

    public interface InitDataBroadcastReceiverListener{
        void InitDataEvent();
    }

}
