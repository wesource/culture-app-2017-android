package dk.aarhus2017.aarhusevents.util;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.media.MediaCodec;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.internal.util.Predicate;



import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Pattern;



import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.model.DBFavoriteEventModel;
import dk.aarhus2017.aarhusevents.model.EventData;
import dk.aarhus2017.aarhusevents.model.EventDataBinding;
import dk.aarhus2017.aarhusevents.model.EventModel;
import dk.aarhus2017.aarhusevents.model.GeoLocationModel;
import dk.aarhus2017.aarhusevents.model.OpeningHourModel;
import dk.aarhus2017.aarhusevents.model.ScheduleDateModel;
import dk.aarhus2017.aarhusevents.model.ScheduledEventModel;

/**
 * Created by smart on 1/17/2017.
 */

public class EventHelper {


    public static ArrayList<EventDataBinding> ConvertToEventDataFilterByFavorite(Context activity, Map<String, ArrayList<EventData>> events) {
        ArrayList<EventDataBinding> results = new ArrayList<EventDataBinding>();
        ArrayList<String> holidays = GeneralHelper.GetHolidays(activity);

        ArrayList<DBFavoriteEventModel> favoriteItems = SharedPreferenceHelper.GetFavoriteEvents(activity);
        if (favoriteItems.size() < 1) {
            return results;
        }
        try {
            for (Map.Entry<String, ArrayList<EventData>> item : events.entrySet()) {
                ArrayList<EventDataBinding> groupItems = new ArrayList<>();
                boolean isHoliday = holidays.contains(item.getKey());
                for (EventData eventData : item.getValue()) {
                    for (DBFavoriteEventModel model : favoriteItems) {
                        if (model.getScheduledEventID() == eventData.getScheduledEventID() && model.getScheduledDate().equals(eventData.getShortDate())) {
                            eventData.setFavoriteEvent(true);
                            groupItems.add(new EventDataBinding(false, eventData.getCurrentDate(), eventData, isHoliday));

                            favoriteItems.remove(model);
                            break;
                        }
                    }
                }
                if (groupItems.size() > 0) {
                    results.add(new EventDataBinding(true, GlobalConfig.FormatDate.parse(item.getKey()), new EventData(), isHoliday));
                    results.addAll(groupItems);
                }

            }
        } catch (Exception ex) {
            Log.e("Convert Error", ex.getMessage());
        }
        return results;
    }


    public static ArrayList<EventDataBinding> ConvertToFilterEventData(Context activity, Map<String, ArrayList<EventData>> events, String eventType, Date startDate, Date endDate, Set<String> artFormsFilter, Set<String> audienceFilter, GeoLocationModel location, Integer distance) {
        ArrayList<EventDataBinding> results = new ArrayList<EventDataBinding>();
        ArrayList<EventDataBinding> groupItems = new ArrayList<>();

        ArrayList<DBFavoriteEventModel> favoriteItems = SharedPreferenceHelper.GetFavoriteEvents(activity);
        ArrayList<String> dateItems = GeneralHelper.GetRangeDates(startDate, endDate);
        ArrayList<String> holidays = GeneralHelper.GetHolidays(activity);
        try {
            for (Map.Entry<String, ArrayList<EventData>> item : events.entrySet()) {
                boolean isHoliday = holidays.contains(item.getKey());
                if (dateItems.contains(item.getKey())) {
                    groupItems = new ArrayList<>();
                    for (EventData eventData : item.getValue()) {
                        if (GeneralHelper.IsValidateEventType(eventType, eventData.getExternallyCreated())) {
                            boolean isCondition = false;
                            if (artFormsFilter.size() > 0) {
                                for (String artform : eventData.getArtFormListItems()) {
                                    if (artFormsFilter.contains(artform)) {
                                        isCondition = true;
                                        break;
                                    }
                                }
                            } else {
                                isCondition = true;
                            }

                            if (isCondition) {
                                isCondition = false;
                                if (audienceFilter.size() > 0) {
                                    for (String audience : eventData.getAudienceListItems()) {
                                        if (audienceFilter.contains(audience)) {
                                            isCondition = true;
                                            break;
                                        }
                                    }
                                } else {
                                    isCondition = true;
                                }
                            }
                            if (isCondition && location != null) {
                                Location loc = new Location(eventData.getLocationModel().getName());
                                loc.setLatitude(location.getLatitude());
                                loc.setLongitude(location.getLongitude());
                                eventData.getLocationModel().setLocation(loc);
                                if (eventData.getLocationModel().getDistance() <= distance) {
                                    isCondition = true;
                                } else {
                                    isCondition = false;
                                }
                            }
                            if (isCondition) {
                                eventData.setFavoriteEvent(false);
                                for (DBFavoriteEventModel favorite : favoriteItems) {
                                    if (favorite.getScheduledEventID() == eventData.getScheduledEventID() && favorite.getScheduledDate().equals(eventData.getShortDate())) {
                                        eventData.setFavoriteEvent(true);
                                        favoriteItems.remove(favorite);
                                        break;
                                    }
                                }

                                groupItems.add(new EventDataBinding(false, eventData.getCurrentDate(), eventData, isHoliday));
                            }
                        }
                    }
                    if (groupItems.size() > 0) {
                        results.add(new EventDataBinding(true, GlobalConfig.FormatDate.parse(item.getKey()), new EventData(), isHoliday));
                        results.addAll(groupItems);
                    }

                }
            }
        } catch (Exception ex) {
            Log.e("Parse", ex.getMessage());
        }

        return results;
    }


    public static ArrayList<EventDataBinding> UpdateFavoriteEvents(ArrayList<EventDataBinding> values) {
        ArrayList<EventDataBinding> results = new ArrayList<>();
        EventDataBinding eventGroup = new EventDataBinding();
        ArrayList<EventDataBinding> groupItems = new ArrayList<>();
        for (EventDataBinding item : values) {
            if (item.isGroupHeader()) {
                if (groupItems.size() > 0) {
                    results.add(eventGroup);
                    results.addAll(groupItems);
                }
                eventGroup = item;
                groupItems = new ArrayList<EventDataBinding>();
            } else {
                groupItems.add(item);
            }

        }
        if (groupItems.size() > 0) {
            results.add(eventGroup);
            results.addAll(groupItems);
        }
        return results;
    }


    public static ArrayList<EventData> ConvertToEventData(Context context, ArrayList<EventModel> events) {
        ArrayList<EventData> results = new ArrayList<EventData>();
        try {
            String allDay = context.getString(R.string.opening_hour_allday).toLowerCase();
            String allTime = context.getString(R.string.opening_hour_alltime).toLowerCase();
            String allDayDisplay = context.getString(R.string.event_allday);
            String seeWebsite = context.getString(R.string.see_website);
            for (EventModel eventModel : events) {
                for (ScheduledEventModel scheduledEventModel : eventModel.getScheduledEvents()) {
                    ArrayList<ScheduleDateModel> scheduledDates = GeneralHelper.GetRangeDates(scheduledEventModel.getStartDateTime(), scheduledEventModel.getEndDateTime());
                    for (final ScheduleDateModel scheduledDate : scheduledDates) {
                        EventData eventItem = null;

                        if (!eventModel.getOpeningHoursAlternativeText().isEmpty()) {
                            eventItem = new EventData(eventModel.getEventId(), eventModel.getEventHeadline(), eventModel.getImageUrl(), eventModel.getEventUrl(), eventModel.getLanguageDisabled(),
                                    eventModel.getDescriptionHeadline(), eventModel.getDescription(), scheduledEventModel.getTime(), scheduledEventModel.getLocationModel(), scheduledEventModel.getTicketUrl(), eventModel.getVideoUrl(), eventModel.getPracticalInfo());
                            String openHourAltText = eventModel.getOpeningHoursAlternativeText().toLowerCase();

                            eventItem.setTimeSpan(scheduledDate.getTimeSpan());
                            eventItem.setEndTimeSpan(scheduledDate.getEndTimeSpan());
                            if (openHourAltText.equals(allDay) || openHourAltText.equals(allTime)) {
                                eventItem.setTime(allDayDisplay);
                            } else {
                                eventItem.setTime(seeWebsite);
                            }

                        } else {
                            if (scheduledDates.size() == 1) {
                                eventItem = new EventData(eventModel.getEventId(), eventModel.getEventHeadline(), eventModel.getImageUrl(), eventModel.getEventUrl(), eventModel.getLanguageDisabled(),
                                        eventModel.getDescriptionHeadline(), eventModel.getDescription(), scheduledEventModel.getTime(), scheduledEventModel.getLocationModel(), scheduledEventModel.getTicketUrl(), eventModel.getVideoUrl(), eventModel.getPracticalInfo());
                                eventItem.setTimeSpan(scheduledDate.getTimeSpan());
                                eventItem.setEndTimeSpan(scheduledDate.getEndTimeSpan());
                                eventItem.setTime(GeneralHelper.TimeFormat(eventItem.getTimeSpan(), eventItem.getEndTimeSpan()));

                            } else {
                                OpeningHourModel openingHour = getOpeningHourModel(eventModel.getOpeningHourModels(), scheduledDate.getWeekdayId());
                                if (openingHour != null) {
                                    eventItem = new EventData(eventModel.getEventId(), eventModel.getEventHeadline(), eventModel.getImageUrl(), eventModel.getEventUrl(), eventModel.getLanguageDisabled(),
                                            eventModel.getDescriptionHeadline(), eventModel.getDescription(), scheduledEventModel.getTime(), scheduledEventModel.getLocationModel(), scheduledEventModel.getTicketUrl(), eventModel.getVideoUrl(), eventModel.getPracticalInfo());

                                    eventItem.setTimeSpan(GeneralHelper.GetTimeSpanOfDay(openingHour.getOpens()));
                                    eventItem.setEndTimeSpan(GeneralHelper.GetTimeSpanOfDay(openingHour.getCloses()));
                                    eventItem.setTime(GeneralHelper.TimeFormat(eventItem.getTimeSpan(), eventItem.getEndTimeSpan()));
                                    eventItem.setOpeningHour(openingHour);
                                }
                            }
                        }

                        if (eventItem != null) {
                            eventItem.setShortDate(GlobalConfig.FormatDate.format(scheduledDate.getDate()));
                            eventItem.setCurrentDate(scheduledDate.getDate());

                            eventItem.setDescriptionHtml(eventModel.getDescriptionHtml());
                            eventItem.setAudienceList(eventModel.getAudienceList());
                            eventItem.setArtFormList(eventModel.getArtFormList());

                            eventItem.setStartDateSimple(scheduledEventModel.getStartDateTime());
                            eventItem.setEndDateSimple(scheduledEventModel.getEndDateTime());
                            eventItem.setScheduledEventID(scheduledEventModel.getId());
                            eventItem.setDisabilityAccess(scheduledEventModel.getDisablilityAccess());
                            eventItem.setExternallyCreated(eventModel.getExternallyCreated());
                            eventItem.setEventVenueUrl(eventModel.getEventVenueUrl());

                            eventItem.setSoldOut(scheduledEventModel.getSoldOut());
                            eventItem.setPriceFrom(scheduledEventModel.getFromPrice());
                            eventItem.setPriceTo(scheduledEventModel.getToPrice());
                            eventItem.setWeekdayId(scheduledDate.getWeekdayId());
                            eventItem.setOpeningHourModels(eventModel.getOpeningHourModels());


                            results.add(eventItem);
                        }
                    }
                }

            }
        } catch (Exception ex) {
            Log.e("Convert Error", ex.getMessage());
        }

        return results;

    }

    public static Map<String, ArrayList<EventData>> PrepareEvents(Context context, ArrayList<EventModel> events, Date fromDate, Date toDate) {
        ArrayList<EventData> eventDataItems = ConvertToEventData(context, events);
        List<String> dateItems = GeneralHelper.GetRangeDates(fromDate, toDate);

        // Group by
        Map<String, ArrayList<EventData>> results = new TreeMap<>();
        for (EventData model : eventDataItems) {
            if (dateItems.contains(model.getShortDate())) {
                ArrayList<EventData> groupItems = results.get(model.getShortDate());
                if (groupItems == null) {
                    groupItems = new ArrayList<>();
                    results.put(model.getShortDate(), groupItems);
                }
                groupItems.add(model);
            }
        }


        // Sorting
        for (Map.Entry<String, ArrayList<EventData>> entry : results.entrySet()) {

            Collections.sort(entry.getValue(), new Comparator<EventData>() {
                @Override
                public int compare(EventData eventData, EventData t1) {
                    return (eventData.getTimeSpan() < t1.getTimeSpan() ? -1 : 0);
                }
            });
        }

        return results;
    }


    public static ArrayList<EventDataBinding> UpdateDeletedEvents(ArrayList<EventDataBinding> existedEvents, ArrayList<Integer> deletedEvents) {
        ArrayList<EventDataBinding> results = new ArrayList<EventDataBinding>();

        EventDataBinding eventGroup = new EventDataBinding();
        ArrayList<EventDataBinding> groupItems = new ArrayList<>();
        for (EventDataBinding item : existedEvents) {
            if (item.isGroupHeader()) {
                if (groupItems.size() > 0) {
                    results.add(eventGroup);
                    results.addAll(groupItems);
                }
                eventGroup = item;
                groupItems = new ArrayList<EventDataBinding>();
            } else {
                if (!deletedEvents.contains(item.getEventData().getEventId())) {
                    groupItems.add(item);
                }
            }
        }
        if (groupItems.size() > 0) {
            results.add(eventGroup);
            results.addAll(groupItems);
        }
        return results;
    }

    public static Map<String, ArrayList<EventData>> UpdateDeletedEvents(Map<String, ArrayList<EventData>> events, ArrayList<Integer> deletedEvents) {
        //Map<String, ArrayList<EventData>> results=new Map<String, ArrayList<EventData>>();
        for (Map.Entry<String, ArrayList<EventData>> entry : events.entrySet()) {

            // entry.getValue().removeIf(c-> deletedEvents.contains(c.getEventId()));
            ArrayList<EventData> items = new ArrayList<>();
            for (EventData item : entry.getValue()) {
                if (!deletedEvents.contains(item.getEventId())) {
                    // entry.getValue().remove(item);
                    items.add(item);
                } else {
                }
            }
            entry.setValue(items);
        }

        return events;

    }

    public static ArrayList<EventDataBinding> SearchEvents(Context context, Map<String, ArrayList<EventData>> eventModels, final String search, Date currentDate, Date endDate) {
        Map<String, ArrayList<EventData>> events = new TreeMap<>();
        ArrayList<DBFavoriteEventModel> favoriteItems = SharedPreferenceHelper.GetFavoriteEvents(context);
        ArrayList<String> dateItems = GeneralHelper.GetRangeDates(currentDate, endDate);
        ArrayList<String> holidays = GeneralHelper.GetHolidays(context);
//        String searchValue="(?i:.*"+search+".*)";
//        Pattern pattern = Pattern.compile(search,Pattern.CASE_INSENSITIVE);
        for (Map.Entry<String, ArrayList<EventData>> entry : eventModels.entrySet()) {
            if (dateItems.contains(entry.getKey())) {
                ArrayList<EventData> items = new ArrayList<>();
                for (EventData item: entry.getValue()) {
//                    if (item.getEventHeadline().toLowerCase().contains(search) || item.getDescriptionHeadline().toLowerCase().contains(search) ||
//                            item.getDescriptionHtml().toLowerCase().contains(search) || item.getDescription().toLowerCase().contains(search) ||
//                            item.getPracticalInfo().getContactName().toLowerCase().contains(search)||item.getPracticalInfo().getContactPhone().toLowerCase().contains(search)||
//                            item.getArtFormList().toLowerCase().contains(search) || item.getAudienceList().toLowerCase().contains(search) ||
//                            item.getLocationModel().getCity().toLowerCase().contains(search) || item.getLocationModel().getAddress().toLowerCase().contains(search) ||
//                            item.getLocationModel().getName().toLowerCase().contains(search) || item.getLocationModel().getVenue().toLowerCase().contains(search)) {
//                        items.add(item);
//                    }
                    if (item.getEventHeadline().toLowerCase().contains(search) || item.getDescription().toLowerCase().contains(search)) {
                        items.add(item);
                    }

                }
                if (items.size() > 0) {
                    events.put(entry.getKey(), items);
                }
            }
            if (events.size() > 30) {
                break;
            }
        }


        ArrayList<EventDataBinding> eventDataBindings = new ArrayList<>();
        ArrayList<EventDataBinding> groupItems = new ArrayList<>();
        try {
            for (Map.Entry<String, ArrayList<EventData>> item : events.entrySet()) {
                boolean isHoliday = holidays.contains(item.getKey());
                groupItems = new ArrayList<>();
                for (EventData eventData : item.getValue()) {
                    for (DBFavoriteEventModel favorite : favoriteItems) {
                        if (favorite.getScheduledEventID() == eventData.getScheduledEventID() && favorite.getScheduledDate().equals(eventData.getShortDate())) {
                            eventData.setFavoriteEvent(true);
                            favoriteItems.remove(favorite);
                            break;
                        } else {
                            eventData.setFavoriteEvent(false);
                        }
                    }
                    groupItems.add(new EventDataBinding(false, eventData.getCurrentDate(), eventData, isHoliday));
                }
                if (groupItems.size() > 0) {
                    eventDataBindings.add(new EventDataBinding(true, GlobalConfig.FormatDate.parse(item.getKey()), new EventData(), isHoliday));
                    eventDataBindings.addAll(groupItems);
                }
            }
        } catch (Exception ex) {
        }


        return eventDataBindings;
    }


    private static OpeningHourModel getOpeningHourModel(ArrayList<OpeningHourModel> values, int weekdayId) {
        OpeningHourModel model = null;
        for (OpeningHourModel open : values) {
            if (open.getIsClosed() == false && open.getWeekdayId() == weekdayId) {
                model = new OpeningHourModel(open.getWeekdayId(), open.getWeekdayLabel(), open.getOpens(), open.getCloses(), open.getIsClosed(), open.getClosedString());
                break;
            }
        }
        return model;
    }
}
