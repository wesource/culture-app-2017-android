package dk.aarhus2017.aarhusevents.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.adapter.ArtformFilterAdapter;
import dk.aarhus2017.aarhusevents.model.ArtformFilterModel;
import dk.aarhus2017.aarhusevents.util.ConvertHelper;
import dk.aarhus2017.aarhusevents.util.FBTrackerHelper;
import dk.aarhus2017.aarhusevents.util.FontHelper;
import dk.aarhus2017.aarhusevents.util.GeneralHelper;
import dk.aarhus2017.aarhusevents.util.SharedPreferenceHelper;

public class ArtformFilterActivity extends AppCompatActivity implements View.OnClickListener {

    ArrayList<ArtformFilterModel> artformItems = new ArrayList<>();
    ArtformFilterAdapter artformFilterAdapter;
    ArtformFilterAdapter.ArtformFilterListener artformFilterListener;


    LinearLayout subpageFilterBackHolderLinearLayout;
    ImageView subpageFilterBackImageView;
    Button subpageFilterClearButton;
    TextView subpageFilterTitleTextView;

    ArtformFilterActivity self = this;

    Set<String> artformStored;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artform_filter);

        ListView artformFilter = (ListView) findViewById(R.id.artformFilterListView);

        subpageFilterBackHolderLinearLayout = (LinearLayout) findViewById(R.id.subpageFilterBackHolderLinearLayout);
        subpageFilterBackImageView = (ImageView) findViewById(R.id.subpageFilterBackImageView);
        subpageFilterTitleTextView = (TextView) findViewById(R.id.subpageFilterTitleTextView);
        subpageFilterClearButton = (Button) findViewById(R.id.subpageFilterClearButton);


        artformItems= GeneralHelper.GetArtformModels(self);
//        artformItems.add(new ArtformFilterModel(getString(R.string.artform_music), (R.drawable.ic_musicandsound), false));
//        artformItems.add(new ArtformFilterModel(getString(R.string.artform_performingart), (R.drawable.ic_performingarts), false));
//        artformItems.add(new ArtformFilterModel(getString(R.string.artform_hightlight), (R.drawable.ic_highlight), false));
//        artformItems.add(new ArtformFilterModel(getString(R.string.artform_design), (R.drawable.ic_design), false));
//        artformItems.add(new ArtformFilterModel(getString(R.string.artform_visualart), (R.drawable.ic_visualarts), false));
//        artformItems.add(new ArtformFilterModel(getString(R.string.artform_festival), (R.drawable.ic_festivalandhappenings), false));
//        artformItems.add(new ArtformFilterModel(getString(R.string.artform_film), (R.drawable.ic_filmandanimation), false));
//        artformItems.add(new ArtformFilterModel(getString(R.string.artform_litterature), (R.drawable.ic_litterature), false));
//        artformItems.add(new ArtformFilterModel(getString(R.string.artform_architecture), (R.drawable.ic_architecture), false));


        Intent intent = getIntent();
        ArrayList<String> previousValues = intent.getStringArrayListExtra("ArtformList");
        artformStored = ConvertHelper.ConvertArrayListToSet(previousValues);
        if(artformStored.size() > 0){
            subpageFilterClearButton.setVisibility(View.VISIBLE);
        }else{
            subpageFilterClearButton.setVisibility(View.GONE);
        }


        for (ArtformFilterModel item : artformItems) {
            if (artformStored.contains(item.getName())) {
                item.setSelected(true);
            }
        }


        artformFilterAdapter = new ArtformFilterAdapter(this, artformItems);
        artformFilter.setAdapter(artformFilterAdapter);
        artformFilterListener = new ArtformFilterAdapter.ArtformFilterListener() {
            @Override
            public void onStatusChanged(ArtformFilterModel rowData) {
                if (GeneralHelper.CheckStatusArtformFilter(artformItems)) {
                    subpageFilterClearButton.setVisibility(View.VISIBLE);
                } else {
                    subpageFilterClearButton.setVisibility(View.INVISIBLE);
                }
            }
        };
        artformFilterAdapter.setArtformFilterListener(artformFilterListener);

        // Register events
        subpageFilterBackHolderLinearLayout.setOnClickListener(this);
        subpageFilterBackImageView.setOnClickListener(this);
        subpageFilterClearButton.setOnClickListener(this);
        subpageFilterTitleTextView.setText(getString(R.string.filter_artform));

        // Set fonts
        setFonts();

    }

    private void setFonts() {
        Typeface sourceSansProBold = FontHelper.SourceSansProBold(self);
        Typeface sourceSansProRegular = FontHelper.SourceSansProRegular(self);
        subpageFilterTitleTextView.setTypeface(sourceSansProBold);
        subpageFilterClearButton.setTypeface(sourceSansProBold);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.subpageFilterClearButton: {
                if (artformFilterAdapter != null) {
                    for (ArtformFilterModel item : artformItems) {
                        item.setSelected(false);
                    }
                    artformFilterAdapter.updateData(artformItems);
                    subpageFilterClearButton.setVisibility(View.INVISIBLE);
                }
                break;
            }
            case R.id.subpageFilterBackHolderLinearLayout:
            case R.id.subpageFilterBackImageView: {
                // SharedPreferenceHelper.SaveArtformFilter(self,artformItems);
                GoBack();
                break;
            }
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        GoBack();
        super.onBackPressed();
    }

    private void GoBack() {
        ArrayList<String> items = new ArrayList<>();
        for (ArtformFilterModel model : artformItems) {
            if (model.getSelected()) {
                items.add(model.getName());
            }
        }

        Intent returnIntent = new Intent();
        returnIntent.putExtra("ArtformList", items);

        setResult(self.RESULT_OK, returnIntent);
        super.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FBTrackerHelper.LogScreenView(this,getString(R.string.filter_artform));
    }
}
