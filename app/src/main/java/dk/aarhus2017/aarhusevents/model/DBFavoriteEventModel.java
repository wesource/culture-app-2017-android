package dk.aarhus2017.aarhusevents.model;

/**
 * Created by Dell on 12/21/2016.
 */

public class DBFavoriteEventModel {
    private int scheduledEventID;
    private String scheduledDate;

    public DBFavoriteEventModel(int _scheduledEventID, String _scheduledDate)
    {
        this.scheduledEventID =_scheduledEventID;
        this.scheduledDate =_scheduledDate;
    }
    public int getScheduledEventID(){return this.scheduledEventID;}

    public String getScheduledDate(){return this.scheduledDate;}

}
