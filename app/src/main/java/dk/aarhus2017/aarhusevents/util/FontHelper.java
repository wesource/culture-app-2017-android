package dk.aarhus2017.aarhusevents.util;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Dell on 12/29/2016.
 */

public class FontHelper {

    public static Typeface SourceSansProRegular(Context context)
    {
        return Typeface.createFromAsset(context.getAssets(), "fonts/SourceSansPro-Regular.otf");
    }
    public static Typeface SourceSansProBold(Context context)
    {
        return Typeface.createFromAsset(context.getAssets(), "fonts/SourceSansPro-Bold.otf");
    }

    public static Typeface FlavinRegular(Context context)
    {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Flavin-Regular.otf");
    }

    public static Typeface FlavinBold(Context context)
    {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Flavin Bold.otf");
    }

}
