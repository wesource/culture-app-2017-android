package dk.aarhus2017.aarhusevents.model;

/**
 * Created by Dell on 3/14/2017.
 */

public class OpeningHourModel {
    private int weekdayId;
    private String weekdayLabel;
    private String opens;
    private String closes;
    private boolean isClosed;
    private String closedString;

    public OpeningHourModel(){

    }

    public OpeningHourModel(int _weekdayId, String _weekdayLabel, String _opens, String _closes, boolean _isClosed, String _closedString){
        this.weekdayId =_weekdayId;
        this.weekdayLabel = _weekdayLabel;
        this.opens = _opens;
        this.closes =_closes;
        this.isClosed =_isClosed;
        this.closedString = _closedString;
    }

    public int getWeekdayId(){return this.weekdayId;}

    public String getWeekdayLabel(){return this.weekdayLabel;}

    public String getOpens(){return this.opens;}

    public String getCloses(){return this.closes;}

    public boolean getIsClosed(){return this.isClosed;}

    public String getClosedString(){return this.closedString;}
}
