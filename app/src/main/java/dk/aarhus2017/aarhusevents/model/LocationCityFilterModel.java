package dk.aarhus2017.aarhusevents.model;

/**
 * Created by Dell on 12/23/2016.
 */

public class LocationCityFilterModel {
    private String city;
    private double latitude;
    private double longitude;
    private int zoom;
    private Boolean selected;
    private Boolean isCurrentLocation;

    public LocationCityFilterModel(String _city, Double _latitude, Double _longitude, int _zoom, Boolean _selected) {
        this.city = _city;
        this.latitude = _latitude;
        this.longitude = _longitude;
        this.zoom =_zoom;
        this.selected = _selected;
        this.isCurrentLocation=false;
    }
    public LocationCityFilterModel(Boolean _isCurrentLocation, String _city, Double _latitude, Double _longitude, int _zoom, Boolean _selected) {

        this.city = _city;
        this.latitude = _latitude;
        this.longitude = _longitude;
        this.zoom =_zoom;
        this.selected = _selected;
        this.isCurrentLocation=_isCurrentLocation;
    }

    public void setSelected(Boolean _selected)
    {
        this.selected =_selected;
    }

    public void setLatitude(Double _latitude)
    {
        this.latitude = _latitude;
    }
    public void setLongitude(Double _longitude)
    {
        this.longitude =_longitude;
    }



    public String getCity(){return this.city;}

    public double getLatitude(){return this.latitude;}

    public double getLongitude(){return this.longitude;}

    public int getZoom(){return this.zoom;}

    public Boolean getSelected(){return this.selected;}

    public Boolean getIsCurrentLocation(){return this.isCurrentLocation;}
}
