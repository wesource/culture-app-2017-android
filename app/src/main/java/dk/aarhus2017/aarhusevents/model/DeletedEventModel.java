package dk.aarhus2017.aarhusevents.model;

/**
 * Created by Dell on 3/1/2017.
 */

public class DeletedEventModel {
    private String year;
    private String month;
    private String day;
    public DeletedEventModel(String _year, String _month, String _day){
        this.year = _year;
        this.month =_month;
        this.day =_day;
    }

    public String getYear(){return this.year;}

    public String getMonth(){return this.month;}

    public String getDay(){return this.day;}
}
