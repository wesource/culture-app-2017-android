package dk.aarhus2017.aarhusevents.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import dk.aarhus2017.aarhusevents.util.InternetHelper;

/**
 * Created by smart on 1/19/2017.
 */

public class ConnectivityReceiver extends BroadcastReceiver {

    protected ConnectivityReceiverListener connectivityReceiverListener;

    public ConnectivityReceiver(){
        super();
    }

    public void setConnectivityReceiverListener(ConnectivityReceiver.ConnectivityReceiverListener _listener){
        connectivityReceiverListener = _listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if(connectivityReceiverListener != null){
            boolean isConnected = InternetHelper.HasInternetConnection(context);
            connectivityReceiverListener.onNetworkConnectionChanged(isConnected);
        }
    }

    public interface ConnectivityReceiverListener{
        void onNetworkConnectionChanged(boolean isConnected);
    }


}
