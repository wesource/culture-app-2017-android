package dk.aarhus2017.aarhusevents.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.text.style.TtsSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.model.AudienceFilterModel;
import dk.aarhus2017.aarhusevents.util.FBTrackerHelper;
import dk.aarhus2017.aarhusevents.util.FontHelper;
import dk.aarhus2017.aarhusevents.util.GeneralHelper;
import dk.aarhus2017.aarhusevents.util.SharedPreferenceHelper;

public class DateRangeFilterActivity extends AppCompatActivity implements View.OnClickListener {

    //Button audienceFilterClear;

    DateRangeFilterActivity self =this;

    LinearLayout subpageFilterBackHolderLinearLayout;
    ImageView daterangeFilterImageView;
    TextView subpageFilterTitle;

    TextView datarangeFilterFromDateLabelTextView;
    TextView datarangeFilterFromDateTextView;
    TextView daterangeFilterToDateLabelTextView;
    TextView daterangeFilterToDateTextView;
    DatePicker daterangeFilterDatePicker;
    Button daterangeFilterSelectButton;
    LinearLayout daterangePlaceholder;

    SimpleDateFormat simpleDateFormat =new SimpleDateFormat("EEEE dd. MMMM");//EEEE dd. MMMM yyyy
    final Calendar fromDate = new GregorianCalendar();
    final GregorianCalendar toDate =new GregorianCalendar();
    GregorianCalendar selectedDate  =new GregorianCalendar();

    boolean isSelectedFromDate=false;



    DatePicker.OnDateChangedListener dateChangedListener;

    DatePicker.OnDateChangedListener endDateChangedListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_range_filter);

        subpageFilterBackHolderLinearLayout =(LinearLayout)findViewById(R.id.subpageFilterBackHolderLinearLayout);
        daterangeFilterImageView=(ImageView)findViewById(R.id.subpageFilterBackImageView) ;
        subpageFilterTitle =(TextView)findViewById(R.id.subpageFilterTitleTextView);

        datarangeFilterFromDateLabelTextView = (TextView)findViewById(R.id.daterangeFilterFromDateLabelTextView);
        datarangeFilterFromDateTextView =(TextView)findViewById(R.id.daterangeFilterFromDateTextView);
        daterangeFilterToDateLabelTextView = (TextView)findViewById(R.id.daterangeFilterToDateLabelTextView);
        daterangeFilterToDateTextView = (TextView)findViewById(R.id.daterangeFilterToDateTextView);

        daterangeFilterDatePicker =(DatePicker)findViewById(R.id.daterangeFilterDatePicker);
        daterangeFilterSelectButton =(Button)findViewById(R.id.daterangeFilterSelectedButton);
        daterangePlaceholder =(LinearLayout)findViewById(R.id.daterangePlaceholderLinearLayout);

        // Set fonts
        setFonts();


        subpageFilterTitle.setText(getString(R.string.filter_date_range));
        subpageFilterBackHolderLinearLayout.setOnClickListener(this);
        daterangeFilterImageView.setOnClickListener(this);
        datarangeFilterFromDateTextView.setOnClickListener(this);
        daterangeFilterToDateTextView.setOnClickListener(this);
        daterangeFilterSelectButton.setOnClickListener(this);

        Intent intent =getIntent();
        long fDate = intent.getLongExtra("FromDate",0);
        long tDate = intent.getLongExtra("ToDate",0);

        if(fDate > 0) {
            fromDate.setTimeInMillis(fDate);
        }
        datarangeFilterFromDateTextView.setText(simpleDateFormat.format(fromDate.getTime()));

        if(tDate > 0)
        {
            toDate.setTimeInMillis(tDate);
        }
        else {
            toDate.setTimeInMillis(new Date().getTime() + (14 * 24 * 60 * 60 * 1000));
        }
        daterangeFilterToDateTextView.setText(simpleDateFormat.format(toDate.getTime()));

//        daterangeFilterDatePicker.setMinDate(System.currentTimeMillis() - 1000);
//        daterangeFilterDatePicker.setMaxDate(GeneralHelper.GetShortDate("2017-12-31").getTime());

        dateChangedListener = new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int i, int i1, int i2) {
                selectedDate.set(i,i1,i2);
            }
        };

        endDateChangedListener = new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int i, int i1, int i2) {
                selectedDate.set(i,i1,i2);
            }
        };

        daterangeFilterDatePicker.init(fromDate.YEAR, fromDate.MONTH, fromDate.DAY_OF_MONTH,dateChangedListener);


    }

    private void setFonts()
    {
        Typeface sourceSansProBold = FontHelper.SourceSansProBold(self);
        Typeface sourceSansProRegular = FontHelper.SourceSansProRegular(self);
        subpageFilterTitle.setTypeface(sourceSansProBold);

        datarangeFilterFromDateLabelTextView.setTypeface(sourceSansProRegular);
        datarangeFilterFromDateTextView.setTypeface(sourceSansProBold);

        daterangeFilterToDateLabelTextView.setTypeface(sourceSansProRegular);
        daterangeFilterToDateTextView.setTypeface(sourceSansProBold);
        daterangeFilterSelectButton.setTypeface(sourceSansProBold);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.subpageFilterBackHolderLinearLayout:
            case R.id.subpageFilterBackImageView:
            {
                GoBack();
                break;
            }
            case R.id.daterangeFilterFromDateTextView:
            {
                daterangePlaceholder.setAlpha (Float.parseFloat("0.5"));
                datarangeFilterFromDateTextView.setEnabled(false);
                daterangeFilterToDateTextView.setEnabled(false);


                daterangeFilterDatePicker.setVisibility(View.VISIBLE);
                daterangeFilterSelectButton.setVisibility(View.VISIBLE);

                isSelectedFromDate = true;

                daterangeFilterDatePicker.setMinDate(0);
                daterangeFilterDatePicker.setMaxDate(1000);
                //daterangeFilterDatePicker.setMaxDate(toDate.getTime().getTime());
                daterangeFilterDatePicker.setMaxDate(GeneralHelper.GetShortDate("2017-12-31").getTime());
                daterangeFilterDatePicker.setMinDate(System.currentTimeMillis() - 1000);
                daterangeFilterDatePicker.init(fromDate.get(Calendar.YEAR),fromDate.get(Calendar.MONTH), fromDate.get(Calendar.DAY_OF_MONTH),dateChangedListener );

                break;
            }
            case R.id.daterangeFilterToDateTextView:
            {
                daterangePlaceholder.setAlpha (Float.parseFloat("0.5"));
                datarangeFilterFromDateTextView.setEnabled(false);
                daterangeFilterToDateTextView.setEnabled(false);


                daterangeFilterDatePicker.setVisibility(View.VISIBLE);
                daterangeFilterSelectButton.setVisibility(View.VISIBLE);


                isSelectedFromDate = false;

                //selectedDate.setTimeInMillis(toDate.getTimeInMillis());


                daterangeFilterDatePicker.setMinDate(0);
                daterangeFilterDatePicker.setMaxDate(1000);
                daterangeFilterDatePicker.setMaxDate(GeneralHelper.GetShortDate("2017-12-31").getTime());
                daterangeFilterDatePicker.setMinDate(System.currentTimeMillis() - 1000);
                //daterangeFilterDatePicker.setMinDate(fromDate.getTime().getTime()-1000);

                daterangeFilterDatePicker.init(toDate.get(Calendar.YEAR),toDate.get(Calendar.MONTH), toDate.get(Calendar.DAY_OF_MONTH),endDateChangedListener );

                break;
            }
            case R.id.daterangeFilterSelectedButton:
            {
                daterangePlaceholder.setAlpha (Float.parseFloat("1"));
                datarangeFilterFromDateTextView.setEnabled(true);
                daterangeFilterToDateTextView.setEnabled(true);

                daterangeFilterDatePicker.setVisibility(View.GONE);
                daterangeFilterSelectButton.setVisibility(View.GONE);

                if(isSelectedFromDate) {
                    fromDate.setTimeInMillis(selectedDate.getTime().getTime());
                    datarangeFilterFromDateTextView.setText(simpleDateFormat.format(fromDate.getTime().getTime()));

                    if(selectedDate.getTimeInMillis()>toDate.getTimeInMillis())
                    {
                        toDate.setTimeInMillis(selectedDate.getTimeInMillis());
                        daterangeFilterToDateTextView.setText(simpleDateFormat.format(toDate.getTime().getTime()));
                    }
                }
                else
                {
                    toDate.setTimeInMillis(selectedDate.getTime().getTime());
                    daterangeFilterToDateTextView.setText(simpleDateFormat.format(toDate.getTime().getTime()));

                    if(toDate.getTimeInMillis()<fromDate.getTimeInMillis()){
                        fromDate.setTimeInMillis(toDate.getTimeInMillis());
                        datarangeFilterFromDateTextView.setText(simpleDateFormat.format(fromDate.getTime().getTime()));
                    }
                }
                isSelectedFromDate=false;

                break;
            }
            default:
                break;
        }
    }



    @Override
    public void onBackPressed() {
        GoBack();
        super.onBackPressed();
    }

    private void GoBack()
    {
        Intent returnIntent = new Intent();
        long fDate = fromDate.getTimeInMillis();
        long tDate=toDate.getTimeInMillis();
        if(fromDate.getTimeInMillis()>tDate){
            fDate = tDate;
        }
        returnIntent.putExtra("FromDate",fDate);
        returnIntent.putExtra("ToDate",tDate);

        setResult(self.RESULT_OK, returnIntent);
        super.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FBTrackerHelper.LogScreenView(this,getString(R.string.filter_date_range));
    }
}
