package dk.aarhus2017.aarhusevents.model;

/**
 * Created by Dell on 12/23/2016.
 */

public class MenuModel {
    private String id;
    private String name;

    private Boolean selected;

    public MenuModel(String _id, String _name, Boolean _selected) {
        this.id =_id;
        this.name = _name;
        this.selected = _selected;
    }

    public void setSelected(Boolean _selected)
    {
        this.selected =_selected;
    }


    public String getId(){return  this.id;}

    public String getName(){return this.name;}


    public Boolean getSelected(){return this.selected;}
}
