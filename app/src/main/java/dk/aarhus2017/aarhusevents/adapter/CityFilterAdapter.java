package dk.aarhus2017.aarhusevents.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.model.LocationCityFilterModel;
import dk.aarhus2017.aarhusevents.util.FontHelper;

/**
 * Created by Dell on 1/13/2017.
 */

public class CityFilterAdapter extends ArrayAdapter<LocationCityFilterModel> {
    ArrayList<LocationCityFilterModel> items =new ArrayList<>();
    Context mContent;

    protected CityFilterAdapter.CityFilterListener listener;

    public  CityFilterAdapter(Context _context, ArrayList<LocationCityFilterModel> _values)
    {
        super(_context,-1, _values);
        this.items = _values;
        this.mContent = _context;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    public LocationCityFilterModel getPosition(int position)
    {
        return items.get(position);
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)mContent.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.filter_city_item, parent, false);
        final Typeface sourceSansProRegular = FontHelper.SourceSansProRegular(mContent);
        final Typeface sourceSansProBold = FontHelper.SourceSansProBold(mContent);

        final LocationCityFilterModel dataRow = getItem(position);

        final TextView cityName =(TextView)row.findViewById(R.id.locationFilterCityNameTextView);
        final ImageView selected = (ImageView) row.findViewById(R.id.locationFilterCitySelectedImageView);

        cityName.setText(dataRow.getCity());

        if(dataRow.getSelected())
        {
            cityName.setTypeface(sourceSansProBold);
            selected.setVisibility(View.VISIBLE);
        }
        else
        {
            cityName.setTypeface(sourceSansProRegular);
            selected.setVisibility(View.INVISIBLE);
        }
        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener!=null){
                    listener.onCityChanged(dataRow, position);
                    if(dataRow.getIsCurrentLocation()==false) {
                        for (LocationCityFilterModel model : items) {
                            model.setSelected(false);
                        }
                        dataRow.setSelected(true);
                    }
                }
            }
        });
        return row;
    }

    public void setTaskListener(CityFilterAdapter.CityFilterListener _listener){
        listener = _listener;
    }

    public interface CityFilterListener {
        void onCityChanged(LocationCityFilterModel rowData, int position);

    }
}
