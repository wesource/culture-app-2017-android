package dk.aarhus2017.aarhusevents.model;

import android.location.Location;
import android.text.Html;
import android.util.Log;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


import dk.aarhus2017.aarhusevents.util.ConvertHelper;
import dk.aarhus2017.aarhusevents.util.GeneralHelper;

import static android.text.TextUtils.join;

/**
 * Created by Dell on 12/1/2016.
 */

public class EventData {
    private int eventId;
    private String eventHeadline;
    private String imageUrl;
    private String eventUrl;
    private Boolean languageDisabled;
    private String descriptionTitle;
    private String description;
    private String descriptionHtml;
    private String time;
    private boolean disabilityAccess;
    private boolean externallyCreated;
    private long timeSpan;
    private long endTimeSpan;
    private String shortDate;
    private Date currentDate;
    private String eventVenueUrl;


    private LocationModel locationModel;
    private String ticketUrl;


    public PracticalInfoModel practicalInfo;
    private String videoUrl;

    private String distance = "";
    //    private Date fromDate;
//    private Date toDate;
    private ArrayList<String> audienceList;
    private ArrayList<String> artFormList;

    private boolean isFavoriteEvent;
    private String startDateSimple;
    private String endDateSimple;

    private int scheduledEventID;
    private Boolean soldOut;
    private int priceFrom;
    private int priceTo;
    private int weekdayId;

    private OpeningHourModel openingHourModel;

    private ArrayList<OpeningHourModel> openingHourModels;


    public EventData(int _eventId, String _eventHeadline, String _imageUrl, String _eventUrl, boolean _languageDisabled, String _descriptionTitle, String _description,
                     String _time, LocationModel _locationModel, String _ticketUrl, String _videoUrl, PracticalInfoModel _practicalInfo) {
        this.eventId = _eventId;
        this.eventHeadline = _eventHeadline;
        this.imageUrl = _imageUrl;
        this.eventUrl = _eventUrl;
        this.languageDisabled = _languageDisabled;
        this.descriptionTitle = _descriptionTitle;
        this.description = _description;
        this.time = _time;
        this.locationModel = _locationModel;
        this.ticketUrl = _ticketUrl;
        this.videoUrl = _videoUrl;
        this.practicalInfo = _practicalInfo;
        this.isFavoriteEvent = false;
    }

    public EventData() {
    }


    public void setDescriptionHtml(String _descriptionHtml) {
        this.descriptionHtml = _descriptionHtml;
    }

    public void setAudienceList(ArrayList<String> _audienceList) {
        this.audienceList = _audienceList;
    }

    public void setArtFormList(ArrayList<String> _artFormList) {
        this.artFormList = _artFormList;
    }

    public void setFavoriteEvent(boolean _isFavorite) {
        this.isFavoriteEvent = _isFavorite;
    }

    public void setStartDateSimple(String _startDate) {
        this.startDateSimple = _startDate;
    }

    public void setEndDateSimple(String _endDate) {
        this.endDateSimple = _endDate;
    }

    public void setScheduledEventID(int _scheduledEventID) {
        this.scheduledEventID = _scheduledEventID;
    }

    public void setDisabilityAccess(Boolean _disabilityAccess) {
        this.disabilityAccess = _disabilityAccess;
    }

    public void setExternallyCreated(Boolean _externallyCreated) {
        this.externallyCreated = _externallyCreated;
    }

    public void setTimeSpan(long _timeSpan) {
        this.timeSpan = _timeSpan;
    }

    public void setEndTimeSpan(long _endTimeSpan) {
        this.endTimeSpan = _endTimeSpan;
    }

    public void setShortDate(String _shortDate) {
        this.shortDate = _shortDate;
    }

    public void setCurrentDate(Date _currentDate) {
        this.currentDate = _currentDate;
    }

    public void setWeekdayId(int _weekdayId) {
        this.weekdayId = _weekdayId;
    }

    public void setSoldOut(Boolean _soldOut) {
        this.soldOut = _soldOut;
    }

    public void setPriceFrom(int _priceFrom) {
        this.priceFrom = _priceFrom;
    }

    public void setPriceTo(int _priceTo) {
        this.priceTo = _priceTo;
    }

    public void setEventVenueUrl(String _eventVenueUrl) {
        this.eventVenueUrl = _eventVenueUrl;
    }

    public void setOpeningHour(OpeningHourModel _openingHour) {
        this.openingHourModel = _openingHour;
    }

    public void setOpeningHourModels(ArrayList<OpeningHourModel> _openingHous) {
        this.openingHourModels = _openingHous;
    }

    public int getEventId() {
        return eventId;
    }


    public String getEventHeadline() {
        return "/ " + eventHeadline;
    }


    public String getImageUrl() {
        return imageUrl + "?width=200&height=200&mode=crop";
    }//mode=scale

    public String getImageFullSize() {
        return imageUrl;
    }//+ "?width=1500&mode=scale"

    public String getEventUrl() {
        return eventUrl;
    }

    public boolean getLanguageDisabled() {
        return languageDisabled;
    }

    public String getDescriptionHeadline() {
        return descriptionTitle;
    }

    public String getTime() {
        return this.time;
    }

    public void setTime(String time) {
        this.time = time;
    }


    public String getDescription() {
        return description;
    }

    public LocationModel getLocationModel() {
        return this.locationModel;
    }

    public String getTicketUrl() {
        if (!this.ticketUrl.isEmpty()) {
            if (!this.ticketUrl.contains("http")) {
                this.ticketUrl = "http://" + this.ticketUrl;
            }
        }
        return ticketUrl;
    }


    public String getVideoUrl() {
        return videoUrl;
    }

    public ArrayList<String> getVideoUrls() {
        return ConvertHelper.ConvertStringToArrayListVideo(this.videoUrl);
    }


    public PracticalInfoModel getPracticalInfo() {
        return this.practicalInfo;
    }


    public String getDescriptionHtml() {
        return this.descriptionHtml;
    }

    public String getShortDescriptionHtml() {
        String shortDes = "";
        if (!this.descriptionHtml.isEmpty()) {
            int index = this.descriptionHtml.indexOf("<br />");
            if (index > 0) {
                shortDes = this.descriptionHtml.substring(0, index);
            } else {
                index = this.descriptionHtml.indexOf("<br/>");
                if (index > 0) {
                    shortDes = this.descriptionHtml.substring(0, index);
                } else {
                    index = this.descriptionHtml.indexOf("</strong>");
                    if (index > 0) {
                        shortDes = this.descriptionHtml.substring(0, index);
                    } else {
                        shortDes = this.descriptionHtml;
                    }
                }
            }

            shortDes = shortDes.replaceAll("\\<[^>]*>", "");
        } else {
            shortDes = this.description;
        }
        return shortDes;
    }


    public ArrayList<String> getAudienceListItems() {
        return this.audienceList;
    }

    public String getAudienceList() {
        if (this.audienceList != null) {
            return join(", ", audienceList);
        } else {
            return "";
        }
    }

    public ArrayList<String> getArtFormListItems() {
        return this.artFormList;
    }

    public String getArtFormList() {
        if (this.artFormList != null) {
            return join(", ", artFormList);
        } else {
            return "";
        }
    }

    public boolean getFavoriteEvent() {
        return this.isFavoriteEvent;
    }

    public Date getStartDateSimple() {
        return GeneralHelper.GetFullDate(this.startDateSimple);
        // try {
//            SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//            if (!this.startDateSimple.isEmpty()) {
//                date = simple.parse(this.startDateSimple);
//            }


//        }catch (Exception ex){
//            Log.e("getStartDateSimpleError",ex.getMessage());
//        }
//        return date;
//
    }

    public Date getEndDateSimple() {
        return GeneralHelper.GetFullDate(this.endDateSimple);// ConvertHelper.ConvertUtcToLocalTime(this.endDateSimple);
//        try {
//            SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//            if (!this.endDateSimple.isEmpty()) {
//                date = simple.parse(this.endDateSimple);
//            }
//        }catch (Exception ex){
//            Log.e("getEndDateSimpleError",ex.getMessage());
//        }
//        return date;
    }

    public int getScheduledEventID() {
        return this.scheduledEventID;
    }

    public boolean getDisabilityAccess() {
        return this.disabilityAccess;
    }

    public boolean getExternallyCreated() {
        return this.externallyCreated;
    }

    public long getTimeSpan() {
        return this.timeSpan;
    }

    public long getEndTimeSpan() {
        return this.endTimeSpan;
    }

    public String getShortDate() {
        return this.shortDate;
    }

    public Date getCurrentDate() {
        return this.currentDate;
    }

    public Boolean getSoldOut() {
        return this.soldOut;
    }

    public int getPriceFrom() {
        return this.priceFrom;
    }

    public int getPriceTo() {
        return this.priceTo;
    }

    public String getEventVenueUrl() {
        if (!this.eventVenueUrl.isEmpty() && !this.eventVenueUrl.equalsIgnoreCase("null") && !this.eventVenueUrl.contains("http")) {
            this.eventVenueUrl = "http://" + this.eventVenueUrl;
        } else if (this.eventVenueUrl.equalsIgnoreCase("null")) {
            this.eventVenueUrl = "";
        }
        return this.eventVenueUrl;
    }

    public OpeningHourModel getOpeningHourModel() {
        return this.openingHourModel;
    }

    public int getWeekdayId() {
        return this.weekdayId;
    }

    public ArrayList<OpeningHourModel> getOpeningHourModels() {
        return this.openingHourModels;
    }


}
