package dk.aarhus2017.aarhusevents.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Set;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.adapter.ArtformFilterAdapter;
import dk.aarhus2017.aarhusevents.adapter.AudienceFilterAdapter;
import dk.aarhus2017.aarhusevents.model.ArtformFilterModel;
import dk.aarhus2017.aarhusevents.model.AudienceFilterModel;
import dk.aarhus2017.aarhusevents.util.ConvertHelper;
import dk.aarhus2017.aarhusevents.util.FBTrackerHelper;
import dk.aarhus2017.aarhusevents.util.FontHelper;
import dk.aarhus2017.aarhusevents.util.GeneralHelper;
import dk.aarhus2017.aarhusevents.util.SharedPreferenceHelper;

public class AudienceFilterActivity extends AppCompatActivity implements View.OnClickListener {


    LinearLayout subpageFilterBackHolderLinearLayout;
    ImageView subpageFilterBackImageView;
    Button audienceFilterClear;
    TextView subpageFilterTitle;

    AudienceFilterActivity self=this;

    ListView audienceFilter;

    ArrayList<AudienceFilterModel> audienceItems =new ArrayList<>();
    AudienceFilterAdapter audienceFilterAdapter;
    AudienceFilterAdapter.AudienceFilterListener audienceFilterListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audience_filter);

        subpageFilterBackHolderLinearLayout =(LinearLayout)findViewById(R.id.subpageFilterBackHolderLinearLayout);
        subpageFilterBackImageView=(ImageView)findViewById(R.id.subpageFilterBackImageView) ;
        audienceFilterClear =(Button)findViewById(R.id.subpageFilterClearButton);

        subpageFilterTitle =(TextView)findViewById(R.id.subpageFilterTitleTextView);
        audienceFilter =(ListView)findViewById(R.id.audienceFilterListView);

        subpageFilterBackHolderLinearLayout.setOnClickListener(this);
        subpageFilterBackImageView.setOnClickListener(this);
        subpageFilterTitle.setText(getString(R.string.filter_audience));
        audienceFilterClear.setOnClickListener(this);



        audienceItems = GeneralHelper.GetAudienceModels(self);
//        audienceItems.add(new AudienceFilterModel(getString(R.string.audience_children),false));
//        audienceItems.add(new AudienceFilterModel(getString(R.string.audience_everyone),false));
//        audienceItems.add(new AudienceFilterModel(getString(R.string.audience_family),false));
//        audienceItems.add(new AudienceFilterModel(getString(R.string.audience_adult),false));
//        audienceItems.add(new AudienceFilterModel(getString(R.string.audience_youngpeople),false));
//        audienceItems.add(new AudienceFilterModel(getString(R.string.audience_senior),false));

       // Set<String> audienceSettings= SharedPreferenceHelper.GetAudienceFilter(self);
        Intent intent = getIntent();
        ArrayList<String> previousValues = intent.getStringArrayListExtra("AudienceList");
        Set<String> audienceSettings = ConvertHelper.ConvertArrayListToSet(previousValues);
        if(audienceSettings.size() >0 ){
            audienceFilterClear.setVisibility(View.VISIBLE);
        }else{
            audienceFilterClear.setVisibility(View.GONE);
        }

        for(AudienceFilterModel model: audienceItems){
            if(audienceSettings.contains(model.getName()))
            {
                model.setSelected(true);
            }
        }


        audienceFilterAdapter = new AudienceFilterAdapter(self,audienceItems);
        audienceFilter.setAdapter(audienceFilterAdapter);

        audienceFilterListener =new AudienceFilterAdapter.AudienceFilterListener() {
            @Override
            public void onStatusChanged(AudienceFilterModel rowData) {
                if(GeneralHelper.CheckStatusAudienceFilter(audienceItems))
                {
                    audienceFilterClear.setVisibility(View.VISIBLE);
                }
                else
                {
                    audienceFilterClear.setVisibility(View.INVISIBLE);
                }
            }
        };
        audienceFilterAdapter.setListenerTask(audienceFilterListener);

        // Set fonts
        setFonts();

    }

    private void setFonts() {
        Typeface sourceSansProBold = FontHelper.SourceSansProBold(self);
       // Typeface sourceSansProRegular = FontHelper.SourceSansProRegular(self);
        subpageFilterTitle.setTypeface(sourceSansProBold);
        audienceFilterClear.setTypeface(sourceSansProBold);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.subpageFilterClearButton:
            {
                if(audienceFilterAdapter !=null) {
                    for (AudienceFilterModel item : audienceItems) {
                        item.setSelected(false);
                    }
                    audienceFilterAdapter.updateData(audienceItems);
                   audienceFilterClear.setVisibility(View.INVISIBLE);
                }

                break;
            }
            case R.id.subpageFilterBackHolderLinearLayout:
            case R.id.subpageFilterBackImageView:
            {
                //SharedPreferenceHelper.SaveAudienceFilter(self,audienceItems);
                GoBack();
                break;
            }

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        GoBack();
        super.onBackPressed();
    }

    private void GoBack()
    {
        ArrayList<String> items =new ArrayList<>();
        for (AudienceFilterModel model :
                audienceItems) {
            if (model.getSelected()) {
                items.add(model.getName());
            }
        }

        Intent returnIntent = new Intent();
        returnIntent.putExtra("AudienceList",items);
        setResult(self.RESULT_OK, returnIntent);
        super.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FBTrackerHelper.LogScreenView(this,getString(R.string.filter_audience));
    }


}
