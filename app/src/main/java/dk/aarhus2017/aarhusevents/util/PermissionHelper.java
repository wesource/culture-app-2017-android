package dk.aarhus2017.aarhusevents.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;

import dk.aarhus2017.aarhusevents.model.PermissionResult;

/**
 * Created by Dell on 12/15/2016.
 */

public class PermissionHelper {

    public static PermissionResult CheckCalendarPermission(Activity context)
    {
        PermissionResult result;
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED) {
            result= PermissionResult.Granted;
        }
        else
        {
            if(ActivityCompat.shouldShowRequestPermissionRationale(context,Manifest.permission.WRITE_CALENDAR))
            {
                result= PermissionResult.Denied;

            }
            else
            {
                result= PermissionResult.Requested;
            }
        }
        return result;
    }

    public static PermissionResult CheckLocationPermission(Activity context)
    {
        PermissionResult result;
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            result= PermissionResult.Granted;
        }
        else
        {
            if(ActivityCompat.shouldShowRequestPermissionRationale(context,Manifest.permission.ACCESS_FINE_LOCATION))
            {
                result= PermissionResult.Denied;

            }
            else
            {
                result= PermissionResult.Requested;
            }
        }
        return result;
    }
}
