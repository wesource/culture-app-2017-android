package dk.aarhus2017.aarhusevents.model;

/**
 * Created by Dell on 1/4/2017.
 */

public enum PromptPermissionResult {
    Yes,
    No,
    Ignore
}
