package dk.aarhus2017.aarhusevents.model;

import java.io.Serializable;

/**
 * Created by Dell on 11/29/2016.
 */

public class PracticalInfoModel {

    private String contactName;
    private String contactPhone;
    private String contactEmail;

    public PracticalInfoModel(String _contactName, String _contactPhone, String _contactEmail)
    {
        this.contactName=_contactName;
        this.contactPhone =_contactPhone;
        this.contactEmail=_contactEmail;
    }

    public String getContactName(){
            return this.contactName;
    }

    public String getContactPhone(){
        if(this.contactPhone.isEmpty())
        {
            return "";
        }
        else {
            return " " + this.contactPhone;
        }
    }

    public String getContactEmail(){
        if(this.contactEmail.isEmpty())
        {
            return "";
        }else {
            return " "+ this.contactEmail;
        }
    }
}
