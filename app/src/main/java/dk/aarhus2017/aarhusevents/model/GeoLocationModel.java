package dk.aarhus2017.aarhusevents.model;

/**
 * Created by Dell on 12/31/2016.
 */

public class GeoLocationModel {
    private double latitude;
    private double longitude;
    private int zoom;

    public GeoLocationModel(double _latitude, double _longitude, int _zoom)
    {
        this.latitude = _latitude;
        this.longitude =_longitude;
        this.zoom =_zoom;
    }

    public GeoLocationModel(){
        this.latitude=0;
        this.longitude=0;
        this.zoom=0;
    }
    public void  setLatitude(double _latitude){
        this.latitude =_latitude;
    }
    public void setLongitude(double _longitude){
        this.longitude =_longitude;
    }

    public double getLatitude(){return this.latitude;}

    public double getLongitude(){return this.longitude;}

    public int getZoom(){return this.zoom;}
}
