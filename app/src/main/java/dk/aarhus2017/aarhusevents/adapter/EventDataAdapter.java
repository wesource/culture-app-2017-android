package dk.aarhus2017.aarhusevents.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.location.Location;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.util.TimeUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.games.internal.constants.TimeSpan;


import java.sql.Time;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.model.EventDataBinding;
import dk.aarhus2017.aarhusevents.util.FontHelper;
import dk.aarhus2017.aarhusevents.util.GeneralHelper;
import dk.aarhus2017.aarhusevents.util.GlobalConfig;

/**
 * Created by Dell on 11/12/2016.
 */

public class EventDataAdapter extends RecyclerView.Adapter<EventDataAdapter.ViewHolder> {
    Context context;
    ArrayList<EventDataBinding> events = new ArrayList<>();

    protected EventDataAdapter.EventListener eventAdapter;
    int index = 0;
    Location myLocation;

    Typeface sourceSansProRegular;
    Typeface sourceSansProBold;
    Typeface flavinBold;
    boolean isAllowLoadMore = false;

    public EventDataAdapter(Context context, ArrayList<EventDataBinding> data, boolean isAllowLoadMore) {

        this.context = context;
        this.events = data;
        this.isAllowLoadMore = isAllowLoadMore;

        sourceSansProRegular = FontHelper.SourceSansProRegular(context);
        sourceSansProBold = FontHelper.SourceSansProBold(context);
        flavinBold = FontHelper.FlavinBold(context);
    }

    public void updateData(ArrayList<EventDataBinding> data) {
        this.events = data;
        notifyDataSetChanged();
    }

    public void updateLocation(Location _myLocation) {
        this.myLocation = _myLocation;
        notifyDataSetChanged();
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        EventDataBinding dataRow = getItem(position);
        if (!dataRow.isGroupHeader()) {

            holder.eventItemHolderLinearLayout.setVisibility(View.VISIBLE);
            holder.eventItemSectionHeaderHolderLinearLayout.setVisibility(View.GONE);
            // Set values
            holder.eventItemEventNameTextView.setText(dataRow.getEventData().getEventHeadline());
            //holder.eventItemDescriptionTextView.setText(dataRow.getEventData().getDescription());
            holder.eventItemDescriptionTextView.setText(dataRow.getEventData().getShortDescriptionHtml());

            //holder.eventItemTimeTextView.setText(GeneralHelper.TimeFormat(dataRow.getEventData().getTimeSpan(),dataRow.getEventData().getEndTimeSpan()));
            holder.eventItemTimeTextView.setText(dataRow.getEventData().getTime());

            // Make a seperation for next item
            if ((position + 1) < getItemCount()) {
                EventDataBinding nextItem = getItem(position + 1);
                if (!nextItem.isGroupHeader()) {
                    holder.eventItemSeperationLinearLayout.setVisibility(View.VISIBLE);
                } else {
                    holder.eventItemSeperationLinearLayout.setVisibility(View.GONE);
                }
            }

            if (dataRow.getEventData().getFavoriteEvent()) {
                holder.eventItemFavoriteImageView.setVisibility(View.VISIBLE);
            } else {
                holder.eventItemFavoriteImageView.setVisibility(View.GONE);
            }


            if (myLocation != null) {
                holder.eventLocationGroupPlaceHolder.setVisibility(View.VISIBLE);
                final Location toLocation = new Location("");
                toLocation.setLatitude(dataRow.getEventData().getLocationModel().getLatitude());
                toLocation.setLatitude(dataRow.getEventData().getLocationModel().getLatitude());
                dataRow.getEventData().getLocationModel().setLocation(myLocation);
                holder.eventItemDistanceTextView.setText(String.format("%.0f km", dataRow.getEventData().getLocationModel().getDistance()));
            } else {
                //holder.eventLocationGroupPlaceHolder.setVisibility(View.GONE);

                String city = dataRow.getEventData().getLocationModel().getCity();
                if (!city.isEmpty()) {
                    holder.eventItemDistanceTextView.setText(city);
                    holder.eventLocationGroupPlaceHolder.setVisibility(View.VISIBLE);
                } else {
                    holder.eventLocationGroupPlaceHolder.setVisibility(View.GONE);
                }
            }
            Glide.with(context).load(dataRow.getEventData().getImageUrl()).crossFade().into(holder.eventItemEventImageImageView);
            if (dataRow.getIsHoliday()) {
                holder.eventItemHolderLinearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.event_holiday_background));
                holder.eventItemSeperationLinearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.event_holiday_background));
            } else {
                holder.eventItemHolderLinearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.background_event_list));
                holder.eventItemSeperationLinearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.background_event_list));
            }

            if (isAllowLoadMore) {
                if (this.events.size() - 1 == position) {
                    holder.programItemPullDownHolderLinearLayout.setVisibility(View.VISIBLE);
                } else {
                    holder.programItemPullDownHolderLinearLayout.setVisibility(View.GONE);
                }
            } else {
                holder.programItemPullDownHolderLinearLayout.setVisibility(View.GONE);
            }

        } else {
            holder.eventItemHolderLinearLayout.setVisibility(View.GONE);
            holder.eventItemSectionHeaderHolderLinearLayout.setVisibility(View.VISIBLE);
            holder.eventItemSeperationLinearLayout.setVisibility(View.GONE);
            holder.eventItemSectionNameTextView.setText(GlobalConfig.FormatDateEventGroup.format(dataRow.getDate()));
            holder.programItemPullDownHolderLinearLayout.setVisibility(View.GONE);

            if (dataRow.getIsHoliday()) {
                holder.eventItemSectionHeaderHolderLinearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.group_event_holiday_background));
                holder.eventItemSectionNameTextView.setTextColor(ContextCompat.getColor(context, R.color.white));
                holder.eventItemSectionNameTextView.setText(GlobalConfig.FormatDateEventGroup.format(dataRow.getDate()) + " - " + context.getString(R.string.holiday));
            } else {
                holder.eventItemSectionHeaderHolderLinearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.background_section_event));
                holder.eventItemSectionNameTextView.setTextColor(ContextCompat.getColor(context, R.color.text));
                holder.eventItemSectionNameTextView.setText(GlobalConfig.FormatDateEventGroup.format(dataRow.getDate()));
            }
        }

    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public EventDataBinding getItem(int position) {
        return events.get(position);
    }


    public void add(EventDataBinding object) {
        events.add(object);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_item, parent, false);
        return new ViewHolder(view);
    }


    public void setTaskListener(EventListener listener) {
        eventAdapter = listener;
    }

    public interface EventListener {
        void onRowClick(EventDataBinding rowData);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout eventItemHolderLinearLayout;
        LinearLayout eventItemContentHolderLinearLayout;

        TextView eventItemEventNameTextView;
        TextView eventItemDescriptionTextView;
        TextView eventItemTimeTextView;
        TextView eventItemDistanceTextView;
        LinearLayout eventItemSeperationLinearLayout;
        ImageView eventItemEventImageImageView;

        LinearLayout eventLocationGroupPlaceHolder;

        ImageView eventItemFavoriteImageView;

        // Header
        LinearLayout eventItemSectionHeaderHolderLinearLayout;
        TextView eventItemSectionNameTextView;

        LinearLayout programItemPullDownHolderLinearLayout;
        TextView programItemPullDownTextView;

        public ViewHolder(View view) {
            super(view);

            View row = view;

            eventItemHolderLinearLayout = (LinearLayout) row.findViewById(R.id.eventItemHolderLinearLayout);
            eventItemContentHolderLinearLayout = (LinearLayout) row.findViewById(R.id.eventItemContentHolderLinearLayout);

            eventItemEventNameTextView = (TextView) row.findViewById(R.id.eventItemEventNameTextView);
            eventItemDescriptionTextView = (TextView) row.findViewById(R.id.eventItemDescriptionTextView);
            eventItemTimeTextView = (TextView) row.findViewById(R.id.eventItemTimeTextView);
            eventItemDistanceTextView = (TextView) row.findViewById(R.id.eventItemDistanceTextView);
            eventItemSeperationLinearLayout = (LinearLayout) row.findViewById(R.id.eventItemSeperationLinearLayout);
            eventItemEventImageImageView = (ImageView) row.findViewById(R.id.eventItemEventImageImageView);

            eventLocationGroupPlaceHolder = (LinearLayout) row.findViewById(R.id.eventLocationGroupPlaceHolder);
            eventItemFavoriteImageView = (ImageView) row.findViewById(R.id.eventItemFavoriteImageView);

            // Header
            eventItemSectionHeaderHolderLinearLayout = (LinearLayout) row.findViewById(R.id.eventItemSectionHeaderHolderLinearLayout);
            eventItemSectionNameTextView = (TextView) row.findViewById(R.id.eventItemSectionNameTextView);

            // Pull down
            programItemPullDownHolderLinearLayout = (LinearLayout) row.findViewById(R.id.programItemPullDownHolderLinearLayout);
            programItemPullDownTextView = (TextView) row.findViewById(R.id.programItemPullDownTextView);

            // Set fonts
            eventItemEventNameTextView.setTypeface(flavinBold);
            eventItemDescriptionTextView.setTypeface(sourceSansProRegular);
            eventItemTimeTextView.setTypeface(sourceSansProBold);
            eventItemDistanceTextView.setTypeface(sourceSansProBold);
            programItemPullDownTextView.setTypeface(sourceSansProBold);


            // Register event
            eventItemHolderLinearLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (eventAdapter != null) {
                int position = getAdapterPosition();
                eventAdapter.onRowClick(getItem(position));
            }
        }
    }


}
