package dk.aarhus2017.aarhusevents.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Dell on 3/1/2017.
 */

public class LocalBroadcastReceiver extends  BroadcastReceiver {


   protected LocalBroadcastReceiverListener listener;

    public LocalBroadcastReceiver(){
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(listener!=null){
            listener.onReceiveListener();
        }
    }
    public void setLocalBroadcastReceiverListener(LocalBroadcastReceiver.LocalBroadcastReceiverListener _listener){
        listener = _listener;
    }

    public interface LocalBroadcastReceiverListener{
        void onReceiveListener();
    }

}
