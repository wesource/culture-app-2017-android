package dk.aarhus2017.aarhusevents.util;

import android.app.Activity;
import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

import dk.aarhus2017.aarhusevents.R;

/**
 * Created by Dell on 2/6/2017.
 */

public class LocalDataHelper {
    public static JSONArray ReadDataJson(Context activity, String language)
    {
        JSONArray resultJsonArray=null;
        String rawJson = "";
        int dataFile;
        switch (language)
        {
            case "da":
            {
                dataFile = R.raw.data_da;
                break;
            }
            case "de":
            {
                dataFile = R.raw.data_de;
                break;
            }
            case "en":
            {
                dataFile = R.raw.data_en;
                break;
            }
            default:{
                dataFile = R.raw.data_en;
                break;
            }
        }

        try {
            InputStream is = activity.getResources().openRawResource(dataFile);
            int size = is.available();
            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();
            rawJson = new String(buffer, "UTF-8");
        } catch (IOException ex) {

        }
        try {
            resultJsonArray = new JSONArray(rawJson);
        }catch (Exception ex){

        }

        return resultJsonArray;
    }
}
