package dk.aarhus2017.aarhusevents.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Dell on 12/21/2016.
 */

public class BaseDataSource {
    protected Context context;
    protected SQLiteDatabase database;
    protected DatabaseHelper dbHelper;

    public BaseDataSource(Context _context)
    {
        context = _context;
        dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
    }



    public void open()
    {
        try {

            if(dbHelper==null)
            {
                dbHelper=new DatabaseHelper(context);
            }
            database = dbHelper.getWritableDatabase();
        }catch (Exception ex){
            Log.e("Open DB Error",ex.getMessage());
        }
    }

    public void close()
    {
        dbHelper.close();
    }

    public Boolean DeleteAllTables(){
         boolean result=false;
        try {

            context.deleteDatabase(DatabaseHelper.DatabaseName);
            result=true;
        }catch (Exception ex){
            result=false;
        }
        return result;
    }
}
