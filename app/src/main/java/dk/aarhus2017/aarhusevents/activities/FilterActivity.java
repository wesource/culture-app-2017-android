package dk.aarhus2017.aarhusevents.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.model.ArtformFilterModel;
import dk.aarhus2017.aarhusevents.model.AudienceFilterModel;
import dk.aarhus2017.aarhusevents.model.GeoLocationModel;
import dk.aarhus2017.aarhusevents.util.ConvertHelper;
import dk.aarhus2017.aarhusevents.util.FBTrackerHelper;
import dk.aarhus2017.aarhusevents.util.FontHelper;
import dk.aarhus2017.aarhusevents.util.GATrackerHelper;
import dk.aarhus2017.aarhusevents.util.GeneralHelper;
import dk.aarhus2017.aarhusevents.util.GlobalConstant;
import dk.aarhus2017.aarhusevents.util.LocationHelper;
import dk.aarhus2017.aarhusevents.util.SharedPreferenceHelper;

public class FilterActivity extends AppCompatActivity implements View.OnClickListener {

    FilterActivity self = this;

    // Header
    LinearLayout filterHeaderCloseLinearLayout;
    Button filterClearButton;
    TextView filterTitleTextView;

    // Content
    LinearLayout filterDaterangePlaceholder;
    TextView filterDaterangeLabelTextView;
    TextView filterDaterangeValueTextView;
    LinearLayout filterArtformPlaceHolder;
    TextView filterArtformLabelTextView;
    TextView filterArtformValueTextView;

    LinearLayout filterAudiencePlaceholder;
    TextView filterAudienceLabelTextView;
    TextView filterAudienceValueTextView;

    LinearLayout filterLocationPlaceholder;
    TextView filterLocationLabelTextView;

    ImageButton filterLocationPreValue;
    TextView filterLocationValue;
    Button filterDoneButton;



    Set<String> audienceResult;
    Set<String> artformResult;
    Date fromDate;
    Date toDate;
    String address;
    int distance;
    Boolean isCurrentMode;
    long fDateTime;
    long tDateTime;
    GeoLocationModel chooseLocation;
    //Location chooseLocation = new Location("");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MMM");
    Typeface sourceSansProBold;
    Typeface sourceSansProRegular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        sourceSansProBold = FontHelper.SourceSansProBold(self);
        sourceSansProRegular = FontHelper.SourceSansProRegular(self);
        // Header
        filterHeaderCloseLinearLayout = (LinearLayout) findViewById(R.id.filterHeaderCloseLinearLayout);
        filterClearButton = (Button) findViewById(R.id.filterClearButton);
        filterTitleTextView = (TextView)findViewById(R.id.filterTitleTextView);

       // Content
        filterDaterangePlaceholder = (LinearLayout) findViewById(R.id.filterDaterangePlaceholderLinearLayout);
        filterDaterangeLabelTextView = (TextView)findViewById(R.id.filterDaterangeLabelTextView);
        filterDaterangeValueTextView = (TextView) findViewById(R.id.filterDaterangeValueTextView);

        filterArtformPlaceHolder = (LinearLayout) findViewById(R.id.artformFilterPlaceholderLinearPlayout);
        filterArtformLabelTextView = (TextView)findViewById(R.id.filterArtformLabelTextView);
        filterArtformValueTextView = (TextView) findViewById(R.id.filterArtformValueTextView);

        filterAudiencePlaceholder = (LinearLayout) findViewById(R.id.filterAudiencePlaceholderLinearLayout);
        filterAudienceLabelTextView =(TextView)findViewById(R.id.filterAudenceLabelTextView);
        filterAudienceValueTextView = (TextView) findViewById(R.id.filterAudienceValueTextView);

        filterLocationPlaceholder = (LinearLayout) findViewById(R.id.filterLocationPlaceholderLinearLayout);
        filterLocationLabelTextView = (TextView)findViewById(R.id.filterLocationLabelTextView);
        filterLocationPreValue = (ImageButton) findViewById(R.id.filterLocationPreValueImageButton);
        filterLocationValue = (TextView) findViewById(R.id.filterLocationValueTextView);

        filterDoneButton = (Button) findViewById(R.id.filterDoneButton);

        // Register events
        filterHeaderCloseLinearLayout.setOnClickListener(this);
        filterClearButton.setOnClickListener(this);
        filterDoneButton.setOnClickListener(this);

        filterDaterangePlaceholder.setOnClickListener(this);
        filterArtformPlaceHolder.setOnClickListener(this);
        filterAudiencePlaceholder.setOnClickListener(this);
        filterLocationPlaceholder.setOnClickListener(this);

        // Set fonts
        setFonts();

        // Set Values

        isCurrentMode = SharedPreferenceHelper.GetIsCurrentLocationMode(self);

        fDateTime = SharedPreferenceHelper.GetFromDateFilter(self);
        tDateTime = SharedPreferenceHelper.GetToDateFilter(self);

        if (fDateTime > 0 && tDateTime > 0) {
            fromDate = new Date(fDateTime);
            toDate = new Date(tDateTime);
            filterDaterangeValueTextView.setText(simpleDateFormat.format(fromDate) + " - " + simpleDateFormat.format(toDate));
            filterClearButton.setVisibility(View.VISIBLE);
        } else {
            fromDate = new Date();
            toDate = new Date();
            toDate.setTime(new Date().getTime() + (14 * 24 * 60 * 60 * 1000));

            filterDaterangeValueTextView.setText(simpleDateFormat.format(new Date()) + " - " + simpleDateFormat.format(toDate));
        }


        artformResult = SharedPreferenceHelper.GetArtformFilter(self);
        if (artformResult.size() > 0) {
            String artformForm = ConvertHelper.ConvertToString(artformResult);
            filterArtformValueTextView.setText(artformForm);
            filterClearButton.setVisibility(View.VISIBLE);
        } else {
            filterArtformValueTextView.setText(getResources().getString(R.string.filter_artform_value));
        }

        audienceResult = SharedPreferenceHelper.GetAudienceFilter(self);
        if (audienceResult.size() > 0) {
            String audience = ConvertHelper.ConvertToString(audienceResult);
            filterAudienceValueTextView.setText(audience);
            filterClearButton.setVisibility(View.VISIBLE);
        } else {
            filterAudienceValueTextView.setText(getResources().getString(R.string.filter_audience_value));
        }


        distance = SharedPreferenceHelper.GetLocationDistanceFilter(self);
        chooseLocation = SharedPreferenceHelper.GetLocationFilter(self);
        address = SharedPreferenceHelper.GetCityFilter(self);
        filterLocationValue.setText(address);
        if (address.equals(getString(R.string.city_aarhus))) {
            filterLocationPreValue.setVisibility(View.GONE);
        } else {
            filterLocationPreValue.setVisibility(View.VISIBLE);
            filterClearButton.setVisibility(View.VISIBLE);
        }
    }

    private void setFonts(){
        filterTitleTextView.setTypeface(sourceSansProBold);
        filterClearButton.setTypeface(sourceSansProBold);

        filterDaterangeLabelTextView.setTypeface(sourceSansProRegular);
        filterArtformLabelTextView.setTypeface(sourceSansProRegular);
        filterAudienceLabelTextView.setTypeface(sourceSansProRegular);
        filterLocationLabelTextView.setTypeface(sourceSansProRegular);

        filterDaterangeValueTextView.setTypeface(sourceSansProBold);
        filterArtformValueTextView.setTypeface(sourceSansProBold);
        filterAudienceValueTextView.setTypeface(sourceSansProBold);
        filterLocationValue.setTypeface(sourceSansProRegular);
        filterLocationValue.setTypeface(sourceSansProBold);

        filterDoneButton.setTypeface(sourceSansProBold);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.artformFilterPlaceholderLinearPlayout:
            {
                Intent artformIntent = new Intent(self, ArtformFilterActivity.class);
                artformIntent.putExtra("ArtformList", ConvertHelper.ConvertSetToArrayList(artformResult));
                startActivityForResult(artformIntent, GlobalConstant.StartActivityForArtformFilterResult);
                break;
            }
            case R.id.filterAudiencePlaceholderLinearLayout: {
                Intent audienIntent = new Intent(self, AudienceFilterActivity.class);
                audienIntent.putExtra("AudienceList", ConvertHelper.ConvertSetToArrayList(audienceResult));
                startActivityForResult(audienIntent, GlobalConstant.StartActivityForAudienceFilterResult);
                break;
            }
            case R.id.filterDaterangePlaceholderLinearLayout: {

                Intent audienIntent = new Intent(self, DateRangeFilterActivity.class);
                audienIntent.putExtra("FromDate", fromDate.getTime());
                audienIntent.putExtra("ToDate", toDate.getTime());
                startActivityForResult(audienIntent, GlobalConstant.StartActivityForDaterangeFilterResult);
                break;
            }
            case R.id.filterLocationPlaceholderLinearLayout: {
                Intent locationIntent = new Intent(self, LocationFilterActivity.class);
                locationIntent.putExtra("Address", address);
                locationIntent.putExtra("Distance", distance);
                locationIntent.putExtra("Latitude", chooseLocation.getLatitude());
                locationIntent.putExtra("Longitude", chooseLocation.getLongitude());
                locationIntent.putExtra("IsCurrentLocationMode",isCurrentMode);

                startActivityForResult(locationIntent, GlobalConstant.StartActivityForLocationFilterResult);
                break;
            }
            case R.id.filterHeaderCloseLinearLayout:
            case R.id.filterDoneButton: {
                SharedPreferenceHelper.SaveDateRangeFilter(self, fDateTime, tDateTime);
                SharedPreferenceHelper.SaveArtformFilter(self, artformResult);
                SharedPreferenceHelper.SaveAudienceFilter(self, audienceResult);
                SharedPreferenceHelper.SaveCurrentLocationFilter(self, address, distance);
                SharedPreferenceHelper.SaveLocationFilter(self, chooseLocation);
                SharedPreferenceHelper.SaveIsCurrentLocationMode(self,isCurrentMode);

                GATrackerHelper.ApplyFilter(self,fDateTime,tDateTime,artformResult,audienceResult,address);

                GoBack();
                break;
            }
            case R.id.filterClearButton:
            {
                fromDate = new Date();
                toDate = new Date();
                toDate.setTime(new Date().getTime() + (14 * 24 * 60 * 60 * 1000));
                fDateTime = 0;
                tDateTime = 0;
                artformResult = new HashSet<>();
                audienceResult = new HashSet<>();
                address = getString(R.string.city_aarhus);
                distance = 10;
                chooseLocation = LocationHelper.getDefaultAarhusCity();


                filterDaterangeValueTextView.setText(simpleDateFormat.format(fromDate) + " - " + simpleDateFormat.format(toDate));
                filterArtformValueTextView.setText(getString(R.string.filter_artform_value));
                filterAudienceValueTextView.setText(getString(R.string.filter_audience_value));
                filterLocationValue.setText(address);

                filterLocationPreValue.setVisibility(View.GONE);
                filterClearButton.setVisibility(View.INVISIBLE);

                break;
            }
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case GlobalConstant.StartActivityForArtformFilterResult: {
                ArrayList<String> result = data.getStringArrayListExtra("ArtformList");
                artformResult = ConvertHelper.ConvertArrayListToSet(result);

                if (artformResult.size() > 0) {
                    String artformForm = ConvertHelper.ConvertToString(artformResult);
                    filterArtformValueTextView.setText(artformForm);
                    filterClearButton.setVisibility(View.VISIBLE);
                } else {
                    filterArtformValueTextView.setText(getResources().getString(R.string.filter_artform_value));
                }
                break;
            }
            case GlobalConstant.StartActivityForAudienceFilterResult: {
                ArrayList<String> result = data.getStringArrayListExtra("AudienceList");
                audienceResult = ConvertHelper.ConvertArrayListToSet(result);

                if (audienceResult.size() > 0) {
                    String audience = ConvertHelper.ConvertToString(audienceResult);
                    filterAudienceValueTextView.setText(audience);
                    filterClearButton.setVisibility(View.VISIBLE);
                } else {
                    filterAudienceValueTextView.setText(getResources().getString(R.string.filter_audience_value));
                }
                break;
            }
            case GlobalConstant.StartActivityForDaterangeFilterResult: {
                fDateTime = data.getLongExtra("FromDate", 0);
                tDateTime = data.getLongExtra("ToDate", 0);

                if (fDateTime > 0 & tDateTime > 0) {
                    fromDate = new Date(fDateTime);
                    toDate = new Date(tDateTime);
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MMM");
                    filterDaterangeValueTextView.setText(simpleDateFormat.format(fromDate) + " - " + simpleDateFormat.format(toDate));
                    filterClearButton.setVisibility(View.VISIBLE);
                }
                break;
            }
            case GlobalConstant.StartActivityForLocationFilterResult: {
                address = data.getStringExtra("Address");
                distance = data.getIntExtra("Distance", 0);
                isCurrentMode = data.getBooleanExtra("IsCurrentLocationMode",false);
                chooseLocation =new GeoLocationModel();
                chooseLocation.setLatitude(data.getDoubleExtra("Latitude", 0));
                chooseLocation.setLongitude(data.getDoubleExtra("Longitude", 0));

                filterLocationValue.setText(address);
                if (address.equals(getString(R.string.city_aarhus))) {
                    filterLocationPreValue.setVisibility(View.GONE);
                } else {
                    filterLocationPreValue.setVisibility(View.VISIBLE);
                    filterClearButton.setVisibility(View.VISIBLE);
                }
                break;
            }
            default:
                break;
        }
    }


    @Override
    public void onBackPressed() {
        GoBack();
        super.onBackPressed();
    }

    private void GoBack() {
        Intent returnIntent = new Intent();
        setResult(self.RESULT_OK, returnIntent);
        super.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FBTrackerHelper.LogScreenView(this,getString(R.string.filter_header_title));
    }
}
