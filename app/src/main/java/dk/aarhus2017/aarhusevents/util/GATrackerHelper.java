package dk.aarhus2017.aarhusevents.util;

import android.app.Activity;
import android.content.Context;


import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.Product;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import dk.aarhus2017.aarhusevents.model.AnalyticsApplication;
import dk.aarhus2017.aarhusevents.model.ArtformFilterModel;
import dk.aarhus2017.aarhusevents.model.AudienceFilterModel;

/**
 * Created by Dell on 1/8/2017.
 */

public class GATrackerHelper {

    private static String[] artFormKeys = new String[]{
            "HIGHLIGHT",
            "MUSIC_AND_SOUND",
            "PERFORMING_ARTS",
            "DESIGN",
            "VISUAL_ARTS_AND_EXHIBITIONS",
            "FESTIVALS_AND_HAPPENINGS",
            "FILM_AND_ANIMATIONS",
            "LITERATURE",
            "ARCHITECTURE"
    };
    private static String[] audienceKeys = new String[]{
            "CHILDREN",
            "EVERYONE",
            "FAMILIES",
            "ADULTS",
            "YOUNG_PEOPLE",
            "SENIORS"
    };

    public static String EventListScreen="Events List";
    public static String SearchScreen="Search";
    public static String My2017Screen="My 2017";
    public static String InfoScreen="Info";




    public static void SendGAScreenView(String screenName, Activity context) {
        AnalyticsApplication application = (AnalyticsApplication) context.getApplication();
        Tracker tracker = application.getDefaultTracker();
        tracker.setScreenName(screenName);
        tracker.send(new HitBuilders.ScreenViewBuilder().setNewSession().build());
    }

    public static void SendGAAction(String action, Activity context) {
        AnalyticsApplication application = (AnalyticsApplication) context.getApplication();
        Tracker tracker = application.getDefaultTracker();
        tracker.setScreenName(action);
        tracker.send(new HitBuilders.EventBuilder().setNewSession().setCategory("Action").setAction(action).build());

    }

    private static void SendGA(Context activity, Map<String, String> values) {
        AnalyticsApplication application = (AnalyticsApplication) activity.getApplicationContext();
        Tracker tracker = application.getDefaultTracker();
        tracker.send(values);
    }

    public static void SaveFavorite(Activity activity, int eventID, String eventName, Date eventDate, boolean isSwipe) {
        Map<String, String> values = EventBuild(eventID, eventName, eventDate, GA_Category.UI_Action, isSwipe ? GA_UI_Action.Swipe : GA_UI_Action.Touch, "save");
        SendGA(activity, values);
    }

    public static void WayFinder(Activity activity, int eventID, String eventName, Date eventDate) {
        Map<String, String> values = EventBuild(eventID, eventName, eventDate, GA_Category.UI_Action, GA_UI_Action.Touch, "way_finder");
        SendGA(activity, values);
    }

    public static void Ticket(Activity activity, int eventID, String eventName, Date eventDate) {
        Map<String, String> values = EventBuild(eventID, eventName, eventDate, GA_Category.UI_Action, GA_UI_Action.Touch, "tickets");
        SendGA(activity, values);
    }

    public static void ShowEventDetail(Activity activity, int eventID, String eventName, Date eventDate, int selectedEventType) {
        String eventType = "";
        switch (selectedEventType) {
            case 0: {
                eventType = "all";
                break;
            }
            case 1: {
                eventType = "official";
                break;
            }
            case 2: {
                eventType = "other";
                break;
            }
        }

        Map<String, String> values = EventBuild(eventID, eventName, eventDate, GA_Category.UI_Action, GA_UI_Action.Touch, eventType);
        SendGA(activity, values);
    }

    public static void SelectedLanguage(Activity activity, String language) {
        Map<String, String> values = new HitBuilders.EventBuilder().setCategory(GA_Category.UI_Language).setAction(GA_UI_Action.Select)
                .setLabel(language).setNewSession().build();
        SendGA(activity, values);
    }

    private static Map<String, String> EventBuild(int eventID, String eventName, Date eventDate, String category, String ui_Action, String label) {

        Product product = new Product();
        product.setId(String.valueOf(eventID));
        product.setName(eventName);
        product.setCustomDimension(GA_Dimension.StartDate, GeneralHelper.ShortDateFormat(eventDate));

        return new HitBuilders.EventBuilder().set(GeneralHelper.getDefaultLanguage(), String.valueOf(GA_Dimension.Language)).addProduct(product).setCategory(category).setAction(ui_Action).setLabel(label).build();
    }

    public static void SelectEventType(Activity activity, int selectedIndex) {
        String eventType = "";
        switch (selectedIndex) {
            case 0: {
                eventType = "all";
                break;
            }
            case 1: {
                eventType = "official";
                break;
            }
            case 2: {
                eventType = "other";
                break;
            }
        }
        Map<String, String> values = new HitBuilders.EventBuilder().setCategory(GA_Category.UI_Action).setAction(GA_UI_Action.Select_Event_Type)
                .setLabel(eventType).build();
        SendGA(activity, values);
    }

    public static void SynCalendar(Context activity) {

        Map<String, String> values = new HitBuilders.EventBuilder().set(GeneralHelper.getDefaultLanguage(), String.valueOf(GA_Dimension.Language)).setCategory(GA_Category.UI_Action).setAction(GA_UI_Action.Accept)
                .setLabel("sync").build();
        SendGA(activity, values);
    }

    public static void GeolocationAccept(Activity activity) {

        Map<String, String> values = new HitBuilders.EventBuilder().setCategory(GA_Category.UI_Action).setAction(GA_UI_Action.Accept)
                .setLabel("geolocation").set(GeneralHelper.getDefaultLanguage(), String.valueOf(GA_Dimension.Language)).build();
        SendGA(activity, values);
    }

    public static void ApplyFilter(Activity activity, long fDate, long tDate,  Set<String> artforms, Set<String> audiences, String city ) {

        HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder().setCategory(GA_Category.UI_Action).setAction(GA_UI_Action.Touch)
                .setLabel("apply_filter");
        builder.set(GeneralHelper.getDefaultLanguage(), getDimension(GA_Dimension.Language));
        builder.set(GeneralHelper.ShortDateFormat(fDate),getDimension(GA_Dimension.StartDate));
        builder.set(String.valueOf(GeneralHelper.GetNumberOfDate(fDate,tDate)),getDimension(GA_Dimension.NumberOfDays));
        builder.set(city, getDimension(GA_Dimension.Location));
        ArrayList<ArtformFilterModel> artformFilterModels = GeneralHelper.GetArtformModels(activity);
        for (int i=0; i <artformFilterModels.size();i++){
            String artfom=artformFilterModels.get(i).getName();
            if(artforms.contains(artfom)){
                builder.set(String.valueOf(1), artformFilterModels.get(i).getMetricIndex());
            }
        }
        ArrayList<AudienceFilterModel> audienceFilterModels = GeneralHelper.GetAudienceModels(activity);
        for (int i=0; i <audienceFilterModels.size();i++){
            String audience=audienceFilterModels.get(i).getName();
            if(audiences.contains(audience)){
               builder.set(String.valueOf(1),audienceFilterModels.get(i).getMetricIndex());
            }
        }

        Map<String, String> values = builder.build();

        SendGA(activity, values);
    }




    public class GA_Category {
        final public static String Action = "Action";
        final public static String UI_Action = "ui_action";
        final public static String UI_Language = "ui_language";
    }

    public class GA_UI_Action {
        final public static String Touch = "touch";
        final public static String Swipe = "swipe";
        final public static String Accept = "accept";
        final public static String Select = "select";
        final public static String Select_Event_Type = "select_event_type";
    }

    public class GA_Dimension {
        final public static int Language = 1;
        final public static int StartDate = 2;
        final public static int NumberOfDays = 3;
        final public static int Location = 4;
    }

    private static String getDimension(int index){
        return "dimension"+index;
    }



}
