package dk.aarhus2017.aarhusevents.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.model.LocationCityFilterModel;
import dk.aarhus2017.aarhusevents.model.MenuModel;


/**
 * Created by Dell on 12/23/2016.
 */

public class MenuAdapter extends ArrayAdapter<MenuModel> {
    Context context;
    ArrayList<MenuModel> cityItems=new ArrayList<>();
    protected MenuAdapter.MenuListener menuListener;

    public MenuAdapter(Context _context, ArrayList<MenuModel> values)
    {
        super(_context,-1,values);
        this.cityItems = values;
        this.context=_context;
    }



    @Override
    public void add(MenuModel object) {
        cityItems.add(object);
        super.add(object);
    }


    public void UpdateData(ArrayList<MenuModel> values){
        this.cityItems =values;
        notifyDataSetChanged();
    }

    @Override
    public MenuModel getItem(int position) {
        return cityItems.get(position);
    }

    @Override
    public int getCount() {
        return  this.cityItems.size();
    }


//    @Override
//    public View getDropDownView(int position, View convertView, ViewGroup parent) {
//        return customView(position, convertView, parent);
//
//    }



      @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
          return customView(position, convertView, parent);
    }


    private View customView(int position, final View convertView, final ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.menu_item, parent, false);

        final MenuModel dataRow = getItem(position);

        final TextView menuItemName =(TextView)row.findViewById(R.id.menuItemNameTextView);
        final ImageView selected = (ImageView)row.findViewById(R.id.menuItemSelectedImageView);
        Typeface sourceSansProRegular = Typeface.createFromAsset(context.getAssets(), "fonts/SourceSansPro-Regular.otf");

        menuItemName.setText(dataRow.getName());
        menuItemName.setTypeface(sourceSansProRegular);

        if(dataRow.getSelected())
        {
           // menuItemName.setTextColor(ContextCompat.getColor(context,R.color.filter_active));
            menuItemName.setTypeface(sourceSansProRegular, Typeface.BOLD);
            selected.setVisibility(View.VISIBLE);
        }
        else
        {
            menuItemName.setTypeface(sourceSansProRegular, Typeface.NORMAL);
        }



        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(menuListener!=null){
                    for (MenuModel model : cityItems ) {
                        model.setSelected(false);
                    }
                    menuListener.onMenuItemChanged(dataRow);
                    dataRow.setSelected(true);
                    notifyDataSetChanged();
                }
            }
        });


        return row;
    }



    public void setTaskListener(MenuAdapter.MenuListener _listener){
        this.menuListener = _listener;
    }

    public interface MenuListener {
        void onMenuItemChanged(MenuModel rowData);

    }
}
