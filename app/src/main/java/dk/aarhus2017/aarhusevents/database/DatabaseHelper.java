package dk.aarhus2017.aarhusevents.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import dk.aarhus2017.aarhusevents.util.SharedPreferenceHelper;

/**
 * Created by Dell on 12/21/2016.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    static  final String DatabaseName ="AarhusDB";
    static final int DatabaseVersion = 3;

    // Favorite table
    public static final String Table_Favorite="FavoriteEvent";
    public static final String Table_Favorite_ID="FavoriteID";
    public static final String Table_Favorite_ScheduledEventID="ScheduledEventID";
    public static final String Table_Favorite_ScheduledDate ="ScheduledDate";
    public static final String Table_Favorite_CalendarEventID ="CalendarEventID";


    static final String CREATE_FAVORITE_TABLE = "CREATE TABLE IF NOT EXISTS "+ Table_Favorite + " ("
            + Table_Favorite_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + Table_Favorite_ScheduledEventID + " INTEGER, "
            + Table_Favorite_ScheduledDate + " TEXT, "
            + Table_Favorite_CalendarEventID + " INTEGER "
            + ") ";

    public static final String Table_Event ="Event";
    public static final String Table_Event_ID="EventID";
    public static final String Table_Event_StartDate="StartDate";
    public static final String Table_Event_EndDate="EndDate";
    public static final String Table_Event_Headline="EventHeadline";
    public static final String Table_Event_ArtformList="ArtformList";
    public static final String Table_Event_LocationList = "LocationList";
    public static final String Table_Event_AudienceList ="AudienceList";
    public static final String Table_Event_ImageUrl ="ImageUrl";
    public static final String Table_Event_EventUrl ="EventUrl";
    public static final String Table_Event_LanguageDisabled="LanguageDisabled";
    public static final String Table_Event_LastUpdated ="LastUpdated";
    public static final String Table_Event_ExternallyCreated ="ExternallyCreated";
    public static final String Table_Event_DescriptionTitle="DescriptionTitle";
    public static final String Table_Event_Description ="Description";
    public static final String Table_Event_DescriptionHtml ="DescriptionHtml";
    public static final String Table_Event_ContactName ="ContactName";
    public static final String Table_Event_ContactPhone ="ContactPhone";
    public static final String Table_Event_ContactEmail ="ContactEmail";

    public static final String Table_Event_TicketUrl="TicketUrl";
    public static final String Table_Event_LanguageCulture = "LanguageCulture";
    public static final String Table_Event_LanguageTwoLetterCode ="LanguageTwoLetterCode";
    public static final String Table_Event_VideoUrl ="VideoUrl";
    public static final String Table_Event_EventVenueURL ="EventVenueURL";
    public static final String Table_Event_OpeningHourAlternativeText ="OpeningHourAlternativeText";


    static final String CREATE_EVENT_TABLE = "CREATE TABLE IF NOT EXISTS "+ Table_Event + " ("
            + Table_Event_ID + " INTEGER PRIMARY KEY, "
            + Table_Event_Headline + " TEXT, "
            + Table_Event_StartDate + " INT8, "
            + Table_Event_EndDate + " INT8, "
            + Table_Event_ArtformList + " TEXT, "
            + Table_Event_LocationList + " TEXT, "
            + Table_Event_AudienceList + " TEXT, "
            + Table_Event_ImageUrl + " TEXT, "
            + Table_Event_EventUrl + " TEXT, "
            + Table_Event_LanguageDisabled + " INTEGER, "
            + Table_Event_LastUpdated + " TEXT, "
            + Table_Event_ExternallyCreated + " INTEGER, "
            + Table_Event_DescriptionTitle + " TEXT, "
            + Table_Event_Description + " TEXT, "
            + Table_Event_DescriptionHtml + " TEXT, "
            + Table_Event_ContactName + " TEXT, "
            + Table_Event_ContactPhone + " TEXT, "
            + Table_Event_ContactEmail + " TEXT, "
            + Table_Event_TicketUrl + " TEXT, "
            + Table_Event_LanguageCulture + " TEXT, "
            + Table_Event_LanguageTwoLetterCode + " TEXT, "
            + Table_Event_VideoUrl + " TEXT, "
            + Table_Event_EventVenueURL + " TEXT, "
            + Table_Event_OpeningHourAlternativeText + " TEXT "
            + ") ";



    public static final String Table_ScheduledEvent ="ScheduledEvent";
    public static final String Table_ScheduledEvent_ID ="ID";
    public static final String Table_ScheduledEvent_EventID="EventID";
    public static final String Table_ScheduledEvent_StartDateTime ="StartDateTime";
    public static final String Table_ScheduledEvent_EndDateTime ="EndDateTime";
    public static final String Table_ScheduledEvent_Time ="Time";
    public static final String Table_ScheduledEvent_Venue="Venue";
    public static final String Table_ScheduledEvent_DisablilityAccess="DisabilityAccess";
    public static final String Table_ScheduledEvent_PriceFrom ="PriceFrom";
    public static final String Table_ScheduledEvent_PriceTo="PriceTo";
    public static final String Table_ScheduledEvent_Location_Name="LocationName";
    public static final String Table_ScheduledEvent_Location_Address="LocationAddress";
    public static final String Table_ScheduledEvent_Location_ZipCity="LocationZipCity";
    public static final String Table_ScheduledEvent_Location_Venue="LocationVenue";
    public static final String Table_ScheduledEvent_Location_Latitude="LocationLatitude";
    public static final String Table_ScheduledEvent_Location_Longitude="LocationLongitude";
    public static final String Table_ScheduledEvent_Location_ZoomDistance="LocationZoomDistance";

    public static final String Table_ScheduledEvent_SoldOut="SoldOut";
    public static final String Table_ScheduledEvent_TicketUrl="TicketUrl";

    private static final String CREATE_SCHEDULEDEVENT_TABLE = "CREATE TABLE IF NOT EXISTS "+ Table_ScheduledEvent + " ("
            + Table_ScheduledEvent_ID + " INTEGER PRIMARY KEY, "
            + Table_ScheduledEvent_EventID + " INTEGER, "
            + Table_ScheduledEvent_StartDateTime + " TEXT, "
            + Table_ScheduledEvent_EndDateTime + " TEXT, "
            + Table_ScheduledEvent_Time + " TEXT, "
            + Table_ScheduledEvent_Venue + " TEXT, "
            + Table_ScheduledEvent_DisablilityAccess + " INTEGER, "
            + Table_ScheduledEvent_PriceFrom + " INTEGER, "
            + Table_ScheduledEvent_PriceTo + " INTEGER, "
            + Table_ScheduledEvent_Location_Name + " TEXT, "
            + Table_ScheduledEvent_Location_Address + " INTEGER, "
            + Table_ScheduledEvent_Location_ZipCity + " TEXT, "
            + Table_ScheduledEvent_Location_Venue + " TEXT, "
            + Table_ScheduledEvent_Location_Latitude + " REAL, "
            + Table_ScheduledEvent_Location_Longitude + " REAL, "
            + Table_ScheduledEvent_Location_ZoomDistance + " INTEGER, "
            + Table_ScheduledEvent_SoldOut + " INTEGER, "
            + Table_ScheduledEvent_TicketUrl + " TEXT "
            + ") ";


    public static final String Table_OpeningHour ="OpeningHour";
    public static final String Table_OpeningHour_ID ="ID";
    public static final String Table_OpeningHour_EventID="EventID";
    public static final String Table_OpeningHour_WeekdayLabel ="WeekdayLabel";
    public static final String Table_OpeningHour_WeekdayId ="WeekdayId";
    public static final String Table_OpeningHour_Opens ="Opens";
    public static final String Table_OpeningHour_Closes="Closes";
    public static final String Table_OpeningHour_IsClosed="IsClosed";
    public static final String Table_OpeningHour_ClosedString ="ClosedString";

    private static final String CREATE_OPENINGHOUR_TABLE = "CREATE TABLE IF NOT EXISTS "+ Table_OpeningHour + " ("
            + Table_OpeningHour_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + Table_OpeningHour_EventID + " INTEGER, "
            + Table_OpeningHour_WeekdayLabel + " TEXT, "
            + Table_OpeningHour_WeekdayId + " INTEGER, "
            + Table_OpeningHour_Opens + " TEXT, "
            + Table_OpeningHour_Closes + " TEXT, "
            + Table_OpeningHour_IsClosed + " INTEGER, "
            + Table_OpeningHour_ClosedString + " TEXT "

            + ") ";

    private static final String DROP_EVENT_TABLE="DROP TABLE IF EXISTS "+Table_Event;
    private static final String DROP_SCHEDULEDEVENT_TABLE="DROP TABLE IF EXISTS "+Table_ScheduledEvent;
    private static final String DROP_OPENINGHOUR_TABLE="DROP TABLE IF EXISTS "+Table_OpeningHour;



    public  DatabaseHelper(Context context){
        super(context,DatabaseName,null, DatabaseVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try{
            sqLiteDatabase.execSQL(CREATE_FAVORITE_TABLE);
            sqLiteDatabase.execSQL(CREATE_EVENT_TABLE);
            sqLiteDatabase.execSQL(CREATE_SCHEDULEDEVENT_TABLE);
            sqLiteDatabase.execSQL(CREATE_OPENINGHOUR_TABLE);
        }
        catch (Exception ex)
        {
            Log.e("Create Database Error",ex.getMessage());
            ex.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        try{
            sqLiteDatabase.execSQL(DROP_OPENINGHOUR_TABLE);
            sqLiteDatabase.execSQL(DROP_SCHEDULEDEVENT_TABLE);
            sqLiteDatabase.execSQL(DROP_EVENT_TABLE);

            // Create
            sqLiteDatabase.execSQL(CREATE_EVENT_TABLE);
            sqLiteDatabase.execSQL(CREATE_SCHEDULEDEVENT_TABLE);
            sqLiteDatabase.execSQL(CREATE_OPENINGHOUR_TABLE);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
    @Override
    public void onOpen(SQLiteDatabase db)
    {
        super.onOpen(db);
        if (!db.isReadOnly())
        {
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }


}
