package dk.aarhus2017.aarhusevents.util;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;

import java.security.BasicPermission;
import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Filter;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.database.BaseDataSource;
import dk.aarhus2017.aarhusevents.database.EventDataSource;
import dk.aarhus2017.aarhusevents.database.FilterDataSource;
import dk.aarhus2017.aarhusevents.model.ArtformFilterModel;
import dk.aarhus2017.aarhusevents.model.AudienceFilterModel;
import dk.aarhus2017.aarhusevents.model.DBFavoriteEventModel;
import dk.aarhus2017.aarhusevents.model.DeletedEventModel;
import dk.aarhus2017.aarhusevents.model.EventDataBinding;
import dk.aarhus2017.aarhusevents.model.EventModel;
import dk.aarhus2017.aarhusevents.model.GeoLocationModel;
import dk.aarhus2017.aarhusevents.model.PromptPermissionResult;

/**
 * Created by Dell on 12/13/2016.
 */

public class SharedPreferenceHelper {



    public static boolean SaveFavoriteEvent(Context activity, int scheduledEventId, Date scheduledDate)
    {
        Boolean result=false;
        FilterDataSource db=new FilterDataSource(activity);
        result = db.InsertFavoriteEvent(scheduledEventId, scheduledDate);
        db.close();

        return result;
    }

    public static boolean RemoveFavoriteEvent(Activity activity, ContentResolver contentResolver, int scheduledEventId, Date scheduledDate)
    {
        Boolean result=false;
        long calendarEventID = 0;
        FilterDataSource db=new FilterDataSource(activity);
        calendarEventID = db.GetCalendarID(scheduledEventId,scheduledDate);// .GetCalendarEventID(scheduledEventId);
        result = db.RemoveFavoriteEvent(scheduledEventId, scheduledDate);
        db.close();
        result = GeneralHelper.DeleteCalendarEvent(contentResolver, calendarEventID);

        return result;
    }
    public static boolean UpdateFavoriteEvent(Context activity, int scheduledEventId, long calendarEventID, Date scheduledDate)
    {
        Boolean result=false;
        FilterDataSource db=new FilterDataSource(activity);
        result = db.UpdateFavoriteEvent(scheduledEventId, calendarEventID, scheduledDate);
        db.close();


        return result;
    }


    public static ArrayList<DBFavoriteEventModel> GetFavoriteEvents(Context activity)
    {
        ArrayList<DBFavoriteEventModel> items =new ArrayList<>();
        FilterDataSource db=new FilterDataSource(activity);
        items= db.GetFavoriteEvents();
        db.close();
        return items;
    }




    public static void SaveArtformFilter(Activity activity, ArrayList<ArtformFilterModel> values)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);

        Set<String> items =  new HashSet<String>();
        for (ArtformFilterModel model : values) {
            if (model.getSelected())
            {
                items.add(model.getName());
            }
        }

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(GlobalConfig.ArtformFilterKey,items);
        editor.apply();
    }
    public static void SaveArtformFilter(Activity activity, Set<String> values)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(GlobalConfig.ArtformFilterKey,values);
        editor.apply();
    }

    public static Set<String> GetArtformFilter(Context activity)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);

        Set<String> items =  new HashSet<String>();
        items= sharedPreferences.getStringSet(GlobalConfig.ArtformFilterKey,new HashSet<String>());

        return items;
    }



    public static void SaveAudienceFilter(Activity activity, ArrayList<AudienceFilterModel> values)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        Set<String> items =  new HashSet<String>();
        for (AudienceFilterModel model : values) {
            if (model.getSelected())
            {
                items.add(model.getName());
            }
        }

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(GlobalConfig.AudienceFilterKey,items);
        editor.apply();
    }
    public static void SaveAudienceFilter(Activity activity,  Set<String> values)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(GlobalConfig.AudienceFilterKey,values);
        editor.apply();
    }

    public static Set<String> GetAudienceFilter(Context activity)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);

        Set<String> items =  new HashSet<String>();
        items= sharedPreferences.getStringSet(GlobalConfig.AudienceFilterKey, new HashSet<String>());
        return items;
    }

    public static void SaveDateRangeFilter(Activity activity, long fromDate, long toDate)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(GlobalConfig.FromDateFilterKey,fromDate);
        editor.putLong(GlobalConfig.ToDateFilterKey, toDate);
        editor.apply();
    }
    public static long GetFromDateFilter(Context activity)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        long fromDate = sharedPreferences.getLong(GlobalConfig.FromDateFilterKey,0);
        return fromDate;
    }
    public static long GetToDateFilter(Context activity)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        long toDate = sharedPreferences.getLong(GlobalConfig.ToDateFilterKey,0);
        return toDate;
    }

    public static void SaveLocationDistanceFilter(Activity activity, int distance)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(GlobalConfig.DistanceFilterKey,distance);
        editor.apply();
    }



    public static void SaveCurrentLocationFilter(Activity activity, String address, int distance)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(GlobalConfig.CityFilterKey, address);
        editor.putInt(GlobalConfig.DistanceFilterKey, distance);
        editor.apply();
    }
    public static void SaveIsCurrentLocationMode(Activity activity, Boolean isCurrentLocationMode)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(GlobalConfig.IsCurrentLocationModeFilterKey, isCurrentLocationMode);
        editor.apply();
    }



    public static Boolean GetIsCurrentLocationMode(Context activity)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(GlobalConfig.IsCurrentLocationModeFilterKey,false);

    }

    public static void SaveCalendarPromptSetting(Context activity, PromptPermissionResult result)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(GlobalConfig.CalendarPromptSettingKey, String.valueOf(result));
        editor.apply();
    }
    public static PromptPermissionResult GetCalendarPromptSetting(Context activity)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString(GlobalConfig.CalendarPromptSettingKey,"");

        PromptPermissionResult result;
        switch (value)
        {
            case "Yes":
            {
                result = PromptPermissionResult.Yes;
                break;
            }
            case "No":
            {
                result = PromptPermissionResult.No;
                break;
            }
            case "Ignore":
            case "":
            {
                result = PromptPermissionResult.Ignore;
                break;
            }
            default:
            {
                result = PromptPermissionResult.Ignore;
                break;
            }
        }
        return result;

    }

    public static void SaveGeoLocationPromptSetting(Activity activity, PromptPermissionResult result)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(GlobalConfig.GeoLocationPromptSettingKey, String.valueOf(result));
        editor.apply();
    }
    public static PromptPermissionResult GetGeoLocationPromptSetting(Activity activity)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString(GlobalConfig.GeoLocationPromptSettingKey,"");

        PromptPermissionResult result;
        switch (value)
        {
            case "Yes":
            {
                result = PromptPermissionResult.Yes;
                break;
            }
            case "No":
            {
                result = PromptPermissionResult.No;
                break;
            }
            case "Ignore":
            case "":
            {
                result = PromptPermissionResult.Ignore;
                break;
            }
            default:
            {
                result = PromptPermissionResult.Ignore;
                break;
            }
        }
        return result;

    }

    public static int GetLocationDistanceFilter(Context activity)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        int distance = sharedPreferences.getInt(GlobalConfig.DistanceFilterKey,10);

       return distance;
    }

    public static float GetLatitudeFilter(Activity activity)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        float latitude = sharedPreferences.getFloat(GlobalConfig.LatitudeFilterKey,0);
        return latitude;

    }

    public static float GetLongitudeFilter(Activity activity)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        float longitude = sharedPreferences.getFloat(GlobalConfig.LongitudeFilterKey,0);
        return longitude;
    }

    public static void SaveLocationFilter(Activity activity, GeoLocationModel location)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat(GlobalConfig.LatitudeFilterKey, Float.parseFloat(String.valueOf(location.getLatitude())));
        editor.putFloat(GlobalConfig.LongitudeFilterKey, Float.parseFloat(String.valueOf(location.getLongitude())));
        editor.apply();
    }
    public static GeoLocationModel GetLocationFilter(Context activity)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        float latitude = sharedPreferences.getFloat(GlobalConfig.LatitudeFilterKey,0);
        float longitude = sharedPreferences.getFloat(GlobalConfig.LongitudeFilterKey,0);

        GeoLocationModel location =new GeoLocationModel();
        if(latitude==0 && longitude==0)
        {
            if(!SharedPreferenceHelper.GetIsCurrentLocationMode(activity)) {
                // Return default location
                location = LocationHelper.getDefaultAarhusCity();
            }
        }
        else {
            location.setLatitude(latitude);
            location.setLongitude(longitude);
        }

        return location;

    }


    public static String GetCityFilter(Context activity)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        String city = sharedPreferences.getString(GlobalConfig.CityFilterKey,activity.getResources().getString(R.string.city_aarhus));
        return  city;
    }


    public static boolean SaveEvents(Context context, ArrayList<EventModel> eventModels)
    {
        EventDataSource db=new EventDataSource(context);
        Boolean result = db.InsertEvents(eventModels);
        db.close();
        return result;
    }

    public static ArrayList<EventModel> GetEvents(Context context, String evenType, String language, Date fromDate, Date toDate)
    {
        ArrayList<EventModel> eventModels =new ArrayList<>();
        EventDataSource db=new EventDataSource(context);
        eventModels = db.GetEvents(evenType, language, fromDate,toDate);
        db.close();
        return eventModels;
    }

    public static Boolean CheckSynchronizeToCalendar(Context context, ArrayList<EventDataBinding> items)
    {
        boolean result=false;
        FilterDataSource db=new FilterDataSource(context);
        int numberOfFavorite = db.GetNotSyncScheduledEvents().size();
        db.close();
        if(numberOfFavorite > 0){
            result=true;
        }
        return result;
    }

    public static Boolean CheckIsExistedEventEventVenueURLColumn(Context context){
        EventDataSource db=new EventDataSource(context);
        Boolean result = db.IsExistEventVenueURLColumn();
        db.close();
        return result;
    }
    public static Boolean ResetDatabase(Context context){
        BaseDataSource db=new BaseDataSource(context);
        Boolean result = db.DeleteAllTables();
        db.close();
        return result;
    }

    public static Boolean SynchronizeToCalendar(Context context, ContentResolver contentResolver, ArrayList<EventDataBinding> items)
    {
        boolean result=false;

        try {
            FilterDataSource db = new FilterDataSource(context);
            ArrayList<DBFavoriteEventModel> notSyncItems = db.GetNotSyncScheduledEvents();


            for (DBFavoriteEventModel item : notSyncItems) {
                for (EventDataBinding scheduledItem : items) {
                    if (scheduledItem.isGroupHeader() == false) {
                        if (item.getScheduledEventID() == scheduledItem.getEventData().getScheduledEventID() &&
                                item.getScheduledDate().equals(GlobalConfig.FormatDate.format(scheduledItem.getDate()))) {
                            long calendarID = GeneralHelper.AddEventToCalendar(contentResolver, scheduledItem.getDate(), scheduledItem.getEventData().getTimeSpan(), scheduledItem.getEventData().getEndTimeSpan() ,scheduledItem.getEventData().getEventHeadline(), scheduledItem.getEventData().getDescription(), scheduledItem.getEventData().getEventUrl(),
                                    scheduledItem.getEventData().getLocationModel().getName() + ", " + scheduledItem.getEventData().getLocationModel().getAddress());
                            db.UpdateFavoriteEvent(item.getScheduledEventID(), calendarID, scheduledItem.getDate());
                        }
                    }
                }
            }
            db.close();
            result = true;
        }catch (Exception ex){
            result=false;
        }
        return result;

    }

    public static Boolean GetSynchronizeEventData(Context activity){

        Boolean current=false;
        EventDataSource db=new EventDataSource(activity);
        Boolean hasEvents = db.IsSynchronizeEvents();
        db.close();
        if(hasEvents==false){
            SaveIsSynchronizeEventData(activity,false);
        }else{
            SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
            current = sharedPreferences.getBoolean(GlobalConfig.IsSynchronizedEventKey,false);
        }

        return  (current && hasEvents);
    }
    public static void SaveIsSynchronizeEventData(Context activity, Boolean isSync)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(GlobalConfig.IsSynchronizedEventKey, isSync);
        editor.apply();
    }

    public static Boolean GetGALanguageStatus(Activity activity){
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        Boolean current = sharedPreferences.getBoolean(GlobalConfig.GALanguageKey,false);
        return  current;
    }
    public static void SaveGALanguageStatus(Activity activity)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(GlobalConfig.GALanguageKey, true);
        editor.apply();
    }

    // Deletes the events at local storage
    public static ArrayList<Integer> DeletedEventsSynchronization(Context context, ArrayList<Integer> values){
        ArrayList<Integer> results=new ArrayList<>();
        EventDataSource db=new EventDataSource(context);
        for (Integer item : values) {
            if(db.DeleteEvents(item)){
                results.add(item);
            }
        }
        db.close();
        return results;
    }

    public static void SaveDeletedEventSynchronization(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        Date currentDate=new Date();
        SimpleDateFormat simpleDateFormat =new SimpleDateFormat("yyyy");

        editor.putString(GlobalConfig.YearKey, simpleDateFormat.format(currentDate));
        simpleDateFormat=new SimpleDateFormat("MM");
        editor.putString(GlobalConfig.MonthKey, simpleDateFormat.format(currentDate));

        simpleDateFormat=new SimpleDateFormat("dd");
        editor.putString(GlobalConfig.DayKey, simpleDateFormat.format(currentDate));

        editor.apply();
    }

    public static DeletedEventModel GetDeletedEventSynchronization(Context context){
        DeletedEventModel model;
        SharedPreferences sharedPreferences = context.getSharedPreferences(GlobalConfig.PreferenceNameFilter, Context.MODE_PRIVATE);
        String year = sharedPreferences.getString(GlobalConfig.YearKey,"");
        if(year.isEmpty()){
            model =new DeletedEventModel("2016","10","01");
        }else{
            String month=sharedPreferences.getString(GlobalConfig.MonthKey,"");
            String day=sharedPreferences.getString(GlobalConfig.DayKey,"");
            model=new DeletedEventModel(year,month,day);
        }
        return model;
    }

    public static Date getMinDateFavorite(Context context){
        Date minDate =new Date();
        long min = minDate.getTime();
        ArrayList<DBFavoriteEventModel> favoriteEventModels=new ArrayList<>();
        FilterDataSource filterDataSource=new FilterDataSource(context);
        favoriteEventModels= filterDataSource.GetFavoriteEvents();
        filterDataSource.close();
        try {
            for (DBFavoriteEventModel item :
                    favoriteEventModels) {
                Date favoriteDate = GlobalConfig.FormatDate.parse(item.getScheduledDate());
                if(min>favoriteDate.getTime()){
                    min = favoriteDate.getTime();
                }

            }
        }catch (Exception ex){

        }
        minDate.setTime(min);
        return minDate;

    }
}
