package dk.aarhus2017.aarhusevents.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;

import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import dk.aarhus2017.aarhusevents.model.GeoLocationModel;

/**
 * Created by Dell on 12/6/2016.
 */

public class LocationHelper {


    private static final long MIN_DISTANCE_FOR_UPDATE = 10;
    private static final long MIN_TIME_FOR_UPDATE = 1000 * 60 * 10; // 10 minutes

    static LocationManager locationManager;
    static LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            Log.e("onLocationChanged","onLocationChanged");
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
            Log.e("onStatusChanged",s);
        }

        @Override
        public void onProviderEnabled(String s) {
            Log.e("onProviderEnabled",s);
        }

        @Override
        public void onProviderDisabled(String s) {
            Log.e("onProviderDisabled",s);
        }
    };
    public static boolean checkGPS(Context context) {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        return isGPS;
    }

    public static Location getLocation(Context context){
        Location location = null;
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,MIN_TIME_FOR_UPDATE, MIN_DISTANCE_FOR_UPDATE, locationListener);
                if (locationManager != null) {
                    location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }
            } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,MIN_TIME_FOR_UPDATE, MIN_DISTANCE_FOR_UPDATE, locationListener);
                if (locationManager != null) {
                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
            } else if(locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER))
            {
                locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER,MIN_TIME_FOR_UPDATE, MIN_DISTANCE_FOR_UPDATE, locationListener);
                if (locationManager != null) {
                    location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                }
            }

        }
        return location;
    }


    public static String getAddressFromMyLocation(Activity activity, Location myLocation )
    {
        String result="";
        if(myLocation !=null) {
            Geocoder geocoder = new Geocoder(activity, Locale.getDefault());
            List<Address> addresses;

            try {
                addresses = geocoder.getFromLocation(myLocation.getLatitude(), myLocation.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                Log.e("Add", String.valueOf(addresses));

                result = address + ", " + (city == null ? state : city);
            } catch (Exception ex) {
                Log.e("GetLocationAddressError", ex.getMessage());
            }
        }

        return  result;
    }


    public static GeoLocationModel getDefaultAarhusCity()
    {
        GeoLocationModel location =new GeoLocationModel(56.162939,10.203921000000037,8);
        return location;
    }

    public static float getDistance(Location from, Location to){
        if(from != null && to !=null )
        {
            float distance= from.distanceTo(to);
            if(distance>0)
            {
                distance = distance/1000;
            }
            return  distance;
        }
        else
        {
            return 0;
        }
    }
}
