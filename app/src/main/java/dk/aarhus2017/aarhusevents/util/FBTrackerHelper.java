package dk.aarhus2017.aarhusevents.util;

import android.content.Context;
import android.os.Bundle;

import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;

/**
 * Created by Dell on 3/18/2017.
 */

public class FBTrackerHelper {
    public static void LogViewedEvent(Context context, String eventHeadline){
        AppEventsLogger logger = AppEventsLogger.newLogger(context);
        Bundle params=new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE,"Event");
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID,eventHeadline);
        logger.logEvent(AppEventsConstants.EVENT_NAME_VIEWED_CONTENT,params);
    }
    public static void LogScreenView(Context context, String screenName){
        AppEventsLogger logger = AppEventsLogger.newLogger(context);
        Bundle params=new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE,"Screen");
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID,screenName);
        logger.logEvent(AppEventsConstants.EVENT_NAME_VIEWED_CONTENT,params);
    }

    public static void LogSearchedEvent (Context context, String searchString, boolean success, int numOfItems) {
        AppEventsLogger logger = AppEventsLogger.newLogger(context);
        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "Event");
        params.putString(AppEventsConstants.EVENT_PARAM_SEARCH_STRING, searchString);
        params.putInt(AppEventsConstants.EVENT_PARAM_SUCCESS, success ? 1 : 0);
        logger.logEvent(AppEventsConstants.EVENT_NAME_SEARCHED,numOfItems ,params);
    }
}
