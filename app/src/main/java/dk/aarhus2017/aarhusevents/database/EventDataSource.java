package dk.aarhus2017.aarhusevents.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.icu.text.DateFormat;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import dk.aarhus2017.aarhusevents.model.DBFavoriteEventModel;
import dk.aarhus2017.aarhusevents.model.EventModel;
import dk.aarhus2017.aarhusevents.model.LocationModel;
import dk.aarhus2017.aarhusevents.model.OpeningHourModel;
import dk.aarhus2017.aarhusevents.model.PracticalInfoModel;
import dk.aarhus2017.aarhusevents.model.ScheduledEventModel;
import dk.aarhus2017.aarhusevents.util.ConvertHelper;
import dk.aarhus2017.aarhusevents.util.GeneralHelper;
import dk.aarhus2017.aarhusevents.util.GlobalConfig;

/**
 * Created by Dell on 12/22/2016.
 */

public class EventDataSource extends BaseDataSource {

    public EventDataSource(Context _context) {
        super(_context);
    }


    public Boolean InsertEvents(ArrayList<EventModel> eventModels) {
        ArrayList<Integer> existedEvents = GetEventIDs();
        ArrayList<Integer> existedScheduledItems = GetScheduledIDs();
        Boolean result = false;
        try {
            for (EventModel model : eventModels) {
                ContentValues values = new ContentValues();

                values.put(DatabaseHelper.Table_Event_Headline, model.getEventHeadline());
                values.put(DatabaseHelper.Table_Event_StartDate, model.getStartDate());
                values.put(DatabaseHelper.Table_Event_EndDate, model.getEndDate());
                values.put(DatabaseHelper.Table_Event_ArtformList, ConvertHelper.ConvertArrayListToString(model.getArtFormList()));
                values.put(DatabaseHelper.Table_Event_LocationList, model.getLocationList());
                values.put(DatabaseHelper.Table_Event_AudienceList, ConvertHelper.ConvertArrayListToString(model.getAudienceList()));
                values.put(DatabaseHelper.Table_Event_ImageUrl, model.getImageUrl());
                values.put(DatabaseHelper.Table_Event_EventUrl, model.getEventUrl());
                values.put(DatabaseHelper.Table_Event_LanguageDisabled, model.getLanguageDisabled() ? 1 : 0);
                values.put(DatabaseHelper.Table_Event_LastUpdated, model.getLastUpdated());
                values.put(DatabaseHelper.Table_Event_ExternallyCreated, model.getExternallyCreated() ? 1 : 0);
                values.put(DatabaseHelper.Table_Event_DescriptionTitle, model.getDescriptionHeadline());
                values.put(DatabaseHelper.Table_Event_Description, model.getDescription());
                values.put(DatabaseHelper.Table_Event_DescriptionHtml, model.getDescriptionHtml());
                values.put(DatabaseHelper.Table_Event_ContactName, model.getPracticalInfo().getContactName());
                values.put(DatabaseHelper.Table_Event_ContactPhone, model.getPracticalInfo().getContactPhone());
                values.put(DatabaseHelper.Table_Event_ContactEmail, model.getPracticalInfo().getContactEmail());
                values.put(DatabaseHelper.Table_Event_TicketUrl, model.getTicketUrl());
                values.put(DatabaseHelper.Table_Event_LanguageCulture, model.getLanguageCulture());
                values.put(DatabaseHelper.Table_Event_LanguageTwoLetterCode, model.getLanguageTwoLetterCode());
                values.put(DatabaseHelper.Table_Event_VideoUrl, model.getVideoUrl());
                values.put(DatabaseHelper.Table_Event_EventVenueURL, model.getEventVenueUrl());
                values.put(DatabaseHelper.Table_Event_OpeningHourAlternativeText, model.getOpeningHoursAlternativeText());
                long eventIdResult = 0;
                if (existedEvents.contains(model.getEventId())) {
                    String where = DatabaseHelper.Table_Event_ID + " = ?";
                    String[] selectionArg = {String.valueOf(model.getEventId())};
                    eventIdResult = database.update(DatabaseHelper.Table_Event, values, where, selectionArg);
                } else {
                    values.put(DatabaseHelper.Table_Event_ID, model.getEventId());
                    eventIdResult = database.insertWithOnConflict(DatabaseHelper.Table_Event, null, values, SQLiteDatabase.CONFLICT_IGNORE);
                }
                if (eventIdResult > 0) {
                    database.delete(DatabaseHelper.Table_ScheduledEvent, DatabaseHelper.Table_ScheduledEvent_EventID + " = ?", new String[]{String.valueOf(model.getEventId())});

                    for (ScheduledEventModel scheduledEvent : model.getScheduledEvents()) {
                        ContentValues scheduledValues = new ContentValues();

                        scheduledValues.put(DatabaseHelper.Table_ScheduledEvent_EventID, model.getEventId());
                        scheduledValues.put(DatabaseHelper.Table_ScheduledEvent_StartDateTime, scheduledEvent.getStartDateTime());
                        scheduledValues.put(DatabaseHelper.Table_ScheduledEvent_EndDateTime, scheduledEvent.getEndDateTime());
                        scheduledValues.put(DatabaseHelper.Table_ScheduledEvent_Time, scheduledEvent.getTime());
                        scheduledValues.put(DatabaseHelper.Table_ScheduledEvent_Venue, scheduledEvent.getVenue());
                        scheduledValues.put(DatabaseHelper.Table_ScheduledEvent_DisablilityAccess, scheduledEvent.getDisablilityAccess() ? 1 : 0);
                        scheduledValues.put(DatabaseHelper.Table_ScheduledEvent_PriceFrom, scheduledEvent.getFromPrice());
                        scheduledValues.put(DatabaseHelper.Table_ScheduledEvent_PriceTo, scheduledEvent.getToPrice());
                        scheduledValues.put(DatabaseHelper.Table_ScheduledEvent_Location_Name, scheduledEvent.getLocationModel().getName());
                        scheduledValues.put(DatabaseHelper.Table_ScheduledEvent_Location_Address, scheduledEvent.getLocationModel().getAddress());
                        scheduledValues.put(DatabaseHelper.Table_ScheduledEvent_Location_ZipCity, scheduledEvent.getLocationModel().getZipCity());
                        scheduledValues.put(DatabaseHelper.Table_ScheduledEvent_Location_Venue, scheduledEvent.getLocationModel().getVenue());
                        scheduledValues.put(DatabaseHelper.Table_ScheduledEvent_Location_Latitude, scheduledEvent.getLocationModel().getLatitude());
                        scheduledValues.put(DatabaseHelper.Table_ScheduledEvent_Location_Longitude, scheduledEvent.getLocationModel().getLongitude());
                        scheduledValues.put(DatabaseHelper.Table_ScheduledEvent_Location_ZoomDistance, scheduledEvent.getLocationModel().getZoomDistance());
                        scheduledValues.put(DatabaseHelper.Table_ScheduledEvent_SoldOut, scheduledEvent.getSoldOut() ? 1 : 0);
                        scheduledValues.put(DatabaseHelper.Table_ScheduledEvent_TicketUrl, scheduledEvent.getTicketUrl());
                        long scheduledIDResult;
//                        if(existedScheduledItems.contains(scheduledEvent.getId())){
//                            String where = DatabaseHelper.Table_ScheduledEvent_ID + " = ?";
//                            String[] selectionArg = {String.valueOf(scheduledEvent.getId())};
//                            scheduledIDResult = database.update(DatabaseHelper.Table_ScheduledEvent,scheduledValues, where, selectionArg);
//                        }else {
                        scheduledValues.put(DatabaseHelper.Table_ScheduledEvent_ID, scheduledEvent.getId());
                        scheduledIDResult = database.insert(DatabaseHelper.Table_ScheduledEvent, null, scheduledValues);
                        // }
                    }
                }
                // Openings hours
                if(eventIdResult > 0){
                    database.delete(DatabaseHelper.Table_OpeningHour, DatabaseHelper.Table_OpeningHour_EventID + " = ?", new String[]{String.valueOf(model.getEventId())});
                    for (OpeningHourModel openingHour : model.getOpeningHourModels()) {
                        ContentValues openingValues = new ContentValues();
                        openingValues.put(DatabaseHelper.Table_OpeningHour_EventID,model.getEventId());
                        openingValues.put(DatabaseHelper.Table_OpeningHour_WeekdayId, openingHour.getWeekdayId());
                        openingValues.put(DatabaseHelper.Table_OpeningHour_WeekdayLabel,openingHour.getWeekdayLabel());
                        openingValues.put(DatabaseHelper.Table_OpeningHour_Opens, openingHour.getOpens());
                        openingValues.put(DatabaseHelper.Table_OpeningHour_Closes,openingHour.getCloses());
                        openingValues.put(DatabaseHelper.Table_OpeningHour_IsClosed, openingHour.getIsClosed()? 1: 0);
                        openingValues.put(DatabaseHelper.Table_OpeningHour_ClosedString, openingHour.getClosedString());
                        database.insert(DatabaseHelper.Table_OpeningHour, null, openingValues);

                    }
                }


            }
            result = true;
        } catch (Exception ex) {
            Log.e("Insert Event Error", ex.getMessage());
            result = false;
        }
        return result;
    }

    public ArrayList<EventModel> GetEvents(String eventType, String language, Date fromDate, Date toDate) {
        ArrayList<EventModel> eventItems = new ArrayList<>();


        String selectQuery = "SELECT  * FROM " + DatabaseHelper.Table_Event;// + whereClause;
        Cursor cursor = database.rawQuery(selectQuery, null);

        int eventId;
        String eventHeadline;
        ArrayList<String> artFormList;
        String locationList;
        ArrayList<String> audienceList;
        String imageUrl;
        String eventUrl;
        Boolean languageDisabled;
        String descriptionTitle;
        String description;
        String descriptionHtml;
        String ticketUrl;
        String languageCulture;
        String languageTwoLetterCode;
        String videoUrl;
        String eventVenueUrl;
        String lastUpdated;
        Boolean externallyCreated;
        ArrayList<ScheduledEventModel> scheduledEventModels = new ArrayList<>();

        PracticalInfoModel practicalInforModel;
        String contactName;
        String contactPhone;
        String contactEmail;

        String openingHourAlternativeText;

        if (cursor.moveToFirst()) {
            do {
                eventId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Table_Event_ID));
                eventHeadline = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Event_Headline));
                String artform = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Event_ArtformList));
                artFormList = ConvertHelper.ConvertStringToArrayList(artform);
                locationList = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Event_LocationList));
                String audience = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Event_AudienceList));
                audienceList = ConvertHelper.ConvertStringToArrayList(audience);
                imageUrl = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Event_ImageUrl));
                eventUrl = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Event_EventUrl));
                languageDisabled = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Table_Event_LanguageDisabled)) == 1 ? true : false;
                descriptionTitle = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Event_DescriptionTitle));
                description = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Event_Description));
                descriptionHtml = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Event_DescriptionHtml));
                ticketUrl = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Event_TicketUrl));
                languageCulture = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Event_LanguageCulture));
                languageTwoLetterCode = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Event_LanguageTwoLetterCode));
                videoUrl = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Event_VideoUrl));
                eventVenueUrl = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Event_EventVenueURL));
                lastUpdated = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Event_LastUpdated));
                externallyCreated = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Table_Event_ExternallyCreated)) == 1 ? true : false;

                contactName = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Event_ContactName));
                contactPhone = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Event_ContactPhone));
                contactEmail = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Event_ContactEmail));

                practicalInforModel = new PracticalInfoModel(contactName, contactPhone, contactEmail);
                openingHourAlternativeText =cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_Event_OpeningHourAlternativeText));

                EventModel model = new EventModel(eventId, eventHeadline, artFormList, locationList, audienceList, imageUrl, eventUrl, languageDisabled, descriptionTitle, description, descriptionHtml, ticketUrl, languageCulture, languageTwoLetterCode, videoUrl,
                        lastUpdated, externallyCreated, scheduledEventModels, practicalInforModel);
                model.setEventVenueUrl(eventVenueUrl);
                model.setOpeningHoursAlternativeText(openingHourAlternativeText);
                eventItems.add(model);


            } while (cursor.moveToNext());
        }
        cursor.close();

        //Fetch ScheduleEvent;
        for (EventModel eventModel : eventItems) {
            eventModel.setScheduledEvents(GetScheduledEvents(eventModel.getEventId()));
            eventModel.setOpeningHourModels(GetOpeningHours(eventModel.getEventId()));
        }


        return eventItems;

    }

    private ArrayList<ScheduledEventModel> GetScheduledEvents(int eventID) {
        ArrayList<ScheduledEventModel> items = new ArrayList<>();

        String selectQuery = "SELECT  * FROM " + DatabaseHelper.Table_ScheduledEvent + " WHERE EventID =" + eventID;
        Cursor cursor = database.rawQuery(selectQuery, null);

        int id;
        int eventId;
        String startDateTime;
        String endDateTime;
        String time;
        String venue;
        Boolean disabilityAccess;
        int priceFrom;
        int priceTo;
        String locationName;
        String locationAddress;
        String locationZipCity;
        String locationVenue;
        Double locationLatitude;
        Double locationLongitude;
        int locationZoomDistance;
        LocationModel locationModel;
        Boolean soldOut;
        String ticketUrl;

        try {
            if (cursor.moveToFirst()) {
                do {
                    id = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Table_ScheduledEvent_ID));
                    startDateTime = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_ScheduledEvent_StartDateTime));
                    endDateTime = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_ScheduledEvent_EndDateTime));
                    time = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_ScheduledEvent_Time));
                    venue = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_ScheduledEvent_Venue));
                    disabilityAccess = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Table_ScheduledEvent_DisablilityAccess)) == 1 ? true : false;
                    priceFrom = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Table_ScheduledEvent_PriceFrom));
                    priceTo = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Table_ScheduledEvent_PriceTo));
                    locationName = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_ScheduledEvent_Location_Name));
                    locationAddress = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_ScheduledEvent_Location_Address));
                    locationZipCity = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_ScheduledEvent_Location_ZipCity));
                    locationVenue = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_ScheduledEvent_Location_Venue));
                    locationLatitude = cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.Table_ScheduledEvent_Location_Latitude));
                    locationLongitude = cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.Table_ScheduledEvent_Location_Longitude));
                    locationZoomDistance = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Table_ScheduledEvent_Location_ZoomDistance));
                    soldOut = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Table_ScheduledEvent_SoldOut)) == 1 ? true : false;
                    ticketUrl = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_ScheduledEvent_TicketUrl));

                    locationModel = new LocationModel(locationName, locationAddress, locationZipCity, locationVenue, locationLatitude, locationLongitude, locationZoomDistance);

                    ScheduledEventModel model = new ScheduledEventModel(id, startDateTime, endDateTime, time, venue, disabilityAccess, priceFrom, priceTo, locationModel, soldOut, ticketUrl);
                    model.setStartDateTimeFormat(GlobalConfig.FormatDate.parse(startDateTime));
                    model.setEndDateTimeFormat(GlobalConfig.FormatDate.parse(endDateTime));
                    model.setFullStartDate(GlobalConfig.DateFullFormat.parse(startDateTime));
                    model.setFullEndDate(GlobalConfig.DateFullFormat.parse(endDateTime));

                    items.add(model);

                } while (cursor.moveToNext());

            }
        } catch (Exception ex) {
            Log.e("ScheduledEvent Error", ex.getMessage());
        }
        return items;

    }

    private ArrayList<OpeningHourModel> GetOpeningHours(int eventID) {
        ArrayList<OpeningHourModel> items = new ArrayList<>();

        String selectQuery = "SELECT  * FROM " + DatabaseHelper.Table_OpeningHour + " WHERE EventID =" + eventID+ " AND "+DatabaseHelper.Table_OpeningHour_IsClosed+" = 0";
       //String selectQuery = "SELECT  * FROM " + DatabaseHelper.Table_OpeningHour + " WHERE EventID = " + eventID;
        Cursor cursor = database.rawQuery(selectQuery, null);

        int weekdayId;
        String weekdayLabel;
        String opens;
        String closes;
        Boolean isClosed;
        String closedString;

        OpeningHourModel model;

        try {
            if (cursor.moveToFirst()) {
                do {
                    weekdayId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Table_OpeningHour_WeekdayId));
                    weekdayLabel = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_OpeningHour_WeekdayLabel));
                    isClosed = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Table_OpeningHour_IsClosed)) == 1 ? true : false;
                    opens = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_OpeningHour_Opens));
                    closes = cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_OpeningHour_Closes));
                    closedString=cursor.getString(cursor.getColumnIndex(DatabaseHelper.Table_OpeningHour_ClosedString));

                    model =new OpeningHourModel(weekdayId,weekdayLabel,opens,closes,isClosed,closedString);

                    items.add(model);

                } while (cursor.moveToNext());

            }
        } catch (Exception ex) {
            Log.e("OpeningHours Error", ex.getMessage());
        }
        return items;

    }


    private ArrayList<Integer> GetEventIDs() {
        ArrayList<Integer> eventItems = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + DatabaseHelper.Table_Event;
        Cursor cursor = database.rawQuery(selectQuery, null);

        int eventId;
        if (cursor.moveToFirst()) {
            do {
                eventId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Table_Event_ID));
                eventItems.add(eventId);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return eventItems;

    }

    private ArrayList<Integer> GetScheduledIDs() {
        ArrayList<Integer> eventItems = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + DatabaseHelper.Table_ScheduledEvent;
        Cursor cursor = database.rawQuery(selectQuery, null);

        int eventId;
        if (cursor.moveToFirst()) {
            do {
                eventId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Table_ScheduledEvent_ID));
                eventItems.add(eventId);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return eventItems;

    }




    public Boolean IsExistEventVenueURLColumn() {

        Boolean isExist = false;
        String selectQuery = "SELECT * FROM " + DatabaseHelper.Table_Event + " LIMIT 1";
        Cursor cursor = database.rawQuery(selectQuery, null);
        int value = cursor.getColumnIndex(DatabaseHelper.Table_Event_EventVenueURL);
        if (value > 0) {
            isExist = true;
        }
        return isExist;
    }
    public Boolean IsSynchronizeEvents() {

        Boolean result = false;
        String selectQuery = "SELECT * FROM " + DatabaseHelper.Table_Event + " LIMIT 1";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            result = true;
        }
        return result;
    }


    private ArrayList<Integer> GetEventIDs(Integer eventID) {
        ArrayList<Integer> eventItems = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + DatabaseHelper.Table_Event+" WHERE "+DatabaseHelper.Table_Event_ID+" = "+eventID;
        Cursor cursor = database.rawQuery(selectQuery, null);

        int eventId;
        if (cursor.moveToFirst()) {
            do {
                eventId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Table_Event_ID));
                eventItems.add(eventId);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return eventItems;

    }

    public boolean DeleteEvents(int eventId){
       boolean result=false;
        try {

            ArrayList<Integer> ids = GetEventIDs(eventId);

           int scheduleResult= database.delete(DatabaseHelper.Table_ScheduledEvent, DatabaseHelper.Table_ScheduledEvent_EventID + " = ?", new String[]{String.valueOf(eventId)});

            int eventResult= database.delete(DatabaseHelper.Table_Event, DatabaseHelper.Table_Event_ID + " = ?", new String[]{String.valueOf(eventId)});

            if(scheduleResult > 0 || eventResult > 0){
                result = true;
            }
       }catch (Exception ex){
       }
        return result;
    }
}
