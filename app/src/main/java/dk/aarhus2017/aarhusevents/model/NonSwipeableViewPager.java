package dk.aarhus2017.aarhusevents.model;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Dell on 3/7/2017.
 */

public class NonSwipeableViewPager extends ViewPager {

    public NonSwipeableViewPager(Context context){
        super(context);
    }

    public NonSwipeableViewPager(Context context, AttributeSet attrs){
        super(context,attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }
}
