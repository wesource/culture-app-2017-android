package dk.aarhus2017.aarhusevents.model;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.Date;

import dk.aarhus2017.aarhusevents.util.GlobalConfig;

import static android.support.v7.widget.RecyclerView.TOUCH_SLOP_DEFAULT;

/**
 * Created by Dell on 3/4/2017.
 */

public abstract class SearchEndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {

    int firstVisibleItem, totalItemCount;

    private int y;

    private LinearLayoutManager mLinearLayoutManager;

    public SearchEndlessRecyclerOnScrollListener(LinearLayoutManager _linearLayoutManager) {
        this.mLinearLayoutManager = _linearLayoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
//        totalItemCount = mLinearLayoutManager.getItemCount();
//        firstVisibleItem = mLinearLayoutManager.findLastVisibleItemPosition();
        y=dy;
//        if (dy > -1) {
//           onHideKeyboard();
//        }
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        totalItemCount = mLinearLayoutManager.getItemCount();
        firstVisibleItem = mLinearLayoutManager.findLastVisibleItemPosition();

        if(newState==TOUCH_SLOP_DEFAULT && y>-1 ){//;firstVisibleItem == firstVisibleItem+2
            onHideKeyboard();
        }
    }



    public abstract void onHideKeyboard();
}
