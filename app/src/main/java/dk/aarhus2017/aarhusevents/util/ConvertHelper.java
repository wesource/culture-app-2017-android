package dk.aarhus2017.aarhusevents.util;

import android.app.Activity;
import android.content.Context;
import android.icu.text.TimeZoneNames;
import android.location.Location;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;

import dk.aarhus2017.aarhusevents.model.DBFavoriteEventModel;
import dk.aarhus2017.aarhusevents.model.EventData;
import dk.aarhus2017.aarhusevents.model.EventDataBinding;
import dk.aarhus2017.aarhusevents.model.EventModel;
import dk.aarhus2017.aarhusevents.model.GeoLocationModel;
import dk.aarhus2017.aarhusevents.model.ScheduleDateModel;
import dk.aarhus2017.aarhusevents.model.ScheduledEventModel;

import static android.text.TextUtils.getOffsetBefore;
import static android.text.TextUtils.getReverse;
import static android.text.TextUtils.join;

/**
 * Created by Dell on 12/1/2016.
 */

public class ConvertHelper {

//
//    public static ArrayList<EventDataBinding> ConvertToEventData(ArrayList<EventModel> events, Date fromDate, Date endDate) {
//        ArrayList<EventDataBinding> values = new ArrayList<EventDataBinding>();
//        ArrayList<Date> collectionDates = new ArrayList<>();
//        Calendar fCalendarDate = getDate(fromDate); //GeneralHelper.GetFromDate(fromDate);
//        try {
//            Calendar tCalendarDate = getDate(endDate); //GeneralHelper.GetToDate(endDate);
//            collectionDates = getAllDays(fCalendarDate, tCalendarDate);
//
//            for (Date date : collectionDates) {
//                ArrayList<EventData> eventDataItem = new ArrayList<>();
//                long cDate = date.getTime();
//                for (EventModel eventModel : events) {
//                    for (ScheduledEventModel scheduledEventModel : eventModel.getScheduledEvents()) {
//                        long fDate = scheduledEventModel.getFullStartDate().getTime();
//                        long tDate = scheduledEventModel.getFullEndDate().getTime();
//
//                        if (cDate >= fDate && cDate <= tDate) {
//                            EventData eventItem = new EventData(eventModel.getEventId(), eventModel.getEventHeadline(), eventModel.getImageUrl(), eventModel.getEventUrl(), eventModel.getLanguageDisabled(),
//                                    eventModel.getDescriptionHeadline(), eventModel.getDescription(), scheduledEventModel.getTime(), scheduledEventModel.getLocationModel(), scheduledEventModel.getTicketUrl(), eventModel.getVideoUrl(), eventModel.getPracticalInfo());
//
////                            eventItem.setFromDate(scheduledEventModel.getStartDateTimeFormat());
////                            eventItem.setToDate(scheduledEventModel.getEndDateTimeFormat());
//                            eventItem.setDescriptionHtml(eventModel.getDescriptionHtml());
//                            eventItem.setAudienceList(eventModel.getAudienceList());
//                            eventItem.setArtFormList(eventModel.getArtFormList());
//
//                            eventItem.setStartDateSimple(scheduledEventModel.getStartDateTime());
//                            eventItem.setEndDateSimple(scheduledEventModel.getEndDateTime());
//                            eventItem.setScheduledEventID(scheduledEventModel.getId());
//                            eventItem.setDisabilityAccess(scheduledEventModel.getDisablilityAccess());
//                            eventItem.setExternallyCreated(eventModel.getExternallyCreated());
//
//
//                            eventDataItem.add(eventItem);
//                        }
//                    }
//                }
//
//
//                if (eventDataItem.size() > 0) {
//                    values.add(new EventDataBinding(true, date, new EventData()));
//                    for (EventData event : eventDataItem) {
//                        values.add(new EventDataBinding(false, date, event));
//                    }
//                }
//            }
//
//
//        } catch (Exception ex) {
//            Log.e("Convert Error", ex.getMessage());
//        }
//
//        return values;
//
//    }






    public static Calendar getDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight
        calendar.set(Calendar.MINUTE, 0);                 // set minute in hour
        calendar.set(Calendar.SECOND, 0);                 // set second in minute
        calendar.set(Calendar.MILLISECOND, 0);            // set millis in second
        return calendar;
    }


    private static ArrayList<Date> getAllDays(Calendar fromDate, Calendar toDate) {
        ArrayList<Date> collectionDates = new ArrayList<>();
        if (!GlobalConfig.FormatDate.format(fromDate.getTime()).equals(GlobalConfig.FormatDate.format(toDate.getTime()))) {
            collectionDates.add(fromDate.getTime());
        }
        try {
            while (fromDate.before(toDate)) {
                fromDate.add(Calendar.DAY_OF_MONTH, 1);
                collectionDates.add(fromDate.getTime());
            }

        } catch (Exception ex) {
            Log.e("Date Error", ex.getMessage());
        }

        return collectionDates;
    }

    public static String ConvertToString(Set<String> values) {
        String result = "";
        if (values.size() > 0) {
            result = join(", ", values);
        }
        return result;
    }


    public static String ConvertArrayListToString(ArrayList<String> values) {
        String result = "";
        if (values.size() > 0) {
            result = join(", ", values);
        }
        return result;
    }

    public static ArrayList<String> ConvertStringToArrayList(String values) {
        ArrayList<String> items = new ArrayList<>();

        if (!values.isEmpty()) {
            if (values.contains(",")) {
                String[] spliteValue = values.split(",");
                if (spliteValue.length > 0) {
                    for (String item : spliteValue) {
                        items.add(item);
                    }
                }
            } else {
                items.add(values);
            }
        }

        return items;

    }

    public static ArrayList<String> ConvertStringToArrayListVideo(String values) {
        ArrayList<String> items = new ArrayList<>();

        if (!values.isEmpty()) {
            if (values.contains(" ")) {
                String[] spliteValue = values.split(" ");
                if (spliteValue.length > 0) {
                    for (String item : spliteValue) {
                        items.add(item);
                    }
                }
            } else {
                items.add(values);
            }
        }

        return items;

    }


    public static Set<String> ConvertArrayListToSet(ArrayList<String> values) {
        Set<String> results = new HashSet<>();
        for (String str :
                values) {
            results.add(str);
        }

        return results;
    }

    public static ArrayList<String> ConvertSetToArrayList(Set<String> values) {
        ArrayList<String> results = new ArrayList<>();
        for (String str : values) {
            results.add(str);
        }

        return results;
    }




    public static GeoLocationModel ConvertToGeoLocation(String coordinate) {
        double latitude = 0;
        double longitude = 0;
        int zoom = 0;

        if (!coordinate.isEmpty() && coordinate.contains(",")) {
            String[] values = coordinate.split(",");
            if (values.length > 0) {
                latitude = Double.parseDouble(values[0]);
                longitude = Double.parseDouble(values[1]);
                zoom = Integer.parseInt(values[2]);
            }
        }

        return new GeoLocationModel(latitude, longitude, zoom);
    }

    public static int ConvertDpToPx(Context context, int dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        int pixels = (int) (dp * scale + 0.5f);
        return pixels;
    }

//    // Convert utc datetime to local time
//    public static long ConvertUtcToLocalTimeTimeSpan(String _utcDate){
//        long time =0;
//        try{
//            Date date =GlobalConfig.GetApiDateUtcFormat().parse(_utcDate);
//
//            time = date.getTime()+ TimeZone.getDefault().getRawOffset();
//        }
//        catch(Exception ex) {}
//        return time;
//    }
//
//    public static Date ConvertUtcToLocalTime(String _utcDate){
//        Date date =new Date();
//        try{
//            date =GlobalConfig.GetApiDateUtcFormat().parse(_utcDate);
//
//            date.setTime(date.getTime()+ TimeZone.getDefault().getRawOffset());
//        }
//        catch(Exception ex) {}
//        return date;
//    }

    public static long ConvertDenmarkToLocalTime(long timeSpan){
        TimeZone denmark = TimeZone.getTimeZone("GMT+1");
        int offsetDenmark= denmark.getRawOffset();

        long utc = timeSpan - offsetDenmark;

       long localTime = utc + TimeZone.getDefault().getRawOffset();

        return localTime;

    }
}
