package dk.aarhus2017.aarhusevents.util;

import android.icu.text.TimeZoneNames;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import dk.aarhus2017.aarhusevents.model.EventData;

/**
 * Created by Dell on 11/28/2016.
 */

public class GlobalConfig {

    public static String EventListUrl = "http://www.aarhus2017.dk/umbraco/api/EventApi/BuildListEvent";
    public static String DeletedEventApiUrl = "http://www.aarhus2017.dk/umbraco/api/eventapi/deletedlist?year=%s&month=%s&day=%s";


    public static String EnWebsiteUrl = "http://www.aarhus2017.dk/en/";
    public static String DaWebsiteUrl = "http://www.aarhus2017.dk/da/";
    public static String DeWebsiteUrl = "http://www.aarhus2017.dk/de/";


    public static String PreferenceNameFilter = "SharedPreferenceFilter";


    public static String ArtformFilterKey = "ArtformFilter";

    public static String AudienceFilterKey = "AudienceFilter";

    public static String FromDateFilterKey = "FromDateFilter";
    public static String ToDateFilterKey = "ToDateFilter";

    public static String DistanceFilterKey = "DistanceFilter";

    public static String LatitudeFilterKey = "LatitudeFilter";

    public static String LongitudeFilterKey = "LongitudeFilter";

    public static String YearKey = "YearKey";
    public static String MonthKey = "MonthKey";
    public static String DayKey = "DayKey";

    public static String CurrentLocationFilterKey = "CurrentLocationFilter";

    public static String IsSynchronizedEventKey = "IsSynchronizedEventKey";

    public static String IsCurrentLocationModeFilterKey = "IsCurrentLocationModeFilterKey";

    public static String CityFilterKey = "CityFilter";

    public static String CalendarPromptSettingKey = "CalendarPromptSettingKey";

    public static String GeoLocationPromptSettingKey = "GeoLocationPromptSettingKey";

    public static String GALanguageKey = "GALanguageKey";

    public static SimpleDateFormat FormatDate = new SimpleDateFormat("yyyy-MM-dd");

    public static SimpleDateFormat FormatDateEventGroup = new SimpleDateFormat("EEEE dd. MMMM yyyy");

    public static SimpleDateFormat DateFullFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    public static SimpleDateFormat OnlyTimeFormat = new SimpleDateFormat("kk:mm");

    public static Map<String, ArrayList<EventData>> SharedEvents;

    public static Date GetDefaultMaxDate() {
        return GeneralHelper.GetShortDate("2017-12-31");
    }

    public static Date GetDefaultMinDate() {
        return GeneralHelper.GetShortDate("2017-01-01");
    }

    //    public static SimpleDateFormat GetApiDateFormat(){
//        SimpleDateFormat utc =new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//        //utc.setTimeZone(TimeZone.getTimeZone("UTC"));
//        return utc;
//    }
//
    public static SimpleDateFormat GetApiDateUtcFormat() {
        SimpleDateFormat utc = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        utc.setTimeZone(TimeZone.getTimeZone("UTC"));
        return utc;
    }

    public static String ServiceAction = "ServiceAction";

    public static class ServiceType {
        final public static String DeletedEventSynchronization = "DeletedEventSynchronization";
        final public static String InitGlobalData = "InitGlobalData";
        final public static String UpdatedOrDeletedEventSynchronization = "UpdatedOrDeletedEventSynchronization";
    }

    public enum MainActivityStatus {
        Welcome,
        EventList,
        My2017
    }


}
