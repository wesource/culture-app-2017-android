package dk.aarhus2017.aarhusevents.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

import dk.aarhus2017.aarhusevents.fragment.EventFragment;
import dk.aarhus2017.aarhusevents.fragment.InfoFragment;
import dk.aarhus2017.aarhusevents.fragment.My2017Fragment;
import dk.aarhus2017.aarhusevents.fragment.SearchFragment;
import dk.aarhus2017.aarhusevents.model.GeoLocationModel;

/**
 * Created by Dell on 3/5/2017.
 */

public class MainViewPagerAdapter extends FragmentPagerAdapter {

    ArrayList<Fragment> pages=new ArrayList<>();

    public  MainViewPagerAdapter(FragmentManager _manager, ArrayList<Fragment> _pages)
    {
        super(_manager);
        this.pages =_pages;
    }

    @Override
    public Fragment getItem(int position) {
       return pages.get(position);
    }


    @Override
    public int getCount() {
        return pages.size();
    }


}
