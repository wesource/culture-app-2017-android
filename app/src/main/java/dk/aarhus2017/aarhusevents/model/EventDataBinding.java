package dk.aarhus2017.aarhusevents.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Dell on 12/2/2016.
 */

public class EventDataBinding {
    private boolean isGroupHeader;
    private Date date;
    private EventData eventData;

    private boolean isHoliday;

    public EventDataBinding(boolean _isGroupHeader, Date _date ,EventData _eventData, Boolean _isHoliday)
    {
        this.isGroupHeader = _isGroupHeader;
        this.date =_date;
        this.eventData = _eventData;
        this.isHoliday = _isHoliday;
    }

    public EventDataBinding()
    {}

    public boolean isGroupHeader(){return  this.isGroupHeader;}

    public  Date getDate(){return this.date;}

    public EventData getEventData(){return  this.eventData;}

    public boolean getIsHoliday(){return this.isHoliday;}
}
