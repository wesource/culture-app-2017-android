package dk.aarhus2017.aarhusevents.model;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

import dk.aarhus2017.aarhusevents.util.GlobalConfig;

/**
 * Created by Dell on 11/29/2016.
 */

public class ScheduledEventModel {
    private int id;
    private String startDateTime;
    private String endDateTime;
    private String time;
    private String venue;
    private  boolean disabilityAccess;
    private int fromPrice;
    private int toPrice;
    private LocationModel locationModel;
    private boolean soldOut;
    private String ticketUrl;


    private Date startDateTimeFormat;
    private Date endDateTimeFormat;

    private Date fullStartDate;
    private Date fullEndDate;

    public ScheduledEventModel(int _id, String _startDateTime, String _endDateTime, String _time, String _venue, boolean _disabilityAccess, int _fromPrice, int _toPrice,
                               LocationModel _locationModel, boolean _soldOut, String _ticketUrl)
    {
        this.id = _id;
        this.startDateTime = _startDateTime;
        this.endDateTime =_endDateTime;
        this.time = _time;
        this.venue =_venue;
        this.disabilityAccess=_disabilityAccess;
        this.fromPrice =_fromPrice;
        this.toPrice = _toPrice;
        this.locationModel=_locationModel;
        this.soldOut=_soldOut;
        this.ticketUrl =_ticketUrl;
    }

    public ScheduledEventModel()
    {}

    public int getId(){return this.id;}
    public String getStartDateTime(){return  this.startDateTime;}

    public String getEndDateTime(){return this.endDateTime;}

    public String getTime(){return  this.time;}

    public String getVenue(){return this.venue;}

    public boolean getDisablilityAccess(){return  this.disabilityAccess;}

    public int getFromPrice(){return this.fromPrice;}

    public int getToPrice(){return this.toPrice;}

    public LocationModel getLocationModel(){return  this.locationModel;}

    public boolean getSoldOut(){return this.soldOut;}

    public String getTicketUrl(){return this.ticketUrl;}

    public void setStartDateTimeFormat(Date _date)
    {
        this.startDateTimeFormat = _date;

    }
    public void setEndDateTimeFormat(Date _date)
    {
        this.endDateTimeFormat=_date;
    }

    public Date getStartDateTimeFormat(){
       return this.startDateTimeFormat;
    }

    public void setFullStartDate(Date _fullStartDate){
        this.fullStartDate =_fullStartDate;
    }
    public void setFullEndDate(Date _fullEndDate){
        this.fullEndDate =_fullEndDate;
    }

    public Date getEndDateTimeFormat(){
        return this.endDateTimeFormat;
    }

    public Date getFullStartDate(){return this.fullStartDate;}

    public Date getFullEndDate(){return this.fullEndDate;}



}
