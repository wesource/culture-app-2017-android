package dk.aarhus2017.aarhusevents.model;

import java.util.Date;

/**
 * Created by smart on 1/17/2017.
 */

public class ScheduleDateModel {

    private Date date;
    private long timeSpan;
    private long endTimeSpan;
    private int weekdayId;

    public ScheduleDateModel(Date _date, long _timeSpan, long _endTimeSpan, int _weekdayId)
    {
        this.date  = _date;
        this.timeSpan = _timeSpan;
        this.endTimeSpan = _endTimeSpan;
        this.weekdayId =_weekdayId;
    }

    public void setTimeSpan(long _timeSpan){
        this.timeSpan = _timeSpan;
    }
    public void setEndTimeSpan(long _endTimeSpan){
        this.endTimeSpan = _endTimeSpan;
    }

    public Date getDate(){return this.date;}

    public long getTimeSpan(){return this.timeSpan;}

    public long getEndTimeSpan(){return this.endTimeSpan;}

    public int getWeekdayId(){return this.weekdayId;}
}
