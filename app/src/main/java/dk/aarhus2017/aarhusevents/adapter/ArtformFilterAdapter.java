package dk.aarhus2017.aarhusevents.adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.model.ArtformFilterModel;
import dk.aarhus2017.aarhusevents.model.EventDataBinding;
import dk.aarhus2017.aarhusevents.util.FontHelper;

/**
 * Created by Dell on 12/16/2016.
 */

public class ArtformFilterAdapter extends ArrayAdapter<ArtformFilterModel> {

    Context contextAdapter;
    ArrayList<ArtformFilterModel> artformItems=new ArrayList<>();
    protected ArtformFilterListener artformFilterListener;

    public ArtformFilterAdapter(Context context, ArrayList<ArtformFilterModel> artformFilterModels)
    {
        super(context,-1,artformFilterModels);
        this.artformItems = artformFilterModels;
        this.contextAdapter=context;
    }

    @Override
    public int getCount() {
        return artformItems.size();
    }

    @Override
    public void add(ArtformFilterModel object) {
        artformItems.add(object);
        super.add(object);
    }

    public void updateData(ArrayList<ArtformFilterModel> values)
    {
        this.artformItems = values;
        notifyDataSetChanged();
    }

    @Nullable
    @Override
    public ArtformFilterModel getItem(int position) {
        return artformItems.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.filter_artform_item, parent, false);
        final ArtformFilterModel dataRow = getItem(position);
        final Typeface sourceSansProRegular = FontHelper.SourceSansProRegular(contextAdapter);
        final Typeface sourceSansProBold = FontHelper.SourceSansProBold(contextAdapter);

        final ImageView icon = (ImageView)row.findViewById(R.id.artformFilterIconImageView);
        final TextView name =(TextView)row.findViewById(R.id.artformFilterNameTextView);
        final ImageView selected = (ImageView)row.findViewById(R.id.artformFilterSelectedImageView);

        icon.setImageResource(dataRow.getIcon());
        name.setText(dataRow.getName());

        if(dataRow.getSelected())
        {
            selected.setVisibility(View.VISIBLE);
            icon.setColorFilter(R.color.filter_active);
            name.setTypeface(sourceSansProBold);
        }
        else
        {
            selected.setVisibility(View.INVISIBLE);
            name.setTypeface(sourceSansProRegular);
        }


        row.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(dataRow.getSelected())
                {
                    icon.setColorFilter(R.color.filter_deactive);
                    name.setTypeface(sourceSansProRegular);
                    selected.setVisibility(View.INVISIBLE);
                    dataRow.setSelected(false);
                }
                else
                {
                    icon.setColorFilter(R.color.filter_active);
                    name.setTypeface(sourceSansProBold);
                    selected.setVisibility(View.VISIBLE);
                    dataRow.setSelected(true);
                }
                notifyDataSetChanged();
                if(artformFilterListener!=null){
                    artformFilterListener.onStatusChanged(dataRow);
                }
            }
        });

        return row;
    }

   public void setArtformFilterListener(ArtformFilterListener _listener){
       this.artformFilterListener = _listener;
   }

    public interface ArtformFilterListener {
        void onStatusChanged(ArtformFilterModel rowData);

    }
}
