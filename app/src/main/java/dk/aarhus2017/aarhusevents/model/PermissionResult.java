package dk.aarhus2017.aarhusevents.model;

/**
 * Created by Dell on 12/15/2016.
 */

public enum PermissionResult {
    Granted,
    Denied,
    Requested
}
