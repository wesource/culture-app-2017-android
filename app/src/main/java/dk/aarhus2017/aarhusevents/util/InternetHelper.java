package dk.aarhus2017.aarhusevents.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Dell on 12/23/2016.
 */

public class InternetHelper {
    public static Boolean HasInternetConnection(Context context){
        boolean result=false;
        try {
            ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();
            result = networkInfo !=null && networkInfo.isConnectedOrConnecting();

        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        }
        return result;

    }

}
