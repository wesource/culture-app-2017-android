package dk.aarhus2017.aarhusevents.model;

/**
 * Created by Dell on 1/16/2017.
 */

public enum  My2017EventType {
    SharedData,
    RawLocal,
    RawApi,
    LoadMoreData
}
