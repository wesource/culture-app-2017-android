package dk.aarhus2017.aarhusevents.util;

/**
 * Created by Dell on 12/4/2016.
 */

public class GlobalConstant {
    final public static int RequestLocationPermissionResultCode = 1;
    final public static int RequestCalendarPermissionResultCode = 2;
    final public static int RequestCalendarPermissionSynchronizationResultCode = 3;

    final public static int StartForEventDetailResult = 1;
    final public static int StartActivityForFilterResult = 2;
    final public static int StartActivityForArtformFilterResult = 3;
    final public static int StartActivityForAudienceFilterResult = 4;
    final public static int StartActivityForDaterangeFilterResult = 5;
    final public static int StartActivityForLocationFilterResult = 6;





}
