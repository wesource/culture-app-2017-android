package dk.aarhus2017.aarhusevents.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.facebook.FacebookActivity;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.RunnableFuture;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.model.PermissionResult;
import dk.aarhus2017.aarhusevents.model.PromptPermissionResult;
import dk.aarhus2017.aarhusevents.util.ConvertHelper;
import dk.aarhus2017.aarhusevents.util.FBTrackerHelper;
import dk.aarhus2017.aarhusevents.util.FontHelper;
import dk.aarhus2017.aarhusevents.util.GATrackerHelper;
import dk.aarhus2017.aarhusevents.util.GeneralHelper;
import dk.aarhus2017.aarhusevents.util.GlobalConstant;
import dk.aarhus2017.aarhusevents.util.HttpHelper;
import dk.aarhus2017.aarhusevents.util.LocationHelper;
import dk.aarhus2017.aarhusevents.util.PermissionHelper;
import dk.aarhus2017.aarhusevents.util.SharedPreferenceHelper;

public class EventDetailActivity extends AppCompatActivity implements View.OnClickListener {

    int eventId;
    String ticketUrl;
    String imageUrl = "";
    String eventHeadline;
    String descriptionHeadline;
    String description;
    String descriptionHtml;
    double distance;
    long timeSpan;
    long endTimeSpan;
    String time;
    String city = "";
    String locationName;
    String address;
    Date currentGroupDate;
    Date fromDate;
    Date toDate;
    String videoUrl;
    ArrayList<String> videoUrls;

    String artFormList;
    String audienceList;
    Boolean disability;
    String eventUrl;
    String eventVenueUrl;
    String contactName;
    String contactPhone;
    String contactEmail;
    Boolean soldOut;
    int priceFrom;
    int priceTo;
    Double latitude;
    Double longitude;
    int zoom;
    boolean isFavorite;
    int scheduledEventID;
    Boolean isMy2017Event = false;
    int tabPosition=0;
    boolean isHoliday=false;


    // Container
    LinearLayout holderLinearLayout;

    // Header
    LinearLayout eventDetailHeaderBackHolderLinearLayout;
    ImageView eventDetailHeaderBackImageView;
    LinearLayout eventDetailHeaderBuyTicketHolderLinearLayout;
    ImageButton eventDetailBuyTicket;
    TextView eventDetailHeaderBuyTicketTextView;
    LinearLayout eventDetailHeaderWayFinderHolderLinearLayout;
    ImageButton eventDetailWayFinderImageButton;
    TextView eventDetailHeaderWayFinderTextView;
    LinearLayout eventDetailHeaderSaveHolderLinearLayout;
    ImageButton eventDetailSaveImageButton;
    TextView eventDetailHeaderSaveTextView;

    // Content
    ProgressBar eventDetailImageLoadingProgressBar;
    ImageView eventDetailThumb;
    TextView eventDetailEventHeadline;
    LinearLayout eventDetailBelowEventHeadlineLinearLayout;
    TextView eventDetailTime;
    LinearLayout eventDetailDistanceHolderLinearLayout;
    TextView eventDetailDistance;
    LinearLayout eventDetailDescriptionHolderLinearLayout;
    TextView eventDetailDescription;
    TextView eventDetailLocationNameTextView;
    TextView eventDetailAddressTextView;
    TextView eventDetailDate;
    LinearLayout eventDetailMoreHolderLinearLayout;
    Button eventDetailMoreDetail;
    Button eventDetailLessButton;

    LinearLayout eventDetailVideoHolderLinearLayout;

    LinearLayout eventDetailLocationNameHolderLinearLayout;
    ImageView eventDetailContentWayFinderImageView;
    LinearLayout eventDetailArtformHolderLinearLayout;
    TextView eventDetailArtFormListTextView;
    LinearLayout eventDetailAudienceHolderLinearLayout;
    TextView eventDetailAudienceListTextView;
    LinearLayout eventDetailDisabilityHolderLinearLayout;
    TextView eventDetailDisabilityLabelTextView;
    TextView eventDetailDisabilityTextView;

    LinearLayout eventDetailLinkHolderLinearLayout;
    TextView eventDetailEventUrlTextView;
    LinearLayout eventDetailContactHolderLinearLayout;
    TextView eventDetailContactNameLabelTextView;
    TextView eventDetailContactNameTextView;
    TextView eventDetailContactPhoneLabelTextView;
    TextView eventDetailContactPhoneTextView;
    TextView eventDetailContactEmailLabelTextView;
    TextView eventDetailContactEmailTextView;
    TextView eventDetailPriceValueTextView;
    TextView eventDetailPriceLabelTextView;

    //WebView eventDetailVideoView;


    //Menu footer
    TabLayout footerTabLayout;


    EventDetailActivity self = this;

    private int idx = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);

        // Global
        holderLinearLayout = (LinearLayout) findViewById(R.id.holderLinearLayout);

        // Header
        eventDetailHeaderBackHolderLinearLayout = (LinearLayout) findViewById(R.id.eventDetailHeaderBackHolderLinearLayout);
        eventDetailHeaderBackImageView = (ImageView) findViewById(R.id.eventDetailHeaderBackImageView);
        eventDetailHeaderBuyTicketHolderLinearLayout = (LinearLayout) findViewById(R.id.eventDetailHeaderBuyTicketHolderLinearLayout);
        eventDetailBuyTicket = (ImageButton) findViewById(R.id.eventDetailBuyTicketImageButton);
        eventDetailHeaderBuyTicketTextView = (TextView) findViewById(R.id.eventDetailHeaderBuyTicketTextView);
        eventDetailHeaderWayFinderHolderLinearLayout = (LinearLayout) findViewById(R.id.eventDetailHeaderWayFinderHolderLinearLayout);
        eventDetailWayFinderImageButton = (ImageButton) findViewById(R.id.eventDetailWayFinderImageButton);
        eventDetailHeaderWayFinderTextView = (TextView) findViewById(R.id.eventDetailHeaderWayFinderTextView);
        eventDetailHeaderSaveHolderLinearLayout = (LinearLayout) findViewById(R.id.eventDetailHeaderSaveHolderLinearLayout);
        eventDetailSaveImageButton = (ImageButton) findViewById(R.id.eventDetailSaveImageButton);
        eventDetailHeaderSaveTextView = (TextView) findViewById(R.id.eventDetailHeaderSaveTextView);

        // Menu footer
        footerTabLayout = (TabLayout) findViewById(R.id.footerTabLayout);

        // Menu footer
        eventDetailImageLoadingProgressBar = (ProgressBar) findViewById(R.id.eventDetailImageLoadingProgressBar);
        eventDetailThumb = (ImageView) findViewById(R.id.eventDetailThumbImageView);
        eventDetailEventHeadline = (TextView) findViewById(R.id.eventDetailEventHeadlineTextView);
        eventDetailBelowEventHeadlineLinearLayout = (LinearLayout) findViewById(R.id.eventDetailBelowEventHeadlineLinearLayout);
        eventDetailDate = (TextView) findViewById(R.id.eventDetailDateTextView);
        eventDetailTime = (TextView) findViewById(R.id.eventDetailTimeTextView);
        eventDetailDistanceHolderLinearLayout = (LinearLayout) findViewById(R.id.eventDetailDistanceHolderLinearLayout);
        eventDetailDistance = (TextView) findViewById(R.id.eventDetailDistanceTextView);
        eventDetailDescriptionHolderLinearLayout = (LinearLayout) findViewById(R.id.eventDetailDescriptionHolderLinearLayout);
        eventDetailDescription = (TextView) findViewById(R.id.eventDetailDescriptionTextView);
        eventDetailMoreHolderLinearLayout = (LinearLayout) findViewById(R.id.eventDetailMoreHolderLinearLayout);
        eventDetailMoreDetail = (Button) findViewById(R.id.eventDetailMoreDetailButton);
        eventDetailLessButton = (Button) findViewById(R.id.eventDetailLessButton);
        eventDetailLocationNameHolderLinearLayout = (LinearLayout) findViewById(R.id.eventDetailLocationNameHolderLinearLayout);
        eventDetailContentWayFinderImageView = (ImageView) findViewById(R.id.eventDetailContentWayFinderImageView);
        eventDetailLocationNameTextView = (TextView) findViewById(R.id.eventDetailLocationNameTextView);
        eventDetailAddressTextView = (TextView) findViewById(R.id.eventDetailAddressTextView);
        eventDetailArtformHolderLinearLayout = (LinearLayout) findViewById(R.id.eventDetailArtformHolderLinearLayout);
        eventDetailArtFormListTextView = (TextView) findViewById(R.id.eventDetailArtFormListTextView);
        eventDetailAudienceHolderLinearLayout = (LinearLayout) findViewById(R.id.eventDetailAudienceHolderLinearLayout);
        eventDetailAudienceListTextView = (TextView) findViewById(R.id.eventDetailAudienceListTextView);
        eventDetailDisabilityHolderLinearLayout = (LinearLayout) findViewById(R.id.eventDetailDisabilityHolderLinearLayout);
        eventDetailDisabilityLabelTextView = (TextView) findViewById(R.id.eventDetailDisabilityLabelTextView);
        eventDetailDisabilityTextView = (TextView) findViewById(R.id.eventDetailDisabilityTextView);
        eventDetailLinkHolderLinearLayout = (LinearLayout) findViewById(R.id.eventDetailLinkHolderLinearLayout);
        eventDetailEventUrlTextView = (TextView) findViewById(R.id.eventDetailEventUrlTextView);
        eventDetailContactHolderLinearLayout = (LinearLayout) findViewById(R.id.eventDetailContactHolderLinearLayout);
        eventDetailContactNameLabelTextView = (TextView) findViewById(R.id.eventDetailContactNameLabelTextView);
        eventDetailContactNameTextView = (TextView) findViewById(R.id.eventDetailContactNameTextView);
        eventDetailContactPhoneLabelTextView = (TextView) findViewById(R.id.eventDetailContactPhoneTextView);
        eventDetailContactPhoneTextView = (TextView) findViewById(R.id.eventDetailContactPhoneTextView);
        eventDetailContactEmailLabelTextView = (TextView) findViewById(R.id.eventDetailContactEmailLabelTextView);
        eventDetailContactEmailTextView = (TextView) findViewById(R.id.eventDetailContactEmailTextView);
        eventDetailVideoHolderLinearLayout = (LinearLayout) findViewById(R.id.eventDetailVideoHolderLinearLayout);

        eventDetailPriceLabelTextView = (TextView) findViewById(R.id.eventDetailPriceLabelTextView);
        eventDetailPriceValueTextView = (TextView) findViewById(R.id.eventDetailPriceValueTextView);


        // Register event for header
        eventDetailHeaderBackHolderLinearLayout.setOnClickListener(this);
        eventDetailHeaderBackImageView.setOnClickListener(this);
        eventDetailHeaderBuyTicketHolderLinearLayout.setOnClickListener(this);
        eventDetailBuyTicket.setOnClickListener(this);
        eventDetailHeaderWayFinderHolderLinearLayout.setOnClickListener(this);
        eventDetailWayFinderImageButton.setOnClickListener(this);
        eventDetailHeaderSaveHolderLinearLayout.setOnClickListener(this);
        eventDetailSaveImageButton.setOnClickListener(this);

        // Register events for content
        eventDetailMoreDetail.setOnClickListener(this);
        eventDetailLessButton.setOnClickListener(this);
        eventDetailContentWayFinderImageView.setOnClickListener(this);
        eventDetailAddressTextView.setOnClickListener(this);
        eventDetailContactPhoneTextView.setOnClickListener(this);
        eventDetailEventUrlTextView.setOnClickListener(this);
        eventDetailContactEmailTextView.setOnClickListener(this);


        // Register events for footer



        // Get data
        Intent intent = getIntent();
        eventId = intent.getIntExtra("EventID", 0);
        ticketUrl = intent.getStringExtra("TicketUrl");
        imageUrl = intent.getStringExtra("ImageUrl");
        eventHeadline = intent.getStringExtra("EventHeadline");
        timeSpan = intent.getLongExtra("TimeSpan", 0);
        endTimeSpan =  intent.getLongExtra("EndTimeSpan", 0);
        time = intent.getStringExtra("Time");
        city = intent.getStringExtra("City");
        distance = intent.getDoubleExtra("Distance", 0);
        descriptionHeadline = intent.getStringExtra("DescriptionHeadline");
        description = intent.getStringExtra("Description");
        descriptionHtml = intent.getStringExtra("DescriptionHtml");
        locationName = intent.getStringExtra("LocationName");
        address = intent.getStringExtra("Address");
        currentGroupDate = (Date) intent.getSerializableExtra("CurrentGroupDate");
        fromDate = (Date) intent.getSerializableExtra("FromDate");
        toDate = (Date) intent.getSerializableExtra("ToDate");

        videoUrl = intent.getStringExtra("VideoUrl");
        videoUrls = intent.getStringArrayListExtra("VideoUrls");
        artFormList = intent.getStringExtra("ArtFormList");
        audienceList = intent.getStringExtra("AudienceList");
        disability = intent.getBooleanExtra("Disability", false);
        eventUrl = intent.getStringExtra("EventUrl");
        eventVenueUrl = intent.getStringExtra("EventVenueUrl");
        contactName = intent.getStringExtra("ContactName");
        contactPhone = intent.getStringExtra("ContactPhone");
        contactEmail = intent.getStringExtra("ContactEmail");
        soldOut = intent.getBooleanExtra("SoldOut", false);
        priceFrom = intent.getIntExtra("PriceFrom", 0);
        priceTo = intent.getIntExtra("PriceTo", 0);

        latitude = intent.getDoubleExtra("Latitude", 0);
        longitude = intent.getDoubleExtra("Longitude", 0);
        zoom = intent.getIntExtra("Zoom", 0);
        isFavorite = intent.getBooleanExtra("IsFavoriteEvent", false);
        scheduledEventID = intent.getIntExtra("ScheduledEventID", 0);
       // isMy2017Event = intent.getBooleanExtra("IsMy2017Event", false);
        tabPosition = intent.getIntExtra("TabPosition",0);
        isHoliday=intent.getBooleanExtra("IsHoliday",false);




        // Set values
        if (isFavorite) {
            eventDetailSaveImageButton.setImageResource(R.drawable.save);
            eventDetailSaveImageButton.setColorFilter(ContextCompat.getColor(self, R.color.transparent));
            eventDetailHeaderSaveTextView.setText(getString(R.string.event_detail_header_remote));
        }
        if (ticketUrl.isEmpty()) {
            eventDetailHeaderBuyTicketHolderLinearLayout.setVisibility(View.GONE);
        }
        eventDetailEventHeadline.setText(eventHeadline);
        String date = new SimpleDateFormat("d. MMMM yyyy").format(currentGroupDate);

        eventDetailDate.setText(date.toLowerCase());
        try {
            //Picasso.with(this).load(imageUrl).into(eventDetailThumb);
            eventDetailThumb.post(new Runnable() {
                @Override
                public void run() {
                    //imageUrl = imageUrl+"?width="+eventDetailThumb.getWidth()+"&height="+ ConvertHelper.ConvertDpToPx(self,182);//+"&mode=crop";
                    imageUrl = imageUrl + "?width=774";
                    Glide.with(self).load(imageUrl).crossFade().listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            eventDetailImageLoadingProgressBar.setVisibility(View.GONE);
                            eventDetailThumb.setVisibility(View.VISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            eventDetailImageLoadingProgressBar.setVisibility(View.GONE);
                            eventDetailThumb.setVisibility(View.VISIBLE);
                            return false;
                        }
                    })
                            .priority(Priority.IMMEDIATE).diskCacheStrategy(DiskCacheStrategy.RESULT).into(eventDetailThumb);
                }
            });


        } catch (Exception ex) {
            Log.e("Load Image Exception", ex.getMessage());
        }


        eventDetailTime.setText(time);

        Location location = LocationHelper.getLocation(self);
        if (location != null) {
            eventDetailDistanceHolderLinearLayout.setVisibility(View.VISIBLE);
            Location dest = new Location("");
            dest.setLatitude(latitude);
            dest.setLongitude(longitude);
            float dis = LocationHelper.getDistance(location, dest);
            eventDetailDistance.setText(String.format("%.0f km", dis));
        } else {
            // eventDetailDistanceHolderLinearLayout.setVisibility(View.GONE);
            if (!city.isEmpty()) {
                eventDetailDistance.setText(city);
                eventDetailDistanceHolderLinearLayout.setVisibility(View.VISIBLE);
            } else {
                eventDetailDistanceHolderLinearLayout.setVisibility(View.GONE);
            }
        }


        Spanned html;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            html = Html.fromHtml(descriptionHtml, Html.FROM_HTML_MODE_LEGACY);
        } else {
            html = Html.fromHtml(descriptionHtml);
        }

        eventDetailDescription.setText(html);
        eventDetailDescription.setMovementMethod(LinkMovementMethod.getInstance());

        eventDetailLocationNameTextView.setText(locationName);
        eventDetailAddressTextView.setText(address);

        eventDetailArtFormListTextView.setText(artFormList);
        eventDetailAudienceListTextView.setText(audienceList);
        if (disability) {
            eventDetailDisabilityTextView.setText(getString(R.string.event_detail_disability_yes));
        } else {
            eventDetailDisabilityTextView.setText(getString(R.string.event_detail_disability_no));
        }
        //eventDetailEventUrlTextView.setText(eventUrl);
        eventDetailContactNameTextView.setText(contactName);
        eventDetailContactPhoneTextView.setText(contactPhone);
        eventDetailContactEmailTextView.setText(contactEmail);


        if (soldOut) {
            eventDetailPriceValueTextView.setText(getString(R.string.event_detail_soldout));
        } else {
            if (priceFrom == 0 && priceTo == 0) {
                eventDetailPriceValueTextView.setText(getString(R.string.event_detail_price_free));
            } else {
                if (priceFrom == 0) {
                    eventDetailPriceValueTextView.setText(priceTo + " DDK");
                } else {
                    if(priceFrom==priceTo){
                        eventDetailPriceValueTextView.setText(priceTo + " DDK");
                    }else {
                        if(priceTo>priceFrom) {
                            eventDetailPriceValueTextView.setText(priceFrom + " - " + priceTo + " DDK");
                        }else{
                            eventDetailPriceValueTextView.setText(priceFrom + " DDK");
                        }
                    }
                }
            }
        }

        if(!eventVenueUrl.isEmpty()){
            eventDetailLinkHolderLinearLayout.setVisibility(View.VISIBLE);
        }else{
            eventDetailLinkHolderLinearLayout.setVisibility(View.GONE);
        }



        setFonts();
        setAnimation();



        if (videoUrls.size() > 0) {
            for (String url : videoUrls) {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ConvertHelper.ConvertDpToPx(self, 200));
                WebView webView = new WebView(self);
                webView.getSettings().setJavaScriptEnabled(true);
                webView.getSettings().setAppCacheEnabled(true);
                webView.getSettings().setSaveFormData(true);
                webView.loadUrl(url);

                int margin = ConvertHelper.ConvertDpToPx(self, 5);
                layoutParams.setMargins(0, margin, 0, margin);
                webView.setLayoutParams(layoutParams);

                eventDetailVideoHolderLinearLayout.addView(webView);
            }
        }



        footerTabLayout.getTabAt(tabPosition).select();
        footerTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabPosition = tab.getPosition();
                GoBack(tabPosition);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                tabPosition = tab.getPosition();
                GoBack(tabPosition);
            }
        });
    }

    private void setAnimation() {
        final ArrayList<View> controls = new ArrayList<>();
        controls.add(eventDetailThumb);
        controls.add(eventDetailImageLoadingProgressBar);
        controls.add(eventDetailEventHeadline);
        controls.add(eventDetailBelowEventHeadlineLinearLayout);
        controls.add(eventDetailDescriptionHolderLinearLayout);
        controls.add(eventDetailMoreHolderLinearLayout);
        controls.add(eventDetailLocationNameHolderLinearLayout);
        controls.add(eventDetailArtformHolderLinearLayout);
        controls.add(eventDetailAudienceHolderLinearLayout);
        controls.add(eventDetailDisabilityHolderLinearLayout);
        controls.add(eventDetailLinkHolderLinearLayout);
        controls.add(eventDetailContactHolderLinearLayout);

        for (int i = 0; i < controls.size(); i++) {
            View item = controls.get(i);
            Animation animation = AnimationUtils.loadAnimation(self, R.anim.fade_in);
            animation.setStartOffset(i * 150);
            item.startAnimation(animation);
        }

//
//        animationFadeIn.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                if(idx==0)
//                {
//                    eventDetailThumb.clearAnimation();
//                }
//                if(idx>0)
//                {
//                    View item = controls.get(idx-1);
//                    item.clearAnimation();
//                }
//
//                if(idx < controls.size()) {
//                    View item = controls.get(idx);
//
//                    item.startAnimation(animationFadeIn);
//                    idx++;
//                }
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//
//            }
//        });
//        eventDetailThumb.startAnimation(animationFadeIn);

    }

    private void setFonts() {
        Typeface flavinBold = FontHelper.FlavinBold(self);
        Typeface sourceSansProBold = FontHelper.SourceSansProBold(self);
        Typeface sourceSansProRegular = FontHelper.SourceSansProRegular(self);

        eventDetailHeaderBuyTicketTextView.setTypeface(flavinBold);
        eventDetailHeaderWayFinderTextView.setTypeface(flavinBold);
        eventDetailHeaderSaveTextView.setTypeface(flavinBold);

        eventDetailEventHeadline.setTypeface(flavinBold);
        eventDetailDate.setTypeface(sourceSansProBold);
        eventDetailTime.setTypeface(sourceSansProBold);
        eventDetailDistance.setTypeface(sourceSansProBold);
        eventDetailDescription.setTypeface(sourceSansProRegular);
        eventDetailMoreDetail.setTypeface(sourceSansProBold);

        eventDetailLocationNameTextView.setTypeface(sourceSansProRegular);
        eventDetailAddressTextView.setTypeface(sourceSansProRegular);
        eventDetailArtFormListTextView.setTypeface(sourceSansProRegular);
        eventDetailAudienceListTextView.setTypeface(sourceSansProRegular);
        eventDetailDisabilityLabelTextView.setTypeface(sourceSansProBold);
        eventDetailDisabilityTextView.setTypeface(sourceSansProRegular);
        eventDetailEventUrlTextView.setTypeface(sourceSansProRegular);

        eventDetailContactNameLabelTextView.setTypeface(sourceSansProBold);
        eventDetailContactNameTextView.setTypeface(sourceSansProRegular);
        eventDetailContactPhoneLabelTextView.setTypeface(sourceSansProRegular);
        eventDetailContactPhoneTextView.setTypeface(sourceSansProRegular);
        eventDetailContactEmailLabelTextView.setTypeface(sourceSansProRegular);
        eventDetailContactEmailTextView.setTypeface(sourceSansProRegular);

        eventDetailPriceLabelTextView.setTypeface(sourceSansProBold);
        eventDetailPriceValueTextView.setTypeface(sourceSansProRegular);

        if (isMy2017Event) {

        } else {

        }


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.eventDetailHeaderBackHolderLinearLayout:
            case R.id.eventDetailHeaderBackImageView: {
                GoBack(tabPosition);
                break;
            }

            case R.id.eventDetailHeaderBuyTicketHolderLinearLayout:
            case R.id.eventDetailBuyTicketImageButton: {
                if (!ticketUrl.isEmpty()) {
                    try {
                        Intent buyTicketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ticketUrl));
                        startActivity(buyTicketIntent);

                        GATrackerHelper.Ticket(self, eventId, eventHeadline, currentGroupDate);
                    } catch (Exception ex) {

                    }
                }
                break;
            }
            case R.id.eventDetailHeaderWayFinderHolderLinearLayout:
            case R.id.eventDetailWayFinderImageButton:
            case R.id.eventDetailAddressTextView:
            case R.id.eventDetailContentWayFinderImageView: {

                Location myLocation = LocationHelper.getLocation(self);
                String mapUri;
                if (myLocation != null && latitude != 0 && longitude != 0) {
                    //  mapUri = String.format(Locale.ENGLISH, "google.navigation:q=%f,%f ( %s )", latitude, longitude, address);
                    mapUri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f?z=%d&q=%s", myLocation.getLatitude(), myLocation.getLongitude(), latitude, longitude, zoom, address);
                } else {
                    // mapUri = String.format(Locale.getDefault(), "geo:%f,%f?z=%d", latitude, longitude, zoom);
                    mapUri = String.format(Locale.ENGLISH, "geo:%f,%f?z=%d&q=%s", latitude, longitude, zoom, address);
                }
                // mapUri = String.format(Locale.getDefault(), "geo:%f,%f", latitude, longitude);

                //mapUri = String.format(Locale.getDefault(), "geo:%f,%f?z=%d&q=%s", latitude, longitude, zoom, address);
                // Uri.parse("http://maps.google.com/maps?saddr=0,0&daddr=20.5666,45.345"));
                //mapUri = String.format(Locale.getDefault(), "http://maps.google.com/maps?saddr=0,0&daddr=%f,%f?z=%d&q=%s", latitude, longitude, zoom, address);

                Intent intentMap = new Intent(Intent.ACTION_VIEW, Uri.parse(mapUri));
                intentMap.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intentMap);

                GATrackerHelper.WayFinder(self, eventId, eventHeadline, currentGroupDate);
                break;
            }
            case R.id.eventDetailHeaderSaveHolderLinearLayout:
            case R.id.eventDetailSaveImageButton: {
                if (isFavorite) {
                    ContentResolver contentResolver = getContentResolver();
                    SharedPreferenceHelper.RemoveFavoriteEvent(self, contentResolver, scheduledEventID, currentGroupDate);
                    isFavorite = false;
                } else {
                    SharedPreferenceHelper.SaveFavoriteEvent(self, scheduledEventID, currentGroupDate);
                    isFavorite = true;
                    GATrackerHelper.SaveFavorite(self, eventId, eventHeadline, currentGroupDate, false);
                }
                if (isFavorite) {
                    eventDetailSaveImageButton.setImageResource(R.drawable.save);
                    eventDetailSaveImageButton.setColorFilter(ContextCompat.getColor(self, R.color.transparent));
                    eventDetailHeaderSaveTextView.setText(getString(R.string.event_detail_header_remote));
                } else {
                    eventDetailSaveImageButton.setImageResource(R.drawable.unsave);
                    eventDetailSaveImageButton.setColorFilter(ContextCompat.getColor(self, R.color.icon));
                    eventDetailHeaderSaveTextView.setText(getString(R.string.event_detail_header_save));
                }

                PromptPermissionResult setting = SharedPreferenceHelper.GetCalendarPromptSetting(self);

                switch (setting) {
                    case Yes: {
                        PermissionResult permissionResult = PermissionHelper.CheckCalendarPermission(self);
                        switch (permissionResult) {
                            case Granted: {
                                if (isFavorite) {
                                    ContentResolver contentResolver = getContentResolver();
                                    long calendarEventID = GeneralHelper.AddEventToCalendar(contentResolver, currentGroupDate, timeSpan,endTimeSpan ,eventHeadline, description, eventUrl, locationName + ", " + address);
                                    SharedPreferenceHelper.UpdateFavoriteEvent(self, scheduledEventID, calendarEventID, currentGroupDate);
                                }
                                break;
                            }
                            case Requested: {
                                ActivityCompat.requestPermissions(self, new String[]{Manifest.permission.WRITE_CALENDAR}, GlobalConstant.RequestCalendarPermissionResultCode);
                                break;
                            }
                            case Denied:
                                break;
                            default:
                                break;
                        }
                        break;
                    }
                    case No: {
                        break;
                    }
                    case Ignore: {
                        final Dialog acceptDialog = new Dialog(self);
                        acceptDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        acceptDialog.setContentView(R.layout.prompt_calendar_dialog);
                        acceptDialog.show();
                        Typeface flavinBold = FontHelper.FlavinBold(self);
                        Typeface flavinRegular = FontHelper.FlavinRegular(self);
                        Typeface sourceSansProBold = FontHelper.SourceSansProBold(self);
                        TextView titleTextView = (TextView) acceptDialog.findViewById(R.id.promtTitleTextView);
                        TextView descriptionTextView = (TextView) acceptDialog.findViewById(R.id.promtDescriptionTextView);
                        Button acceptButton = (Button) acceptDialog.findViewById(R.id.promptAcceptButton);
                        Button noButton = (Button) acceptDialog.findViewById(R.id.promptNoButton);

                        titleTextView.setTypeface(flavinBold);
                        descriptionTextView.setTypeface(flavinRegular);
                        acceptButton.setTypeface(sourceSansProBold);
                        noButton.setTypeface(sourceSansProBold);
                        acceptButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                acceptDialog.dismiss();
                                SharedPreferenceHelper.SaveCalendarPromptSetting(self, PromptPermissionResult.Yes);
                                ActivityCompat.requestPermissions(self, new String[]{Manifest.permission.WRITE_CALENDAR}, GlobalConstant.RequestCalendarPermissionResultCode);
                                GATrackerHelper.SynCalendar(self);
                            }
                        });
                        ImageButton closeImageButton = (ImageButton) acceptDialog.findViewById(R.id.promtCloseImageButton);
                        closeImageButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                acceptDialog.dismiss();
                                SharedPreferenceHelper.SaveCalendarPromptSetting(self, PromptPermissionResult.Ignore);
                            }
                        });
                        noButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                acceptDialog.dismiss();
                                SharedPreferenceHelper.SaveCalendarPromptSetting(self, PromptPermissionResult.No);
                            }
                        });
                        break;
                    }
                    default: {
                        break;
                    }
                }


                break;
            }

            case R.id.eventDetailMoreDetailButton: {
                eventDetailMoreDetail.setVisibility(View.GONE);
                eventDetailDescription.setMaxLines(1000);
                eventDetailMoreHolderLinearLayout.setVisibility(View.GONE);

                eventDetailLessButton.setVisibility(View.VISIBLE);
                if (videoUrls.size() > 0) {
                    eventDetailVideoHolderLinearLayout.setVisibility(View.VISIBLE);
                }


                break;
            }
            case R.id.eventDetailLessButton: {
                eventDetailMoreDetail.setVisibility(View.VISIBLE);
                eventDetailDescription.setMaxLines(5);
                eventDetailLessButton.setVisibility(View.GONE);

                eventDetailVideoHolderLinearLayout.setVisibility(View.GONE);
                eventDetailMoreHolderLinearLayout.setVisibility(View.VISIBLE);


                //     eventDetailMoreHolderLinearLayout.setBackgroundResource(R.drawable.more_button_gradient);
//                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                    eventDetailMoreHolderLinearLayout.setBackground(self.getDrawable(R.drawable.more_button_gradient));
//                } else{
//                    eventDetailMoreHolderLinearLayout.setBackgroundResource(R.drawable.more_button_gradient);// .setBackground(self.getDrawable(R.drawable.more_button_gradient));
//                }

                break;
            }

            case R.id.eventDetailContactPhoneTextView: {
                if (!contactPhone.isEmpty()) {
                    String phone = "tel:" + contactPhone.trim().replace(" ", "");
                    Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.parse(phone));
                    startActivity(callIntent);
                }
                break;
            }
            case R.id.eventDetailEventUrlTextView: {
                if (eventVenueUrl!=null && !eventVenueUrl.isEmpty()) {
                    Intent eventUrlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(eventVenueUrl));
                    startActivity(eventUrlIntent);
                }
                break;
            }
            case R.id.eventDetailContactEmailTextView: {
                if (!contactEmail.isEmpty()) {
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                    emailIntent.setData(Uri.parse("mailto:" + contactEmail));
                    startActivity(emailIntent);
                }
                break;
            }
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        GoBack(tabPosition);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == GlobalConstant.RequestCalendarPermissionResultCode) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                ContentResolver contentResolver = getContentResolver();
                long calendarEventID = GeneralHelper.AddEventToCalendar(contentResolver, currentGroupDate, timeSpan, endTimeSpan,eventHeadline, description, eventUrl, locationName + ", " + address);
                SharedPreferenceHelper.UpdateFavoriteEvent(self, scheduledEventID, calendarEventID, currentGroupDate);
            }

        }
    }

    private void GoBack(int tabPosition) {
        Intent returnIntent = new Intent();

        returnIntent.putExtra("TabPosition",tabPosition);

        setResult(self.RESULT_OK, returnIntent);
        super.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Google analytics
        GATrackerHelper.SendGAScreenView(eventHeadline, self);

        // Facebook analytics
        FBTrackerHelper.LogViewedEvent(self,eventHeadline);
    }
}
