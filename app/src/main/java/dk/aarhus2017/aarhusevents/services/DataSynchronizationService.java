package dk.aarhus2017.aarhusevents.services;

import android.app.IntentService;
import android.app.Notification;
import android.content.Intent;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.ResponseHandlerInterface;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.ResponseHandler;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.protocol.HttpContext;
import dk.aarhus2017.aarhusevents.activities.EventActivity;
import dk.aarhus2017.aarhusevents.model.DeletedEventModel;
import dk.aarhus2017.aarhusevents.model.EventModel;
import dk.aarhus2017.aarhusevents.notification.DeletedEventBroadcastReceiver;
import dk.aarhus2017.aarhusevents.util.EventHelper;
import dk.aarhus2017.aarhusevents.util.GeneralHelper;
import dk.aarhus2017.aarhusevents.util.GlobalConfig;
import dk.aarhus2017.aarhusevents.util.HttpHelper;
import dk.aarhus2017.aarhusevents.util.InternetHelper;
import dk.aarhus2017.aarhusevents.util.LocalDataHelper;
import dk.aarhus2017.aarhusevents.util.ParserHelper;
import dk.aarhus2017.aarhusevents.util.SharedPreferenceHelper;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class DataSynchronizationService extends IntentService {

    private boolean updatedOrDeletedEventBroadCast=false;

    public DataSynchronizationService() {
        super("DataSynchronizationService");
    }


    @Override
    protected void onHandleIntent(final Intent intent) {
        if (intent != null) {
            final String action = intent.getStringExtra(GlobalConfig.ServiceAction);

            final Context self=getApplicationContext();


            switch (action) {
                case GlobalConfig.ServiceType.InitGlobalData:
                {
                    updatedOrDeletedEventBroadCast = false;
                    String language = GeneralHelper.getDefaultLanguage();
                    ArrayList<EventModel> models = new ArrayList<>();
                    Boolean hasLocalData=SharedPreferenceHelper.GetSynchronizeEventData(self);
                    if (hasLocalData) {
                        models = SharedPreferenceHelper.GetEvents(self, "All", language, GlobalConfig.GetDefaultMinDate(), GlobalConfig.GetDefaultMaxDate());
                    }else
                    {
                        JSONArray dataLocal = LocalDataHelper.ReadDataJson(self, language);
                        models = ParserHelper.ParseToEventObject(dataLocal);
                    }
                    //
                    Date minDate = SharedPreferenceHelper.getMinDateFavorite(self);
                    GlobalConfig.SharedEvents = EventHelper.PrepareEvents(self, models,minDate, GlobalConfig.GetDefaultMaxDate());

                    // Notify to MainActivity
                    Intent intentMain = new Intent(GlobalConfig.ServiceType.InitGlobalData);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intentMain);

                    // Add data to local storage in case it is empty
                    if(!hasLocalData)
                    {
                        Boolean result = SharedPreferenceHelper.SaveEvents(self, models);
                        if (result) {
                            SharedPreferenceHelper.SaveIsSynchronizeEventData(self, true);
                        }
                    }



                    if (InternetHelper.HasInternetConnection(self)) {
                        // Updates the data from api
                        Boolean isSynchronized = SharedPreferenceHelper.GetSynchronizeEventData(self);
                        StringEntity params = HttpHelper.GetFullDataHttpParams(null, null, language, isSynchronized);
                        SyncHttpClient syncHttpClient = new SyncHttpClient();
                        syncHttpClient.post(self, GlobalConfig.EventListUrl, params, "application/json", new JsonHttpResponseHandler() {

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                                super.onSuccess(statusCode, headers, response);
                                if (response.length() > 0) {
                                    updatedOrDeletedEventBroadCast = true;
                                    ArrayList<EventModel> models = ParserHelper.ParseToEventObject(response);
                                    boolean result = SharedPreferenceHelper.SaveEvents(self, models);
                                    if (result) {

                                    }
                                }
                            }
                        });

                        // Deleted the event from api
                        DeletedEventModel deletedEventModel = SharedPreferenceHelper.GetDeletedEventSynchronization(getApplicationContext());
                        String url = String.format(GlobalConfig.DeletedEventApiUrl, deletedEventModel.getYear(), deletedEventModel.getMonth(), deletedEventModel.getDay());
                        syncHttpClient = new SyncHttpClient();
                        syncHttpClient.get(url, new JsonHttpResponseHandler() {
                            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                                super.onSuccess(statusCode, headers, response);

                                ArrayList<Integer> deletedEvents = ParserHelper.ParseToDeletedEvents(response);
                                //deletedEvents.add(9181);
                                ArrayList<Integer> deletedResult = SharedPreferenceHelper.DeletedEventsSynchronization(getApplicationContext(), deletedEvents);

                                // Update date synchronization
                                SharedPreferenceHelper.SaveDeletedEventSynchronization(getApplicationContext());
                                // Notify to UI to reload event list
                                if (deletedResult.size() > 0) {
                                    updatedOrDeletedEventBroadCast = true;

                                }
                            }
                        });
                        // Broadcast to update the events have changed
                        if(updatedOrDeletedEventBroadCast){
                            models = SharedPreferenceHelper.GetEvents(self, "All", language, GlobalConfig.GetDefaultMinDate(), GlobalConfig.GetDefaultMaxDate());
                            GlobalConfig.SharedEvents = EventHelper.PrepareEvents(self, models,GlobalConfig.GetDefaultMinDate(), GlobalConfig.GetDefaultMaxDate());

                            Intent intentMainActivity = new Intent(GlobalConfig.ServiceType.UpdatedOrDeletedEventSynchronization);
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intentMainActivity);
                        }
                    }




                    break;
                }

                case GlobalConfig.ServiceType.DeletedEventSynchronization: {
                    DeletedEventModel deletedEventModel = SharedPreferenceHelper.GetDeletedEventSynchronization(getApplicationContext());
                    String url = String.format(GlobalConfig.DeletedEventApiUrl, deletedEventModel.getYear(), deletedEventModel.getMonth(), deletedEventModel.getDay());
                    SyncHttpClient syncHttpClient = new SyncHttpClient();
                    syncHttpClient.get(url, new JsonHttpResponseHandler() {
                        public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                            super.onSuccess(statusCode, headers, response);

                            ArrayList<Integer> deletedEvents = ParserHelper.ParseToDeletedEvents(response);
                            deletedEvents.add(9181);
                            ArrayList<Integer> deletedResult = SharedPreferenceHelper.DeletedEventsSynchronization(getApplicationContext(), deletedEvents);

                            // Update date synchronization
                            SharedPreferenceHelper.SaveDeletedEventSynchronization(getApplicationContext());
                            // Notify to UI to reload event list
                            if (deletedResult.size() > 0) {

                                Intent intentMain = new Intent(GlobalConfig.ServiceType.DeletedEventSynchronization);
                                //  intentMain.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                                intentMain.putIntegerArrayListExtra("DeletedEvent", deletedResult);
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intentMain);
                            }
                        }
                    });

                    break;
                }
            }

        }
    }


}
