package dk.aarhus2017.aarhusevents.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Geocoder;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;

import dk.aarhus2017.aarhusevents.R;
import dk.aarhus2017.aarhusevents.adapter.CityFilterAdapter;
import dk.aarhus2017.aarhusevents.adapter.LocationCityFilterAdapter;
import dk.aarhus2017.aarhusevents.model.GeoLocationModel;
import dk.aarhus2017.aarhusevents.model.LocationCityFilterModel;
import dk.aarhus2017.aarhusevents.model.PermissionResult;
import dk.aarhus2017.aarhusevents.model.PromptPermissionResult;
import dk.aarhus2017.aarhusevents.util.ConvertHelper;
import dk.aarhus2017.aarhusevents.util.FBTrackerHelper;
import dk.aarhus2017.aarhusevents.util.FontHelper;
import dk.aarhus2017.aarhusevents.util.GATrackerHelper;
import dk.aarhus2017.aarhusevents.util.GeneralHelper;
import dk.aarhus2017.aarhusevents.util.GlobalConstant;
import dk.aarhus2017.aarhusevents.util.LocationHelper;
import dk.aarhus2017.aarhusevents.util.PermissionHelper;
import dk.aarhus2017.aarhusevents.util.SharedPreferenceHelper;

public class LocationFilterActivity extends AppCompatActivity implements View.OnClickListener {

    LocationFilterActivity self = this;

    LinearLayout subpageFilterBackHolderLinearLayout;
    ImageView subpageFilterBackImageview;
    TextView subpageFilterTitle;
    Button subpageClearButton;


    TextView locationChooseLocationLabelTextView;
    TextView locationChooseLocationValue;
    ImageButton locationChooseLocationPreValue;
    //Spinner locationChooseLocation;
    LinearLayout cityHolderLinearLayout;


    TextView locationFilterMaximumDistanceLabelTextView;
    SeekBar locationFilterMaximumDistance;
    TextView locationFilterMaximumDistanceValue;


    //Location chooseLocation = new Location("");
    GeoLocationModel chooseLocation = new GeoLocationModel();
    String address;
    int distance;
    Boolean locationMode;
    ArrayList<LocationCityFilterModel> cityFilterModels;
    LocationCityFilterAdapter cityFilterAdapter;

    LocationCityFilterModel currentRowData;
    int currentPosition = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_filter);

        subpageFilterBackHolderLinearLayout = (LinearLayout) findViewById(R.id.subpageFilterBackHolderLinearLayout);
        subpageFilterBackImageview = (ImageView) findViewById(R.id.subpageFilterBackImageView);
        subpageFilterTitle = (TextView) findViewById(R.id.subpageFilterTitleTextView);
        subpageClearButton = (Button) findViewById(R.id.subpageFilterClearButton);

        locationChooseLocationLabelTextView = (TextView) findViewById(R.id.locationChooseLocationLabelTextView);
        locationChooseLocationValue = (TextView) findViewById(R.id.locationChooseLocationValueTextView);
        locationChooseLocationPreValue = (ImageButton) findViewById(R.id.locationChooseLocationPreValueImageButton);
        // locationChooseLocation = (Spinner) findViewById(R.id.locationChooseLocationSpinner);

        cityHolderLinearLayout = (LinearLayout) findViewById(R.id.cityHolderLinearLayout);


        locationFilterMaximumDistanceLabelTextView = (TextView) findViewById(R.id.locationFilterMaximumDistanceLabelTextView);
        locationFilterMaximumDistance = (SeekBar) findViewById(R.id.locationFilterMaximumDistanceSeekBar);
        locationFilterMaximumDistanceValue = (TextView) findViewById(R.id.locationFilterMaximumDistanceValueTextView);


        // Set fonts
        setFonts();

        // Register events
        subpageFilterBackHolderLinearLayout.setOnClickListener(this);
        subpageFilterBackImageview.setOnClickListener(this);
        subpageClearButton.setOnClickListener(this);
        cityHolderLinearLayout.setOnClickListener(this);
        // Set values
        subpageFilterTitle.setText(getString(R.string.filter_location));


        Intent intent = getIntent();
        address = intent.getStringExtra("Address");
        distance = intent.getIntExtra("Distance", 0);
        locationMode = intent.getBooleanExtra("IsCurrentLocationMode", false);
        chooseLocation = new GeoLocationModel();
        chooseLocation.setLatitude(intent.getDoubleExtra("Latitude", 0));
        chooseLocation.setLongitude(intent.getDoubleExtra("Longitude", 0));


        locationChooseLocationValue.setText(address);

        showClearButton();


        if (locationMode) {
            locationChooseLocationPreValue.setVisibility(View.VISIBLE);
        } else {
            locationChooseLocationPreValue.setVisibility(View.GONE);
        }

        locationFilterMaximumDistance.setProgress(distance / 2);
        locationFilterMaximumDistanceValue.setText(String.valueOf(distance) + " km");

        locationFilterMaximumDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                distance = i * 2;
                locationFilterMaximumDistanceValue.setText(String.valueOf(distance) + " km");
                showClearButton();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        cityFilterModels = GetCity(address, locationMode);


//        cityFilterAdapter = new LocationCityFilterAdapter(self, cityFilterModels);
//        locationChooseLocation.setAdapter(cityFilterAdapter);
//        LocationCityFilterAdapter.LocationCityFilterListener locationCityFilterListener = new LocationCityFilterAdapter.LocationCityFilterListener() {
//            @Override
//            public void onCityChanged(LocationCityFilterModel rowData) {
//                try {
//                    Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
//                    method.setAccessible(true);
//                    method.invoke(locationChooseLocation);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//
//                if (rowData.getIsCurrentLocation()) {
//                    setCurrentLocation();
//                } else {
//                    address = rowData.getCity();
//                    chooseLocation = new Location("");
//                    chooseLocation.setLatitude(rowData.getLatitude());
//                    chooseLocation.setLongitude(rowData.getLongitude());
//                    locationChooseLocationPreValue.setVisibility(View.GONE);
//
//                    locationMode = false;
//                    locationChooseLocationValue.setText(address);
//                }
//            }
//        };
//        cityFilterAdapter.setTaskListener(locationCityFilterListener);


    }

    private void setFonts() {
        Typeface sourceSansProBold = FontHelper.SourceSansProBold(self);
        Typeface sourceSansProRegular = FontHelper.SourceSansProRegular(self);
        subpageFilterTitle.setTypeface(sourceSansProBold);
        subpageClearButton.setTypeface(sourceSansProBold);
        locationChooseLocationLabelTextView.setTypeface(sourceSansProRegular);
        locationChooseLocationValue.setTypeface(sourceSansProBold);
        locationFilterMaximumDistanceLabelTextView.setTypeface(sourceSansProRegular);
        locationFilterMaximumDistanceValue.setTypeface(sourceSansProBold);
    }

    private void showClearButton(){
        if(distance==10 && address.equals(getString(R.string.city_aarhus))){
            subpageClearButton.setVisibility(View.GONE);
        }else{
            subpageClearButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.subpageFilterBackHolderLinearLayout:
            case R.id.subpageFilterBackImageView: {
                GoBack();
                break;
            }
            case R.id.subpageFilterClearButton:{
                locationMode = false;
                address = getString(R.string.city_aarhus);
                chooseLocation = LocationHelper.getDefaultAarhusCity();
                locationChooseLocationPreValue.setVisibility(View.GONE);
                for (LocationCityFilterModel model : cityFilterModels) {
                    if (model.getCity().equals(address)) {
                        model.setSelected(true);
                    } else {
                        model.setSelected(false);
                    }

                }
                locationChooseLocationValue.setText(address);
                distance= 10;
                locationFilterMaximumDistance.setProgress(distance / 2);
                locationFilterMaximumDistanceValue.setText(String.valueOf(distance) + " km");
                break;
            }
            case R.id.cityHolderLinearLayout:
            case R.id.locationChooseButtonPlaceholderLinearLayout: {
//                cityFilterModels = GetCity(address, locationMode);
//                cityFilterAdapter.UpdateData(cityFilterModels);
//                locationChooseLocation.performClick();

                final Dialog acceptDialog = new Dialog(self, R.style.Dialog);
                acceptDialog.setContentView(R.layout.prompt_city_dialog);
                acceptDialog.setTitle(R.string.prompt_city_title);
                acceptDialog.show();


                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                Typeface sourceSansProBold = FontHelper.SourceSansProBold(self);
                TextView title = (TextView) acceptDialog.findViewById(android.R.id.title);
                title.setTextColor(ContextCompat.getColor(self, R.color.text));
                title.setGravity(Gravity.CENTER_VERTICAL);
                title.setTypeface(sourceSansProBold);
                int paddingTopBottom = ConvertHelper.ConvertDpToPx(self, 10);
                int paddingLeftRight = ConvertHelper.ConvertDpToPx(self, 20);
                title.setPadding(paddingLeftRight, paddingTopBottom + ConvertHelper.ConvertDpToPx(self, 5), paddingLeftRight, paddingTopBottom);

                title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                title.setLayoutParams(layoutParams);


                ListView promptCityListView = (ListView) acceptDialog.findViewById(R.id.promptCityListView);
                CityFilterAdapter cityFilterAdapter = new CityFilterAdapter(self, cityFilterModels);
                promptCityListView.setAdapter(cityFilterAdapter);
                CityFilterAdapter.CityFilterListener cityFilterListener = new CityFilterAdapter.CityFilterListener() {
                    @Override
                    public void onCityChanged(LocationCityFilterModel rowData, int position) {
                        acceptDialog.dismiss();
                        currentRowData = rowData;
                        currentPosition = position;
                        if (rowData.getIsCurrentLocation()) {
                            setCurrentLocation();
                        } else {
                            address = rowData.getCity();
                            chooseLocation = new GeoLocationModel();

                            chooseLocation.setLatitude(rowData.getLatitude());
                            chooseLocation.setLongitude(rowData.getLongitude());
                            locationChooseLocationPreValue.setVisibility(View.GONE);
                            locationMode = false;
                            locationChooseLocationValue.setText(address);
                            showClearButton();
                        }
                    }
                };
                cityFilterAdapter.setTaskListener(cityFilterListener);

                break;
            }
            default:
                break;
        }
    }

    private void setCurrentLocation() {
        final PromptPermissionResult setting = SharedPreferenceHelper.GetGeoLocationPromptSetting(self);
        switch (setting) {
            case Yes: {
                PermissionResult permissionResult = PermissionHelper.CheckLocationPermission(self);
                switch (permissionResult) {
                    case Granted: {
                        Location location = LocationHelper.getLocation(self);
                        if (location != null) {
                            address = currentRowData.getCity();

                            locationChooseLocationPreValue.setVisibility(View.VISIBLE);
                            chooseLocation = new GeoLocationModel();
                            chooseLocation.setLatitude(location.getLatitude());
                            chooseLocation.setLongitude(location.getLongitude());

                            locationMode = true;
                        } else {
                            locationMode = false;
                            address = getString(R.string.city_aarhus);
                            chooseLocation = LocationHelper.getDefaultAarhusCity();
                            locationChooseLocationPreValue.setVisibility(View.GONE);
                            for (LocationCityFilterModel model : cityFilterModels) {
                                if (model.getCity().equals(address)) {
                                    model.setSelected(true);
                                } else {
                                    model.setSelected(false);
                                }

                            }
                        }
                        locationChooseLocationValue.setText(address);
                        showClearButton();
                        break;
                    }
                    case Requested: {
                        ActivityCompat.requestPermissions(self, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, GlobalConstant.RequestLocationPermissionResultCode);
                        break;
                    }
                    case Denied:
                        break;
                    default:
                        break;
                }
                break;
            }
            case No:
            case Ignore: {
                final Dialog acceptDialog = new Dialog(self);
                acceptDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                acceptDialog.setContentView(R.layout.prompt_location_dialog);
                acceptDialog.show();
                Typeface flavinBold = FontHelper.FlavinBold(self);
                Typeface flavinRegular = FontHelper.FlavinRegular(self);
                Typeface sourceSansProBold = FontHelper.SourceSansProBold(self);
                TextView titleTextView = (TextView) acceptDialog.findViewById(R.id.promptLocationTitleTextView);
                TextView descriptionTextView = (TextView) acceptDialog.findViewById(R.id.promptLocationDescriptionTextView);
                Button yesButton = (Button) acceptDialog.findViewById(R.id.promptGeoLocationYesButton);
                Button noButton = (Button) acceptDialog.findViewById(R.id.promptGeoLocationNoButton);

                titleTextView.setTypeface(flavinBold);
                descriptionTextView.setTypeface(flavinRegular);
                yesButton.setTypeface(sourceSansProBold);
                noButton.setTypeface(sourceSansProBold);
                yesButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        acceptDialog.dismiss();
                        SharedPreferenceHelper.SaveGeoLocationPromptSetting(self, PromptPermissionResult.Yes);
                        ActivityCompat.requestPermissions(self, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, GlobalConstant.RequestLocationPermissionResultCode);
                        GATrackerHelper.GeolocationAccept(self);
                    }
                });
                ImageButton closeImageButton = (ImageButton) acceptDialog.findViewById(R.id.promtCloseImageButton);
                closeImageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        acceptDialog.dismiss();
                        SharedPreferenceHelper.SaveGeoLocationPromptSetting(self, PromptPermissionResult.Ignore);
                    }
                });
                noButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        acceptDialog.dismiss();
                        SharedPreferenceHelper.SaveGeoLocationPromptSetting(self, PromptPermissionResult.No);
                    }
                });
                break;
            }
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case GlobalConstant.RequestLocationPermissionResultCode: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Location location = LocationHelper.getLocation(self);
                    if(location!=null) {
                        address = currentRowData.getCity();
                        chooseLocation=new GeoLocationModel();
                        chooseLocation.setLatitude(location.getLatitude());
                        chooseLocation.setLongitude(location.getLongitude());
                        locationChooseLocationPreValue.setVisibility(View.VISIBLE);
                        locationMode = true;
                    }else {
                        chooseLocation = LocationHelper.getDefaultAarhusCity();
                        address = getString(R.string.city_aarhus);
                        locationMode =false;
                        locationChooseLocationPreValue.setVisibility(View.GONE);
                    }

                    locationChooseLocationValue.setText(address);
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    // Toast.makeText(MainActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                    chooseLocation = LocationHelper.getDefaultAarhusCity();
                    address = getString(R.string.city_aarhus);
                    locationMode =false;

                    locationChooseLocationPreValue.setVisibility(View.GONE);

                }
                locationChooseLocationValue.setText(address);
                for (LocationCityFilterModel model : cityFilterModels) {
                    if (model.getCity().equals(address)) {
                        model.setSelected(true);
                    } else {
                        model.setSelected(false);
                    }
                }
                showClearButton();
                return;
            }
        }
    }


    private ArrayList<LocationCityFilterModel> GetCity(String currentCity, Boolean isCurrentLocation) {

        final PromptPermissionResult setting = SharedPreferenceHelper.GetGeoLocationPromptSetting(self);

        ArrayList<LocationCityFilterModel> cityFilterModels = new ArrayList<>();
//        if (setting != PromptPermissionResult.No) {
        cityFilterModels.add(new LocationCityFilterModel(true, getString(R.string.city_current), 0.0, 0.0, 8, false));
        //}
        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_aarhus), 56.162939, 10.203921000000037, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_favrskov), 56.3012924, 9.892063199999939, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_hedensted), 55.77171800000001, 9.702279999999973, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_herning), 56.138557, 8.967321999999967, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_holstebro), 56.36153400000001, 8.621726999999964, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_horsens), 55.8581302, 9.847588099999939, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_ikast), 56.0145296, 9.233738700000004, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_lemvig), 56.5443443, 8.302487100000008, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_norddjurs), 56.4858413, 10.662047400000006, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_odder), 55.97571799999999, 10.14995799999997, 8, false));

        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_randers), 56.460584, 10.036538999999948, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_region_midtjylland), 56.302139, 9.302776900000026, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_ringkobing), 56.04468869999999, 8.505890000000022, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_samso), 55.81466649999999, 10.588629399999945, 8, false));

        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_silkeborg), 56.176362, 9.554921599999943, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_skanderborg), 56.03724700000001, 9.929798900000037, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_skive), 56.5651232, 9.030908299999965, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_struer), 56.48493, 8.589932999999974, 8, false));

        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_syddjurs), 56.31636779999999, 0.526505799999995, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_viborg), 56.45202699999999, 9.396346999999992, 8, false));
        cityFilterModels.add(new LocationCityFilterModel(getString(R.string.city_pafos), 34.9164594, 32.49200880000001, 8, false));

        for (LocationCityFilterModel city : cityFilterModels) {
            if (isCurrentLocation) {
                if (city.getIsCurrentLocation()) {
                    city.setSelected(true);
                }
            } else {
                if (city.getCity().equals(currentCity)) {
                    city.setSelected(true);
                }
            }
        }
        return cityFilterModels;
    }


    @Override
    public void onBackPressed() {
        GoBack();
        super.onBackPressed();
    }

    private void GoBack() {
        Intent returnIntent = new Intent();

        if (locationMode && chooseLocation == null) {
            address = getString(R.string.city_current);
        }
        returnIntent.putExtra("Address", address);
        returnIntent.putExtra("Distance", distance);
        returnIntent.putExtra("Latitude", chooseLocation == null ? 0 : chooseLocation.getLatitude());
        returnIntent.putExtra("Longitude", chooseLocation == null ? 0 : chooseLocation.getLongitude());
        returnIntent.putExtra("IsCurrentLocationMode", locationMode);

        setResult(self.RESULT_OK, returnIntent);
        super.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FBTrackerHelper.LogScreenView(this,getString(R.string.filter_location));
    }
}
